import com.martiansoftware.jsap.*;
import com.martiansoftware.jsap.stringparsers.FileStringParser;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Tool
 * //todo : Work In Progress
 */
public final class LinxoImporter
{

  private static final Logger logger = Logger.getLogger(LinxoImporter.class);



  public final static String WARNING_COMMENT = "/* Imported by LinxoImporter - DO NOT EDIT */";

  public final static String CLIENT_PROJECT_PATH_OPTION = "clientProjectPathOption";
  public final static String SERVER_PROJECT_PATH_OPTION = "serverProjectPathOption";

  public final static String LINXO_GWT_PROJECT_PATH = "${server.path}/services/gwt/";
  public final static String LINXO_GWT_RPC_PROJECT_PATH = LINXO_GWT_PROJECT_PATH + "rpc/src/main/java/";
  public final static String LINXO_GWT_SECURED_PROJECT_PATH = LINXO_GWT_PROJECT_PATH + "secured/src/main/java/";
  public final static String LINXO_GWT_SERVER_PROJECT_PATH = LINXO_GWT_PROJECT_PATH + "server/src/main/java/";
  public final static String LINXO_DATA_PROJECT_PATH = "${server.path}/data/";
  public final static String LINXO_DATA_SHARED_PROJECT_PATH = LINXO_DATA_PROJECT_PATH + "shared/src/main/java/";

  public final static String CLIENT_REMOVE_START = "//ANDROLINXO-REMOVE-START";
  public final static String CLIENT_REMOVE_END = "//ANDROLINXO-REMOVE-END";

  /**
   * The list of directories to process from the server source code base
   */
  private static final String[] directories = new String[]{
      LINXO_DATA_SHARED_PROJECT_PATH,
      LINXO_GWT_RPC_PROJECT_PATH,
      LINXO_GWT_SECURED_PROJECT_PATH,
      LINXO_GWT_SERVER_PROJECT_PATH,
  };

  /**
   * The list of file patterns to exclude from the server source code base.
   * This list is processed first.
   */
  private static final String[] excludeFilePatterns = new String[] {
      // todo wip : restore this
      ".*Event.java",
      ".*EventHandler.java",
      // todo wip : restore this -- end

      ".*AbstractSyncEvent.java", // internally re-implemented
      ".*LinxoClient.java",
      ".*SecuredAction.java",
      ".*SecuredResult.java",

      ".*AccountListUpdateEvent.java",
      ".*AccountListUpdateEventHandler.java",
      //gwt-dispatch-shared
      ".*InvalidSecretException.java",
      ".*SecretProvider.java",
      ".*SecuredActionException.java",
      ".*InvalidSessionException.java",
      ".*SignatureVerifier.java",

      // c.l.gwt.server.support.json
      ".*VisibilityAction.java",
      ".*com/linxo/gwt/server/support/json/JsonResult.*.java",
      ".*com/linxo/gwt/server/support/json/ErrorResult.java",

      ".*SessionCookieSecretProvider.java",
      ".*GetNextEventHandler.java",
      ".*GetTrendsOverviewActionAdapter.java",

      ".*MailJetAPIAdapter.java"
  };

  /**
   * The list of file patterns to process from the server source code base.
   * This list is processed after the excludeFilePattern had been processed.
   */
  private static final String[] includeFilePatterns = new String[] {

      //Common patterns
      ".*Action.java",
      ".*Result.java",
      ".*Adapter.java",

      //data-shared
      ".*StringUtils.java",
      ".*Version.java",
      ".*DateUtils.java",
      ".*LinxoDate.java",
      ".*Colors.java",
      ".*BillingCycle.java",
      ".*DealSource.java",
      ".*Feature.java",
      ".*PurchaseProperties.java",
      ".*PaymentStatus.java",
      ".*TransactionType.java",
      ".*SavingsType.java",
      ".*AccountSynchroStatus.java",
      ".*AccountType.java",
      ".*SynchroStatus.java",
      ".*ReportPeriodType.java",
      ".*SynchroReminderPeriod.java",
      ".*UserStatus.java",
      ".*AlertType.java",
      ".*UserSubscriptionStatus.java",
      ".*CategoryConstants.java",
      ".*SavingsFamily.java",
      ".*SocialProvider.java",
      ".*IntervalUnit.java",
      ".*ViewToken.java",
      ".*LinxoUrlParameter.java",
      ".*LinxoColor.java",
      ".*ValidationConstants.java",
      ".*LinxoRuntimeException.java",
      ".*Platform.java",

      //gwt-dispatch-shared
      ".*InvalidSecretException.java",
      ".*SecretProvider.java",
      ".*SecuredActionException.java",
      ".*SignatureVerifier.java",
      ".*AssertStringSize.java",
      ".*AssertNotNull.java",

      //gwt-rpc
      ".*AuthCookies.java",
      ".*AuthStatus.java",
      ".*Key.java",
      ".*Icon.java",
      ".*AuthUtil.java",
      ".*InvalidSessionException.java",
      ".*FunctionalException.java",
      ".*TechnicalException.java",
      ".*PermissionException.java",
      ".*Credential.java",
      ".*Event.java",
      ".*EventHandler.java",
      ".*Info.java",
      ".*CategoryType.java",
      ".*BankAccountForecast.java",
      ".*TemporalExpression.java",
      ".*UpcomingTransaction.java",
      ".*UpcomingTransactionOccurrence.java",
      ".*AbstractClientUpcomingTransactionService.java",
      ".*AbstractUpcomingTransactionService.java",
      ".*ClientUpcomingTransactionService.java",
      ".*services.UpcomingTransactionService.java",
      ".*LinxoClient.java",
      ".*Exclude.java",
      ".*CustomizedTypeAdapterFactory.java",
      ".*UpcomingTransactionAdapterFactory.java",
      ".*LinxoFieldExclusionStrategy.java",
      ".*BudgetTarget.java",
      ".*PersonalizedView.java",
      ".*LinxoAsyncClient.java",
      ".*ClientPersonalizedViewsService.java",
      ".*AbstractClientPersonalizedViewsService.java",
      ".*LinxoServiceCallback.java",
      ".*AbstractLinxoAsyncClientCallback.java",
      ".*LinxoAsyncClientCallback.java",
      ".*BankAccountInfoBuilder.java",
      ".*DTOBuilderException.java",
      ".*ActionBuilderException.java",
      ".*GetTransactionsActionBuilder.java",
      ".*SynchronizationCapability.java",
      ".*Document.java",
      ".*DocumentMetadata.java",

//      ".*InvalidSessionException.java",
      ".*FunctionalException.java",
      ".*TechnicalException.java",
      ".*PermissionException.java",
      ".*Credential.java",
      ".*Event.java",
      ".*EventHandler.java",
      ".*Info.java",
      ".*CategoryType.java",
      ".*BankAccountForecast.java",
      ".*TemporalExpression.java",
      ".*UpcomingTransaction.java",
      ".*UpcomingTransactionOccurrence.java",
      ".*AbstractClientUpcomingTransactionService.java",
      ".*AbstractUpcomingTransactionService.java",
      ".*BudgetService.java",
      ".*BudgetTarget.java",
      ".*ClientUpcomingTransactionService.java",
      ".*services.UpcomingTransactionService.java",
      ".*Exclude.java",
      ".*CustomizedTypeAdapterFactory.java",
      ".*UpcomingTransactionAdapterFactory.java",
      ".*LinxoFieldExclusionStrategy.java",

      //gwt-server
      ".*ApiConfiguration*",
      ".*FailSafeEnumTypeAdapterFactory.java",
      ".*JsonActionParseException.java",
      ".*CredentialKeyboardType.java",
      ".*LoginAttempt.java",
      ".*LoginChannel.java",
      ".*BlockedReason.java"
  };


  private static final String[] removeLinesContain = new String[]{
    "com.google.gwt.core.client.GWT",
    "GWT.create",
    "com.google.gwt.i18n.client.LocaleInfo",
    "com.google.gwt.user.datepicker.client.CalendarUtil",
//    "com.linxo.gwt.dispatch.shared" // re-implemented in linxo-client
  };

  private static final Map<String, String> replacements;
  private static final Map<String, String> filePathReplacements;

  static {

    Comparator<String> comparator = new Comparator<String>() {
      @Override
      public int compare(String that, String other)
      {
        // First sort on string length...
        final int lengthDifference = other.length() - that.length();
        if(lengthDifference != 0) return lengthDifference;

        // ... then with inverse natural order
        return other.compareTo(that);
      }
    };

    // String replacement in files
    // We use TreeMap with a Comparator<String> that puts the longest
    // (more specific) items first
    replacements = new TreeMap<String, String>(comparator);
    filePathReplacements = new TreeMap<String, String>(comparator);
    replacements.put("eComptes", "Linxo");
    // Replacements for Android - Uncomment to activate
//    replacements.put("com.google.gson", "com.google.gsonlinxo");
    // package replacements to match the client API

//    replacements.put("com.linxo.gwt.server.support.json", "com.linxo.client.actions");
//    filePathReplacements.put("com/linxo/gwt/server/support/json", "com.linxo.client.actions");

    replacements.put("com.google.gwt.event.shared.EventHandler", "com.linxo.client.dto.events.EventHandler");

    // Groups of lines below goes together to replace the "import" and then the "extends" clauses
    replacements.put("com.linxo.gwt.dispatch.shared.AbstractSecuredResult", "com.linxo.client.actions.SecuredResult");
    replacements.put("com.linxo.gwt.dispatch.shared.SecuredResult",         " com.linxo.client.actions.SecuredResult");
    replacements.put(" AbstractSecuredResult", " SecuredResult"); // /!\ /!\ /!\ space at beginning put intentionally

    replacements.put("com.linxo.gwt.dispatch.shared.AbstractSecuredAction", "com.linxo.client.actions.SecuredAction");
    replacements.put("com.linxo.gwt.dispatch.shared.SecuredAction",         " com.linxo.client.actions.SecuredAction");
    replacements.put(" AbstractSecuredAction", " SecuredAction"); // /!\ /!\ /!\ space at beginning put intentionally

    replacements.put("net.customware.gwt.dispatch.shared.Result", "com.linxo.client.actions.LinxoResult");
    replacements.put(" Result ", " LinxoResult "); // /!\ /!\ /!\ space at beginning put intentionally

    replacements.put("net.customware.gwt.dispatch.shared.Action", "com.linxo.client.actions.LinxoAction");
    replacements.put(" Action ", " LinxoAction "); // /!\ /!\ /!\ space at beginning put intentionally
    replacements.put(" Action<", " LinxoAction<"); // /!\ /!\ /!\ space at beginning put intentionally
    replacements.put("\\(Action ", "(LinxoAction "); // /!\ /!\ /!\ space at beginning put intentionally
    replacements.put("\\(Action<", "(LinxoAction<"); // /!\ /!\ /!\ space at beginning put intentionally


    // ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^
    // STRING REPLACEMENT ABOVE
    //
    // -------------------------
    //
    // PACKAGE RENAMING BELOW...
    // v v v v v v v v v v v v v

    replacements.put("com.linxo.gwt.rpc.client.LinxoClient", "com.linxo.client.net.LinxoClient");
    // We do not move the LinxoClient (in this project, this is a concrete class), so there is
    // no filePathReplacement entry.

    replacements.put(        "com.linxo.gwt.rpc.client.dto",     "com.linxo.client.dto");
    filePathReplacements.put("com/linxo/gwt/rpc/client/dto",     "com/linxo/client/dto");

    replacements.put(        "com.linxo.gwt.rpc.client.exception",     "com.linxo.infrastructure.exceptions");
    filePathReplacements.put("com/linxo/gwt/rpc/client/exception",     "com/linxo/infrastructure/exceptions");

    replacements.put(        "com.linxo.gwt.rpc.client",     "com.linxo.client.actions");
    filePathReplacements.put("com/linxo/gwt/rpc/client",     "com/linxo/client/actions");

    replacements.put(        "com.linxo.gwt.server.support.json",     "com.linxo.client.json");
    filePathReplacements.put("com/linxo/gwt/server/support/json",     "com/linxo/client/json");

    replacements.put(        "com.linxo.data.shared.client", "com.linxo.client.data");
    filePathReplacements.put("com/linxo/data/shared/client", "com/linxo/client/data");

    replacements.put(        "com.linxo.gwt.dispatch.shared.validation", "com.linxo.client.shared");
    filePathReplacements.put("com/linxo/gwt/dispatch/shared/validation", "com/linxo/client/shared");

    // todo : Result => last LinxoResult  + JsonAction/JsonResult
    // todo : that should not be imported

    if(logger.isDebugEnabled()) {
      logger.debug("Running with the following replacements :");
      logger.debug("replacements :");
      logger.debug("--------------");
      for(Map.Entry<String, String> entry : replacements.entrySet())
      {
        logger.debug("["+entry.getKey()+"]=>["+entry.getValue()+"]");
      }
      logger.debug("--------------");

      logger.debug("filePathReplacements :");
      logger.debug("--------------");
      for(Map.Entry<String, String> entry : filePathReplacements.entrySet())
      {
        logger.debug("["+entry.getKey()+"]=>["+entry.getValue()+"]");
      }
      logger.debug("--------------");
    }

  }



  public static void main(String[] args)
  {

    LinxoImporter.getInstance(args)
        .processDirectories();

  }



  private static LinxoImporter getInstance(String [] args)
  {
    SimpleJSAP parser;
    try {
      parser = new SimpleJSAP(
          LinxoImporter.class.getName(),
          "This is the Linxo Importer utility.",
          new Parameter[]{
              // CLIENT_PROJECT_PATH
              new FlaggedOption(CLIENT_PROJECT_PATH_OPTION,
                  FileStringParser.getParser()
                      .setMustBeDirectory(true)
                      .setMustExist(true),
                  JSAP.NO_DEFAULT,
                  JSAP.REQUIRED,
                  'c', "clientPath",
                  "the path to the Client Project."),
              // SERVER_PROJECT_PATH
              new FlaggedOption(SERVER_PROJECT_PATH_OPTION,
                  FileStringParser.getParser()
                      .setMustBeDirectory(true)
                      .setMustExist(true),
                  JSAP.NO_DEFAULT,
                  JSAP.REQUIRED,
                  's', "serverPath",
                  "the path to the Linxo Server Project.")
          }
      );
    }
    catch(JSAPException je){
      logger.fatal("Could not initialize JSAP parsing", je);
      System.exit(-1);
      return null;
    }

    JSAPResult result = parser.parse(args);
    if (parser.messagePrinted()) {
      System.err.println(parser.getHelp());
      System.exit(1);
    }

    return new LinxoImporter(
        result.getFile(CLIENT_PROJECT_PATH_OPTION),
        result.getFile(SERVER_PROJECT_PATH_OPTION)
    );
  }



  ////////////////////////////////////////////////////
  // Instance Fields

  private final File clientProject;
  private final File serverProject;

  private final File output;


  ////////////////////////////////////////////////////
  // Constructors

  public LinxoImporter(File clientProject, File serverProject)
  {
    this.clientProject = clientProject;
    this.serverProject = serverProject;

    this.output = new File(clientProject, "src/");
  }


  ////////////////////////////////////////////////////
  // Instance Methods

  public void processDirectories()
  {
    for(String directory : directories){
      try {
        final File dirIn = getDirectory(directory);
        if(logger.isDebugEnabled()) {
          logger.debug("Processing ["+directory+"] mapped to ["+dirIn+"]");
        }

        processDirectory(dirIn, output);
      }
      catch (IOException ioe) {
        logger.error("Caught I/O Exception while processing ["+directory+"]", ioe);
      }
    }
  }


  private File getDirectory(String path)
  {
    String real_path = path
        .replaceAll("\\$\\{server\\.path\\}", serverProject.getAbsolutePath())
        .replaceAll("\\$\\{client\\.path\\}", clientProject.getAbsolutePath())
        ;

    return new File(real_path);
  }

  public void processDirectory(File dirIn, File dirOut)
      throws IOException
  {
    if (!dirIn.exists()) {
      throw new IOException("Input directory does not exist : " + dirIn.getPath());
    }
    if (!dirIn.isDirectory()) {
      throw new IOException("Input file is not a directory");
    }
    if (!dirOut.exists()) {
      throw new IOException("Output directory does not exist : " + dirOut.getPath());
    }
    if (!dirOut.isDirectory()) {
      throw new IOException("Output file is not a directory");
    }

    //noinspection ConstantConditions
    for (File f : dirIn.listFiles()) {

      if (f.isDirectory()) {
        if (f.getName().startsWith(".")) {
          continue;
        }
        File out = new File(dirOut, f.getName());
        if (!out.exists()) {
          if( !out.mkdir() ) {
            logger.error("Could not create directory ["+out+"]");
          }
        }
        processDirectory(f, out);
      } else if (shouldProcessFile(f)) {
        if (logger.isDebugEnabled()){
          logger.debug("processing " + f.getAbsolutePath());
        }
        processFile(f, new File(dirOut, f.getName()));
      }
    }
  }

  private boolean shouldProcessFile(File f)
      throws IOException
  {
    if (!f.isFile() || f.isHidden() || f.getName().startsWith(".")) {
      return false;
    }

    for (String excludedFilePattern : excludeFilePatterns) {
      if(logger.isTraceEnabled()) {
        logger.trace("Matching [" + f.getPath()+"] with ["+excludedFilePattern+"]");
      }
      if (f.getPath().matches(excludedFilePattern)) {
        if(logger.isTraceEnabled()) {
          logger.trace("Matching ["+f.getPath()+"] is excluded");
        }
        return false;
      }
    }

    for (String filePattern : includeFilePatterns) {
      if(logger.isTraceEnabled()) {
        logger.trace("Matching [" + f.getPath()+"] with ["+filePattern+"]");
      }
      if (f.getPath().matches(filePattern)) {
        if(logger.isTraceEnabled()) {
          logger.trace("Matching ["+f.getPath()+"] is included");
        }
        return true;
      }
    }

    if(logger.isTraceEnabled()) {
      logger.trace("Matching [" + f.getPath() + "] not excluded, but not included => Not considered");
    }
    return false;
  }

  public void processFile(File in, File out)
      throws IOException
  {
    out = processFileName(out);

    if(logger.isTraceEnabled()) {
      logger.trace("Processing ["+in+"] => ["+out+"]");
    }
    BufferedReader reader = new BufferedReader(new FileReader(in));
    BufferedWriter writer = new BufferedWriter(new FileWriter(out));
    try {

      String line;

      writer.write(WARNING_COMMENT);
      writer.newLine();
      writer.newLine();

      boolean inRemoveBlock = false;
      while ((line = reader.readLine()) != null) {
        if (line.contains(CLIENT_REMOVE_START)) {
          inRemoveBlock = true;
        }
        if (!inRemoveBlock && shouldProcessLine(line)) {
          writer.write(processLine(line));
          writer.newLine();
        }
        if (line.contains(CLIENT_REMOVE_END)) {
          inRemoveBlock = false;
        }
      }
    } finally {
      reader.close();
      writer.close();
    }
  }

  private boolean shouldProcessLine(String line)
  {
    for (String p : removeLinesContain) {
      if (line.contains(p)) {
        return false;
      }
    }
    return true;
  }

  private String processLine(String line) {
    String result = line;
    for (Map.Entry<String, String> entry : replacements.entrySet()) {
      result = result.replaceAll(entry.getKey(), entry.getValue());
    }
    return result;
  }

  private File processFileName(File out)
  {
    String resultPath = out.getPath();

    for (Map.Entry<String, String> entry : filePathReplacements.entrySet()) {
      final String updatedPath = resultPath.replaceAll(entry.getKey(), entry.getValue());
      if(logger.isTraceEnabled()) {
        logger.trace("while processing [" + out + "] replacing [" + resultPath + "] with [" + updatedPath + "] using [" + entry.getKey()+"]=>["+entry.getValue()+"]");
      }
      resultPath = updatedPath;
    }

    File result = new File(resultPath);
    File parent = result.getParentFile();
    if(!parent.exists() && !parent.mkdirs())
    {
      logger.fatal("Impossible to create directory ["+parent+"] while processing ["+out+"]");
    }
    return result;
  }
}
