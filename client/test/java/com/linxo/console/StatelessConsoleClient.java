/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 28/08/2015 by hugues.
 */
package com.linxo.console;

import com.linxo.client.actions.auth.AuthorizeDeviceAction;
import com.linxo.client.actions.auth.AuthorizeDeviceResult;
import com.linxo.client.actions.pfm.*;
import com.linxo.client.data.auth.AuthStatus;
import com.linxo.client.dto.device.AppInfo;
import com.linxo.client.net.LinxoClient;
import com.linxo.client.net.LinxoClientException;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;

/**
 * This class provides an example of a stateless LinxoClient.
 *
 * It can send action/result requests directly to the PFM server without
 * any session management.
 */
public class StatelessConsoleClient {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(StatelessConsoleClient.class);

  // Static Initializers

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  // Static Methods


  public static void main(String [] argv) throws Exception
  {


    // To have a stateless LinxoClient, one needs to provide a
    // UserCredentials object in the constructor.
    // This UserCredentials object contains the users' credentials
    // information.
    LinxoClient userCredentialsClient = new LinxoClient(
        ConsoleConstants.SERVER,
        ConsoleConstants.API_KEY,
        ConsoleConstants.API_SECRET,
        new LinxoClient.UserCredentials("dev@linxo.com", "Passw0rd")
    );

    LinxoClient deviceCredentialsClient = null;
    try {

      // In Stateless mode, you still have to invoke initialize,
      // but this will not trigger a former GET /auth.page HTTP request.
      userCredentialsClient.initialize();

      // Now, you can user the Action/Response like in stateful mode,
      // But this will not enable you to request events since there is
      // no session running between your calls.

      // Attempts to get all banks and accounts
      GetAccountGroupsResult banksAndAccountsResult = userCredentialsClient.sendAction(
          new GetAccountGroupsAction()
      );
      logger.info("Bank and accounts: " + banksAndAccountsResult);



      GetBankAccountListResult accountListResult = userCredentialsClient.sendAction(
          new GetBankAccountListAction(false)
      );
      logger.info("Bank Accounts by Type: " + accountListResult);

      // Attempts to get all transactions
      GetTransactionsResult txResult = userCredentialsClient.sendAction(
          new GetTransactionsAction(
              null, // no account type, could be Checkings, Savings, or CreditCard
              null, // accountId
              null, // ArrayList of string to match in the transactions labels + notes
              null, // Long : categoryId
              null, // Long : tagId
              true, // limit search to account the user has selected to compute the trends
              0,    // startIndex in the list
              200    // max # of Transactions
          )
      );
      logger.info("GetTransactionsResult: " + txResult);




      // Now, more fun, register a new device and use its identifier
      AuthorizeDeviceResult authorizeDeviceResult = userCredentialsClient.sendAction(
          new AuthorizeDeviceAction("Java",
              StatelessConsoleClient.class.getSimpleName(), // deviceType
              "" + StatelessConsoleClient.class.hashCode(),      // deviceId
              "dev@linxo.com", "Passw0rd",
              "My Test", new AppInfo())
      );
      logger.info("authorizeDeviceResult: " + authorizeDeviceResult);

      if(authorizeDeviceResult.getStatus() != AuthStatus.Success) {
        logger.error("authorizeDeviceResult.status " + authorizeDeviceResult.getStatus());
        return;
      }

      final String token = authorizeDeviceResult.getToken();
      //
      deviceCredentialsClient = new LinxoClient(
          ConsoleConstants.SERVER,
          ConsoleConstants.API_KEY,
          ConsoleConstants.API_SECRET,
          new LinxoClient.DeviceCredentials(
              "" + StatelessConsoleClient.class.hashCode(),
              token,
              "appIdentifier", "appVersion")
      );
      deviceCredentialsClient.initialize();

      GetAlertsResult getAlertsResult = deviceCredentialsClient.sendAction(
          new GetAlertsAction()
      );

      logger.info("getAlertsResult: " + getAlertsResult);


    }
    catch(LinxoClientException ee) {
      logger.error(ee.getMessage(), ee);
      logger.error(ee.getMessage(), userCredentialsClient.getClientException(ee));
    }
    finally {
      userCredentialsClient.close();
      if( deviceCredentialsClient != null )
        deviceCredentialsClient.close();
    }

  }

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods
}
