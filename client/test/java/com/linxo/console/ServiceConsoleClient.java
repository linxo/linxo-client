/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 15/10/2014 by hugues.
 */
package com.linxo.console;

import com.linxo.client.actions.pfm.trends.GetBalanceHistoryResult;
import com.linxo.client.actions.pfm.trends.GetTotalsAndCountsResult;
import com.linxo.client.data.DateUtils;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.auth.AuthStatus;
import com.linxo.client.dto.account.AccountGroupInfo;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.service.ServiceHub;
import com.linxo.infrastructure.exceptions.FatalException;
import com.linxo.infrastructure.exceptions.TechnicalException;
import com.linxo.infrastructure.interfaces.AppDeviceConfig;
import com.linxo.infrastructure.interfaces.AppStorage;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

/**
 * This class plays a small connection scenario using basic available Action/Result.
 * When launched, press [ENTER] at each step to move forward
 */
public class ServiceConsoleClient {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(ServiceConsoleClient.class);

  // Static Initializers

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  // Static Methods

  public static void main(String [] argv) throws Exception
  {

    ServiceConsoleClient client = new ServiceConsoleClient();

    client.run();

    logger.info("Scenario ended");
  }
    // Instance Fields

  // Instance Initializers

  private ServiceHub serviceHub;

  // Constructors

  private ServiceConsoleClient() throws FatalException
  {
    AppDeviceConfig appDeviceConfig = new AppDeviceConfig() {
      @Override
      public String getDeviceType()
      {
        return getClass().getSimpleName();
      }

      @Override
      public String getDeviceFamily()
      {
        return "Java";
      }

      @Override
      public String getDeviceName()
      {
        return "Test";
      }

      @Override
      public String getAppIdentifier()
      {
        return "identifier";
      }

      @Override
      public String getAppVersion()
      {
        return "1.0-SNAPSHOT";
      }

      @Override
      public String getDomain()
      {
        return ConsoleConstants.SERVER;
      }

      @Override
      public String getApiKey()
      {
        return ConsoleConstants.API_KEY;
      }

      @Override
      public String getApiSecret()
      {
        return ConsoleConstants.API_SECRET;
      }
    };
    AppStorage appStorage = new AppStorage() {

      private HashMap<String, String> properties = new HashMap<String, String>();

      @Override
      public Boolean saveProtected(String key, String data)
      {
        properties.put(key, data);
        return true;
      }

      @Override
      public Boolean save(String key, String data)
      {
        properties.put(key, data);
        return true;
      }

      @Override
      public String getStringData(String key)
      {
        return properties.get(key);
      }

      @Override
      public Integer getIntegerData(String key)
      {
        return Integer.valueOf(properties.get(key));
      }

      @Override
      public String getStringDataProtected(String key)
      {
        return properties.get(key);
      }

      @Override
      public Integer getIntegerDataProtected(String key)
      {
        return Integer.valueOf(properties.get(key));
      }

      @Override
      public Boolean delete(String key)
      {
        return properties.remove(key) != null;
      }
    };
    serviceHub = new ServiceHub(appDeviceConfig, appStorage);
  }

  // Instance Methods


  public void run() throws FatalException, TechnicalException
  {
    Scanner scanner = new Scanner(System.in);

    logger.info("Initializing services...");

    serviceHub.initialize();

    logger.info("Connecting...");

    // TODO : replace with the user credentials
    final AuthStatus authStatus = serviceHub.getAuthenticationService().authorizeDevice("test@linxo.com", "password");
    if(authStatus != AuthStatus.Success )
    {
      logger.error("Could not connect ["+authStatus+"] ");
      endScenario(scanner);
      return;
    }
    logger.info("API device connected------------------------------");
    serviceHub.getAuthenticationService().setPin("1234");

    // wait for keyboard input
    scanner.nextLine();

    logger.info("Listing groups...");
    for (AccountGroupInfo group : serviceHub.getAccountService().getAccountGroupMap().values())
    {
      logger.info("- : " + group);
    }
    logger.info("Listing groups done-------------------------------");


    // wait for keyboard input
    scanner.nextLine();
    logger.info("Listing accounts...");
    final LinxoDate today = DateUtils.getLinxoDate(new Date());
    for (BankAccountInfo account : serviceHub.getAccountService().getBankAccountMap().values())
    {
      logger.info("- : " + account);
      logger.info("- balance history -------------------------------");
      ArrayList<GetBalanceHistoryResult.BalanceAndBalanceDate> history =
          serviceHub.getTransactionService()
              .loadBalanceDates(
                  account.getId(),
                  DateUtils.addMonths(today, -3),
                  today);
      if(history == null) {
        logger.info("- Could not load history");
        endScenario(scanner);
        return;
      }

      for (GetBalanceHistoryResult.BalanceAndBalanceDate balanceAndDate : history)
      {
        logger.info("-- " + balanceAndDate.balanceDate + " => " + balanceAndDate.balance);
      }
      logger.info("Listing accounts done-------------------------------");
      scanner.nextLine();
    }
    logger.info("Listing accounts done-------------------------------");


    // wait for keyboard input
    scanner.nextLine();
    logger.info("Listing Pie Chart Data...");
    GetTotalsAndCountsResult pieChartData = serviceHub.getTransactionService()
        .loadBudgetPieChartData(
            DateUtils.addMonths(today, -3),
            today);


    logger.info(" Result : " + pieChartData);

    logger.info("Listing Pie Chart Data done-------------------------------");

    // wait for keyboard input
    scanner.nextLine();
    logger.info("Listing Net Income Data...");
    GetTotalsAndCountsResult netIncomeData = serviceHub.getTransactionService()
        .loadNetIncomeBarChartData(
            DateUtils.addMonths(today, -3),
            today);


    logger.info(" Result : " + netIncomeData);

    logger.info("Listing Net Income done-------------------------------");

    // wait for keyboard input
    scanner.nextLine();
    logger.info("Stopping event polling");
    serviceHub.getEventPollingService().stop();
    logger.info("Stopped event polling-----------------------------");

    // wait for keyboard input
    scanner.nextLine();

    serviceHub.getAuthenticationService().deAuthorizeDevice();

    logger.debug("De-Authorized Device");

    endScenario(scanner);
  }

  private void endScenario(Scanner scanner) throws FatalException
  {
    scanner.close();
    serviceHub.clear();
  }

}
