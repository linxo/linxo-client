/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 30/09/2015 by hugues.
 */
package com.linxo.console;

import com.linxo.client.actions.auth.AuthorizeDeviceAction;
import com.linxo.client.actions.auth.AuthorizeDeviceResult;
import com.linxo.client.actions.auth.LoginDeviceAction;
import com.linxo.client.actions.auth.LoginDeviceResult;
import com.linxo.client.actions.pfm.GetAlertsAction;
import com.linxo.client.actions.pfm.GetAlertsResult;
import com.linxo.client.data.auth.AuthStatus;
import com.linxo.client.net.LinxoClient;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;

public class DeviceTestClient {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(StatelessConsoleClient.class);

  // Static Initializers

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  // Static Methods


  public static void main(String [] argv) throws Exception
  {
    // To have a stateless LinxoClient, one needs to provide a
    // UserCredentials object in the constructor.
    // This UserCredentials object contains the users' credentials
    // information.
    LinxoClient authorizeDeviceClient = new LinxoClient(
        ConsoleConstants.SERVER,
        ConsoleConstants.API_KEY,
        ConsoleConstants.API_SECRET
    );

    authorizeDeviceClient.initialize();

    AuthorizeDeviceResult authorizeDeviceResult = authorizeDeviceClient.sendAction(
        new AuthorizeDeviceAction("Java",
            StatelessConsoleClient.class.getSimpleName(), // deviceType
            "" + StatelessConsoleClient.class.hashCode(), // deviceId, as String
            "dev@linxo.com", "Passw0rd",
            "My Test",
            null) // AppInfo null
    );
    logger.info("authorizeDeviceResult: " + authorizeDeviceResult.getStatus());
    if(authorizeDeviceResult.getStatus() != AuthStatus.Success) {
      logger.error("authorizeDeviceResult.status " + authorizeDeviceResult.getStatus());
      return;
    }

    // This is the token to store...
    final String myToken = authorizeDeviceResult.getToken();
    logger.info("myToken: " + myToken);

    // terminate the authorize device client
    authorizeDeviceClient.close();

    // -o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-

    LinxoClient loginDeviceClient = new LinxoClient(
        ConsoleConstants.SERVER,
        ConsoleConstants.API_KEY,
        ConsoleConstants.API_SECRET
    );

    loginDeviceClient.initialize();

    LoginDeviceResult loginResult = loginDeviceClient.sendAction(
        new LoginDeviceAction(
            myToken,
            "" + StatelessConsoleClient.class.hashCode(), // deviceId, as String
            null // AppInfo can be null
        )
    );
    logger.info("loginResult: " + loginResult.getStatus());
    if(loginResult.getStatus() != AuthStatus.Success) {
      logger.error("loginResult.status " + loginResult.getStatus());
      return;
    }

    // At this point, we can query data...
    GetAlertsResult getAlertsResult = loginDeviceClient.sendAction(
        new GetAlertsAction()
    );

    logger.info("getAlertsResult: " + getAlertsResult.getAlerts());

    loginDeviceClient.close();
  }


    // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods
}
