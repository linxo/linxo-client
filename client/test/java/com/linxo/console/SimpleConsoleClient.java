/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 30/09/2014 by hugues.
 */
package com.linxo.console;

import com.linxo.client.actions.auth.LoginAction;
import com.linxo.client.actions.auth.LoginResult;
import com.linxo.client.actions.auth.LogoutAction;
import com.linxo.client.actions.auth.LogoutResult;
import com.linxo.client.actions.pfm.*;
import com.linxo.client.actions.pfm.budget.*;
import com.linxo.client.actions.pfm.trends.GetTotalsAndCountsAction;
import com.linxo.client.actions.pfm.trends.GetTotalsAndCountsResult;
import com.linxo.client.data.DateUtils;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.data.upcoming.IntervalUnit;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.dto.budget.BudgetTarget;
import com.linxo.client.net.LinxoClient;
import com.linxo.client.net.LinxoClientBuilder;
import com.linxo.client.net.LinxoClientException;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.util.Calendar;
import java.util.Date;

public class SimpleConsoleClient
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(SimpleConsoleClient.class);

  // Static Initializers

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  // Static Methods

  public static void main(String [] argv) throws Exception
  {

    long start = System.currentTimeMillis();

    LinxoClient client = new LinxoClientBuilder(
            ConsoleConstants.SERVER,
            ConsoleConstants.API_KEY,
            ConsoleConstants.API_SECRET)
            .threadPool(10)
            .timeout(5)
            .build();

    try {
      client.initialize();

      // Attempts to login
      LoginResult loginResult = client.sendAction(
          new LoginAction(ConsoleConstants.DEFAULT_USER, ConsoleConstants.DEFAULT_PASS, false, 10)
      );
      logger.info("LoginResult " + loginResult);

      // LoginResult contains the userId when successful
      if(loginResult.getUserId() == null) {
        logger.info("LoginFailed, user blocked:" + loginResult.isBlocked());
        return;
      }

      // Attempts to get all banks and accounts
//      Future<GetAccountGroupsResult> banksAndAccountsFuture = client.sendAction(
//          new GetAccountGroupsAction()
//      );
//      GetAccountGroupsResult bankAndAccountsResult = banksAndAccountsFuture.get();
//      logger.info("Bank and accounts: " + bankAndAccountsResult);

      // Attempts to get all banks and accounts
      GetBankAccountListResult accountListResult = client.sendAction(
          new GetBankAccountListAction(false)
      );
      logger.info("Bank Accounts by Type: " + accountListResult);

      // Attempts to get all transactions
      GetTransactionsResult txResult = client.sendAction(
          new GetTransactionsAction(
              null, // no account type, could be Checkings, Savings, or CreditCard
              null, // accountId
              null, // ArrayList of string to match in the transactions labels + notes
              null, // Long : categoryId
              null, // Long : tagId
              true, // limit search to account the user has selected to compute the trends
              0,    // startIndex in the list
              200    // max # of Transactions
          )
      );
      logger.info("GetTransactionsResult: " + txResult);

      // Get Budget Targets
      GetBudgetTargetsResult budgetTargetsResult = client.sendAction(
          new GetBudgetTargetsAction()
      );

      logger.info("Got budget targets for all accounts, ["+budgetTargetsResult.getBudgetTargets().size()+"] targets. Will delete them...");

      for(BudgetTarget target : budgetTargetsResult.getBudgetTargets())
      {
        logger.info("Deleting ["+target+"].");

        client.sendAction(new DeleteBudgetTargetAction(target.getId()));

        logger.info("Deleted ["+target+"]");
      }

      BankAccountInfo bankAccountInfo = accountListResult.getAccountsByType().get(AccountType.Checkings).get(0);
      logger.info("Using account ["+bankAccountInfo+"]");

      logger.info(">>>>>>>>>>>>>>");
      logger.info(">>>>>>>>>>>>>>");

      for (GetTotalsAndCountsAction.Per per : GetTotalsAndCountsAction.Per.values()) {
        logger.info(">>>>>>>>>>>>>> Per:" + per.name());

        GetTotalsAndCountsResult totalsAndCountsResult = client.sendAction(
            new GetTotalsAndCountsAction(per,
                LinxoDate.makeDate("01062015"),
                LinxoDate.makeDate("01082015")
            )
        );

        logger.info("Result for all:" + totalsAndCountsResult);

        totalsAndCountsResult = client.sendAction(
            new GetTotalsAndCountsAction(bankAccountInfo.getId(),
                per,
                LinxoDate.makeDate("01062015"),
                LinxoDate.makeDate("01082015")
            )
        );

        logger.info("Result for account["+bankAccountInfo.getName()+"]:" + totalsAndCountsResult);


        logger.info(">>>>>>>>>>>>>>");
      }

      logger.info(">>>>>>>>>>>>>>");
      logger.info(">>>>>>>>>>>>>>");

      final Calendar calendar = Calendar.getInstance();
      calendar.set(Calendar.HOUR_OF_DAY, 0);
      calendar.set(Calendar.MINUTE, 0);
      calendar.set(Calendar.SECOND, 0);
      calendar.set(Calendar.MILLISECOND, 0);
      calendar.set(Calendar.DAY_OF_MONTH, 1);
      final Date firstOfMonth = calendar.getTime();

      final GetBudgetTargetSuggestionResult suggestionResult = client.sendAction(
          new GetBudgetTargetSuggestionAction(bankAccountInfo.getId(), 0, false,
                                              IntervalUnit.month, 1,
                                              DateUtils.getLinxoDate(firstOfMonth))
      );

      logger.info("Got Suggestion ["+suggestionResult.getBudgetTarget().getSuggestedAmount()+"], let's go with it !");

      AddBudgetTargetResult addBudgetTargetResult = client.sendAction(
          new AddBudgetTargetAction(suggestionResult.getBudgetTarget())
      );

      logger.info("Added Suggestion ["+addBudgetTargetResult.getBudgetTarget()+"]");

      BudgetTarget myTarget = addBudgetTargetResult.getBudgetTarget();
      myTarget.setAmount(myTarget.getAmount() + 100.);
      EditBudgetTargetResult editBudgetTargetResult = client.sendAction(
          new EditBudgetTargetAction(myTarget)
      );

      logger.info("Edited Suggestion ["+editBudgetTargetResult.getBudgetTarget()+"]");


      // Logout on User's decision
      LogoutResult logoutResult = client.sendAction(
          new LogoutAction(LogoutAction.LogoutReason.User)
      );
      logger.info("LogoutResult " + logoutResult);

      long end = System.currentTimeMillis();
      logger.info("Whole conversation took " + (end-start) + "ms");

    }
    catch(LinxoClientException ee) {
      logger.error(ee.getMessage(), ee);
      logger.error(ee.getMessage(), client.getClientException(ee));
    }
    finally {
      client.close();
    }
  }

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods
}
