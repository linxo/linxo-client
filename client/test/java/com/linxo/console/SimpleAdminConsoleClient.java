/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 30/09/2014 by hugues.
 */
package com.linxo.console;

import com.linxo.client.actions.auth.LoginResult;
import com.linxo.client.actions.auth.LogoutAction;
import com.linxo.client.actions.auth.LogoutResult;
import com.linxo.client.actions.pfm.GetBankAccountListAction;
import com.linxo.client.actions.pfm.GetBankAccountListResult;
import com.linxo.client.actions.pfm.GetTransactionsAction;
import com.linxo.client.actions.pfm.GetTransactionsResult;
import com.linxo.client.actions.pfm.budget.AddBudgetTargetAction;
import com.linxo.client.actions.pfm.budget.AddBudgetTargetResult;
import com.linxo.client.actions.pfm.budget.DeleteBudgetTargetAction;
import com.linxo.client.actions.pfm.budget.EditBudgetTargetAction;
import com.linxo.client.actions.pfm.budget.EditBudgetTargetResult;
import com.linxo.client.actions.pfm.budget.GetBudgetTargetSuggestionAction;
import com.linxo.client.actions.pfm.budget.GetBudgetTargetSuggestionResult;
import com.linxo.client.actions.pfm.budget.GetBudgetTargetsAction;
import com.linxo.client.actions.pfm.budget.GetBudgetTargetsResult;
import com.linxo.client.actions.pfm.trends.GetTotalsAndCountsAction;
import com.linxo.client.actions.pfm.trends.GetTotalsAndCountsResult;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.data.upcoming.IntervalUnit;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.dto.budget.BudgetTarget;
import com.linxo.client.net.LinxoClient;
import com.linxo.client.net.LinxoClientBuilder;
import com.linxo.client.net.LinxoClientException;
import com.linxo.client.service.AccountService;
import com.linxo.client.service.CategoryService;
import com.linxo.client.service.TransactionService;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;

public class SimpleAdminConsoleClient
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(SimpleAdminConsoleClient.class);

  // Static Initializers

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  // Static Methods

  public static void main(String [] argv) throws Exception
  {

    long start = System.currentTimeMillis();

    Long userId = 50L;

    /*LinxoClient client = new LinxoClientBuilder(
            ConsoleConstants.SERVER,
            ConsoleConstants.API_KEY,
            ConsoleConstants.API_SECRET)
            .threadPool(10)
            .timeout(5)
            .build();*/

    // To have a stateless LinxoClient, one needs to provide a
    // UserCredentials object in the constructor.
    // This UserCredentials object contains the users' credentials
    // information.
    LinxoClient userCredentialsClient = new LinxoClient(
            ConsoleConstants.SERVER,
            ConsoleConstants.API_KEY,
            ConsoleConstants.API_SECRET,
            new LinxoClient.UserCredentials("dev@linxo.com", "Passw0rd")
    );

    TransactionService transactionService = new TransactionService();
    CategoryService categoryService = new CategoryService();
    AccountService accountService = new AccountService();
    try {
      userCredentialsClient.initialize();

      transactionService.setLinxoClient(userCredentialsClient);
      transactionService.setCategoryService(categoryService);
      transactionService.setAccountService(accountService);

      GetTransactionsResult getTransactionsResult = transactionService.loadTransactions(
              new GetTransactionsAction (null, null,
                                         null, null,
                                         null,null,
                                         null,null,
                                         null,null,
                                         false,null,
                                         false, null,
                                         null, false,
                                         false,null,
                                         null,
                                         10000L, 1515093429L,
                                         0,0
      ), userId);


      // getTransactionsResult has transactions ?
      if(getTransactionsResult == null) {
        logger.info("No transactions");
        return;
      }

      logger.info("getTransactionsResult size = " + getTransactionsResult.getTransactions().size());

    }
    catch(LinxoClientException ee) {
      logger.error(ee.getMessage(), ee);
      logger.error(ee.getMessage(), userCredentialsClient.getClientException(ee));
    }
    finally {
      userCredentialsClient.close();
    }
  }

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods
}
