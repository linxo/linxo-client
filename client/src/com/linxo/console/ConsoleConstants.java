/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 07/08/2015 by hugues.
 */
package com.linxo.console;

public interface ConsoleConstants {
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Instance Methods

  // TODO : replace with server address, API_KEY and API_SECRET
  // BUILD
  static final String SERVER = "bred.dev.linxo.com";
  static final String API_KEY = "";
  static final String API_SECRET = "";
  static final String DEFAULT_USER = "dev@linxo.com";
  static final String DEFAULT_PASS = "Passw0rd";


  // PARTNERS
  //static final String SERVER = "partners.linxo.com";
  //static final String API_KEY = "";
  //static final String API_SECRET = "";
  //static final String DEFAULT_USER = "jean@linxo.com";
  //static final String DEFAULT_PASS = "password3";

  // PRODUCTION
//  static final String SERVER = "wwws.linxo.com";
//  static final String API_KEY = "";
//  static final String API_SECRET = "";
//  static final String DEFAULT_USER = "jean@linxo.com";
//  static final String DEFAULT_PASS = "password3";

}
