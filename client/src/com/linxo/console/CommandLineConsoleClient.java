/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/08/2015 by hugues.
 */
package com.linxo.console;

import com.linxo.client.actions.pfm.trends.GetBalanceHistoryResult;
import com.linxo.client.actions.pfm.trends.GetTotalsAndCountsResult;
import com.linxo.client.data.DateUtils;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.StringUtils;
import com.linxo.client.data.auth.AuthStatus;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.dto.account.AccountGroupInfo;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.service.ServiceHub;
import com.linxo.infrastructure.exceptions.FatalException;
import com.linxo.infrastructure.exceptions.TechnicalException;
import com.linxo.infrastructure.interfaces.AppDeviceConfig;
import com.linxo.infrastructure.interfaces.AppStorage;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * A command-line oriented console that connects to a host for a given
 * user and launches an interactive session.
 */
public class CommandLineConsoleClient {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(CommandLineConsoleClient.class);

  private static final DateFormat DATE_FORMAT = SimpleDateFormat.getDateInstance(DateFormat.SHORT);

  // Static Initializers

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  // Static Methods

  public static void main(String [] argv) throws Exception
  {

    CommandLineConsoleClient client = null;

    try {
      client = new CommandLineConsoleClient();

      client.startUp();
      client.mainLoop();
    }
    catch(Exception fatal)
    {
      System.err.println("Error while processing");
      fatal.printStackTrace();
    }
    finally {
      if(client != null) {
        client.shutdown();
      }
      System.err.println("Client shutdown");
    }

  }

  // Instance Fields

  private final ServiceHub serviceHub;
  private final Scanner scanner = new Scanner(System.in);

  // Instance Initializers

  // Constructors


  private CommandLineConsoleClient() throws FatalException
  {
    AppDeviceConfig appDeviceConfig = new AppDeviceConfig() {
      @Override
      public String getDeviceType()
      {
        return "Server";
      }

      @Override
      public String getDeviceFamily()
      {
        return "Java";
      }

      @Override
      public String getDeviceName()
      {
        return "Test";
      }

      @Override
      public String getAppIdentifier()
      {
        return "identifier";
      }

      @Override
      public String getAppVersion()
      {
        return "1.0";
      }

      @Override
      public String getDomain()
      {
        return ConsoleConstants.SERVER;
      }

      @Override
      public String getApiKey()
      {
        return ConsoleConstants.API_KEY;
      }

      @Override
      public String getApiSecret()
      {
        return ConsoleConstants.API_SECRET;
      }
    };
    AppStorage appStorage = new AppStorage() {

      private HashMap<String, String> properties = new HashMap<String, String>();

      @Override
      public Boolean saveProtected(String key, String data)
      {
        properties.put(key, data);
        return true;
      }

      @Override
      public Boolean save(String key, String data)
      {
        properties.put(key, data);
        return true;
      }

      @Override
      public String getStringData(String key)
      {
        return properties.get(key);
      }

      @Override
      public Integer getIntegerData(String key)
      {
        return Integer.valueOf(properties.get(key));
      }

      @Override
      public String getStringDataProtected(String key)
      {
        return properties.get(key);
      }

      @Override
      public Integer getIntegerDataProtected(String key)
      {
        return Integer.valueOf(properties.get(key));
      }

      @Override
      public Boolean delete(String key)
      {
        return properties.remove(key) != null;
      }
    };

    serviceHub = new ServiceHub(appDeviceConfig, appStorage);
  }

  // Instance Methods

  private void startUp() throws FatalException, TechnicalException
  {
    System.out.println("Initializing services...");

    serviceHub.initialize();

    System.out.println("Connecting...");
    boolean connected = false;

    while (!connected) {
      System.out.print("email : ");
      String email = scanner.nextLine();
      if (StringUtils.isEmptyTrimmed(email)) {
        System.err.println("please enter valid email");
        askToContinue();
        continue;
      }
      email = email.trim();


      System.out.print("password : ");
      String password = scanner.nextLine();
      if (StringUtils.isEmptyTrimmed(password)) {
        System.err.println("please enter valid password");
        askToContinue();
        continue;
      }
      password = password.trim();

      final AuthStatus authStatus = serviceHub.getAuthenticationService().authorizeDevice(email, password);
      if(authStatus != AuthStatus.Success )
      {
        System.err.println("Could not connect, status ["+authStatus+"]");
        askToContinue();
        continue;
      }

      logger.info("API connected------------------------------");
      serviceHub.getAuthenticationService().setPin("1234");

      connected = true;
    }
  }

  private void mainLoop() throws FatalException, TechnicalException
  {
    showHelp();

    //noinspection InfiniteLoopStatement
    while(true){
      System.out.println("Command (? for help) >");
      String command = scanner.nextLine();

      if (StringUtils.isEmpty(command))
      {
        continue;
      }

      command = command.trim();

      if(command.equalsIgnoreCase("?") || command.equalsIgnoreCase("h"))
      {
        showHelp();
      }
      else if (command.equalsIgnoreCase("q"))
      {
        askToContinue();
      }
      else if (command.startsWith("groups"))
      {
        showGroups();
      }
      else if (command.startsWith("accounts"))
      {
        showAccounts(command.substring("accounts".length()).trim());
      }
      else if (command.startsWith("account "))
      {
        showAccount(command.substring("account".length()).trim());
      }
      else if (command.startsWith("history "))
      {
        showHistory(command.substring("history".length()).trim());
      }
      else if (command.startsWith("income "))
      {
        incomeGraph(command.substring("income".length()).trim());
      }
    }

  }

  private void showHelp()
  {
    System.out.println("Linxo Console Client ------------------------");
    System.out.println(" - ?/h             : show this help");
    System.out.println(" - q               : quits this program");
    System.out.println(" - groups          : shows the list of bank connexions");
    System.out.println(" - accounts [type] : shows the list of accounts with an possible filter");
    System.out.println("             type: all, Checkings, savings, creditcard (default : all)");
    System.out.println(" - account (id)    : shows the account with this id (if it exists)");
    System.out.println(" - history (id) [startDate [endDate]]  ");
    System.out.println(" -                 : shows the account with this id (if it exists)");
    System.out.println(" -                 : within the given startDate and endDate");
    System.out.println(" -                 : If endDate is not given, it is considered as today.");
    System.out.println(" -                 : If startDate is not given, it is considered as the earliest date we have for this account.");
    System.out.println(" -                 : There is not enough permission, returns an error.");
    System.out.println(" - income (id) [startDate [endDate]]  ");
    System.out.println(" -                 : shows the income date for the account with this id (if it exists)");
    System.out.println(" -                 : within the given startDate and endDate");
    System.out.println(" -                 : If endDate is not given, it is considered as today.");
    System.out.println(" -                 : If startDate is not given, it is considered as the earliest date we have for this account.");
    System.out.println(" -                 : There is not enough permission, returns an error.");
  }

  private void askToContinue()
  {
    System.out.println("Continue ? (Y/n):");
    String answer = scanner.nextLine();

    if(answer != null && answer.trim().equalsIgnoreCase("n")){
      System.out.println("Bye");
      System.exit(0);
    }
  }

  private void showGroups()
  {
    HashMap<Long, AccountGroupInfo> groups = serviceHub.getAccountService().getAccountGroupMap();

    for (AccountGroupInfo group : groups.values())
    {
      System.out.println("Connexion : " + group.getName() + "(" + group.getUrl() + ")");
      System.out.println(" - group : " + group);
      System.out.println("-----------------------------------------------------------------");
    }
  }

  private void showAccounts(String type)
  {
    HashMap<Long, BankAccountInfo> accounts = serviceHub.getAccountService().getBankAccountMap();

    for(BankAccountInfo account : accounts.values())
    {
      switch(account.getType()) {
        case Checkings:
          if (!StringUtils.isEmpty(type) && !AccountType.Checkings.name().equalsIgnoreCase(type)) {
            continue;
          }
          break;

        case Savings:
          if (!StringUtils.isEmpty(type) && !AccountType.Savings.name().equalsIgnoreCase(type)) {
            continue;
          }
          break;

        case CreditCard:
          if (!StringUtils.isEmpty(type) && !AccountType.CreditCard.name().equalsIgnoreCase(type)) {
            continue;
          }
          break;
      }

      // we display the account
      System.out.println("Account : " + account.getName() + "(" + account.getCurrentBalance() + ")");
      System.out.println(" - details : " + account);
      System.out.println("-----------------------------------------------------------------");

    }
  }

  private void showAccount(String id)
  {
    id = id.trim();
    if(StringUtils.isEmpty(id))
    {
      System.err.println("id null or empty");
      return;
    }

    Long longId;
    try {
      longId = Long.parseLong(id);
    }
    catch(NumberFormatException nfe)
    {
      System.err.println("Could not parse the id ["+id+"]");
      System.err.println("Please enter a valid id");
      return;
    }

    HashMap<Long, BankAccountInfo> accounts = serviceHub.getAccountService().getBankAccountMap();
    BankAccountInfo account = accounts.get(longId);
    if(account == null)
    {
      System.err.println("Unknown account with id ["+longId+"]");
      return;
    }

    // we display the account
    System.out.println("Account : " + account.getName() + "(" + account.getCurrentBalance() + ")");
    System.out.println(" - details : " + account);
    System.out.println("-----------------------------------------------------------------");

  }

  private void showHistory(String paramsString)
  {
    paramsString = paramsString.trim();
    String[] params = paramsString.split(" ");

    String id = params[0];
    id = id.trim();
    if(StringUtils.isEmpty(id))
    {
      System.err.println("id null or empty");
      return;
    }

    Long longId;
    try {
      longId = Long.parseLong(id);
    }
    catch(NumberFormatException nfe)
    {
      System.err.println("Could not parse the id ["+id+"]");
      System.err.println("Please enter a valid id");
      return;
    }

    LinxoDate startDate = null;
    LinxoDate endDate = null;
    if(params.length > 1) {
      String startDateString = params[1];
      startDateString = startDateString.trim();
      if(!StringUtils.isEmpty(startDateString))
      {
        try {
          startDate = DateUtils.getLinxoDate(DATE_FORMAT.parse(startDateString));
        }
        catch(ParseException nfe)
        {
          System.err.println("Could not parse the date ["+startDateString+"]");
          System.err.println("Date format dd/mm/yyyy");
          return;
        }
      }

      if(params.length > 2) {
        String endDateString = params[2];
        endDateString = endDateString.trim();
        if(!StringUtils.isEmpty(endDateString))
        {
          try {
            endDate = DateUtils.getLinxoDate(DATE_FORMAT.parse(endDateString));
          }
          catch(ParseException nfe)
          {
            System.err.println("Could not parse the date ["+endDateString+"]");
            System.err.println("Date format dd/mm/yyyy");
            return;
          }
        }
      }
    }

    ArrayList<GetBalanceHistoryResult.BalanceAndBalanceDate> history =
        serviceHub.getTransactionService()
            .loadBalanceDates(longId, startDate, endDate);

    if(history == null) {
      System.err.println("Could not load history");
      return;
    }

    for (GetBalanceHistoryResult.BalanceAndBalanceDate balanceAndDate : history)
    {
      System.out.println("-- " + balanceAndDate.balanceDate + " => " + balanceAndDate.balance);
    }
    System.out.println("-----------------------------------------------------------------");

  }

  private void incomeGraph(String paramsString)
  {
    paramsString = paramsString.trim();
    String[] params = paramsString.split(" ");

    LinxoDate startDate = null;
    LinxoDate endDate = null;
    if(params.length > 0) {
      String startDateString = params[0];
      startDateString = startDateString.trim();
      if(!StringUtils.isEmpty(startDateString))
      {
        try {
          startDate = DateUtils.getLinxoDate(DATE_FORMAT.parse(startDateString));
        }
        catch(ParseException nfe)
        {
          System.err.println("Could not parse the date ["+startDateString+"]");
          System.err.println("Date format dd/mm/yyyy");
          return;
        }
      }

      if(params.length > 1) {
        String endDateString = params[1];
        endDateString = endDateString.trim();
        if(!StringUtils.isEmpty(endDateString))
        {
          try {
            endDate = DateUtils.getLinxoDate(DATE_FORMAT.parse(endDateString));
          }
          catch(ParseException nfe)
          {
            System.err.println("Could not parse the date ["+endDateString+"]");
            System.err.println("Date format dd/mm/yyyy");
            return;
          }
        }
      }
    }

    GetTotalsAndCountsResult pieChartData = serviceHub.getTransactionService()
        .loadBudgetPieChartData(startDate, endDate);

    if (pieChartData == null) {
      System.err.println("Error while collecting data...");
      return;
    }

    new DisplayIncome(serviceHub, pieChartData).show();

  }

  private void shutdown() throws TechnicalException, FatalException
  {
    if(scanner != null)
    {
      scanner.close();
    }

    if(serviceHub != null)
    {
      serviceHub.getAuthenticationService().deAuthorizeDevice();
      serviceHub.clear();
    }
  }
}
