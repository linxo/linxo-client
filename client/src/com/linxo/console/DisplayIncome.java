/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 04/08/2015 by hugues.
 */
package com.linxo.console;

import com.linxo.client.actions.pfm.trends.GetTotalsAndCountsResult;
import com.linxo.client.dto.tx.CatInfo;
import com.linxo.client.service.CategoryService;
import com.linxo.client.service.ServiceHub;

import java.util.ArrayList;
import java.util.Collections;

public class DisplayIncome {

  // Nested Types (mixing inner and static classes is okay)

  private static class Slice implements Comparable<Slice> {
    CatInfo catInfo;
    double amount;
    int count;
    boolean drawable;

    public Slice(CatInfo catInfo, int count, double amount)
    {
      this.amount = amount;
      this.catInfo = catInfo;
      this.count = count;
      this.drawable = false;
    }

    @Override
    public int compareTo(Slice other) // this - other > 0
    {
      if(this.drawable && !other.drawable) return -1;
      if(!this.drawable && other.drawable) return 1;

      if (this.amount - other.amount < 0) return 1;
      else if (this.amount - other.amount > 0) return -1;
      else return 0;
    }

    public String print(Double total)
    {
      return "amount=" + amount +
          (
              (drawable)
                  ?" ( " + ((int)((100*amount)/total)) + " %)"
                  :""
          ) +
          ", cat=" + catInfo.getName() +
          ", count=" + count;
    }
  }


  // Static Fields



  // Static Initializers

  // Static Methods

  // Instance Fields

  private final ServiceHub serviceHub;

  private final GetTotalsAndCountsResult pieChartData;

  // Instance Initializers

  // Constructors

  public DisplayIncome(ServiceHub serviceHub, GetTotalsAndCountsResult pieChartData)
  {
    this.serviceHub = serviceHub;
    this.pieChartData = pieChartData;
  }


  // Instance Methods


  public void show() {

    ArrayList<Slice> slices = new ArrayList<Slice>();

    CatInfo incomeTheme = serviceHub.getCategoryService().categoryById(CategoryService.Theme_Income);

    for(CatInfo sc : incomeTheme.getSubCategories())
    {
      final Integer counts = pieChartData.getCounts().get(sc.getId());
      final Double totals = pieChartData.getTotals().get(sc.getId());
      if ( counts != null && counts > 0)
      {
        Slice slice = new Slice(sc, counts, totals);
        slices.add(slice);
        if(totals != null && totals > 0) {
          slice.drawable = true;
        }
      }
    }

    CatInfo unCategorizedTheme = serviceHub.getCategoryService().categoryById(CategoryService.Theme_Uncategorized);

    for(CatInfo sc : unCategorizedTheme.getSubCategories())
    {
      Integer subCounts = pieChartData.getCreditCounts().get(sc.getId());
      Double subTotals = pieChartData.getCredits().get(sc.getId());

      if(subCounts == null) {
        subCounts = 0; // ensure != null
      }
      if(subTotals == null) {
        subTotals = 0.; // ensure != null
      }

      if( subCounts > 0 ) {
        Slice slice = new Slice(sc, subCounts, subTotals);
        slices.add(slice);
        if ( subTotals > 0 )
        {
          slice.drawable = true;
        }
      }
    }


    // time to perform the display
    for(Slice slice : slices) {
      if (! slice.drawable )
      {
        System.out.println("/!\\ Not all values can be displayed in the pie chart");
        break;
      }
    }

    boolean atLeastOneDrawable = false;
    for (Slice slice : slices) {
      if(slice.drawable) {
        atLeastOneDrawable = true;
        break;
      }
    }

    if(!atLeastOneDrawable && slices.size() > 0) {
      System.out.println("/!\\ Impossible to build the pie chart graph (no data can be graphed)");
    }

    Collections.sort(slices);

    double total = 0.;
    for(Slice slice : slices)
    {
      if(slice.drawable) {
        total += slice.amount;
      }
    }

    System.out.println("+ Pie chart ----------------------------------------------------");
    for (Slice slice : slices)
    {
      System.out.println(slice.print(total));
    }

    System.out.println("+---------------------------------------------------------------");
  }


}


