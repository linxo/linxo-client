/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2016 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 07/09/2016 by hugues.
 */
package com.linxo.client.shared;


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

/**
 * Is used to check the length of String LinxoAction or DTO parameters.
 * The validation is performed after the LinxoAction is deserialized, but before it is
 * actually processed by handler code
 *
 * <blockquote><pre>
 *    Example 1:
 *
 *    &#064;AssertStringSize // Asserts that the fields is at most the default String size
 *    private String field
 *
 *    Example 2:
 *
 *    &#064;AssertStringSize(10, nullable = false) // cannot be null
 *    private String smallerField
 *
 *    Example 3:
 *
 *    &#064;AssertStringSize(1110, voidable = false, nullable = false) // cannot be null or empty
 *    private String biggerField
 *
 * </pre></blockquote>
 *
 * The bean validation associated to this annotation can be performed using
 * reflection on the server side using the
 * {@link com.linxo.gwt.dispatch.server.validation.AnnotationBasedValidator}.
 *
 * @see com.linxo.gwt.dispatch.server.validation.AnnotationBasedValidator
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({FIELD})
public @interface AssertStringSize {

  /**
   * The max length of the String annotated by this annotation.
   *
   * This is the default column size in database for String object mapping.
   */
  int value() default 255;

  /**
   * (Optional) Whether the String field is nullable.
   *
   * default is {@code true}
   */
  boolean nullable() default true;

  /**
   * (Optional) Whether the String field can be empty (zero length).
   *
   * default is {@code true}
   */
  boolean voidable() default true;

  /**
   * (Optional) Whether the String field should be trimmed when testing.
   *
   * default is {@code true}
   */
  boolean trimWhenAsserting() default true;
}
