/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2016 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 24/10/2016 by hugues.
 */
package com.linxo.client.shared;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

/**
 * Is used to check that an LinxoAction or DTO member must not be {@code null}.
 * The validation is performed after the LinxoAction is deserialized, but before it is
 * actually processed by handler code
 *
 * The bean validation associated to this annotation can be performed using
 * reflection on the server side using the
 * {@link com.linxo.gwt.dispatch.server.validation.AnnotationBasedValidator}.
 *
 * @see com.linxo.gwt.dispatch.server.validation.AnnotationBasedValidator
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({FIELD})
public @interface AssertNotNull {
}
