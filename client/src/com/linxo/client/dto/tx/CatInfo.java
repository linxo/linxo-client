/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.dto.tx;

import com.linxo.client.data.CategoryConstants;
import com.linxo.client.data.permissions.Feature;
import com.linxo.client.data.support.Version;
import com.linxo.client.dto.EntityInfo;

import java.util.ArrayList;

public final class CatInfo extends EntityInfo
  implements Comparable<CatInfo>
{
  public static final String KEY = "key";
  public static final String TYPE = "type";
  public static final String THEME_ID = "themeId";
  public static final String PARENT_ID = "parentId";
  public static final String SUB_CATEGORIES = "subCategories";
  public static final String NAME = "name";
  public static final String COMMENT = "comment";
  public static final String COLOR = "color";
  public static final String ICON = "icon";
  public static final String CUSTOM = "custom";
  public static final String MASKED = "masked";
  public static final String DISABLED = "disabled";

  /**
   * Indicates the type of category (aka level in the category tree).
   * The higher, the deeper.
   */
  public enum Type
  {
    Theme(0),
    Category(1),
    SubCategory(2);

    private int type;

    private Type(final int type)
    {
      this.type = type;
    }

    public int getType()
    {
      return type;
    }

    public static Type getType(final int type)
    {
      return type == 0 ? Theme
                       : type == 1 ? Category
                                   : type == 2 ? SubCategory
                                               : null;
    }

    public static Type getType(final String type)
    {
      for (int i = 0; i < Type.values().length; i++) {
        if (type.equalsIgnoreCase(getType(i).name())) {
          return getType(i);
        }
      }
      return null;
    }

  }

  private String key;
  private Type type;
  private CatInfo parent;
  private ArrayList<CatInfo> subCategories = new ArrayList<CatInfo>();
  private String name;
  private String comment;
  private String color;
  private String icon;
  private boolean custom;
  private boolean masked;
  private boolean disabled;
  private CatInfo theme;

  protected CatInfo() {}

  /**
   * @param id the id of this CatInfo
   * @param key the GWT translation key for this CatInfo
   * @param type the type of this CatInfo
   * @deprecated testing only
   */
  @Deprecated
  public CatInfo(final long id, final String key, final Type type)
  {
    this(id, key, null, null, type, null, null, null, null, false, false, false);
  }

  public CatInfo(final long id)
  {
    setId(id);
  }

  public CatInfo(final long id, final String key, final String name, final String comment,
                 final Type type, final CatInfo theme, final CatInfo parent, final String color,
                 final String icon, final boolean custom, final boolean masked,
                 final boolean disabled)
  {
    this();
    setId(id);
    this.key = key;
    this.type = type;
    this.theme = theme;
    this.parent = parent;
    this.color = color;
    this.icon = icon;
    this.name = name;
    this.comment = comment;
    this.custom = custom;
    this.masked = masked;
    this.disabled = disabled;
  }

  public CatInfo(final long id, final String key, final String name, final String comment,
                 final Type type, final CatInfo theme, final CatInfo parent, final String color,
                 final String icon, final boolean custom, final boolean masked,
                 final boolean disabled, final ArrayList<CatInfo> subCatList)
  {
    this(id, key, name, comment, type, theme, parent, color, icon, custom, masked, disabled);
    setSubCategories(subCatList);
  }

  /**
   * @return id for GWT localization
   */
  public String getKey()
  {
    return key;
  }

  /**
   * @return the type / level of this category
   */
  public Type getType()
  {
    return type;
  }

  /**
   * @return the parent category if it exists, or null if this is a theme
   */
  public CatInfo getParent()
  {
    return parent;
  }

  /**
   * @return an list of the daughter categories
   */
  public ArrayList<CatInfo> getSubCategories()
  {
    return subCategories;
  }

  public void setSubCategories(ArrayList<CatInfo> subCategories)
  {
    this.subCategories = subCategories;
  }

  /**
   * @return the name of the category
   */
  public String getName()
  {
    return name;
  }

  public void setName(final String name)
  {
    this.name = name;
  }

  /**
   * @return comments/explanations on the category
   */
  public String getComment()
  {
    return comment;
  }

  public void setComment(final String comment)
  {
    this.comment = comment;
  }

  /**
   * @return the HTML color code to use to color the icon/text/section for this category
   */
  public String getColor()
  {
    return color;
  }

  public void setColor(final String color)
  {
    this.color = color;
  }

  /**
   * @return the URL of the icon to use for this category
   */
  public String getIcon()
  {
    return icon;
  }

  public void setIcon(final String icon)
  {
    this.icon = icon;
  }

  public void setParent(CatInfo parent)
  {
    this.parent = parent;
  }


  /**
   * Indicate if this category is default or custom.
   * @return
   * <ol>
   *   <li>When {@code true} this category is a custom category</li>
   *   <li>When {@code false} this category is a default category</li>
   * </ol>
   */
  public boolean isCustom()
  {
    return custom;
  }

  public void setCustom(boolean custom)
  {
    this.custom = custom;
  }

  /**
   * Indicate if this category is masked or visible.
   * @return
   * <ol>
   *   <li>When {@code true} this category is masked</li>
   *   <li>When {@code false} this category is visible</li>
   * </ol>
   * This field is always false for custom categories.
   */
  public boolean isMasked()
  {
    return masked;
  }

  public void setMasked(boolean masked)
  {
    this.masked = masked;
  }

  /**
   * Indicate if this category is disabled.
   * Disabled categories are custom categories for a User
   * who has lost the {@link Feature#CustomCategories permission to use custom categories}.
   * @return
   * <ol>
   *   <li>When {@code true} this category is disabled (cannot be assigned)</li>
   *   <li>When {@code false} this category is enabled (all actions are possible)</li>
   * </ol>
   * This field is always false for custom categories.
   */
  public boolean isDisabled()
  {
    return disabled;
  }

  public void setDisabled(boolean disabled)
  {
    this.disabled = disabled;
  }

  public CatInfo getTheme()
  {
    return theme;
  }

  public void setTheme(CatInfo theme)
  {
    this.theme = theme;
  }

  private static final char[] ACCENTS = new char[] {
      'À','Á','Â','Ã','Ä','Å','à','á','â','ã','ä','å',
      'Ò','Ó','Ô','Õ','Õ','Ö','Ø','ò','ó','ô','õ','ö','ø',
      'È','É','Ê','Ë','è','é','ê','ë',
      'ð','Ç','ç','Ð',
      'Ì','Í','Î','Ï','ì','í','î','ï',
      'Ù','Ú','Û','Ü','ù','ú','û','ü',
      'Ñ','ñ',
      'Š','š',
      'Ÿ','ÿ','ý',
      'Ž','ž'};

  private static final char[] NO_ACCENTS = new char[] {
      'A','A','A','A','A','A','a','a','a','a','a','a',
      'O','O','O','O','O','O','O','o','o','o','o','o','o',
      'E','E','E','E','e','e','e','e','e',
      'C','c','D',
      'I','I','I','I','i','i','i','i',
      'U','U','U','U','u','u','u','u',
      'N','n',
      'S','s',
      'Y','y','y',
      'Z','z'};

  private static String normalize(final String string)
  {
    String normalizedName = string.toLowerCase();
    for (int i = 0 ; i < ACCENTS.length ; i++) {
      normalizedName = normalizedName.replace(ACCENTS[i], NO_ACCENTS[i]);
    }
    return normalizedName;
  }

  @Override
  public int compareTo(final CatInfo other)
  {
    return normalize(this.name).compareToIgnoreCase(normalize(other.name));
  }

  public void prepareFor(final Version version)
  {
    if (version.hasCustomCategories()) {
      return;
    }
    if (CategoryConstants.OLD_COLOR_PER_CATEGORY_ID.containsKey(getId())) {
      setColor(CategoryConstants.OLD_COLOR_PER_CATEGORY_ID.get(getId()));
    }
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder();
    sb.append("CategoryInfo");
    sb.append("{id='").append(getId()).append('\'');
    sb.append(", key=").append(key);
    sb.append(", type=").append(type);
    sb.append(", color=").append(color);
    sb.append(", icon=").append(icon);
    sb.append(", name=").append(name);
    sb.append(", custom=").append(custom);
    sb.append(", masked=").append(masked);
    sb.append(", disabled=").append(disabled);
    sb.append(", comment=").append(comment);
    sb.append(", parentId=").append((parent != null) ? parent.getId() : "null");
    sb.append(", themeId=").append((theme != null) ? theme.getId() : "null");
    sb.append(", subCategories=").append((subCategories != null) ? subCategories.toString() : "null");
    sb.append('}');
    return sb.toString();
  }

}
