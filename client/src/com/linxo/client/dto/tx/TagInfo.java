/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.dto.tx;

import com.linxo.client.dto.EntityInfo;

public final class TagInfo extends EntityInfo implements Comparable<TagInfo>
{
  private String name;
  private int transactionCount;

  @SuppressWarnings("unused") // s11n
  private TagInfo(){}

  public TagInfo ( long id )
  {
    setId( id );
  }

  public String getName() { return name; }
  public void setName( String name ) { this.name = name; }

  public int getTransactionCount()
  {
    return transactionCount;
  }

  public void setTransactionCount(int transactionCount)
  {
    this.transactionCount = transactionCount;
  }

  @Override
  public boolean equals( Object o )
  {
    if ( this == o ) return true;
    if ( !( o instanceof TagInfo ) ) return false;

    TagInfo tagInfo = (TagInfo) o;

    if ( getId() != null ? !getId().equals( tagInfo.getId() ) : tagInfo.getId() != null ) return false;
    if ( name != null ? !name.equals( tagInfo.name ) : tagInfo.name != null ) return false;
    if (transactionCount!=tagInfo.transactionCount) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = getId() != null ? getId().hashCode() : 0;
    result = 31 * result + ( name != null ? name.hashCode() : 0 );
    result = 31 * result + transactionCount;
    return result;
  }


  // ~~~ implements Comparable<TagInfo>


  /**
   * Compares two tags using first the lexical order on names. If equals, compares on id.
   * @param other - the other object to compare with this.
   * @return  a negative integer, zero, or a positive integer as this object
   *          is less than, equal to, or greater than the specified object.
   */
  @Override
  public int compareTo( TagInfo other )
  {
    int nameDistance = getName().compareTo( other.getName() );
    if( nameDistance != 0 ) return nameDistance;

    return getId().compareTo( other.getId() );
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("TagInfo{");
    sb.append("id=").append(getId());
    sb.append("name='").append(name).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
