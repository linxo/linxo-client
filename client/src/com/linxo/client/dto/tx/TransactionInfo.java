/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 11/07/2010 by hugues.
 */
package com.linxo.client.dto.tx;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.pfm.bank.TransactionType;
import com.linxo.client.data.upcoming.IntervalUnit;
import com.linxo.client.dto.EntityInfo;

import java.util.ArrayList;
import java.util.Date;

public class TransactionInfo extends EntityInfo
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private Date date;
  private Date budgetDate;
  private String label;
  private Long categoryId;
  private double amount;
  private String checkNumber;
  private boolean duplicate;
  private String originalLabel;
  private String notes;
  private ArrayList<Long> tagIds;
  private long bankAccountId;
  /// all attributes prefixed by "original" come from Transaction
  private TransactionType originalType;
  private Date   originalDateAvailable;
  private Date   originalDateInitiated;
  private String originalPostCode;
  private String originalCity;
  private String originalCountry;
  private String originalThirdParty;
  private Boolean originalThirdPartyLabelTruncated;
  private String originalThirdPartyAccountNumber;

  @Deprecated
  private boolean categorizedByUser;
  private Long originalCategory;
  private IntervalUnit potentialIntervalUnit;
  private LinxoDate debitDate;
  private String reference;
  private String extCustomType;
  // ext fields for BRED
  private String extBredCategory;
  private String extBredSubCategory;
  private String extBredRuleCategory;
  private String extBredRuleSubCategory;
  private Long extBredCategorizationRuleId;
  // Timestamp in seconds
  private Long importDate;

  // Instance Initializers

  // Constructors

  protected TransactionInfo() {}

  public TransactionInfo(long id) {
    setId(id);
  }

  public TransactionInfo(long id,
                         double amount,
                         Long categoryId,
                         String checkNumber,
                         Date date,
                         Date budgetDate,
                         String label,
                         boolean duplicate,
                         String originalLabel,
                         String notes,
                         ArrayList<Long> tagIds,
                         long bankAccountId,
                         TransactionType originalType,
                         Date originalDateAvailable,
                         Date originalDateInitiated,
                         String originalPostCode,
                         String originalCity,
                         String originalCountry,
                         String originalThirdParty,
                         Boolean originalThirdPartyLabelTruncated,
                         String originalThirdPartyAccountNumber,
                         @Deprecated Boolean categorizedByUser,
                         Long originalCategory,
                         IntervalUnit potentialIntervalUnit,
                         LinxoDate debitDate,
                         String reference
  )
  {
    this(id, amount, categoryId, checkNumber, date, budgetDate, label, duplicate, originalLabel,
         notes, tagIds, bankAccountId, originalType, originalDateAvailable, originalDateInitiated,
         originalPostCode, originalCity, originalCountry, originalThirdParty,
         originalThirdPartyLabelTruncated, originalThirdPartyAccountNumber, categorizedByUser,
         originalCategory, potentialIntervalUnit, debitDate, reference, null);
  }

  public TransactionInfo(long id,
                         double amount,
                         Long categoryId,
                         String checkNumber,
                         Date date,
                         Date budgetDate,
                         String label,
                         boolean duplicate,
                         String originalLabel,
                         String notes,
                         ArrayList<Long> tagIds,
                         long bankAccountId,
                         TransactionType originalType,
                         Date originalDateAvailable,
                         Date originalDateInitiated,
                         String originalPostCode,
                         String originalCity,
                         String originalCountry,
                         String originalThirdParty,
                         Boolean originalThirdPartyLabelTruncated,
                         String originalThirdPartyAccountNumber,
                         @Deprecated Boolean categorizedByUser,
                         Long originalCategory,
                         IntervalUnit potentialIntervalUnit,
                         LinxoDate debitDate,
                         String reference,
                         String extCustomType
  )
  {
    setId(id);
    // avoid -0.0
    this.amount = amount == 0 ? 0 : amount;
    this.categoryId = categoryId;
    this.checkNumber = checkNumber;
    this.date = date;
    this.budgetDate = budgetDate;
    this.label = label;
    this.duplicate=duplicate;
    this.originalLabel = originalLabel;
    this.notes = notes;
    this.tagIds = tagIds;
    this.bankAccountId = bankAccountId;
    this.originalType = originalType;
    this.originalDateAvailable = originalDateAvailable;
    this.originalDateInitiated = originalDateInitiated;
    this.originalPostCode = originalPostCode;
    this.originalCity = originalCity;
    this.originalCountry = originalCountry;
    this.originalThirdParty = originalThirdParty;
    this.originalThirdPartyLabelTruncated = originalThirdPartyLabelTruncated;
    this.originalThirdParty = originalThirdParty;
    this.originalThirdPartyAccountNumber = originalThirdPartyAccountNumber;
    this.categorizedByUser = categorizedByUser;
    this.originalCategory = originalCategory;
    this.potentialIntervalUnit = potentialIntervalUnit;
    this.debitDate = debitDate;
    this.reference = reference;
    this.extCustomType = extCustomType;
  }

  // Instance Methods

  /**
   * @return the amount of the transaction.
   */
  public final double getAmount()
  {
    return amount;
  }

  /**
   * @return the id the of currently assigned {@link com.linxo.client.dto.tx.CatInfo}
   */
  public final Long getCategoryId()
  {
    return categoryId;
  }

  /**
   * @return the check number when available, or null otherwise
   */
  public final String getCheckNumber()
  {
    return checkNumber;
  }

  /**
   * @return the date posted
   */
  public final Date getDate()
  {
    return date;
  }

  /**
   * A User can change the month to which a transaction is assigned. She can change it to the month before the
   * {@link #date} or the month after. Depending on the choice, this date is the last day of the previous month
   * of the first day of the next month.
   *
   * When a user has not changed the value, or as selected the same month, it has the same value as {@link #date}.
   *
   * @return the date used when computing classification (budget) values
   */
  public final Date getBudgetDate()
  {
    return budgetDate;
  }

  /**
   * @return the current label to display for this transaction
   */
  public final String getLabel()
  {
    return label;
  }

  public final void setAmount(double amount)
  {
    this.amount = amount == 0 ? 0 : amount;
  }

  public final void setCategoryId(Long categoryId)
  {
    this.categoryId = categoryId;
  }

  public final void setCheckNumber(String checkNumber)
  {
    this.checkNumber = checkNumber;
  }

  public final void setDate(Date date)
  {
    this.date = date;
  }

  public final void setBudgetDate(Date budgetDate)
  {
    this.budgetDate = budgetDate;
  }

  public final void setLabel(String label)
  {
    this.label = label;
  }

  /**
   * A User may find that this transaction is actually duplicated (bugs happen...). When she decides that the
   * When this value is {@code true}, this transactions is not used when computing budget values.
   *
   * @return {@code true} or {@code false} depending on the User's choice
   */
  public final boolean isDuplicate()
  {
    return duplicate;
  }

  public void setDuplicate(boolean duplicate)
  {
    this.duplicate = duplicate;
  }

  /**
   * @return - additional notes for this transaction
   */
  public final String getNotes()
  {
    return notes;
  }

  public final void setNotes(String notes)
  {
    this.notes = notes;
  }

  /**
   * @return the label as seen on the bank site when visited
   */
  public final String getOriginalLabel()
  {
    return originalLabel;
  }

  public final void setOriginalLabel(String originalLabel)
  {
    this.originalLabel = originalLabel;
  }

  /**
   * @return a list of {@link com.linxo.client.dto.tx.TagInfo} id.
   */
  public final ArrayList<Long> getTagIds()
  {
    return tagIds;
  }

  public final void setTagIds(ArrayList<Long> tagIds)
  {
    this.tagIds = tagIds;
  }

  /**
   * @return the id of the bank account where this transactions is attached
   */
  public final long getBankAccountId()
  {
    return bankAccountId;
  }

  public final void setBankAccountId(long bankAccountId)
  {
    this.bankAccountId = bankAccountId;
  }

  /**
   * @return The type of this transaction. This cannot be changed.
   */
  public TransactionType getOriginalType()
  {
    return originalType;
  }

  public void setOriginalType(TransactionType originalType)
  {
    this.originalType = originalType;
  }

  /**
   * @return the date when the amount of this transactions actually impacts the balance of the account
   * (In France: "date de valeur"). {@code null} is returned when the values is not available.
   */
  public Date getOriginalDateAvailable()
  {
    return originalDateAvailable;
  }

  public void setOriginalDateAvailable(Date originalDateAvailable)
  {
    this.originalDateAvailable = originalDateAvailable;
  }

  /**
   * @return the date when this transactions was initiated.
   * {@code null} is returned when the values is not available.
   */
  public Date getOriginalDateInitiated()
  {
    return originalDateInitiated;
  }

  public void setOriginalDateInitiated(Date originalDateInitiated)
  {
    this.originalDateInitiated = originalDateInitiated;
  }

  /**
   * @return the postcode of the place where the transactions took place when available.
   * {@code null} is returned when the values is not available.
   */
  public String getOriginalPostCode()
  {
    return originalPostCode;
  }

  public void setOriginalPostCode(String originalPostCode)
  {
    this.originalPostCode = originalPostCode;
  }

  /**
   * @return the city of the place where the transactions took place when available.
   * {@code null} is returned when the values is not available.
   */
  public String getOriginalCity()
  {
    return originalCity;
  }

  public void setOriginalCity(String originalCity)
  {
    this.originalCity = originalCity;
  }

  /**
   * @return the country of the place where the transactions took place when available.
   * {@code null} is returned when the values is not available.
   */
  public String getOriginalCountry()
  {
    return originalCountry;
  }

  public void setOriginalCountry(String originalCountry)
  {
    this.originalCountry = originalCountry;
  }

  /**
   * @return the merchant name found when the label was processed when available. This name may be truncated
   * {@code null} is returned when the values is not available.
   * @see #getOriginalThirdPartyLabelTruncated
   */
  public String getOriginalThirdParty()
  {
    return originalThirdParty;
  }

  public void setOriginalThirdParty(String originalThirdParty)
  {
    this.originalThirdParty = originalThirdParty;
  }

  /**
   * In some banks, the length of the {@link #originalLabel} is limited to a certain number of characters.
   * This may result in truncating the name of the merchant when the label is processed.
   *
   * @return {@code true} when the name of the merchant found when the label was processed when available was truncated.
   * {@code false} otherwise.
   * @see #getOriginalThirdParty
   */
  public Boolean getOriginalThirdPartyLabelTruncated()
  {
    return originalThirdPartyLabelTruncated;
  }

  public void setOriginalThirdPartyLabelTruncated(Boolean originalThirdPartyLabelTruncated)
  {
    this.originalThirdPartyLabelTruncated = originalThirdPartyLabelTruncated;
  }

  /**
   * @return account number to/from which this transactions is transferred from/to.
   * {@code null} is returned when the values is not available.
   */
  public String getOriginalThirdPartyAccountNumber()
  {
    return originalThirdPartyAccountNumber;
  }

  public void setOriginalThirdPartyAccountNumber(String originalThirdPartyAccountNumber)
  {
    this.originalThirdPartyAccountNumber = originalThirdPartyAccountNumber;
  }

  /**
   * @return when {@code true} the user has modified the category assigned originally to this transaction.
   * @deprecated This method is deprecated in favor to comparing the {@link #originalCategory} vs. the
   * {@link #categoryId current Category}.
   */
  @Deprecated
  public boolean isCategorizedByUser()
  {
    return categorizedByUser;
  }

  @Deprecated
  public void setCategorizedByUser(boolean categorizedByUser)
  {
    this.categorizedByUser = categorizedByUser;
  }

  public Long getOriginalCategory()
  {
    return originalCategory;
  }

  public void setOriginalCategory(Long originalCategory)
  {
    this.originalCategory = originalCategory;
  }

  /**
   * During the transaction processing, it may be found that this transaction is a recurring transaction. In this
   * case, the interval unit found is attached to the transaction.
   *
   * @return the potential interval unit of this transaction.
   * {@code null} is returned when the values is not available.
   */
  public IntervalUnit getPotentialIntervalUnit()
  {
    return potentialIntervalUnit;
  }

  public void setPotentialIntervalUnit(IntervalUnit potentialIntervalUnit)
  {
    this.potentialIntervalUnit = potentialIntervalUnit;
  }

  /**
   * This value is used by credit cards.
   *
   * @return the day when the amount of this transaction impacts the account balance.
   * {@code null} is returned when the values is not available.
   */
  public LinxoDate getDebitDate()
  {
    return debitDate;
  }

  public void setDebitDate(LinxoDate debitDate)
  {
    this.debitDate = debitDate;
  }

  /**
   * Gets the reference. May be null.
   *
   * @return The reference. May be null.
   */
  public String getReference()
  {
    return reference;
  }

  public void setReference(String reference)
  {
    this.reference = reference;
  }

  /**
   * Gets the extCustomType. May be null.
   *
   * @return The extCustomType. May be null.
   */
  public String getExtCustomType()
  {
    return extCustomType;
  }

  public void setExtCustomType(String extCustomType)
  {
    this.extCustomType = extCustomType;
  }

  /**
   *
   * @return BRED category found by Linxo, corresponding to the Linxo category
   *         found by the Linxo categorization mechanism.
   *         May be null.
   */
  public String getExtBredCategory() {
    return extBredCategory;
  }

  public void setExtBredCategory(String extBredCategory) {
    this.extBredCategory = extBredCategory;
  }

  /**
   *
   * @return BRED SUBcategory found by Linxo, corresponding to the Linxo category
   *         found by the Linxo categorization mechanism.
   *         May be null.
   */
  public String getExtBredSubCategory() {
    return extBredSubCategory;
  }

  public void setExtBredSubCategory(String extBredSubCategory) {
    this.extBredSubCategory = extBredSubCategory;
  }

  /**
   *
   * @return BRED category found using the categorization rules.
   *         May be null.
   */
  public String getExtBredRuleCategory() {
    return extBredRuleCategory;
  }

  public void setExtBredRuleCategory(String extBredRuleCategory) {
    this.extBredRuleCategory = extBredRuleCategory;
  }

  /**
   *
   * @return BRED subcategory found using the categorization rules.
   *         May be null.
   */
  public String getExtBredRuleSubCategory() {
    return extBredRuleSubCategory;
  }

  public void setExtBredRuleSubCategory(String extBredRuleSubCategory) {
    this.extBredRuleSubCategory = extBredRuleSubCategory;
  }

  public Long getExtBredCategorizationRuleId() {
    return extBredCategorizationRuleId;
  }

  public void setExtBredCategorizationRuleId(Long extBredCategorizationRuleId) {
    this.extBredCategorizationRuleId = extBredCategorizationRuleId;
  }

  public Long getImportDate() {
    return importDate;
  }

  public void setImportDate(Long importDate) {
    this.importDate = importDate;
  }

  @Override
  public final String toString()
  {
    final StringBuilder sb = new StringBuilder();
    sb.append("TransactionInfo");
    sb.append("{date='").append(date).append('\'');
    sb.append(", budgetDate='").append(budgetDate).append('\'');
    sb.append(", label='").append(label).append('\'');
    sb.append(", categoryId='").append(categoryId).append('\'');
    sb.append(", amount=").append(amount);
    sb.append(", checkNumber=").append(checkNumber);
    sb.append(", duplicate=").append(duplicate);
    sb.append(", originalLabel='").append(originalLabel).append('\'');
    sb.append(", notes='").append(notes).append('\'');
    sb.append(", tagIds='").append(tagIds).append('\'');
    sb.append(", bankAccountId='").append(bankAccountId).append('\'');
    sb.append(", originalType='").append(originalType).append('\'');
    sb.append(", originalDateAvailable='").append(originalDateAvailable).append('\'');
    sb.append(", originalDateInitiated='").append(originalDateInitiated).append('\'');
    sb.append(", originalPostCode='").append(originalPostCode).append('\'');
    sb.append(", originalCity='").append(originalCity).append('\'');
    sb.append(", originalCountry='").append(originalCountry).append('\'');
    sb.append(", originalThirdParty='").append(originalThirdParty).append('\'');
    sb.append(", originalThirdPartyLabelTruncated='").append(originalThirdPartyLabelTruncated).append('\'');
    sb.append(", originalThirdPartyAccountNumber='").append(originalThirdPartyAccountNumber).append('\'');
    sb.append(", categorizedByUser='").append(categorizedByUser).append('\'');
    sb.append(", debitDate='").append(debitDate).append('\'');
    sb.append(", reference='").append(reference).append('\'');
    sb.append(", extCustomType='").append(extCustomType).append('\'');
    sb.append(", extBredCategory='").append(extBredCategory).append('\'');
    sb.append(", extBredSubCategory='").append(extBredSubCategory).append('\'');
    sb.append(", extBredRuleCategory='").append(extBredRuleCategory).append('\'');
    sb.append(", extBredRuleSubCategory='").append(extBredRuleSubCategory).append('\'');
    sb.append(", extBredCategorizationRuleId='").append(extBredCategorizationRuleId).append('\'');
    sb.append(", importDate='").append(importDate).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
