/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.tx;

import com.linxo.client.dto.EntityInfo;

import java.util.ArrayList;

/**
 * @deprecated use the {@link TransactionInfo} instead that will contain all the necessary data.
 */
@Deprecated
public final class DetailedTransactionInfo
  extends EntityInfo
{
  private String originalLabel;
  private String accountName;
  private String notes;
  private ArrayList<Long> tagIDs;

  @SuppressWarnings("unused")
  private DetailedTransactionInfo() {}

  public DetailedTransactionInfo(final long transactionId, final String originalLabel,
                                 final String accountName, final String notes,
                                 final ArrayList<Long> tagIDs)
  {
    setId(transactionId);
    this.originalLabel = originalLabel;
    this.accountName = accountName;
    this.notes = notes;
    this.tagIDs = tagIDs;
  }

  public String getOriginalLabel()
  {
    return originalLabel;
  }

  public String getAccountName()
  {
    return accountName;
  }

  public String getNotes()
  {
    return notes;
  }

  public ArrayList<Long> getTagIDs()
  {
    return tagIDs;
  }


  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("DetailedTransactionInfo");
    sb.append("{accountName='").append(accountName).append('\'');
    sb.append(", originalLabel='").append(originalLabel).append('\'');
    sb.append(", notes='").append(notes).append('\'');
    sb.append(", tagIDs=").append(tagIDs);
    sb.append('}');
    return sb.toString();
  }
}
