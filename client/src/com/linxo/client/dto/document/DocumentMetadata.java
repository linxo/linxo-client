/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.document;

import java.io.Serializable;

/**
 * Represents a key/value pair of document metadata
 *
 */
public class DocumentMetadata implements Serializable {

    private static final long serialVersionUID = -4933475572875708093L;

    private String key;

    private String value;

    public DocumentMetadata() {}

    public DocumentMetadata(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "DocumentMetadata {key=" + key + ", value=" + value + "}";
    }



}
