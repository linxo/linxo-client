/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.linxo.client.dto.EntityInfo;


/**
 * Document stored in DocumentStore.
 */
public class Document extends EntityInfo {

    private static final long serialVersionUID = -6515706734569975196L;

    /**
     * The name displayed for the document
     */
    private String name;

    /**
     * The type of document (bill, paycheck, bank account summary...)
     */
    private String type;

    /**
     * The date displayed in the list of documents
     */
    private Date displayedDate;

    /**
     * The amount displayed in the list of documents
     */
    private Long displayedAmount;

    /**
     * An ordered list of metadatas
     */
    private List<DocumentMetadata> metadatas = new ArrayList<>();

    public Document(long id) {
        setId(id);
    }

    public Document(long id, String name, String type, Date displayedDate, Long displayedAmount,
            List<DocumentMetadata> metadatas) {
        setId(id);
        this.name = name;
        this.type = type;
        this.displayedDate = displayedDate;
        this.displayedAmount = displayedAmount;
        this.metadatas = metadatas;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDisplayedDate() {
        return displayedDate;
    }

    public void setDisplayedDate(Date displayedDate) {
        this.displayedDate = displayedDate;
    }

    public Long getDisplayedAmount() {
        return displayedAmount;
    }

    public void setDisplayedAmount(Long displayedAmount) {
        this.displayedAmount = displayedAmount;
    }

    public List<DocumentMetadata> getMetadatas() {
        return metadatas;
    }

    public void setMetadatas(List<DocumentMetadata> metadatas) {
        this.metadatas.clear();
        this.metadatas.addAll(metadatas);
    }

    @Override
    public String toString() {
        return "Document {name=" + name + ", type=" + type + ", displayedDate=" + displayedDate + ", displayedAmount="
                + displayedAmount + ", metadatas=" + metadatas + "}";
    }
}
