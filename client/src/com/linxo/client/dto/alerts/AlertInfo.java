/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.dto.alerts;

import com.linxo.client.data.StringUtils;
import com.linxo.client.data.pfm.alert.AlertType;
import com.linxo.client.data.support.Version;
import com.linxo.client.dto.EntityInfo;

import java.util.Date;


/**
 * Description of an Alert Message DTO.
 */
public final class AlertInfo
    extends EntityInfo
    implements Comparable<AlertInfo>
{

  public static final String TRANSACTION_URL_FORMAT = "{0}transaction?id={1}";
  public static final String ACCOUNT_URL_FORMAT     = "{0}transactions?accountId={1}";
  public static final String GROUP_URL_FORMAT       = "{0}settings/accounts?groupId={1}";

  public static String getItemUrl(String mobileScheme, AlertType type, long itemId) {

    switch(type) {
      // Transactions
      case BankFee:
      case HighDeposit:
      case HighSpending:
        return StringUtils.format(TRANSACTION_URL_FORMAT, mobileScheme, itemId);

      // account
      case LowBalance:
        return StringUtils.format(ACCOUNT_URL_FORMAT, mobileScheme, itemId);

      // group
      case PasswordChangeRequired:
      case TooManyAttempts:
      case UserActionRequired:
        return StringUtils.format(GROUP_URL_FORMAT, mobileScheme, itemId);

      default:
        return null;
    }
  }



  @SuppressWarnings("unused") // s11n
  private AlertInfo(){}
    
  public AlertInfo( long id,
                    long itemId,
                    String htmlMessage,
                    Date postedDate,
                    AlertType type,
                    String label,
                    String accountName,
                    double amount,
                    Date itemDate,
                    String mobileScheme)
  {
    setId( id );
    this.htmlMessage = htmlMessage;
    this.postedDate = postedDate;
    this.type = type;
    this.label = label;
    this.accountName = accountName;
    this.amount = amount;
    this.itemDate = itemDate;
    this.itemUrl = AlertInfo.getItemUrl(mobileScheme, type, itemId);
  }

  private Date postedDate;
  private Date itemDate;
  private String itemUrl;
  private AlertType type;
  private String htmlMessage;
  private String label;
  private String accountName;
  private double amount;
  private Long categoryId;


  /**
   * @return The date the alert was posted (i.e. generated)
   */
  public Date getPostedDate() {
    return postedDate;
  }

  /**
   * @return the date of the item the alert was generated for happened. For instance, if the
   * alert is about a Transaction, this will be the transaction's posted date.
   */
  public Date getItemDate()
  {
    return itemDate;
  }

  public AlertType getType() {
    return type;
  }

  public String getHtmlMessage() {
    return htmlMessage;
  }

  public String getLabel()
  {
    return label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public String getAccountName()
  {
    return accountName;
  }

  public double getAmount()
  {
    return amount;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }

  public void setCategoryId(Long categoryId)
  {
    this.categoryId = categoryId;
  }

  @SuppressWarnings("unused") // s11n only
  public String getItemUrl()
  {
    return itemUrl;
  }

  public void prepareFor(final Version version)
  {
    if (version.hasAlertInfoSchemeFix()) {
      return;
    }
    // Old android clients expect linxo:// itemUrls.
    // On old ios clients, clicking a notification to access
    // the transaction never worked we keep it like this.
    itemUrl = itemUrl.replace("fortuneo-budget://", "linxo://");
  }

  //~~~~~   Implements Comparable

  /**
   *
   * @param o the AlertInfo to be compared.
   * @return a negative integer, zero, or a positive integer as this postedDate
   *         is less than, equal to, or greater than the specified AlertInfo.
   * @throws ClassCastException if the specified object's type prevents it
   *                            from being compared to this Object.
   * @see Comparable#compareTo(Object)
   * @see Date#compareTo(java.util.Date)
   */
  @Override
  public int compareTo( AlertInfo o )
  {
    int dateComp = postedDate.compareTo( o.postedDate );

    if ( dateComp != 0 ) return dateComp;

    return getId().compareTo( o.getId() );
  }



  // ~~~~ toString

  @Override
  public String toString()
  {
    return "AlertInfo{"
           + "htmlMessage=[" + htmlMessage + ']'
           + ", label=[" + label + "]"
           + ", itemUrl=[" + itemUrl + "]"
           + ", postedDate=" + postedDate
           + ", type=" + type
           + ", amount=" + amount
           + ", itemDate=" + itemDate
           + '}';
  }

}
