/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.account;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.linxo.client.data.pfm.group.SynchroStatus;
import com.linxo.client.dto.EntityInfo;
import com.linxo.client.actions.pfm.events.SynchronizationUpdateEvent;

/**
 * This class represents a group of BankAccount and their connection to the FI.
 */
public final class AccountGroupInfo extends EntityInfo implements Comparable<AccountGroupInfo> {
    private String name;
    private double balance;
    private SynchroStatus status;
    private String url;
    private String logoUrl;
    private Date lastSuccessfulUpdate;
    private Date lastUpdate;
    private Date lastStart;
    private Date lastSuccess;
    private Date lastEnd;
    private Boolean semiAuto;
    private String unavailableMessage;
    private Boolean mainGroup;
    private Set<SynchronizationCapability> capabilities;

    /**
     * map : accountId =&gt; BankAccountInfo
     */
    private Map<Long, BankAccountInfo> bankAccounts = new HashMap<Long, BankAccountInfo>();


    private Long fid;
    private SynchronizationUpdateEvent lastEvent;

    @SuppressWarnings("unused")
    public AccountGroupInfo() {}

    public AccountGroupInfo(final long id) {
        setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(final double balance) {
        this.balance = balance;
    }

    public SynchroStatus getStatus() {
        return status;
    }

    public void setStatus(final SynchroStatus status) {
        this.status = status;
    }

    public SynchronizationUpdateEvent getLastEvent() {
        return lastEvent;
    }

    public void setLastEvent(SynchronizationUpdateEvent lastEvent) {
        this.lastEvent = lastEvent;
    }

    public Long getFid() {
        return fid;
    }

    public void setFid(final Long fid) {
        this.fid = fid;
    }

    public Date getLastSuccessfulUpdate() {
        return lastSuccessfulUpdate;
    }

    public void setLastSuccessfulUpdate(Date lastSuccessfulUpdate) {
        this.lastSuccessfulUpdate = lastSuccessfulUpdate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean isSemiAuto() {
        return semiAuto;
    }

    public void setSemiAuto(final Boolean semiAuto) {
        this.semiAuto = semiAuto;
    }

    public Map<Long, BankAccountInfo> getBankAccounts() {
        return bankAccounts;
    }

    public void setUnavailableMessage(String unavailableMessage) {
        this.unavailableMessage = unavailableMessage;
    }

    public String getUnavailableMessage() {
        return unavailableMessage;
    }

    public Date getLastStart() {
        return lastStart;
    }

    public void setLastStart(Date lastStart) {
        this.lastStart = lastStart;
    }

    public Date getLastSuccess() {
        return lastSuccess;
    }

    public void setLastSuccess(Date lastSuccess) {
        this.lastSuccess = lastSuccess;
    }

    public Date getLastEnd() {
        return lastEnd;
    }

    public void setLastEnd(Date lastEnd) {
        this.lastEnd = lastEnd;
    }

    public Boolean getMainGroup() {
        return mainGroup;
    }

    public void setMainGroup(Boolean mainGroup) {
        this.mainGroup = mainGroup;
    }

    public Set<SynchronizationCapability> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(Set<SynchronizationCapability> capabilities) {
        this.capabilities = capabilities;
    }

    @Override
    public int compareTo(final AccountGroupInfo o) {
        return name.compareToIgnoreCase(o.name);
    }

    @Override
    @SuppressWarnings({"RedundantIfStatement"})
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccountGroupInfo)) {
            return false;
        }

        final AccountGroupInfo that = (AccountGroupInfo) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (fid != null ? !fid.equals(that.fid) : that.fid != null) {
            return false;
        }
        if (status != that.status) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = (getId() != null ? getId().hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (fid != null ? fid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AccountGroupInfo{" + "id=" + getId() + ", name=" + name + ", balance=" + balance + ", status=" + status
                + ", fid=" + fid + ", url=" + url + ", logoUrl=" + logoUrl + ", lastSuccessfulUpdate="
                + lastSuccessfulUpdate + ", lastUpdate=" + lastUpdate + ", lastSuccess=" + lastSuccess + ", lastStart="
                + lastStart + ", lastEnd=" + lastEnd + ", semiAuto=" + semiAuto + ", unavailableMessage="
                + unavailableMessage + ", mainGroup=" + mainGroup + '}';
    }

}
