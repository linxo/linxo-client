/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.dto.account;

import com.linxo.client.data.LinxoColor;
import com.linxo.client.dto.EntityInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO for a personalized view.
 */
public class PersonalizedView
    extends EntityInfo
    implements Comparable<PersonalizedView>
{
    /**
     * The name of this personalized view.
     */
    private String name;

    /**
     * The color of this personalized view.
     */
    private LinxoColor color;

    /**
     * The ids of the accounts currently associated to this view.
     */
    private List<Long> accountIds;


    public PersonalizedView() {}

    public PersonalizedView(Long id)
    {
        setId(id);
    }

    public PersonalizedView(PersonalizedView view)
    {
        setId(view.getId());
        this.name = view.getName();
        this.color = new LinxoColor(view.getColor().getCode(), view.getColor().getName());
        this.accountIds = new ArrayList<Long>(view.getAccountIds());
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public LinxoColor getColor()
    {
        return color;
    }

    public void setColor(LinxoColor color)
    {
        this.color = color;
    }

    public List<Long> getAccountIds()
    {
        return accountIds;
    }

    public void setAccountIds(List<Long> accountIds)
    {
        this.accountIds = accountIds;
    }

    @Override
    public int compareTo(final PersonalizedView other)
    {
        return name.compareToIgnoreCase(other.name);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonalizedView that = (PersonalizedView) o;

        //noinspection SimplifiableIfStatement
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return color != null ? color.equals(that.color) : that.color == null;
    }

    @Override
    public int hashCode()
    {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "PersonalizedView{"
               + super.toString()
               + ", name='" + name + '\''
               + ", color=" + color
               + ", accountIds=" + accountIds
               + '}';
    }

}
