/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.account;

import com.linxo.client.dto.EntityInfo;

/**
 *
 */
public final class OperationUpdateInfo
  extends EntityInfo
{
  public enum Type
  {
    // If you change the order here, you MUST
    // also change it in OperationUpdate.Type
    Started, // nothing
    LoggingIn, // nothing
    CredentialsValidated, // variable list of credential keys
    LoggedIn, // nothing
    ReadingAccountList, // nothing
    ReadingAccount, // {0} accountName, {1} currentPage (starts at 1), {2} count of transactions read until now (starts at 0)
    FinishedAccount, // {0} accountName, {1} count of read pages (starts at 0) {2} count of transactions read until now (starts at 0)
    AccountFailure, // {0} accountName
    AccountNotFound, // {0} accountName
    TransactionsRead, // {0} accountName, {1} count of read pages (starts at 0) {2} count of transactions read until now
    LoggingOut, // nothing

    /**
     * The job requires to enter a challenge response. Values would follow this format:
     * <ol>
     * <li>credentialKey1,
     * <li>credentialKey1.arg.arg1_name=arg1_value
     * <li>credentialKey1.arg.arg2_name=arg2_value
     * <li>credentialKey2,
     * <li>credentialKey2.arg.arg1_name=arg1_value
     * </ol>
     * where credentialKeyX are the requested credentials
     */
    ChallengeRequested,
    NeedsSecret, // the user needs to provide the secret to synchronize the group
  }

  private Type type;

  private String[] values;

  @SuppressWarnings("unused") // by s11n
  public OperationUpdateInfo() {}

  public OperationUpdateInfo(Type type, final String... values)
  {
    this.type = type;
    this.values = values;
  }

  public Type getType()
  {
    return type;
  }

  public String[] getValues()
  {
    return values;
  }

  @Override
  public String toString()
  {
    return new StringBuilder()
        .append("OperationUpdateInfo {")
        .append(" type=").append(type)
        .append(", values=").append(values == null ? "null" : values.length)
        .append(" }")
        .toString();
  }

}
