/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2016 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 20/10/2016 by hugues.
 */
package com.linxo.client.dto.account.builder;

import com.linxo.client.data.exception.LinxoRuntimeException;
import com.linxo.client.data.pfm.bank.AccountSynchroStatus;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.data.pfm.group.SynchroStatus;
import com.linxo.client.data.upcoming.IntervalUnit;
import com.linxo.client.dto.DTOBuilderException;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.dto.account.CheckingsAccountInfo;
import com.linxo.client.dto.account.CreditCardAccountInfo;
import com.linxo.client.dto.account.LoanAccountInfo;
import com.linxo.client.dto.account.SavingsAccountInfo;
import com.linxo.client.actions.pfm.events.SynchronizationUpdateEvent;

import java.util.Date;

/**
 * This class is a builder for any kind of {@code *AccountInfo} object.
 */
public class BankAccountInfoBuilder {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  public BankAccountInfoBuilder() {}


  // Instance Methods

  //////////////////////////////////////////////////////////////////////////////
  //
  //                        BANK ACCOUNT FIELDS
  //
  //////////////////////////////////////////////////////////////////////////////

  private Long id;
  public BankAccountInfoBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  private String accountNumber;
  public BankAccountInfoBuilder withAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
    return this;
  }


  private String reference;
  public BankAccountInfoBuilder withReference(String reference) {
    this.reference = reference;
    return this;
  }

  private String name;
  public BankAccountInfoBuilder named(String name) {
    this.name = name;
    return this;
  }

  private double currentBalance;
  public BankAccountInfoBuilder withCurrentBalance(double currentBalance) {
    this.currentBalance = currentBalance;
    return this;
  }

  private String accountGroupName;
  public BankAccountInfoBuilder inGroupNamed(String accountGroupName) {
    this.accountGroupName = accountGroupName;
    return this;
  }


  private Long accountGroupId;
  public BankAccountInfoBuilder inGroupWithId(Long accountGroupId) {
    this.accountGroupId = accountGroupId;
    return this;
  }


  private AccountType type;

  public BankAccountInfoBuilder createCheckings() throws DTOBuilderException {
    if(type != null) {
      throw new DTOBuilderException("Type already decided " + type);
    }
    type = AccountType.Checkings;
    return this;
  }

  public BankAccountInfoBuilder createCreditCard() {
    if(type != null) {
      throw new DTOBuilderException("Type already decided " + type);
    }
    type = AccountType.CreditCard;
    return this;
  }

  public BankAccountInfoBuilder createSavings() {
    if(type != null) {
      throw new DTOBuilderException("Type already decided " + type);
    }
    type = AccountType.Savings;
    return this;
  }

  public BankAccountInfoBuilder createLoan() {
    if(type != null) {
      throw new DTOBuilderException("Type already decided " + type);
    }
    type = AccountType.Loan;
    return this;
  }


  private Boolean includedInTrends;
  public BankAccountInfoBuilder includedInTrends(Boolean includedInTrends) {
    this.includedInTrends = includedInTrends;
    return this;
  }

  private Boolean includedInSavings;
  public BankAccountInfoBuilder includedInSavings(Boolean includedInSavings) {
    this.includedInSavings = includedInSavings;
    return this;
  }

  private Boolean includedInBudget;
  public BankAccountInfoBuilder includedInBudget(Boolean includedInBudget) {
    this.includedInBudget = includedInBudget;
    return this;
  }

  private Boolean includedInAssets;
  public BankAccountInfoBuilder includedInAssets(Boolean includedInAssets) {
    this.includedInAssets = includedInAssets;
    return this;
  }

  private Long viewId;
  public BankAccountInfoBuilder inViewWithId(Long viewId) {
    this.viewId = viewId;
    return this;
  }


  private Date creationDate;
  public BankAccountInfoBuilder createdOn(Date creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  private Date currentBalanceDate;
  public BankAccountInfoBuilder withCurrentBalanceDate(Date currentBalanceDate) {
    this.currentBalanceDate = currentBalanceDate;
    return this;
  }

  private Date closeDate;
  public BankAccountInfoBuilder closedOn(Date closeDate) {
    this.closeDate = closeDate;
    return this;
  }

  private AccountSynchroStatus status;
  public BankAccountInfoBuilder withStatus(AccountSynchroStatus status) {
    this.status = status;
    return this;
  }

  private SynchroStatus accountGroupStatus;
  public BankAccountInfoBuilder inGroupWithStatus(SynchroStatus accountGroupStatus) {
    this.accountGroupStatus = accountGroupStatus;
    return this;
  }

  private SynchronizationUpdateEvent lastEvent;
  public BankAccountInfoBuilder withLastEvent(SynchronizationUpdateEvent lastEvent) {
    this.lastEvent = lastEvent;
    return this;
  }

  private String specializedType;
  public BankAccountInfoBuilder withSpecializedType(String specializedType) {
    this.specializedType = specializedType;
    return this;
  }


  private String specializedTypeFamily;
  public BankAccountInfoBuilder withSpecializedTypeFamily(String specializedTypeFamily) {
    this.specializedTypeFamily = specializedTypeFamily;
    return this;
  }

  //////////////////////////////////////////////////////////////////////////////
  //
  //                        CHECKING ACCOUNT SECTION
  //
  //////////////////////////////////////////////////////////////////////////////


  private Boolean includedInForecast;
  public BankAccountInfoBuilder includedInForecast(Boolean includedInForecast) {
    if(type != AccountType.Checkings)
      throw new DTOBuilderException("Only CheckingsAccountInfo can be set includedInForecast");

    this.includedInForecast = includedInForecast;
    return this;
  }

  //////////////////////////////////////////////////////////////////////////////
  //
  //                        SAVINGS ACCOUNT SECTION
  //
  //////////////////////////////////////////////////////////////////////////////

  private Date effectDate;
  public BankAccountInfoBuilder withEffectDate(Date effectDate) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have an effectDate");

    this.effectDate = effectDate;
    return this;
  }

  private Date endDate; // also in LoanAccountInfo
  public BankAccountInfoBuilder withEndDate(Date endDate) {
    if(type != AccountType.Savings && type != AccountType.Loan)
      throw new DTOBuilderException("Only Savings or Loan accounts can have an effectDate");

    this.endDate = endDate;
    return this;
  }

  private Double yieldRate;
  public BankAccountInfoBuilder withYieldRate(Double yieldRate) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have an effectDate");

    this.yieldRate = yieldRate;
    return this;
  }

  private Double earnedInterestTotal;
  public BankAccountInfoBuilder withEarnedInterestTotal(Double earnedInterest) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have an earnedInterestTotal");

    this.earnedInterestTotal = earnedInterest;
    return this;
  }

  private Double earnedInterestYtD;
  public BankAccountInfoBuilder withEarnedInterestYtD(Double earnedInterestYtD) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have an earnedInterestYtD");

    this.earnedInterestYtD = earnedInterestYtD;
    return this;
  }

  private Double capitalGainAmount;
  public BankAccountInfoBuilder withCapitalGainAmount(Double capitalGainAmount) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have a capitalGainAmount");

    this.capitalGainAmount = capitalGainAmount;
    return this;
  }

  private Double capitalGainPercent;
  public BankAccountInfoBuilder withCapitalGainPercent(Double capitalGainPercent) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have a capitalGainPercent");

    this.capitalGainPercent = capitalGainPercent;
    return this;
  }

  private Double rawInvested;
  public BankAccountInfoBuilder withRawInvested(Double rawInvested) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have an effectDate");

    this.rawInvested = rawInvested;
    return this;
  }

  private Double netInvested;
  public BankAccountInfoBuilder withNetInvested(Double netInvested) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have an effectDate");

    this.netInvested = netInvested;
    return this;
  }

  private Double rawDisinvested;
  public BankAccountInfoBuilder withRawDisinvested(Double rawDisinvested) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have an effectDate");

    this.rawDisinvested = rawDisinvested;
    return this;
  }

  private Long pivotAccountId;
  public BankAccountInfoBuilder withPivotAccountId(Long pivotAccountId) {
    if(type != AccountType.Savings)
      throw new DTOBuilderException("Only Savings can have a pivotAccountId");

    this.pivotAccountId = pivotAccountId;
    return this;
  }

  //////////////////////////////////////////////////////////////////////////////
  //
  //                        CREDIT CARD SECTION
  //
  //////////////////////////////////////////////////////////////////////////////
  private Date nextPaymentDate;
  public BankAccountInfoBuilder withNextPaymentDate(Date nextPaymentDate) {
    if(type != AccountType.CreditCard)
      throw new DTOBuilderException("Only CreditCard can have a nextPaymentDate");

    this.nextPaymentDate = nextPaymentDate;
    return this;
  }

  private Double nextPaymentAmount;
  public BankAccountInfoBuilder withNextPaymentAmount(Double nextPaymentAmount) {
    if(type != AccountType.CreditCard)
      throw new DTOBuilderException("Only CreditCard can have a nextPaymentAmount");

    this.nextPaymentAmount = nextPaymentAmount;
    return this;
  }

  private Long payableAccountId;
  public BankAccountInfoBuilder withPayableAccountId(Long payableAccountId) {
    if(type != AccountType.CreditCard)
      throw new DTOBuilderException("Only CreditCard can have a payableAccountId");

    this.payableAccountId = payableAccountId;
    return this;
  }

  //////////////////////////////////////////////////////////////////////////////
  //
  //                        LOAN SECTION
  //
  //////////////////////////////////////////////////////////////////////////////

  private Date startDate;
  public BankAccountInfoBuilder withStartDate(Date startDate) {
    if(type != AccountType.Loan)
      throw new DTOBuilderException("Only Loan can have a startDate");

    this.startDate = startDate;
    return this;
  }

//  private Date endDate;

  private Double initialLoanedAmount;
  public BankAccountInfoBuilder withInitialLoanedAmount(Double initialLoanedAmount) {
    if(type != AccountType.Loan)
      throw new DTOBuilderException("Only Loan can have a initialLoanedAmount");

    this.initialLoanedAmount = initialLoanedAmount;
    return this;
  }

  private IntervalUnit debitPeriodUnit;
  public BankAccountInfoBuilder withDebitPeriodUnit(IntervalUnit debitPeriod) {
    if(type != AccountType.Loan)
      throw new DTOBuilderException("Only Loan can have a debitPeriodUnit");

    this.debitPeriodUnit = debitPeriod;
    return this;
  }

  private int debitPeriodValue;
  public BankAccountInfoBuilder withDebitPeriodValue(int debitPeriodValue) {
    if(type != AccountType.Loan)
      throw new DTOBuilderException("Only Loan can have a debitPeriodValue");

    this.debitPeriodValue = debitPeriodValue;
    return this;
  }

  private Double nextDebitAmount;
  public BankAccountInfoBuilder withNextDebitAmount(Double nextDebitAmount) {
    if(type != AccountType.Loan)
      throw new DTOBuilderException("Only Loan can have a nextDebitAmount");

    this.nextDebitAmount = nextDebitAmount;
    return this;
  }

  private Date nextDebitDate;
  public BankAccountInfoBuilder withNextDebitDate(Date nextDebitDate) {
    if(type != AccountType.Loan)
      throw new DTOBuilderException("Only Loan can have a nextDebitDate");

    this.nextDebitDate = nextDebitDate;
    return this;
  }

  private Long debitedAccountId;
  public BankAccountInfoBuilder withdDebitedAccountId(Long debitedAccountId) {
    if(type != AccountType.Loan)
      throw new DTOBuilderException("Only Loan can have a debitedAccountId");

    this.debitedAccountId = debitedAccountId;
    return this;
  }

  private Double currentInterestRate;
  public BankAccountInfoBuilder withCurrentInterestRate(Double currentInterestRate) {
    if(type != AccountType.Loan)
      throw new DTOBuilderException("Only Loan can have a currentInterestRate");

    this.currentInterestRate = currentInterestRate;
    return this;
  }

  //////////////////////////////////////////////////////////////////////////////
  //
  //                        BUILDER SECTION
  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Updates the given BankAccountInfo object using the provided parameters.
   *
   * <pre>
   *   CheckingsAccountInfo local = ....
   *   CheckingsAccountInfo newValues = ...
   *
   *   local = (CheckingsAccountInfo)new BankAccountBuilder()
   *     .createCheckings()
   *     .withId(newValues.getId())
   *     .named(newValues.getName())
   *     ...
   *     .update(local);
   * </pre>
   *
   *
   * Note : If parameters are not given to the builder, they will update
   * the given BankAccountInfo with {@code null} values.
   *
   * @return the given BankAccountInfo updated with the given parameters.
   */
  public BankAccountInfo update(BankAccountInfo toUpdate) {
    if (type == AccountType.Checkings) {
      return buildChecking((CheckingsAccountInfo)toUpdate);
    }
    else if (type == AccountType.CreditCard) {
      return buildCreditCard((CreditCardAccountInfo)toUpdate);
    }
    else if (type == AccountType.Savings) {
      return buildSavings((SavingsAccountInfo)toUpdate);
    }
    else if (type == AccountType.Loan) {
      return buildLoan((LoanAccountInfo)toUpdate);
    }
    else {
      throw new DTOBuilderException("Cannot build account of unknown type");
    }
  }

  /**
   * Creates a new BankAccountInfo object using the provided parameters.
   * <pre>
   *   CheckingsAccountInfo local = ....
   *   SomeView view = ...
   *
   *   local = (CheckingsAccountInfo)new BankAccountBuilder()
   *     .createCheckings()
   *     .withId(local.getId())
   *     .named(local.getName())
   *     .inViewWithId(view.getId())
   *     ...
   *     .build();
   * </pre>
   *
   *
   * Note: If there is not {@code type} defined, the result will be a bare
   * {@link BankAccountInfo}.
   * @return the new BankAccountInfo
   * @throws LinxoRuntimeException if the {@link #withId(Long) id is not set}.
   */
  public BankAccountInfo build() {
    if(this.id == null) {
      throw new LinxoRuntimeException("Cannot build a BankAccountInfo with id null");
    }

    if (type == AccountType.Checkings) {
      return buildChecking(new CheckingsAccountInfo(this.id));
    }
    else if (type == AccountType.CreditCard) {
      return buildCreditCard(new CreditCardAccountInfo(this.id));
    }
    else if (type == AccountType.Savings) {
      return buildSavings(new SavingsAccountInfo(this.id));
    }
    else if(type == AccountType.Loan) {
      return buildLoan(new LoanAccountInfo(this.id));
    }
    else {
      return fillCommon(new BankAccountInfo(this.id));
    }
  }

  private <T extends BankAccountInfo> T fillCommon(T account) {

    account.setAccountNumber(accountNumber);
    account.setReference(reference);
    account.setName(name);
    account.setSpecializedType(specializedType);
    account.setSpecializedTypeFamily(specializedTypeFamily);
    account.setCurrentBalance(currentBalance);
    account.setAccountGroupName(accountGroupName);
    account.setAccountGroupId(accountGroupId);
    account.setIncludedInTrends(includedInTrends);
    account.setIncludedInSavings(includedInSavings);
    account.setIncludedInBudget(includedInBudget);
    account.setIncludedInAssets(includedInAssets);
    account.setViewId(viewId);
    account.setCreationDate(creationDate);
    account.setCurrentBalanceDate(currentBalanceDate);
    account.setCloseDate(closeDate);
    account.setStatus(status);
    account.setAccountGroupStatus(accountGroupStatus);
    account.setLastEvent(lastEvent);

    return account;
  }

  private CheckingsAccountInfo buildChecking(CheckingsAccountInfo that) {
    CheckingsAccountInfo result = fillCommon(that);

    result.setIncludedInForecast(includedInForecast);

    return result;
  }

  private CreditCardAccountInfo buildCreditCard(CreditCardAccountInfo that) {
    CreditCardAccountInfo result = fillCommon(that);

    result.setNextPaymentAmount(nextPaymentAmount);
    result.setNextPaymentDate(nextPaymentDate);
    result.setPayableAccountId(payableAccountId);

    return result;
  }

  private SavingsAccountInfo buildSavings(SavingsAccountInfo that) {
    SavingsAccountInfo result = fillCommon(that);

    result.setEffectDate(effectDate);
    result.setEndDate(endDate);
    result.setYieldRate(yieldRate);
    result.setEarnedInterestTotal(earnedInterestTotal);
    result.setEarnedInterestYtD(earnedInterestYtD);
    result.setRawInvested(rawInvested);
    result.setNetInvested(netInvested);
    result.setRawDisinvested(rawDisinvested);
    result.setPivotAccountId(pivotAccountId);
    result.setCapitalGainAmount(capitalGainAmount);
    result.setCapitalGainPercent(capitalGainPercent);

    return result;
  }

  private LoanAccountInfo buildLoan(LoanAccountInfo that) {
    LoanAccountInfo result = fillCommon(that);

    result.setStartDate(startDate);
    result.setEndDate(endDate);
    result.setInitialLoanedAmount(initialLoanedAmount);
    result.setDebitPeriodUnit(debitPeriodUnit);
    result.setDebitPeriodValue(debitPeriodValue);
    result.setNextDebitAmount(nextDebitAmount);
    result.setNextDebitDate(nextDebitDate);
    result.setDebitedAccountId(debitedAccountId);
    result.setCurrentInterestRate(currentInterestRate);

    return result;
  }
}
