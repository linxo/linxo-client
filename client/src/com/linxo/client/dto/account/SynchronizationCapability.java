/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.account;

public enum SynchronizationCapability {

    BankAccounts,

    Documents;
}
