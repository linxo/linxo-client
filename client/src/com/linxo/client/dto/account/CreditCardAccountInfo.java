/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 5:35:56 PM by tarunmalhotra.
*/
package com.linxo.client.dto.account;

import com.linxo.client.data.pfm.bank.AccountType;

import java.util.Date;

public class CreditCardAccountInfo extends BankAccountInfo {

  private Date nextPaymentDate;

  private Double nextPaymentAmount;

  private Long payableAccountId;

  public CreditCardAccountInfo()
  {
    setType(AccountType.CreditCard);
  }

  public CreditCardAccountInfo( long id )
  {
    //noinspection deprecation
    super( id );
    setType(AccountType.CreditCard);
  }

  /**
   * @return the amount of the next payment
   */
  public Double getNextPaymentAmount()
  {
    return nextPaymentAmount;
  }

  public void setNextPaymentAmount(Double nextPaymentAmount)
  {
    this.nextPaymentAmount = nextPaymentAmount;
  }

  /**
   * @return the Date when the next payment will take place
   */
  public Date getNextPaymentDate()
  {
    return nextPaymentDate;
  }

  public void setNextPaymentDate(Date nextPaymentDate)
  {
    this.nextPaymentDate = nextPaymentDate;
  }

  /**
   * @return the id of the bankAccount (likely a {@link CheckingsAccountInfo})
   * where the next payment of this card will be paid.
   */
  public Long getPayableAccountId()
  {
    return payableAccountId;
  }

  public void setPayableAccountId(Long payableAccountId)
  {
    this.payableAccountId = payableAccountId;
  }

  @Override
  public String toString()
  {
    final StringBuilder sb = new StringBuilder("CreditCardAccountInfo{");
    sb.append(super.toString());
    sb.append(", nextPaymentAmount=").append(nextPaymentAmount);
    sb.append(", nextPaymentDate=").append(nextPaymentDate);
    sb.append(", payableAccountId=").append(payableAccountId);
    sb.append('}');
    return sb.toString();
  }
}
