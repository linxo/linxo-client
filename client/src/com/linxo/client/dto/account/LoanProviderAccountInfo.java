/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2017 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 26/02/2017 by hugues.
 */
package com.linxo.client.dto.account;

import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.data.upcoming.IntervalUnit;

import java.util.Date;

public class LoanProviderAccountInfo extends ProviderAccountInfo {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private Date startDate;
  private Date endDate;
  private Double currentInterestRate;
  private Double initialLoanedAmount;
  private IntervalUnit debitPeriodUnit;
  private int debitPeriodValue;
  private Double nextDebitAmount;
  private Date nextDebitDate;
  private String debitedAccountUid;

  // Instance Initializers

  // Constructors

  public LoanProviderAccountInfo() {
    setType(AccountType.Loan);
  }

  // Instance Methods


  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Double getCurrentInterestRate() {
    return currentInterestRate;
  }

  public void setCurrentInterestRate(Double currentInterestRate) {
    this.currentInterestRate = currentInterestRate;
  }

  public Double getInitialLoanedAmount() {
    return initialLoanedAmount;
  }

  public void setInitialLoanedAmount(Double initialLoanedAmount) {
    this.initialLoanedAmount = initialLoanedAmount;
  }

  public IntervalUnit getDebitPeriodUnit() {
    return debitPeriodUnit;
  }

  public void setDebitPeriodUnit(IntervalUnit debitPeriodUnit) {
    this.debitPeriodUnit = debitPeriodUnit;
  }

  public int getDebitPeriodValue() {
    return debitPeriodValue;
  }

  public void setDebitPeriodValue(int debitPeriodValue) {
    this.debitPeriodValue = debitPeriodValue;
  }

  public Double getNextDebitAmount() {
    return nextDebitAmount;
  }

  public void setNextDebitAmount(Double nextDebitAmount) {
    this.nextDebitAmount = nextDebitAmount;
  }

  public Date getNextDebitDate() {
    return nextDebitDate;
  }

  public void setNextDebitDate(Date nextDebitDate) {
    this.nextDebitDate = nextDebitDate;
  }

  public String getDebitedAccountUid() {
    return debitedAccountUid;
  }

  public void setDebitedAccountUid(String debitedAccountUid) {
    this.debitedAccountUid = debitedAccountUid;
  }
}
