/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 5:35:56 PM by tarunmalhotra.
*/
package com.linxo.client.dto.account;

import com.linxo.client.data.pfm.bank.AccountType;

import java.util.List;

public class CheckingsProviderAccountInfo extends ProviderAccountInfo
{
  private List<String> savingsAccountUids;
  private List<String> creditCardAccountUids;
  public CheckingsProviderAccountInfo(){
    setType(AccountType.Checkings);
  }

  public List<String> getCreditCardAccountUids()
  {
    return creditCardAccountUids;
  }

  public void setCreditCardAccountUids(List<String> creditCardAccountUids)
  {
    this.creditCardAccountUids = creditCardAccountUids;
  }

  public List<String> getSavingsAccountUids()
  {
    return savingsAccountUids;
  }

  public void setSavingsAccountUids(List<String> savingsAccountUids)
  {
    this.savingsAccountUids = savingsAccountUids;
  }
}
