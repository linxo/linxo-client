/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 5:35:56 PM by tarunmalhotra.
*/
package com.linxo.client.dto.account;

import com.linxo.client.data.pfm.bank.AccountType;

import java.util.Date;

public class CreditCardProviderAccountInfo extends ProviderAccountInfo
{
  private Date nextPaymentDate;

  private Double nextPaymentAmount;

  private String payableAccountUId;

  public CreditCardProviderAccountInfo(){
    setType(AccountType.CreditCard);
  }

  public Double getNextPaymentAmount()
  {
    return nextPaymentAmount;
  }

  public void setNextPaymentAmount(Double nextPaymentAmount)
  {
    this.nextPaymentAmount = nextPaymentAmount;
  }

  public Date getNextPaymentDate()
  {
    return nextPaymentDate;
  }

  public void setNextPaymentDate(Date nextPaymentDate)
  {
    this.nextPaymentDate = nextPaymentDate;
  }

  public String getPayableAccountUId()
  {
    return payableAccountUId;
  }

  public void setPayableAccountUId(String payableAccountUId)
  {
    this.payableAccountUId = payableAccountUId;
  }
}
