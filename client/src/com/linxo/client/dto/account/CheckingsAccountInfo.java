/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 5:35:56 PM by tarunmalhotra.
*/
package com.linxo.client.dto.account;

import com.linxo.client.data.pfm.bank.AccountType;

public class CheckingsAccountInfo extends BankAccountInfo {

  private Boolean includedInForecast;

  public CheckingsAccountInfo()
  {
    setType(AccountType.Checkings);
  }

  public CheckingsAccountInfo( long id )
  {
    //noinspection deprecation
    super(id);
    setType(AccountType.Checkings);
  }

  /**
   * Indicates whether or not the data attached to this account
   * (BankAccountHistory, Transactions, UpcomingTransactions)
   * should be used for Forecast results
   * @return {@code true} when the account is selected, {@code false} otherwise.
   */
  public Boolean isIncludedInForecast() {
    return includedInForecast;
  }

  public void setIncludedInForecast(Boolean includedInForecast) {
    this.includedInForecast = includedInForecast;
  }


  @Override
  public String toString()
  {
    return "CheckingsAccountInfo{"
           + super.toString()
        + ", includedInForecast=" + includedInForecast
           + '}';
  }

}
