/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 5:35:56 PM by tarunmalhotra.
*/
package com.linxo.client.dto.account;

import com.linxo.client.data.SavingsType;
import com.linxo.client.data.pfm.bank.AccountType;

import java.util.Date;

public class SavingsProviderAccountInfo extends ProviderAccountInfo
{
  private String savingsType;

  private Date effectDate;

  private Date endDate;

  private Double yieldRate;

  private Double earnedInterest;

  private Double rawInvested;

  private Double netInvested;

  private Double rawDisinvested;

  private String pivotAccountUId;

  public SavingsProviderAccountInfo(){
    setType(AccountType.Savings);
  }

  public Double getEarnedInterest()
  {
    return earnedInterest;
  }

  public void setEarnedInterest(Double earnedInterest)
  {
    this.earnedInterest = earnedInterest;
  }

  public Date getEffectDate()
  {
    return effectDate;
  }

  public void setEffectDate(Date effectDate)
  {
    this.effectDate = effectDate;
  }

  public Date getEndDate()
  {
    return endDate;
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }

  public Double getNetInvested()
  {
    return netInvested;
  }

  public void setNetInvested(Double netInvested)
  {
    this.netInvested = netInvested;
  }

  public String getPivotAccountUId()
  {
    return pivotAccountUId;
  }

  public void setPivotAccountUId(String pivotAccountUId)
  {
    this.pivotAccountUId = pivotAccountUId;
  }

  public Double getRawDisinvested()
  {
    return rawDisinvested;
  }

  public void setRawDisinvested(Double rawDisinvested)
  {
    this.rawDisinvested = rawDisinvested;
  }

  public Double getRawInvested()
  {
    return rawInvested;
  }

  public void setRawInvested(Double rawInvested)
  {
    this.rawInvested = rawInvested;
  }

  public String getSavingsType()
  {
    return savingsType;
  }

  public void setSavingsType(SavingsType savingsType)
  {
    this.savingsType = savingsType.name();
  }

  public void setSavingsType(String savingsType)
  {
    this.savingsType = savingsType;
  }

  public Double getYieldRate()
  {
    return yieldRate;
  }

  public void setYieldRate(Double yieldRate)
  {
    this.yieldRate = yieldRate;
  }
}
