/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */

package com.linxo.client.dto.account;

import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.data.pfm.bank.AccountSynchroStatus;
import com.linxo.client.data.pfm.group.SynchroStatus;
import com.linxo.client.shared.AssertNotNull;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.dto.EntityInfo;
import com.linxo.client.actions.pfm.events.SynchronizationUpdateEvent;

import java.util.Date;

/**
 * Represents the common fields associated with BankAccounts.
 * This class should be considered <strong>abstract</strong>
 * and not be directly instantiated.
 *
 * @see com.linxo.client.dto.account.CheckingsAccountInfo
 * @see com.linxo.client.dto.account.CreditCardAccountInfo
 * @see com.linxo.client.dto.account.SavingsAccountInfo
 */
public class BankAccountInfo extends EntityInfo implements Comparable<BankAccountInfo>
{
  @AssertStringSize
  private String accountNumber;

  @AssertStringSize
  private String reference;

  @AssertStringSize(nullable = false)
  private String name;

  private double currentBalance;

  @AssertStringSize(nullable = false)
  private String accountGroupName;

  private Long accountGroupId;

  @AssertNotNull
  private AccountType type;

  @AssertStringSize
  private String specializedType;

  @AssertStringSize
  private String specializedTypeFamily;

  /**
   * @deprecated one should use the associated properties instead
   * @see #includedInBudget
   * @see #includedInSavings
   */
  @Deprecated
  private Boolean includedInTrends;

  private Boolean includedInBudget;
  private Boolean includedInSavings;
  private Boolean includedInAssets;
  private Long viewId;

  private Date creationDate;
  private Date currentBalanceDate;
  private Date closeDate;

  private String currency;

  private AccountSynchroStatus status;
  private SynchroStatus accountGroupStatus;

  private SynchronizationUpdateEvent lastEvent;

  protected BankAccountInfo() {}

  /**
   * Creates a new BankAccountInfo with the given id
   *
   * @param id - technical identifier for this BankAccountInfo
   *
   * @deprecated Use directly the daughter classes instead. This
   * Constructor is here for compatibility and should no more be used.
   */
  @Deprecated
  public BankAccountInfo( long id )
  {
    setId( id );
  }


  /**
   * @return the account number of this account
   */
  public String getAccountNumber()
  {
    return accountNumber;
  }

  /**
   * Sets the account number of this account.
   * @param accountNumber - new account number. This value can be changed
   *                      by the client when the account is manual
   *                      (i.e. {@code status == None}).
   */
  public void setAccountNumber( String accountNumber )
  {
    this.accountNumber = accountNumber;
  }


  /**
   * @return the name given to this bank account
   */
  public String getName()
  {
    return name;
  }

  /**
   * Sets the account name of this account.
   */
  public void setName( String name )
  {
    this.name = name == null ? null : name.replaceAll("\\s+", " ");
  }


  /**
   * @return the balance of this account as see at the date {@link #getCurrentBalanceDate()}.
   */
  public double getCurrentBalance()
  {
    return currentBalance;
  }

  /**
   * Sets the current balance of this account.
   *
   * Note:
   * This value can only be set on manual accounts (i.e. {@code status == None}).
   */
  public void setCurrentBalance( double currentBalance )
  {
    // avoid -0.0
    this.currentBalance = currentBalance == 0 ? 0 : currentBalance;
  }


  /**
   * @return the date when the current balance was last seen.
   * This is also the date of the last synchronization.
   * @see #getCurrentBalance()
   */
  public Date getCurrentBalanceDate()
  {
    return currentBalanceDate;
  }

  /**
   * Sets the date of the current balance of this account.
   * Note:
   * This value can only be set on manual accounts (i.e. {@code status == None}).
   */
  public void setCurrentBalanceDate(Date balanceDate)
  {
    this.currentBalanceDate = balanceDate;
  }


  /**
   * @return the name of the account group this bank account is attached
   */
  public String getAccountGroupName()
  {
    return accountGroupName;
  }

  /**
   * This value cannot be changed by the client.
   * The setter only exists for the server side.
   */
  public void setAccountGroupName(String accountGroupName)
  {
    this.accountGroupName = accountGroupName;
  }

  /**
   * @return ths synchronisation status of this account
   */
  public AccountSynchroStatus getStatus()
  {
    return status;
  }

   /**
    * This value cannot be changed by the client.
    * The setter only exists for the server side.
    */
  public void setStatus(final AccountSynchroStatus status)
  {
    this.status = status;
  }


  /**
   * @return the Date when this account was closed/archived. If the field is not-null,
   * the {@link #getCurrentBalance()} should return 0 and the {@link #getCurrentBalanceDate()}
   * should be the same as this.
   */
  public Date getCloseDate()
  {
    return closeDate;
  }

  /**
   * This value cannot be changed by the client.
   * The setter only exists for the server side.
   */
  public void setCloseDate(Date closeDate)
  {
    this.closeDate = closeDate;
  }

  /**
   * @return the Date when this account was created in the database.
   */
  public Date getCreationDate()
  {
    return creationDate;
  }

  /**
   * This value cannot be changed by the client.
   * The setter only exists for the server side.
   */
  public void setCreationDate(Date creationDate)
  {
    this.creationDate = creationDate;
  }

  /**
   * @return the last synchronisation event for this account, if a synchronisation is running when the object is
   * returned.
   * If there is no synchronization running, this method returns {@code null}.
   */
  public SynchronizationUpdateEvent getLastEvent()
  {
    return lastEvent;
  }

  /**
   * This value cannot be changed by the client.
   * The setter only exists for the server side.
   */
  public void setLastEvent(SynchronizationUpdateEvent lastEvent)
  {
    this.lastEvent = lastEvent;
  }

  /**
   * @return the type of account. This type decides if this object can be see as one of its
   * daughter class.
   * @see com.linxo.client.dto.account.CheckingsAccountInfo
   * @see com.linxo.client.dto.account.CreditCardAccountInfo
   * @see com.linxo.client.dto.account.SavingsAccountInfo
   */
  public AccountType getType()
  {
    return type;
  }

  /**
   * Sets the type of account.
   *
   * Note:
   * This value can only be set on manual accounts (i.e. {@code status == None}).

   * @param type - ther new type of account.
   */
  public void setType(AccountType type)
  {
    this.type = type;
  }

  public String getSpecializedType() {
    return specializedType;
  }

  public void setSpecializedType(String specializedType) {
    this.specializedType = specializedType;
  }

  public String getSpecializedTypeFamily() {
    return specializedTypeFamily;
  }

  public void setSpecializedTypeFamily(String specializedTypeFamily) {
    this.specializedTypeFamily = specializedTypeFamily;
  }

  /**
   * @return the internal DB id of the account group this bank account is attached
   */
  public Long getAccountGroupId()
  {
    return accountGroupId;
  }

  /**
   * This value cannot be changed by the client.
   * The setter only exists for the server side.
   */
  public void setAccountGroupId(Long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
  }


  /**
   * @return the synchronisation status of the group attached to this bankAccount.
   */
  public SynchroStatus getAccountGroupStatus()
  {
    return accountGroupStatus;
  }

  /**
   * This value cannot be changed by the client.
   * The setter only exists for the server side.
   */
  public void setAccountGroupStatus(SynchroStatus accountGroupStatus)
  {
    this.accountGroupStatus = accountGroupStatus;
  }


  /**
   * Indicates whether this BankAccount should be included in the trends graphics.
   * @deprecated In API version
   * {@link com.linxo.client.data.support.Version#HAS_DETAILED_ACCOUNT_VIEWS_SETTINGS}
   * and above, this field is replaced by more specific fields
   * (see the See Also section below).
   * In previous versions, changing this field will change the values of both
   * {@link #isIncludedInSavings()} and {@link #isIncludedInBudget()} in the
   * new model. New fields are sent anyway.
   *
   * @see #isIncludedInAssets()
   * @see #isIncludedInSavings()
   * @see #isIncludedInBudget()
   */
  @Deprecated
  @SuppressWarnings("deprecation")
  public Boolean isIncludedInTrends() {
    return includedInTrends;
  }

  @SuppressWarnings("deprecation")
  @Deprecated
  public void setIncludedInTrends(Boolean includedInForecast) {
    this.includedInTrends = includedInForecast;
  }

  /**
   * Indicates whether or not the data attached to this account
   * (BankAccountHistory, Transactions, UpcomingTransactions)
   * should be used for Budget results
   * @return {@code true} when the account is selected, {@code false} otherwise.
   */
  public Boolean isIncludedInBudget() {
    return includedInBudget;
  }

  public void setIncludedInBudget(Boolean includedInBudget) {
    this.includedInBudget = includedInBudget;
  }
  /**
   * Indicates whether or not the data attached to this account
   * (BankAccountHistory, Transactions, UpcomingTransactions)
   * should be used for Saving results
   * @return {@code true} when the account is selected, {@code false} otherwise.
   */
  public Boolean isIncludedInSavings() {
    return includedInSavings;
  }

  public void setIncludedInSavings(Boolean includedInSavings) {
    this.includedInSavings = includedInSavings;
  }
  /**
   * Indicates whether or not the data attached to this account
   * (BankAccountHistory, Transactions, UpcomingTransactions)
   * should be used for Assets results
   * @return {@code true} when the account is selected, {@code false} otherwise.
   */
  public Boolean isIncludedInAssets() {
    return includedInAssets;
  }

  public void setIncludedInAssets(Boolean includedInAssets) {
    this.includedInAssets = includedInAssets;
  }

  public String getReference()
  {
    return reference;
  }

  public void setReference(final String reference)
  {
    this.reference = reference;
  }

  /**
   * Gets the id of the {@link PersonalizedView} this account is currently associated with.
   *
   * @return The id of the {@link PersonalizedView} this account is currently associated with.
   */
  public Long getViewId()
  {
    return viewId;
  }

  public void setViewId(Long viewId)
  {
    this.viewId = viewId;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  @Override
  public int compareTo(final BankAccountInfo other)
  {
    return name.compareToIgnoreCase(other.name);
  }

  
  @Override
  public boolean equals( Object o )
  {
    if ( this == o ) return true;
    if ( !( o instanceof BankAccountInfo) ) return false;

    BankAccountInfo that = (BankAccountInfo) o;

    if ( accountNumber != null ? !accountNumber.equals( that.accountNumber ) : that.accountNumber != null )
      return false;
    if ( reference != null ? !reference.equals( that.reference) : that.reference != null )
      return false;
    //noinspection RedundantIfStatement
    if ( name != null ? !name.equals( that.name ) : that.name != null ) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result;
    result = ( getId() != null ? getId().hashCode() : 0 );
    result = 31 * result + ( accountNumber != null ? accountNumber.hashCode() : 0 );
    result = 31 * result + ( reference != null ? reference.hashCode() : 0 );
    result = 31 * result + ( name != null ? name.hashCode() : 0 );
    return result;
  }

  @Override
  @SuppressWarnings("deprecation")
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder();
    sb.append("BankAccountInfo");
    sb.append("{");
    sb.append(super.toString());
    sb.append(", type=").append(type);
    sb.append(", status=").append(status);
    sb.append(", creationDate=").append(creationDate);
    sb.append(", name=").append(name);
    sb.append(", accountNumber='" ).append( accountNumber ).append('\'');
    sb.append(", reference='").append(reference).append('\'');
    sb.append(", accountGroupId='" ).append(getAccountGroupId()).append( '\'' );
    sb.append(", accountGroupName=").append( accountGroupName );
    sb.append(", currentBalance=" ).append( currentBalance );
    sb.append(", currentBalanceDate=" ).append(currentBalanceDate);
    sb.append(", accountGroupStatus=" ).append( accountGroupStatus );
    sb.append(", includedInTrends=").append(includedInTrends);
    sb.append(", includedInBudget=").append(includedInBudget);
    sb.append(", includedInSavings=").append(includedInSavings);
    sb.append(", includedInAssets=").append(includedInAssets);
    sb.append(", viewId=").append(viewId);
    sb.append(", closeDate=").append(closeDate);
    sb.append(", lastEvent=").append(lastEvent);
    sb.append(", currency=").append(currency);
    sb.append('}' );
    return sb.toString();
  }

}
