/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 5:35:56 PM by tarunmalhotra.
*/
package com.linxo.client.dto.account;

import com.linxo.client.data.pfm.bank.AccountType;

import java.util.Date;

public class SavingsAccountInfo extends BankAccountInfo {

  private Date effectDate;

  private Date endDate;

  private Double yieldRate;

  private Double earnedInterestYtD;

  private Double earnedInterestTotal;

  private Double rawInvested;

  private Double netInvested;

  private Double rawDisinvested;

  private Long pivotAccountId;

  private Double capitalGainAmount;

  private Double capitalGainPercent;

  // ------------------------------ >8 --------------------------------

  public SavingsAccountInfo()
  {
    setType(AccountType.Savings);
  }

  public SavingsAccountInfo( long id )
  {
    //noinspection deprecation
    super( id );
    setType(AccountType.Savings);
  }

  public Double getEarnedInterestYtD() {
    return earnedInterestYtD;
  }

  public void setEarnedInterestYtD(Double earnedInterestYtD) {
    this.earnedInterestYtD = earnedInterestYtD;
  }

  public Double getEarnedInterestTotal()
  {
    return earnedInterestTotal;
  }

  public void setEarnedInterestTotal(Double earnedInterestTotal)
  {
    this.earnedInterestTotal = earnedInterestTotal;
  }

  public Date getEffectDate()
  {
    return effectDate;
  }

  public void setEffectDate(Date effectDate)
  {
    this.effectDate = effectDate;
  }

  public Date getEndDate()
  {
    return endDate;
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }

  public Double getNetInvested()
  {
    return netInvested;
  }

  public void setNetInvested(Double netInvested)
  {
    this.netInvested = netInvested;
  }

  public Long getPivotAccountId()
  {
    return pivotAccountId;
  }

  public void setPivotAccountId(Long pivotAccountId)
  {
    this.pivotAccountId = pivotAccountId;
  }

  public Double getRawDisinvested()
  {
    return rawDisinvested;
  }

  public void setRawDisinvested(Double rawDisinvested)
  {
    this.rawDisinvested = rawDisinvested;
  }

  public Double getRawInvested()
  {
    return rawInvested;
  }

  public void setRawInvested(Double rawInvested)
  {
    this.rawInvested = rawInvested;
  }

  public String getSavingsType()
  {
    return getSpecializedType();
  }

  public void setSavingsType(String savingsType)
  {
    setSpecializedType(savingsType);
  }

  public Double getYieldRate()
  {
    return yieldRate;
  }

  public void setYieldRate(Double yieldRate)
  {
    this.yieldRate = yieldRate;
  }

  public Double getCapitalGainAmount() {
    return capitalGainAmount;
  }

  public void setCapitalGainAmount(Double capitalGainAmount) {
    this.capitalGainAmount = capitalGainAmount;
  }

  public Double getCapitalGainPercent() {
    return capitalGainPercent;
  }

  public void setCapitalGainPercent(Double capitalGainPercent) {
    this.capitalGainPercent = capitalGainPercent;
  }

  @Override
  public String toString()
  {
    return "SavingsAccountInfo{" + super.toString() +
        ", earnedInterestTotal=" + earnedInterestTotal +
        ", earnedInterestYtD=" + earnedInterestYtD +
        ", capitalGainAmount=" + capitalGainAmount +
        ", capitalGainPercent=" + capitalGainPercent +
        ", effectDate=" + effectDate +
        ", endDate=" + endDate +
        ", yieldRate=" + yieldRate +
        ", rawInvested=" + rawInvested +
        ", netInvested=" + netInvested +
        ", rawDisinvested=" + rawDisinvested +
        ", pivotAccountId=" + pivotAccountId +
        '}';
  }
}
