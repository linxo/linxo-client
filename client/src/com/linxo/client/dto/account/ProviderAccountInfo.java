/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.account;

import com.linxo.client.data.pfm.bank.AccountType;

import java.io.Serializable;
import java.util.Date;

/**
 * This class represents the BankAccount that is collected during a ListAccount.
 */
public class 
    ProviderAccountInfo
    implements Serializable, Comparable<ProviderAccountInfo>
{
  public enum Status
  {
    Disappeared,
    Remains,
    New
  }

  private String uid;
  private String accountNumber;
  private Double balance;
  private Date balanceDate;

  private AccountType type;
  private String specializedType;
  private String specializedFamily;

  private Status status;
  private String accountName;

  private String currency;

  public ProviderAccountInfo() {}

  public String getUid()
  {
    return uid;
  }

  public void setUid(final String uid)
  {
    this.uid = uid;
  }

  public String getAccountNumber()
  {
    return accountNumber;
  }

  public void setAccountNumber(final String accountNumber)
  {
    this.accountNumber = accountNumber;
  }

  public Double getBalance()
  {
    return balance;
  }

  public void setBalance(final Double balance)
  {
    // avoid -0.0
    this.balance = balance == null ? null : (balance == 0 ? 0 : balance);
  }

  public Date getBalanceDate()
  {
    return balanceDate;
  }

  public void setBalanceDate(final Date balanceDate)
  {
    this.balanceDate = balanceDate;
  }

  public AccountType getType()
  {
    return type;
  }

  public void setType(final AccountType type)
  {
    this.type = type;
  }

  public Status getStatus()
  {
    return status;
  }

  public void setStatus(final Status status)
  {
    this.status = status;
  }

  public String getAccountName()
  {
    return accountName;
  }

  public void setAccountName(String accountName)
  {
    this.accountName = accountName == null ? null : accountName.replaceAll("\\s+", " ");
  }

  public String getSpecializedType() {
    return specializedType;
  }

  public void setSpecializedType(String specializedType) {
    this.specializedType = specializedType;
  }

  public String getSpecializedFamily() {
    return specializedFamily;
  }

  public void setSpecializedFamily(String specializedFamily) {
    this.specializedFamily = specializedFamily;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  @Override
  public int compareTo(final ProviderAccountInfo other)
  {
    return accountNumber.compareToIgnoreCase(other.accountNumber);
  }

}
