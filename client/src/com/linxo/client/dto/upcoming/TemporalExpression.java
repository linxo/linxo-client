/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.upcoming;

import com.linxo.client.data.DateUtils;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.upcoming.IntervalUnit;

import java.io.Serializable;
import java.util.*;

/**
 * This class enables to describe one date or a list of dates (a date is said to be included or not) based
 * on a repeating pattern with a start date and an optional end date
 */
public class TemporalExpression
    implements Serializable
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields


  public static final Integer DEFAULT_INTERVAL_VALUE = 1;

  public static final int MONDAY    = 1;
  public static final int TUESDAY   = 2;
  public static final int WEDNESDAY = 3;
  public static final int THURSDAY  = 4;
  public static final int FRIDAY    = 5;
  public static final int SATURDAY  = 6;
  public static final int SUNDAY    = 7;

  private static final List<Integer> WEEK_ACCEPTED_VALUES =
      Collections.unmodifiableList(asList(
          new int [] {
              MONDAY,
              TUESDAY,
              WEDNESDAY,
              THURSDAY,
              FRIDAY,
              SATURDAY,
              SUNDAY
          }
      ));

  private static final List<Integer> MONTH_ACCEPTED_VALUES =
      Collections.unmodifiableList(asList(
          new int [] {
              -1,1,2,3,4,5,6,7,8,9,
              10,11,12,13,14,15,16,17,18,19,
              20,21,22,23,24,25,26,27,28,29,
              30,31
          }
      ));


  public static final int DEFAULT_DAYS_BEFORE_MATCHING = 3;
  public static final int DEFAULT_DAYS_AFTER_MATCHING = 3;

  // Static Initializers

  // Static Methods

  private static ArrayList<Integer> asList(int[] array)
  {
    ArrayList<Integer> result = new ArrayList<Integer>(array.length);
    for(int value : array){
      result.add(value);
    }

    return result;
  }


  // Instance Fields


  private LinxoDate startDate;
  private LinxoDate endDate;
  private String localizedDescription;

  private IntervalUnit intervalUnit;
  private Integer intervalValue;
  private int[] daysInInterval;


  // These dates are used to define the "matching window" when matching an occurrence
  // with the Transactions.
  private int daysBeforeMatching;
  private int daysAfterMatching;

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //              Constructor
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  public TemporalExpression()
  {
    this(DEFAULT_DAYS_AFTER_MATCHING, DEFAULT_DAYS_BEFORE_MATCHING);
  }

  public TemporalExpression(final int daysBeforeMatching, final int daysAfterMatching)
  {
    this.daysBeforeMatching = daysBeforeMatching;
    this.daysAfterMatching = daysAfterMatching;
  }

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           Getter and Setters
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  /**
   * Mandatory field (not null).
   * The date from which to compute the first included date.
   * This date may or may not be included depending on other fields
   * @return the startDate
   */
  public LinxoDate getStartDate()
  {
    return startDate;
  }

  public void setStartDate(LinxoDate startDate)
  {
    this.startDate = startDate;
  }

  /**
   * The last date that may be included
   * @return The last date that may be included
   */
  public LinxoDate getEndDate()
  {
    return endDate;
  }

  public void setEndDate(LinxoDate endDate)
  {
    this.endDate = endDate;
  }

  /**
   * Textual representation of this temporal expression
   * Example: "Tous les jeudis"
   * @return Textual representation of this temporal expression
   */
  public String getLocalizedDescription()
  {
    return localizedDescription;
  }

  public void setLocalizedDescription(String localizedDescription)
  {
    this.localizedDescription = localizedDescription;
  }

  public IntervalUnit getIntervalUnit()
  {
    return intervalUnit;
  }

  public void setIntervalUnit(IntervalUnit intervalUnit)
  {
    this.intervalUnit = intervalUnit;
  }

  /**
   * Number of intervalUnit considered in this TemporalExpression
   * defaults to 1 unit
   * @return the number of intervalUnit considered in this TemporalExpression
   */
  public Integer getIntervalValue()
  {
    // The intervalValue might be null when de-serialized, so we default
    // to the value of DEFAULT_INTERVAL_VALUE (ie. "1") when null.
    return intervalValue==null
        ? DEFAULT_INTERVAL_VALUE
        : intervalValue;
  }

  public void setIntervalValue(Integer intervalValue)
  {
    this.intervalValue = intervalValue;
  }

  /**
   * This field defines the days included in a given interval
   * We use values from the java.util.Calendar class:
   * <ul>
   *   <li> If intervalUnit == day, this field is ignored
   *   <li> If intervalUnit == week, Sunday=1, Monday=2 ... Saturday=7
   *   <li> If intervalUnit == month, first day of month is 1. Maximum is 31.
   *   <li> If intervalUnit == year, this field is ignored
   * </ul>
   *
   * <p>
   * NOTE:
   * If this field is null or empty, we use the weekday of {@link #startDate}.
   * </p>
   * <p>
   * NOTE2:
   * Also, we cannot have a {@code null} value in this array since it is an
   * array of a primitive type.
   * </p>
   * @return the list of days
   * @see #WEEK_ACCEPTED_VALUES
   * @see #MONTH_ACCEPTED_VALUES
   */
  public int[] getDaysInInterval()
  {
    return daysInInterval;
  }

  public void setDaysInInterval(int[] daysInInterval)
  {
    this.daysInInterval = daysInInterval;
  }

  /**
   * Number of days <strong>before</strong> the occurrence date when incoming transactions may be matched
   * @return Number of days before the occurrence date when incoming transactions may be matched
   */
  public int getDaysBeforeMatching()
  {
    return daysBeforeMatching;
  }

  public void setDaysBeforeMatching(int daysBeforeMatching)
  {
    this.daysBeforeMatching = daysBeforeMatching;
  }

  /**
   * Number of days <strong>after</strong> the occurrence date when incoming transactions may be matched
   * @return Number of days before the occurrence date when incoming transactions may be matched
   */
  public int getDaysAfterMatching()
  {
    return daysAfterMatching;
  }

  public void setDaysAfterMatching(int daysAfterMatching)
  {
    this.daysAfterMatching = daysAfterMatching;
  }

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           API
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  /**
   * @param fromDate - the Start date, inclusive
   * @param toDate - the End date, inclusive
   * @param includeMatchingWindow - When true, include the occurrences before {@code fromDate} and after
   *                              {@code toDate} that may happen within the time frame because of their matching
   *                              window.
   * @return list of sorted occurrences (from past to future), between fromDate and
   * toDate dates (inclusive)
   */
  public ArrayList<UpcomingTransactionOccurrence> occurrencesFromTo(LinxoDate fromDate, LinxoDate toDate,
                                                                    boolean includeMatchingWindow)
  {
    if(fromDate == null || toDate == null || toDate.compareTo(fromDate) < 0) {
      return null;
    }

    // Determine if we're able to compute the occurrence
    if(!supportedValues()) {
      return null;
    }

    // We have to adjust the fromDate ant toDate according to the matching window values.
    final LinxoDate realFromDate = (includeMatchingWindow)?DateUtils.addDays(fromDate, -daysAfterMatching):fromDate;
    final LinxoDate realToDate = (includeMatchingWindow)?DateUtils.addDays(toDate, +daysBeforeMatching):toDate;
    ArrayList<UpcomingTransactionOccurrence> occurrences = new ArrayList<UpcomingTransactionOccurrence>();

    // The temporal expression starts only after the provided time range
    if(this.startDate.compareTo(realToDate) > 0) {
      // Returns the empty array
      return occurrences;
    }

    // Loop and compute the occurrences
    for(LinxoDate currentDate = nextOccurrenceAfter(realFromDate);
        currentDate != null && currentDate.compareTo(realToDate) <= 0
            && (this.endDate == null || currentDate.compareTo(this.endDate) <= 0);
        currentDate = nextOccurrenceAfter(DateUtils.addDays(currentDate, 1)))
    {

      occurrences.add(UpcomingTransactionOccurrence.occurrenceWithDate(currentDate));
    }

    return occurrences;
  }

  /**
   * @param startIndex - the position of the first element
   * @param length - the maximum number of elements
   * @return a list of sorted occurrences (from past to future), starting at
   * index startIndex and of length length.
   * The number of occurrences returned can be less than length if there are no
   * more occurrences available
   */
  public ArrayList<UpcomingTransactionOccurrence> occurrencesFromIndex(int startIndex, int length)
  {
    if(startIndex < 0 || length < 0)
    {
      throw new IllegalArgumentException("startIndex and length must be positive");
    }

    // Determine if we're able to compute the occurrence
    if(!supportedValues()) {
      return null;
    }

    // This loop terminates as we either reach the endDate or the length
    // The "once" case
    ArrayList<UpcomingTransactionOccurrence> occurrences = new ArrayList<UpcomingTransactionOccurrence>();
    int count = 0;
    LinxoDate currentDate = this.startDate;
    while(currentDate != null && occurrences.size() < length)
    {

      if(startIndex <= count)
      {
        occurrences.add(UpcomingTransactionOccurrence.occurrenceWithDate(currentDate));
      }

      currentDate = nextOccurrenceAfter(DateUtils.addDays(currentDate, 1));
      count++;
    }

    return occurrences;
  }

  public final TemporalExpression copy()
  {
    TemporalExpression te = new TemporalExpression();

    if(this.startDate != null) {
      te.setStartDate(this.startDate.copy());
    }

    if(this.endDate != null) {
      te.setEndDate(this.endDate.copy());
    }

    te.setLocalizedDescription(this.getLocalizedDescription());
    te.setIntervalUnit(this.getIntervalUnit());
    te.setIntervalValue(this.getIntervalValue());
    te.setDaysInInterval(this.getDaysInInterval() != null ? Arrays.copyOf(this.getDaysInInterval(), this.getDaysInInterval().length) : null);
    te.setDaysBeforeMatching(this.getDaysBeforeMatching());
    te.setDaysAfterMatching(this.getDaysAfterMatching());

    return te;
  }

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //    equals + hashCode
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  @SuppressWarnings("RedundantIfStatement")
  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (!(o instanceof TemporalExpression)) return false;

    TemporalExpression that = (TemporalExpression) o;

    if (!Arrays.equals(daysInInterval, that.daysInInterval)) return false;
    if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
    if (intervalUnit != that.intervalUnit) return false;
    if (intervalValue != null ? !intervalValue.equals(that.intervalValue) : that.intervalValue != null) return false;
    if (localizedDescription != null ? !localizedDescription.equals(that.localizedDescription) : that.localizedDescription != null)
      return false;
    if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = startDate != null ? startDate.hashCode() : 0;
    result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
    result = 31 * result + (localizedDescription != null ? localizedDescription.hashCode() : 0);
    result = 31 * result + (intervalUnit != null ? intervalUnit.hashCode() : 0);
    result = 31 * result + (intervalValue != null ? intervalValue.hashCode() : 0);
    result = 31 * result + (daysInInterval != null ? Arrays.hashCode(daysInInterval) : 0);
    return result;
  }


  @Override
  public String toString()
  {
    if (localizedDescription!=null)
    {
      return localizedDescription;
    }

    //noinspection StringBufferMayBeStringBuilder
    final StringBuffer sb = new StringBuffer("TemporalExpression{");
    sb.append("daysAfterMatching=").append(daysAfterMatching);
    sb.append(", startDate=").append(startDate);
    sb.append(", endDate=").append(endDate);
    sb.append(", intervalUnit=").append(intervalUnit);
    sb.append(", intervalValue=").append(intervalValue);
    sb.append(", daysInInterval=");
    if (daysInInterval == null) sb.append("null");
    else {
      sb.append('[');
      for (int i = 0; i < daysInInterval.length; ++i)
        sb.append(i == 0 ? "" : ", ").append(daysInInterval[i]);
      sb.append(']');
    }
    sb.append(", daysBeforeMatching=").append(daysBeforeMatching);
    sb.append('}');
    return sb.toString();
  }

  /**
   * Tells whether the temporal expression is equivalent to another one.
   * It is equivalent if dates are equal, and the recurrence are equivalent
   * @param other the other temporal expression
   * @return true if equivalent, false otherwise
   */
  public boolean isEquivalentTo(TemporalExpression other)
  {
    if(this == other) {
      return true;
    }

    if(other == null) {
      return false;
    }

    if(startDate != null && other.getStartDate() == null) {
      return false;
    }
    if(startDate == null && other.getStartDate() != null) {
      return false;
    }
    if(startDate != null && other.getStartDate() != null && !startDate.equals(other.getStartDate())) {
      return false;
    }

    if(endDate != null && other.getEndDate() == null) {
      return false;
    }
    if(endDate == null && other.getEndDate() != null) {
      return false;
    }
    if(endDate != null && other.getEndDate() != null && !endDate.equals(other.getEndDate())) {
      return false;
    }

    if(!isRecurrenceEquivalentTo(other)) {
      return false;
    }

    return true;
  }

  /**
   * Tells whether the recurrence are equivalent. They are equivalent if their interval units
   * are equal, their interval values are equivalent (one can be null, the other one equal to 1,
   * hence they are equivalent) and their daysInInterval arrays are equal
   * @param other the other recurrence
   * @return true if equivalent, false otherwise
   */
  public boolean isRecurrenceEquivalentTo(TemporalExpression other)
  {
    if(this == other) {
      return true;
    }

    if(other == null) {
      return false;
    }

    if(intervalUnit != other.getIntervalUnit()) {
      return false;
    }

    int thisIntervalValue = intervalValue != null ? intervalValue : 1;
    int otherIntervalValue = other.getIntervalValue() != null ? other.getIntervalValue() : 1;
    if(thisIntervalValue != otherIntervalValue) {
      return false;
    }

    if(!Arrays.equals(daysInInterval, other.getDaysInInterval())) {
      return false;
    }

    return true;
  }


  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           Utility methods
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  private boolean areAllValuesInList(int[] array, List<Integer> list)
  {
    if (array == null || array.length == 0) return true;

    ArrayList<Integer> result = asList(array);

    result.removeAll(list);

    // if a value remains, it's not in the parameter "list"
    return result.size() == 0;
  }


  private boolean supportedValues()
  {
    // The reference implementation (Objective-C) checks for known values
    // of the IntervalUnit. In Java, this is an enum and thus is naturally
    // checked at compile-time (Java Rocks !).

    switch(intervalUnit) {
      case month:
        return areAllValuesInList(this.daysInInterval, MONTH_ACCEPTED_VALUES);

      case week:
        return areAllValuesInList(this.daysInInterval, WEEK_ACCEPTED_VALUES);

      default:
        return true;
    }
  }



  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //    Utility methods to compute day
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  /**
   * @param date - must be not null
   * @return the Date of the next occurrence at or after the given date,
   * or null if no occurrence happen after this date.
   * @throws java.lang.NullPointerException if the given date is null
   */
  LinxoDate nextOccurrenceAfter(LinxoDate date)
      throws NullPointerException
  {
    if (date == null){
      throw new NullPointerException("date should not be null");
    }

    if(this.startDate.compareTo(date) >= 0)
    {
      return this.startDate;
    }

    // no iteration here...
    if(this.intervalUnit == IntervalUnit.once)
    {
      // We should return "date.equals(this.startDate)?startDate:null"
      // However, this would always return null since equality is tested above.
      return null;
    }

    LinxoDate currentDate = date;
    while( !includes(currentDate) )
    {
      // If it's not included because we have reached the endDate
      // => ends the loop and return
      if(this.endDate != null && this.endDate.compareTo(currentDate) < 0)
      {
        return null;
      }
      currentDate = DateUtils.addDays(currentDate, 1);
    }

    return currentDate;
  }

  private boolean includes(LinxoDate date)
  {
    if (date == null) return false;

    if(this.startDate.compareTo(date) > 0) return false;

    if(this.endDate != null && this.endDate.compareTo(date) < 0) return false;

    if(this.startDate.equals(date)) return true;

    // Check the dates

    if(intervalUnit == IntervalUnit.once) {
      return false; // if we're not on startDate this date is not included
    }
    else if(intervalUnit == IntervalUnit.day) {
      return dayIncludes(date);
    }
    else if(intervalUnit == IntervalUnit.week) {
      return weekIncludes(date);
    }
    else if(intervalUnit == IntervalUnit.month) {
      return monthIncludes(date);
    }
    else if(intervalUnit == IntervalUnit.year) {
      return yearIncludes(date);
    }
    // else its unknown
    return false;
  }

  private boolean dayIncludes(LinxoDate date)
  {
    Date start = DateUtils.getDate(this.startDate);
    Date thisDate = DateUtils.getDate(date);
    int ivValue = this.intervalValue == null ? 1 : this.intervalValue;

    long deltaInDays = (thisDate.getTime() - start.getTime())/DateUtils.DAY_MS;

    return (deltaInDays % ivValue) == 0;
  }

  //           WEEKS
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  private boolean weekIncludes(LinxoDate date)
  {
    Date thisDate = DateUtils.getDate(date);
    return (weekDayMatches(thisDate) && weekMatches(thisDate));
  }

  @SuppressWarnings({ "deprecation" }) // Date#getDay()
  private boolean weekDayMatches(Date date)
  {
    int weekDay = date.getDay();
    // Date#getDay() may return 0 for SUNDAY => put our value instead
    weekDay = (weekDay == 0)?SUNDAY:weekDay;

    // If the array is null or empty, we use startDate as reference
    if(this.daysInInterval == null || this.daysInInterval.length == 0) {
      // Date#getDay() may return 0 for SUNDAY => put our value instead
      int startDateWeekDay = DateUtils.getDate(this.startDate).getDay();
      startDateWeekDay =  (startDateWeekDay == 0)?SUNDAY:startDateWeekDay;
      return weekDay == startDateWeekDay;
    }

    // else we look into the array
    return asList(this.daysInInterval).contains(weekDay);
  }

  private boolean weekMatches(Date date)
  {
    // We assume startDate < date as this is checked previously.
    // - currentStartWeek = the beginning of the week. begins with the beginning of the week of startDate
    // - dateStartWeek = the beginning of the week for date
    // Then we check how many days/weeks is the difference between both.

    Date currentStartWeek = DateUtils.getStartOfWeek(DateUtils.getDate(this.startDate));
    final Date dateStartWeek = DateUtils.getStartOfWeek(date);

    //Then find the number of week between these two dates
    int count = 0;
    while(!DateUtils.isSameDay(currentStartWeek, dateStartWeek)){
      count++;
      currentStartWeek = DateUtils.addDays(currentStartWeek, 7);
    }

    int ivValue = this.intervalValue == null ? 1 : this.intervalValue;
    return (count % ivValue) == 0;
  }


  //           MONTHS
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  private boolean monthIncludes(LinxoDate date)
  {
    Date thisDate = DateUtils.getDate(date);

    final boolean monthDayMatches = monthDayMatches(thisDate);
    final boolean monthMatches = monthMatches(thisDate);
    return (monthDayMatches && monthMatches);
  }

  @SuppressWarnings({ "deprecation" }) // Date#getDate()
  private boolean monthDayMatches(Date date)
  {
    if(this.daysInInterval == null)
    {
      return date.getDate() == DateUtils.getDate(this.startDate).getDate();
    }

    if(asList(this.daysInInterval).contains(date.getDate()))
    {
      return true;
    }

    // Do we have to check for "last day of month" ?
    if(asList(daysInInterval).contains(-1))
    {
      // if yes, are we at the end of the month ?
      Date endOfMonth = DateUtils.lastDayOfMonth(date.getMonth(), date.getYear());
      return DateUtils.isSameDay(endOfMonth, date);
    }

    return false;
  }

  private boolean monthMatches(Date date)
  {
    int ivValue = this.intervalValue == null ? 1 : this.intervalValue;
    return (DateUtils.monthSpan(this.startDate, DateUtils.getLinxoDate(date)) % ivValue) == 0;
  }


  //           YEARS
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  private boolean yearIncludes(LinxoDate date)
  {
    int ivValue = this.intervalValue == null ? 1 : this.intervalValue;

    if (date.getDay()   != this.startDate.getDay() ||
        date.getMonth() != this.startDate.getMonth())
    {
      return false;
    }

    // day and month match => check the year
    return ((date.getYear() - this.startDate.getYear())%ivValue) == 0;
  }

}
