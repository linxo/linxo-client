/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 24/10/2014 by hugues.
 */
package com.linxo.client.dto.upcoming;

import com.linxo.client.data.LinxoDate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * This class represents the forecast values to display for a given account.
 *
 * It contains :
 * <ul>
 *   <li>Tha bank account id of the bank account considered and its current balance,</li>
 *   <li>the list of balances for the next 30 days,</li>
 *   <li>information about the minimum balance for the next 30 days (see the {@code getMinimum*()} method) and,</li>
 *   <li>the next date of the next status change, or the status in {@link #MAX_FORECAST_SPAN} days if the does
 *   not change in the next 30 days (see the {@code getNotice*()} method).</li>
 * </ul>
 */
public class BankAccountForecast
    implements Serializable
{

  // Nested Types (mixing inner and static classes is okay)

  /**
   * This enum defines the states (icons that may be rendered to the UI),
   * as well as an order for these states.
   *
   * @see #getDisplayComparator()
   */
  public enum NoticeState {

    /** the status is unknown. */
    Undefined(-1),

    /** The current balance is above the {@code overdraftThreshold}.
     *  The value of the balance of the event is above the {@code lowBalanceThreshold}. */
    Sun(0),

    /** The current balance is above the {@code overdraftThreshold}.
     *  The value of the balance of the event is below the {@code lowBalanceThreshold},
     *  and above the {@code overdraftThreshold}.
     */
    Clouds(1),

    /** The current balance is above the {@code overdraftThreshold}.
     *  The value of the balance of the event is below the {@code overdraftThreshold}.
     */
    Storm(2),

    /** The current balance is below the {@code overdraftThreshold}. */
    Tornado(3),

    /** The current account did not synchronize recently enough to be able to
     * compute anything meaningful.
     */
    UnSynchronized(4);

    private int order;

    NoticeState(int order) {
      this.order = order;
    }

  }



  // Static Fields

  /** The max number of days we compute a projected balance */
  public static final Integer MAX_FORECAST_SPAN = 30;

  // Static Initializers

  // Static Methods

  @SuppressWarnings("unused")
  public static int getMaxForecastSpan()
  {
    return MAX_FORECAST_SPAN;
  }

  /**
   * @return a Comparator based on the {@link BankAccountForecast#minimumState}
   */
  @SuppressWarnings("unused")
  public static Comparator<BankAccountForecast> getDisplayComparator() {
    return new Comparator<BankAccountForecast>() {
      @Override
      public int compare(BankAccountForecast that, BankAccountForecast other)
      {
        return that.minimumState.order - other.minimumState.order;
      }
    };
  }

  // Instance Fields

  private double balance;

  private ArrayList<Double> balances;

  private LinxoDate noticeDate;
  private Integer noticeDaySpan;
  private Double  noticeBalance;
  private Integer noticeBalanceIndex;

  private LinxoDate minimumDate;
  private Integer minimumDaySpan;
  private Double  minimumBalance;
  private Integer minimumBalanceIndex;
  private NoticeState minimumState = NoticeState.Undefined;

  private long bankAccountId;



  // Instance Initializers

  // Constructors

  // Instance Methods

  /** @return current balance for this account */
  public double getBalance()
  {
    return balance;
  }

  public void setBalance(double balance)
  {
    this.balance = balance;
  }

  /**
   * @return the date when the next event will take place (at end of the day).
   */
  public LinxoDate getNoticeDate()
  {
    return noticeDate;
  }

  public void setNoticeDate(LinxoDate noticeDate)
  {
    this.noticeDate = noticeDate;
  }

  /**
   * The number of days between today and the date of the notice.
   * <ul>
   *   <li>0 : today</li>
   *   <li>1 : tomorrow</li>
   *   <li>...</li>
   *   <li>n : n-th day after today</li>
   * </ul>
   *
   * This value is different from the {@link #getNoticeBalanceIndex()}
   *
   * @return the number of days between today and the date of the notice.
   * {@code null} if the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions.
   */
  public Integer getNoticeDaySpan()
  {
    return noticeDaySpan;
  }

  public void setNoticeDaySpan(Integer noticeDaySpan)
  {
    this.noticeDaySpan = noticeDaySpan;
  }

  /**
   * The list of balances, day by day for this current account, taking into account the
   * occurrences for each day.
   *
   * <ul>
   *   <li>The first element is the current balance <code>balance == balances[0]</code> </li>
   *   <li>The second element is the balance _today_ after the pending transactions for today have been counted</li>
   *   <li>Next elements are the balances computed using the occurrences for each day, day by day.
   *   <code>
   *     balances[2] // balance tomorrow EndOfDay
   *     balances[3] // balance the days after tomorrow EOD
   *     ...
   *     balances[n] // balance the (n-2)th days after tomorrow EOD
   *   </code>
   *   </li>
   * </ul>
   *
   * @return The list of balances, day by day for this current account, taking into account the
   * occurrences for each day.
   * An empty list if the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions.

   */
  public ArrayList<Double> getBalances()
  {
    return balances;
  }

  public void setBalances(ArrayList<Double> balances)
  {
    this.balances = balances;
  }

  /**
   * Projected value of the account balance at the time of the notice.
   *
   * The result is equivalent to {@code getBalances().get(getNoticeBalanceIndex)}
   *
   * @return the projected value of the account balance for at the time of the notice.
   * {@code null} if the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions.
   */
  public Double getNoticeBalance()
  {
    return noticeBalance;
  }

  public void setNoticeBalance(Double noticeBalance)
  {
    this.noticeBalance = noticeBalance;
  }

  /**
   * The bank Account Id (Internal to Linxo) that references the bank account for this forecast.
   * @return The bank Account Id (Internal to Linxo) that references the bank account for this forecast.
   */
  public long getBankAccountId()
  {
    return bankAccountId;
  }

  public void setBankAccountId(long bankAccountId)
  {
    this.bankAccountId = bankAccountId;
  }

  /**
   * The index in the {@link #getBalances() list of balances} to find the balance value
   * of the event.
   *
   * This value is different from the {@link #getNoticeDaySpan()}
   *
   * @return The index in the {@link #getBalances() list of balances} to find the balance value
   * of the event.
   * {@code null} if the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions.
   */
  @SuppressWarnings("unused")
  public Integer getNoticeBalanceIndex() {
    return noticeBalanceIndex;
  }

  public void setNoticeBalanceIndex(Integer noticeBalanceIndex) {
    this.noticeBalanceIndex = noticeBalanceIndex;
  }

  /**
   * @return the date when the projected balance (at end of the day) will be at its minimum.
   * {@code null} if the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions.
   */
  @SuppressWarnings("unused")
  public LinxoDate getMinimumDate() {
    return minimumDate;
  }

  public void setMinimumDate(LinxoDate minimumDate) {
    this.minimumDate = minimumDate;
  }

  /**
   * The number of days between today and the date
   * when the projected balance (at end of the day) will be at its minimum.
   * <ul>
   *   <li>0 : today</li>
   *   <li>1 : tomorrow</li>
   *   <li>...</li>
   *   <li>n : n-th day after today</li>
   * </ul>
   *
   * This value is different from the {@link #getMinimumBalanceIndex()}
   *
   * @return the number of days between today and the date
   * when the projected balance (at end of the day) will be at its minimum.
   * {@code null} if the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions.
   */
  @SuppressWarnings("unused")
  public Integer getMinimumDaySpan() {
    return minimumDaySpan;
  }

  public void setMinimumDaySpan(Integer minimumDaySpan) {
    this.minimumDaySpan = minimumDaySpan;
  }

  /**
   * Projected value of the account balance
   * when the projected balance (at end of the day) will be at its minimum.
   *
   * The result is equivalent to {@code getBalances().get(getMinimumBalanceIndex)}
   *
   * @return the projected value of the account balance
   * when the projected balance (at end of the day) will be at its minimum.
   * {@code null} if the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions.
   */
  @SuppressWarnings("unused")
  public Double getMinimumBalance() {
    return minimumBalance;
  }

  public void setMinimumBalance(Double minimumBalance) {
    this.minimumBalance = minimumBalance;
  }

  /**
   * The index in the {@link #getBalances() list of balances} to find the minimum balance value.
   *
   * This value is different from the {@link #getMinimumDaySpan()}
   *
   * @return The index in the {@link #getBalances() list of balances} to find the minimum balance value.
   * {@code null} if the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions.
   */
  @SuppressWarnings("unused")
  public Integer getMinimumBalanceIndex() {
    return minimumBalanceIndex;
  }

  public void setMinimumBalanceIndex(Integer minimumBalanceIndex) {
    this.minimumBalanceIndex = minimumBalanceIndex;
  }

  /**
   * The status to display for this account
   *
   * @return The status to display for this account
   * If the user has not the
   * {@link com.linxo.client.data.permissions.Feature#UpcomingTransactions permission associated}
   * with the Upcoming Transactions, the status will be based on the {@link #getBalance() current balance}.
   */
  @SuppressWarnings("unused")
  public NoticeState getMinimumState() {
    return minimumState;
  }

  public void setMinimumState(NoticeState minimumState) {
    this.minimumState = minimumState;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("BankAccountForecast{");
    sb.append("balance=").append(balance);
    sb.append(", balances=").append(balances);
    sb.append(", noticeDate=").append(noticeDate);
    sb.append(", noticeDaySpan=").append(noticeDaySpan);
    sb.append(", noticeBalance=").append(noticeBalance);
    sb.append(", noticeBalanceIndex=").append(noticeBalanceIndex);
    sb.append(", minimumDate=").append(minimumDate);
    sb.append(", minimumDaySpan=").append(minimumDaySpan);
    sb.append(", minimumBalance=").append(minimumBalance);
    sb.append(", minimumBalanceIndex=").append(minimumBalanceIndex);
    sb.append(", minimumState=").append(minimumState);
    sb.append(", bankAccountId=").append(bankAccountId);
    sb.append('}');
    return sb.toString();
  }

  @SuppressWarnings("RedundantIfStatement") // to facilitate reading
  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (!(o instanceof BankAccountForecast)) return false;

    BankAccountForecast forecast = (BankAccountForecast) o;

    if (Double.compare(forecast.balance, balance) != 0) return false;
    if (bankAccountId != forecast.bankAccountId) return false;
    if (balances != null ? !balances.equals(forecast.balances) : forecast.balances != null) return false;
    if (minimumBalance != null ? !minimumBalance.equals(forecast.minimumBalance) : forecast.minimumBalance != null)
      return false;
    if (minimumBalanceIndex != null ? !minimumBalanceIndex.equals(forecast.minimumBalanceIndex) : forecast.minimumBalanceIndex != null)
      return false;
    if (minimumDate != null ? !minimumDate.equals(forecast.minimumDate) : forecast.minimumDate != null) return false;
    if (minimumDaySpan != null ? !minimumDaySpan.equals(forecast.minimumDaySpan) : forecast.minimumDaySpan != null)
      return false;
    if (minimumState != forecast.minimumState) return false;
    if (noticeBalance != null ? !noticeBalance.equals(forecast.noticeBalance) : forecast.noticeBalance != null)
      return false;
    if (noticeBalanceIndex != null ? !noticeBalanceIndex.equals(forecast.noticeBalanceIndex) : forecast.noticeBalanceIndex != null)
      return false;
    if (noticeDate != null ? !noticeDate.equals(forecast.noticeDate) : forecast.noticeDate != null) return false;
    if (noticeDaySpan != null ? !noticeDaySpan.equals(forecast.noticeDaySpan) : forecast.noticeDaySpan != null)
      return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result;
    long temp;
    temp = Double.doubleToLongBits(balance);
    result = (int) (temp ^ (temp >>> 32));
    result = 31 * result + (balances != null ? balances.hashCode() : 0);
    result = 31 * result + (noticeDate != null ? noticeDate.hashCode() : 0);
    result = 31 * result + (noticeDaySpan != null ? noticeDaySpan.hashCode() : 0);
    result = 31 * result + (noticeBalance != null ? noticeBalance.hashCode() : 0);
    result = 31 * result + (noticeBalanceIndex != null ? noticeBalanceIndex.hashCode() : 0);
    result = 31 * result + (minimumDate != null ? minimumDate.hashCode() : 0);
    result = 31 * result + (minimumDaySpan != null ? minimumDaySpan.hashCode() : 0);
    result = 31 * result + (minimumBalance != null ? minimumBalance.hashCode() : 0);
    result = 31 * result + (minimumBalanceIndex != null ? minimumBalanceIndex.hashCode() : 0);
    result = 31 * result + (minimumState != null ? minimumState.hashCode() : 0);
    result = 31 * result + (int) (bankAccountId ^ (bankAccountId >>> 32));
    return result;
  }
}
