/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.upcoming;

import com.linxo.client.data.DateUtils;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.pfm.bank.TransactionType;
import com.linxo.client.dto.EntityInfo;
import com.linxo.infrastructure.exceptions.TechnicalException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 *
 */
public class UpcomingTransaction
    extends EntityInfo
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields


  // Static Initializers

  // Static Methods

  // Instance Fields

  private TransactionType type;
  private boolean createdByUser;
  private Long categoryId;
  private String checkNumber;
  private TemporalExpression temporalExpression;
  private String label;
  private String originalLabel;
  private double amount;
  private Long accountId;
  private String sourceAccountUid;
  private LinxoDate lastMatchedDate;
  private ArrayList<UpcomingTransactionOccurrence> modifiedOccurrences = new ArrayList<UpcomingTransactionOccurrence>();
  private Boolean externalCardPayment;
  private String reference;

  private String extRawData;
  private String extFttcCode;
  private String extBforBankCode;

  // Constructors

  /**
   * Constructor that does not set the ID
   */
  public UpcomingTransaction()
  {}

  /**
   * Constructor that sets the ID
   * @param id the internal id of this UpcomingTransaction
   */
  public UpcomingTransaction(Long id)
  {
    setId(id);
  }



  // Instance Methods

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           Getter and Setters
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  /**
   * @return the type of the transaction
   */
  public TransactionType getType()
  {
    return type;
  }

  public void setType(TransactionType type)
  {
    this.type = type;
  }

  public void setCreatedByUser(boolean createdByUser)
  {
    this.createdByUser = createdByUser;
  }

  /**
   * @return indicate whether this UpcomingTransaction was created (and is manageable ) by the User
   */
  public boolean isCreatedByUser()
  {
    return this.createdByUser;
  }

  /**
   * @return the {@link com.linxo.client.dto.tx.CatInfo#getId() Id of the Category}
   * of this UpcomingTransaction
   */
  public Long getCategoryId()
  {
    return categoryId;
  }

  public void setCategoryId(Long categoryId)
  {
    this.categoryId = categoryId;
  }

  /**
   * @return a potential CheckNumber string for this UpcomingTransaction
   */
  public String getCheckNumber()
  {
    return checkNumber;
  }

  public void setCheckNumber(String checkNumber)
  {
    this.checkNumber = checkNumber;
  }

  /**
   * @return the label for this UpcomingTransaction.
   * This label will be used to match the incoming transactions
   * and attach the UpcomingTransaction to the
   * {@link com.linxo.client.dto.tx.TransactionInfo}.
   */
  public String getLabel()
  {
    return label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  /**
   * @return the originalLabel for this UpcomingTransaction.
   */
  public String getOriginalLabel()
  {
    return originalLabel;
  }

  public void setOriginalLabel(String originalLabel)
  {
    this.originalLabel = originalLabel;
  }

  /**
   * @return the expected amount for this UpcomingTransaction.
   * This amount will be used to match the incoming transactions
   * and attach the UpcomingTransaction to the Transaction.
   */
  public double getAmount()
  {
    return amount;
  }

  public void setAmount(double amount)
  {
    this.amount = amount;
  }

  /**
   * Returns id of the BankAccountInfo to which this UpcomingTransaction is attached.
   * UpcomingTransaction objects can only be attached to
   * {@link com.linxo.client.dto.account.CheckingsAccountInfo}.
   *
   * Only Transactions of this CheckingsAccountInfo (and its potentially attached
   * {@link com.linxo.client.dto.account.CreditCardAccountInfo}) will be
   * matched against this UpcomingTransaction.
   * @return  the id of the BankAccount
   */
  public Long getAccountId()
  {
    return accountId;
  }

  public void setAccountId(Long accountId)
  {
    this.accountId = accountId;
  }

  /**
   * @return the TemporalExpression defining the occurrences of this UpcomingTransaction
   */
  public TemporalExpression getTemporalExpression()
  {
    return temporalExpression;
  }

  public void setTemporalExpression(TemporalExpression temporalExpression)
  {
    this.temporalExpression = temporalExpression;
  }

  /**
   * Returns the UID of the account that has triggered the creation of this UpcomingTransaction.
   *
   * For instances, the Credit Card payment UpcomingTransaction of a credit card account will show
   * in a CheckingsAccountInfo where the payment is going to take place, and the sourceAccountUid
   * will be the pseudo account number of the CreditCardAccountInfo.
   * @return the UID of the account that has triggered the creation of this UpcomingTransaction.
   */
  public String getSourceAccountUid()
  {
    return sourceAccountUid;
  }

  public void setSourceAccountUid(String sourceAccountUid)
  {
    this.sourceAccountUid = sourceAccountUid;
  }

  /**
   * @return the date when the last matching occurred.
   */
  public LinxoDate getLastMatchedDate()
  {
    return lastMatchedDate;
  }

  public void setLastMatchedDate(LinxoDate lastMatchedDate)
  {
    this.lastMatchedDate = lastMatchedDate;
  }

  /**
   * @return the list of modified UpcomingTransactionOccurrence objects.
   */
  public ArrayList<UpcomingTransactionOccurrence> getModifiedOccurrences()
  {
    return modifiedOccurrences;
  }

  /**
   * Used only when the {@link #type} is {@link com.linxo.client.data.pfm.bank.TransactionType#CreditCardPayment}.
   * Indicates that the associated card is on an AccountGroup different from the BankAccountInfo
   * of this UpcomingTransaction (like an American Express card for example).
   * @return {@code true} if the associated card is on an AccountGroup different from the BankAccountInfo
   */
  public Boolean isExternalCardPayment()
  {
    return externalCardPayment;
  }

  public void setExternalCardPayment(Boolean externalCardPayment)
  {
    this.externalCardPayment = externalCardPayment;
  }

  /**
   * @return a custom reference for this UpcomingTransaction
   */
  public String getReference()
  {
    return reference;
  }

  public void setReference(String reference)
  {
    this.reference = reference;
  }

  public String getExtRawData()
  {
    return extRawData;
  }

  public void setExtRawData(String extRawData)
  {
    this.extRawData = extRawData;
  }

  public String getExtFttcCode()
  {
    return extFttcCode;
  }

  public void setExtFttcCode(String extFttcCode)
  {
    this.extFttcCode = extFttcCode;
  }

  public String getExtBforBankCode()
  {
    return extBforBankCode;
  }

  public void setExtBforBankCode(String extBforBankCode)
  {
    this.extBforBankCode = extBforBankCode;
  }


  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           API
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  /**
   * @param fromDate the first (aka minimum) date a generated occurrences may have, inclusive
   * @param toDate the last (aka maximum) date a generated occurrences may have, inclusive
   * @param includeDeleted - When true, the list will also contain the occurrences materializing the deletion
   *                       (i.e. the occurrences with the with the
   *                       {@link com.linxo.client.dto.upcoming.UpcomingTransactionOccurrence#modifiedDate}
   *                       set to {@code null}.
   * @param includeMatchingWindow - When true, include the occurrences before {@code fromDate} and after
   *                              {@code toDate} that may happen  within the timeframe because of their matching
   *                              window.
   * @return a list of sorted occurrences, from fromDate to toDate (both inclusive).
   * This method takes care of putting the modified occurrences when needed.
   *
   * Optional: the list can include the deleted ones (useful for displaying the full list of occurrences)
   *
   * NOTE: If the {@link #lastMatchedDate} exist and is after the {@code fromDate}, the occurrences are
   * computed from the day after lastMatchedDate.
   *
   * Return null if we're not able to compute occurrences
   */
  public ArrayList<UpcomingTransactionOccurrence> occurrencesFromTo(LinxoDate fromDate, LinxoDate toDate,
                                                                    boolean includeDeleted,
                                                                    boolean includeMatchingWindow)
  {
    if(fromDate == null || toDate == null || this.temporalExpression == null) {
      return null;
    }

    // If the lastMatchedDate exists and is after (bigger than) or equals to the fromDate,
    // the real fromDate is the day after the lastMatchedDate.
    final LinxoDate realFromDate =
        lastMatchedDate != null && lastMatchedDate.compareTo(fromDate) >= 0
            ?DateUtils.addDays(lastMatchedDate, 1)
            :fromDate;

    // It may now occur that (realFromDate > toDate) -- This means that we're querying occurrences
    // within dates that are already matched.
    // => in this case, we just return an empty list.
    //
    // This happens for instance when we look for the nextOccurrenceAfter() of a #once UT that is matched.
    // In this case, when collecting the next occurrence the parameters fromDate == toDate == lastMatchedDate.
    if(realFromDate.compareTo(toDate) > 0) {
      return new ArrayList<UpcomingTransactionOccurrence>();
    }

    // Get the occurrences without taking into account the modified ones
    ArrayList<UpcomingTransactionOccurrence> teOccurrences = temporalExpression.occurrencesFromTo(realFromDate, toDate, includeMatchingWindow);
    if(teOccurrences == null) {
      // The temporal expression is not able to determine how to compute the occurrences
      // -> we'll fallback asking the server to do it for us
      return null;
    }

    if(lastMatchedDate != null) {
      // Due to the matching window, we may have occurrences that are before or on lastMatchedDate =>
      // We have to remove them. Luckily, this list is correctly ordered so that when the displayDate
      // is strictly after {@code lastMatchedDate} we can stop removing and break the loop...
      Iterator<UpcomingTransactionOccurrence> removeIterator = teOccurrences.iterator();
      while(removeIterator.hasNext()) {
        if(removeIterator.next().getExpectedDate().compareTo(lastMatchedDate) > 0)
        {
          break;
        }
        removeIterator.remove();
      }
    }

    // If there is no modified occurrence, we just fill the occurrences and return them
    if(modifiedOccurrences == null || modifiedOccurrences.size() == 0) {

      final ArrayList<UpcomingTransactionOccurrence> filled
          = fill(teOccurrences);


      return filled;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // The principle is to remove from the list of "regular" occurrences, the ones that are modified.
    // Then we review the list of modifiedOccurrences and add the ones that have a modifiedDate in the
    // time criteria and "add" the "removal" ones if queried.
    /////////////////////////////////////////////////////////////////////////////////////////////////////

    ArrayList<UpcomingTransactionOccurrence> occurrences = new ArrayList<UpcomingTransactionOccurrence>();

    for(UpcomingTransactionOccurrence occ : teOccurrences) {

      // See if we have a modified occurrence for this computed occurrence
      UpcomingTransactionOccurrence modOcc = null;

      for(UpcomingTransactionOccurrence mo : modifiedOccurrences) {
        if(occ.getOriginalDate().equals(mo.getOriginalDate())) {
          // A modified occurrence has its original date match the given date
          // -> we have a modified occurrence, it's (possible) modified date is stored in modifiedDate
          modOcc = mo;
          break;
        }
      }

      if(modOcc == null)
      {
        // No modified occurrence, just fill and add the computed occurrence
        occurrences.add(fill(occ));
      }
      // If we have a modified occurrence for this computed occurrence.
      // We skip the current occurrence and will find the modified one when we iterate over the
      // complete list of modified occurrences.
    }

    // We may now have modified occurrences for occurrences out of the temporal expression,
    // but that have a modified date within our search dates.
    // We also need to update the search dates to take care of the matching window around
    // each occurrence and the lastMatchedDate when requested to do so.
    final LinxoDate matchingFromDate =
        (includeMatchingWindow)
            ?DateUtils.addDays(fromDate, -this.temporalExpression.getDaysAfterMatching())
            :fromDate;
    // In case {@code includeMatchingWindow == false}, {@code realMatchingFromDate == realFromDate}.
    final LinxoDate realMatchingFromDate =
        (lastMatchedDate != null && lastMatchedDate.compareTo(matchingFromDate) >= 0)
            ?DateUtils.addDays(lastMatchedDate, 1)
            :matchingFromDate;

    final LinxoDate matchingToDate =
        (includeMatchingWindow)
            ?DateUtils.addDays(toDate, this.temporalExpression.getDaysBeforeMatching())
            :toDate;
    for(UpcomingTransactionOccurrence mo : modifiedOccurrences) {
      if (mo.getModifiedDate() != null) {
        // If the date is modified, then include the occurrence if it's still
        // between start and end dates
        if (mo.getModifiedDate().isBetween(realMatchingFromDate, matchingToDate, true)) {
          occurrences.add(mo);
        }
      }
      else {
        // Otherwise this is a removed occurrence, we include it if we
        // were queried to and if the original date matches the criteria.
        if (includeDeleted && mo.getOriginalDate().isBetween(realMatchingFromDate, matchingToDate, true)) {
          occurrences.add(mo);
        }
      }
    }

    // Sort based on the occurrences' comparator
    Collections.sort(occurrences);

    return occurrences;
  }

  /**
   * NOTE: This method includes the occurrences due to the matching window.
   *
   * @param date - must be not null, or an exception is thrown.
   * @return the next occurrence at or after the given date,
   * or null if no occurrence happens after this date.
   * @throws NullPointerException if the given date is null
   * @throws TechnicalException if there is an error computing the occurrences
   */
  public UpcomingTransactionOccurrence nextOccurrenceAfter(LinxoDate date)
      throws NullPointerException, TechnicalException
  {
    if (date == null){
      throw new NullPointerException("date should not be null");
    }

    // We will ask the expected nextOccurrenceAfter date to the temporal expression and then check that there
    // is no modified occurrence between the date returned by the temporal expression and the given argument.
    // If there is one, we return it. If there is none, we iterate the same process, but we use the last date
    // returned by the temporal expression as an argument until we reach the end Date if there is one.
    //
    // If there is no end date, we might iterate infinitely. That would mean that there is an infinite list
    // of modified occurrences, and this last condition is impossible.

    LinxoDate currentStart = date;
    UpcomingTransactionOccurrence occurrence = null;
    while(occurrence == null && (
        temporalExpression.getEndDate() == null || temporalExpression.getEndDate().compareTo(currentStart) >= 0))
    {
      LinxoDate firstPossible = DateUtils.addDays(currentStart, -temporalExpression.getDaysAfterMatching());
      LinxoDate expectedDate = temporalExpression.nextOccurrenceAfter(firstPossible);
      if(expectedDate == null)
      {
        // There is no "regular" occurrence after currentStart. But we might have modified occurrences
        // between currentStart and the endDate if we have one.
        if(temporalExpression.getEndDate() == null){
          // We have no end date, we can break safely.
          break;
        }
        expectedDate = temporalExpression.getEndDate();
      }

      if(currentStart.compareTo(expectedDate) > 0) {
        expectedDate = currentStart;
      }

      ArrayList<UpcomingTransactionOccurrence> occurrences = occurrencesFromTo(currentStart, expectedDate, false, true);
      if (occurrences == null) {
        throw new TechnicalException("Impossible to get the occurrences between ["+currentStart+"] and ["+expectedDate+"]");
      }

      if(occurrences.isEmpty()) {
        // There is no occurrence on 'expectedDate' => we continue from the next day.
        currentStart = DateUtils.addDays(expectedDate, 1);
        continue;
      }

      // At this point, we have at least one occurrence. The list being sorted, the first element
      // is the earliest => we just set the occurrence to return: this stops the loop.
      occurrence = occurrences.get(0);
    }

    // At this point, we exited the loop because we have an occurrence, or because the loop went after the end date.
    return occurrence;
  }

  private ArrayList<UpcomingTransactionOccurrence> fill(ArrayList<UpcomingTransactionOccurrence> occurrences)
  {
    for(UpcomingTransactionOccurrence occurrence : occurrences)
    {
      fill(occurrence);
    }

    return occurrences;
  }

  private UpcomingTransactionOccurrence fill(UpcomingTransactionOccurrence occurrence)
  {
    occurrence.setAccountId(accountId);
    occurrence.setAmount(amount);
    occurrence.setCategoryId(categoryId);
    occurrence.setCheckNumber(checkNumber);
    occurrence.setLabel(label);
    occurrence.setUpcomingTransaction(this);

    return occurrence;
  }


  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer("UpcomingTransaction{");
    sb.append("amount=").append(amount);
    sb.append(", label='").append(label).append('\'');
    sb.append(", originalLabel='").append(originalLabel).append('\'');
    sb.append(", temporalExpression=").append(temporalExpression);
    sb.append(", lastMatchedDate=").append(lastMatchedDate);
    sb.append(", externalCardPayment=").append(externalCardPayment);
    sb.append(", reference='").append(reference).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
