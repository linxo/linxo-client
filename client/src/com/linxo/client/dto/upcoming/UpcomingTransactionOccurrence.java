/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.upcoming;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.dto.EntityInfo;
import com.linxo.client.actions.support.json.Exclude;

import java.text.Collator;
import java.util.Locale;

import static com.linxo.client.data.DateUtils.*;

/**
 *
 */
public class UpcomingTransactionOccurrence
  extends EntityInfo
  implements Comparable<UpcomingTransactionOccurrence>
{
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields


  // Static Initializers

  // Static Methods

  public static UpcomingTransactionOccurrence occurrenceWithDate(LinxoDate date)
  {
    return new UpcomingTransactionOccurrence(date);
  }


  // Instance Fields

  private LinxoDate originalDate;

  @Exclude  // Not serialized in JSON
  private UpcomingTransaction upcomingTransaction;

  // Optional : if missing -> the occurrence is deleted. otherwise matches originalDate or modifies it.
  private LinxoDate modifiedDate;

  private Long categoryId;

  private String checkNumber;

  private String label;

  private double amount;

  private Long accountId;

  // Constructors

  /**
   * Constructor that does not set the ID
   */
  public UpcomingTransactionOccurrence(){}

  /**
   * Constructor that does not set the ID
   * @param id the internal id of this occurrence
   */
  public UpcomingTransactionOccurrence(Long id)
  {
    setId(id);
  }

  /**
   * Constructor that sets the modified and original date to {date}. In other words, it creates an
   * occurrence
   * NOTE : This constructor does not set the id.
   *
   * @param date - the value of the {@link #getOriginalDate() originalDate} and {@link #getModifiedDate() modifiedDate}
   */
  private UpcomingTransactionOccurrence(LinxoDate date)
  {
    this.originalDate = date;
    this.modifiedDate = date;
  }


  // Instance Methods

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           Getter and Setters
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  public Long getCategoryId()
  {
    return categoryId;
  }

  public void setCategoryId(Long categoryId)
  {
    this.categoryId = categoryId;
  }

  public String getCheckNumber()
  {
    return checkNumber;
  }

  public void setCheckNumber(String checkNumber)
  {
    this.checkNumber = checkNumber;
  }

  public String getLabel()
  {
    return label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public double getAmount()
  {
    return amount;
  }

  public void setAmount(double amount)
  {
    this.amount = amount;
  }

  public Long getAccountId()
  {
    return accountId;
  }

  public void setAccountId(Long accountId)
  {
    this.accountId = accountId;
  }

  public LinxoDate getOriginalDate()
  {
    return originalDate;
  }

  public void setOriginalDate(LinxoDate originalDate)
  {
    this.originalDate = originalDate;
  }

  public LinxoDate getModifiedDate()
  {
    return modifiedDate;
  }

  public void setModifiedDate(LinxoDate modifiedDate)
  {
    this.modifiedDate = modifiedDate;
  }

  public UpcomingTransaction getUpcomingTransaction()
  {
    return upcomingTransaction;
  }

  public void setUpcomingTransaction(UpcomingTransaction upcomingTransaction)
  {
    this.upcomingTransaction = upcomingTransaction;
  }

//-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //          Implements Comparable
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  @SuppressWarnings("NullableProblems") // other ??
  @Override
  public int compareTo(UpcomingTransactionOccurrence other)
      throws NullPointerException
  {
    // interface contract
    if(other == null) {
      throw new NullPointerException("other value is null while testing equality");
    }

    if(this == other) return 0;

    // Compare the date first
    int dateCompare = this.getExpectedDate().compareTo(other.getExpectedDate());
    if(dateCompare != 0) {
      return dateCompare;
    }

    // If dates equal, then sort by amount desc (i.e. the opposite of the natural order on numbers)
    double amountDiff = this.amount - other.amount;
    if(amountDiff < 0) {
      return +1;
    } else if(amountDiff > 0) {
      return -1;
    }

    // If dates and amounts equal, then sort by label, alphanumeric, case insensitive, diacritic
    Collator collator = Collator.getInstance(Locale.FRANCE); // todo APP-5341 : france ?? To set the strength, the locale is required ??
    collator.setStrength(Collator.PRIMARY);
    return collator.compare(this.label, other.label);
  }



  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //                 API
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  /**
   * @param date the date to evaluate
   * @return {@code true} if the given date is between
   * {@code {@link #getExpectedDate displayDate} - daysBeforeMatching} and
   * {@code {@link #getExpectedDate displayDate} + daysAfterMatching} inclusive.
   * Returns {@code false} otherwise.
   *
   * @see TemporalExpression#daysBeforeMatching
   * @see TemporalExpression#daysAfterMatching
   */
  public boolean isInMatchingWindow(LinxoDate date)
  {
    LinxoDate minDate = addDays(getExpectedDate(), -getUpcomingTransaction().getTemporalExpression().getDaysBeforeMatching());
    LinxoDate maxDate = addDays(getExpectedDate(), +getUpcomingTransaction().getTemporalExpression().getDaysAfterMatching());

    if(date.compareTo(minDate) < 0) {
      return false;
    }

    //noinspection RedundantIfStatement
    if(maxDate.compareTo(date) < 0) {
      return false;
    }

    return true;
  }

  public LinxoDate getDisplayDate()
  {
    LinxoDate expectedDate = getExpectedDate();
    if(expectedDate.compareTo(LinxoDate.today()) <= 0)
    {
      return LinxoDate.today();
    }

    return expectedDate;
  }

  public LinxoDate getExpectedDate()
  {
    if(this.modifiedDate != null) {
      return this.modifiedDate;
    }

    return this.originalDate;
  }

  public boolean isDeleted()
  {
    return this.modifiedDate == null;
  }

  public boolean isDateModified()
  {
    return this.modifiedDate == null || !(this.originalDate.equals(this.modifiedDate));
  }

  /**
   * NOTE: This method is to be used when the user modifies the fist occurrence of an {@link UpcomingTransaction} and
   * selects to modify "This one and all the next ones". In this case, we do not create an exception, but modify
   * the {@link #upcomingTransaction attached UpcomingTransaction}.
   *
   * NOTE 2: This method always return true when the UpcomingTransaction has a
   * {@link com.linxo.client.data.upcoming.IntervalUnit#once once IntervalUnit}.
   *
   * @return {@code true} if the current occurrence is the first occurrence of the attached
   * {@link #upcomingTransaction UpcomingTransaction}. {@code false} otherwise.
   */
  public boolean isFirstOccurrence()
  {
    return this.originalDate.equals(upcomingTransaction.getTemporalExpression().getStartDate());
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (!(o instanceof UpcomingTransactionOccurrence)) return false;

    UpcomingTransactionOccurrence that = (UpcomingTransactionOccurrence) o;

    if (Double.compare(that.amount, amount) != 0) return false;
    if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) return false;
    if (categoryId != null ? !categoryId.equals(that.categoryId) : that.categoryId != null) return false;
    if (checkNumber != null ? !checkNumber.equals(that.checkNumber) : that.checkNumber != null) return false;
    if (label != null ? !label.equals(that.label) : that.label != null) return false;
    if (modifiedDate != null ? !modifiedDate.equals(that.modifiedDate) : that.modifiedDate != null) return false;
    if (!originalDate.equals(that.originalDate)) return false;
    if (upcomingTransaction != null ? !upcomingTransaction.equals(that.upcomingTransaction) : that.upcomingTransaction != null) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result;
    long temp;
    result = originalDate.hashCode();
    result = 31 * result + (upcomingTransaction != null ? upcomingTransaction.hashCode() : 0);
    result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
    result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
    result = 31 * result + (checkNumber != null ? checkNumber.hashCode() : 0);
    result = 31 * result + (label != null ? label.hashCode() : 0);
    temp = Double.doubleToLongBits(amount);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
    return result;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString,StringBufferMayBeStringBuilder
    final StringBuffer sb = new StringBuffer("UpcomingTransactionOccurrence{");
    sb.append("originalDate=").append(originalDate);
    sb.append(", upcomingTransaction=").append(upcomingTransaction);
    sb.append(", modifiedDate=").append(modifiedDate);
    sb.append(", categoryId=").append(categoryId);
    sb.append(", checkNumber='").append(checkNumber).append('\'');
    sb.append(", label='").append(label).append('\'');
    sb.append(", amount=").append(amount);
    sb.append(", accountId=").append(accountId);
    sb.append('}');
    return sb.toString();
  }
}
