/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 3:09:43 PM by tarunmalhotra.
*/
package com.linxo.client.dto.device;

import com.linxo.client.dto.EntityInfo;

import java.util.Date;

public final class DeviceAuthInfo extends EntityInfo
{
  private String              deviceType;
  private String              deviceFamily;
  private String              deviceId;
  private String              deviceName;
  private Date                deviceAuthorizationTime;

  @SuppressWarnings("unused")
  private DeviceAuthInfo(){}

  public DeviceAuthInfo(String deviceType, String deviceFamily, String deviceId, String deviceName, Date deviceAuthorizationTime)
  {
    this.deviceType = deviceType;
    this.deviceFamily = deviceFamily;
    this.deviceId = deviceId;
    this.deviceName = deviceName;
    this.deviceAuthorizationTime = deviceAuthorizationTime;
  }

  public String getDeviceFamily()
  {
    return deviceFamily;
  }

  public String getDeviceId()
  {
    return deviceId;
  }

  public String getDeviceType()
  {
    return deviceType;
  }

  public String getDeviceName()
  {
    return deviceName;
  }

  public Date getDeviceAuthorizationTime()
  {
    return deviceAuthorizationTime;
  }
}
