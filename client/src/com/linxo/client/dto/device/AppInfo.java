/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 29/10/2012 by hugues.
 */
package com.linxo.client.dto.device;


import java.io.Serializable;

public final class AppInfo implements Serializable
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private String appIdentifier;
  private String appVersion;
  private Boolean upgradeRequired;

  // Instance Initializers

  // Constructors

  public AppInfo(){}

  public AppInfo(String appIdentifier, String appVersion, Boolean upgradeRequired)
  {
    this.appIdentifier = appIdentifier;
    this.appVersion = appVersion;
    this.upgradeRequired = upgradeRequired;
  }

  // Instance Methods


  public void setAppIdentifier(String appIdentifier)
  {
    this.appIdentifier = appIdentifier;
  }

  public String getAppIdentifier()
  {
    return appIdentifier;
  }

  public void setAppVersion(String appVersion)
  {
    this.appVersion = appVersion;
  }

  public String getAppVersion()
  {
    return appVersion;
  }

  public void setUpgradeRequired(Boolean upgradeRequired)
  {
    this.upgradeRequired = upgradeRequired;
  }

  public Boolean getUpgradeRequired()
  {
    return upgradeRequired;
  }
}
