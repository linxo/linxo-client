/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.dto;

import com.linxo.client.shared.AssertNotNull;

import java.io.Serializable;

public abstract class EntityInfo implements Serializable {

  public static final String ID = "id";

  @AssertNotNull
  private Long id;

  protected final void setId( Long id )
  {
    this.id = id;
  }

  /**
   * Returns the Long representation of the Object internal DB ID (Long)
   * @return the Long representation of the Object internal DB ID (Long)
   */
  public final Long getId()
  {
    return id;
  }


  /**
   * Returns the String representation of the internal DB ID (Long).
   * If the object is not yet defined (it has no id),
   * this methods returns <code>null</code>.
   *
   * @return the String representation of the Object internal DB ID (Long)
   */
  public final String getIdAsString()
  {
    return ( id != null ) ? id.toString() : null;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("EntityInfo{");
    sb.append("id=").append(id);
    sb.append('}');
    return sb.toString();
  }
}
