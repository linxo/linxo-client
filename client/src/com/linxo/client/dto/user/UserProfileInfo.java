/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 12:19:15 PM by tarunmalhotra.
*/
package com.linxo.client.dto.user;

import com.linxo.client.dto.EntityInfo;

import java.util.List;

public final class UserProfileInfo extends EntityInfo
{
  private String title;
  private String firstName;
  private String lastName;
  private String zipCode;
  private String address;
  private String address2;
  private String city;
  private String residence;
  private int birthDay;
  private int birthMonth;
  private int birthYear;
  private int adults;
  private int infants = -1;
  private String maritalStatus;
  private String profession;
  private String email;
  private String unverifiedEmail;
  private String sponsorCode;
  private boolean isSponsorUserDefined;
  private int sponsoredCount;
  private boolean isEmailVerified;
  private String secretQuestion;
  private String xid;
  private List<SocialUserInfo> socialUserInfos = null;

  @SuppressWarnings("unused") // s11n
  private UserProfileInfo()
  {}

  public UserProfileInfo(long id)
  {
    setId(id);
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }

  public String getResidence()
  {
    return residence;
  }

  public void setResidence(String residence)
  {
    this.residence = residence;
  }


  public int getAdults()
  {
    return adults;
  }

  public void setAdults(int adults)
  {
    this.adults = adults;
  }

  public int getInfants()
  {
    return infants;
  }

  public void setInfants(int infants)
  {
    this.infants = infants;
  }

  public String getMaritalStatus()
  {
    return maritalStatus;
  }

  public void setMaritalStatus(String maritalStatus)
  {
    this.maritalStatus = maritalStatus;
  }

  public String getProfession()
  {
    return profession;
  }

  public void setProfession(String profession)
  {
    this.profession = profession;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getZipCode()
  {
    return zipCode;
  }

  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }

  public String getAddress()
  {
    return address;
  }

  public void setAddress(String address)
  {
    this.address = address;
  }

  public String getAddress2()
  {
    return address2;
  }

  public void setAddress2(String address2)
  {
    this.address2 = address2;
  }

  public String getCity()
  {
    return city;
  }

  public void setCity(String city)
  {
    this.city = city;
  }

  public int getBirthDay()
  {
    return birthDay;
  }

  public void setBirthDay(int birthDay)
  {
    this.birthDay = birthDay;
  }

  public int getBirthMonth()
  {
    return birthMonth;
  }

  public void setBirthMonth(int birthMonth)
  {
    this.birthMonth = birthMonth;
  }

  public void setBirthYear(int birthYear)
  {
    this.birthYear = birthYear;
  }

  public int getBirthYear()
  {
    return birthYear;
  }

  public String getSponsorCode()
  {
    return sponsorCode;
  }

  public void setSponsorCode(String sponsorCode)
  {
    this.sponsorCode = sponsorCode;
  }

  public boolean isSponsorUserDefined()
  {
    return isSponsorUserDefined;
  }

  public void setSponsorUserDefined(boolean isSponsorUserDefined)
  {
    this.isSponsorUserDefined = isSponsorUserDefined;
  }

  public int getSponsoredCount()
  {
    return sponsoredCount;
  }

  public void setSponsoredCount(int sponsoredCount)
  {
    this.sponsoredCount = sponsoredCount;
  }

  public String getUnverifiedEmail()
  {
    return unverifiedEmail;
  }

  public void setUnverifiedEmail(String unverifiedEmail)
  {
    this.unverifiedEmail = unverifiedEmail;
  }

  public boolean isEmailVerified() {
    return isEmailVerified;
  }

  public void setEmailVerified(boolean emailVerified) {
    isEmailVerified = emailVerified;
  }

  public String getXid()
  {
    return xid;
  }

  public void setXid(String xid)
  {
    this.xid = xid;
  }

  /**
   * When this information is null, the secret question &amp; answer are not defined.
   * @return the content of the secret question.
   */
  public String getSecretQuestion()
  {
    return secretQuestion;
  }

  public void setSecretQuestion(String secretQuestion)
  {
    this.secretQuestion = secretQuestion;
  }

  public List<SocialUserInfo> getSocialUserInfos()
  {
    return socialUserInfos;
  }

  public void setSocialUserInfos(List<SocialUserInfo> socialUserInfos)
  {
    this.socialUserInfos = socialUserInfos;
  }
}
