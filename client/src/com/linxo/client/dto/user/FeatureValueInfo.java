/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.user;

import com.linxo.client.data.permissions.Feature;
import com.linxo.client.dto.EntityInfo;

public final class FeatureValueInfo
    extends EntityInfo
{
  private Feature feature;
  private Integer value;

  @SuppressWarnings("unused")
  private FeatureValueInfo()
  {
    this.feature = null;
    this.value = null;
  }

  public FeatureValueInfo(final Feature feature)
  {
    this.feature = feature;
    this.value = null;
  }

  public FeatureValueInfo(final Feature feature, final Integer value)
  {
    this.feature = feature;
    this.value = value;
  }

  public Feature getFeature()
  {
    return feature;
  }

  public Integer getValue()
  {
    return value;
  }

  @Override
  public String toString()
  {
    return "FeatureValueInfo{"
        + "feature=" + feature
        + ", value=" + value
        + '}';
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FeatureValueInfo)) {
      return false;
    }

    final FeatureValueInfo that = (FeatureValueInfo) o;

    if (feature != that.feature) {
      return false;
    }
    //noinspection RedundantIfStatement
    if (value != null ? !value.equals(that.value) : that.value != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = feature.hashCode();
    result = 31 * result + (value != null ? value.hashCode() : 0);
    return result;
  }

}
