/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 07/02/2013 by hugues.
 */
package com.linxo.client.dto.user;

import com.linxo.client.data.permissions.Feature;

import java.io.Serializable;

public final class PermissionInfo implements Serializable {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private Feature feature;
  private Integer value;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private PermissionInfo()
  {}

  public PermissionInfo(Feature feature)
  {
    this.feature = feature;
    this.value = null;
  }

  public PermissionInfo(Feature feature, Integer value)
  {
    this.value = value;
    this.feature = feature;
  }

  // Instance Methods


  public Feature getFeature()
  {
    return feature;
  }

  public Integer getValue()
  {
    return value;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (!(o instanceof PermissionInfo)) return false;

    PermissionInfo that = (PermissionInfo) o;

    if (feature != that.feature) return false;
    //noinspection RedundantIfStatement
    if (value != null ? !value.equals(that.value) : that.value != null) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = feature.hashCode();
    result = 31 * result + (value != null ? value.hashCode() : 0);
    return result;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString,StringBufferMayBeStringBuilder
    final StringBuffer sb = new StringBuffer("PermissionInfo{");
    sb.append("feature=").append(feature);
    sb.append(", value=").append(value);
    sb.append('}');
    return sb.toString();
  }
}
