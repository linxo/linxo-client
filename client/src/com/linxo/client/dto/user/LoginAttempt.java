/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.user;

import com.linxo.client.dto.EntityInfo;
import com.linxo.client.data.auth.BlockedReason;
import com.linxo.client.data.auth.LoginChannel;

import java.util.Date;

/**
 * DTO for the {@link LoginAttemptEntity} class.
 */
public final class LoginAttempt
    extends EntityInfo
{
  private Date timestamp;
  private boolean success;
  private String loginIdentifier;
  private String IPAddress;
  private boolean blocking;
  private LoginChannel loginChannel;
  private BlockedReason blockedReason;
  private Long userId;

  @SuppressWarnings("unused") // s11n
  private LoginAttempt() {}

  public LoginAttempt(final Long id, final Date timestamp, final boolean success,
                      final String loginIdentifier, final String IPAddress,
                      final boolean blocking, final LoginChannel loginChannel,
                      final BlockedReason blockedReason, final Long userId)
  {
    setId(id);
    this.timestamp = timestamp;
    this.success = success;
    this.loginIdentifier = loginIdentifier;
    this.IPAddress = IPAddress;
    this.blocking = blocking;
    this.loginChannel = loginChannel;
    this.blockedReason = blockedReason;
    this.userId = userId;
  }

  public Date getTimestamp()
  {
    return timestamp;
  }

  public boolean isSuccess()
  {
    return success;
  }

  public String getLoginIdentifier()
  {
    return loginIdentifier;
  }

  public String getIPAddress()
  {
    return IPAddress;
  }

  public boolean isBlocking()
  {
    return blocking;
  }

  public LoginChannel getLoginChannel()
  {
    return loginChannel;
  }

  public BlockedReason getBlockedReason()
  {
    return blockedReason;
  }

  public Long getUserId()
  {
    return userId;
  }

}
