/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.user;

import com.linxo.client.data.subscriptions.PaymentStatus;
import com.linxo.client.dto.EntityInfo;

public final class PaymentInfo
    extends EntityInfo
{
  /** Payment Date */
  private long timestamp;

  /** Payment Price in euros (from the Linxo Offer). */
  private double price;

  /**
   * Formatted price including the currency.
   * Maybe useful for debug: we'll know exactly what the user paid.
   */
  private String formattedPrice;

  /** Payment status. */
  private PaymentStatus status;

  /** Payment reference. For Paypal: pay key (used for reimbursement, etc). */
  private String reference;

  /** Reference of the original payment (in case this payment is a restore). */
  private String originalReference;

  @SuppressWarnings("unused")
  private PaymentInfo()
  {
    this.timestamp = 0;
    this.price = 0;
    this.formattedPrice = null;
    this.status = null;
    this.reference = null;
    this.originalReference = null;
  }

  public PaymentInfo(final long timestamp, final double price, final String formattedPrice,
                     final PaymentStatus status, final String reference,
                     final String originalReference)
  {
    this.timestamp = timestamp;
    this.price = price;
    this.formattedPrice = formattedPrice;
    this.status = status;
    this.reference = reference;
    this.originalReference = originalReference;
  }

  public long getTimestamp()
  {
    return timestamp;
  }

  public double getPrice()
  {
    return price;
  }

  public String getFormattedPrice()
  {
    return formattedPrice;
  }

  public PaymentStatus getStatus()
  {
    return status;
  }

  public String getReference()
  {
    return reference;
  }

  public String getOriginalReference()
  {
    return originalReference;
  }

  @Override
  public String toString()
  {
    return "PaymentInfo{"
           + super.toString()
           + ", timestamp=" + timestamp
           + ", price=" + price
           + ", formattedPrice=" + formattedPrice
           + ", status=" + status
           + ", reference='" + reference + '\''
           + ", originalReference='" + originalReference + '\''
           + '}';
  }

}
