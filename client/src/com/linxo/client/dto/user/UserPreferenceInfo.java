/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 3:44:31 AM by tarunmalhotra.
*/
package com.linxo.client.dto.user;

import com.linxo.client.data.auth.SynchroReminderPeriod;
import com.linxo.client.data.auth.ReportPeriodType;
import com.linxo.client.dto.EntityInfo;

public final class UserPreferenceInfo extends EntityInfo
{
  private boolean acceptPartnerOffers;
  private ReportPeriodType reportPeriod;
  private SynchroReminderPeriod synchroReminderPeriod;
  private int creditAlertAmount;
  private int debitAlertAmount;
  private int balanceAlertAmount;
  private int bankChargeAlertAmount;
  private boolean autoSsoUserVoice;
  private boolean isEmailVerified;

  @SuppressWarnings("unused") // s11n
  private UserPreferenceInfo(){}

  public UserPreferenceInfo(long id)
  {
    setId(id);
  }

  public boolean isAcceptPartnerOffers()
  {
    return acceptPartnerOffers;
  }

  public void setAcceptPartnerOffers(boolean acceptPartnerOffers)
  {
    this.acceptPartnerOffers = acceptPartnerOffers;
  }

  public ReportPeriodType getReportPeriod()
  {
    return reportPeriod;
  }

  public void setReportPeriod(ReportPeriodType reportPeriod)
  {
    this.reportPeriod = reportPeriod;
  }

  public int getCreditAlertAmount()
  {
    return creditAlertAmount;
  }

  public void setCreditAlertAmount(int creditAlertAmount)
  {
    this.creditAlertAmount = creditAlertAmount;
  }

  public int getDebitAlertAmount()
  {
    return debitAlertAmount;
  }

  public void setDebitAlertAmount(int debitAlertAmount)
  {
    this.debitAlertAmount = debitAlertAmount;
  }

  public int getBalanceAlertAmount()
  {
    return balanceAlertAmount;
  }

  public void setBalanceAlertAmount(int balanceAlertAmount)
  {
    this.balanceAlertAmount = balanceAlertAmount;
  }

  public int getBankChargeAlertAmount()
  {
    return bankChargeAlertAmount;
  }

  public void setBankChargeAlertAmount(int bankChargeAlertAmount)
  {
    this.bankChargeAlertAmount = bankChargeAlertAmount;
  }

  public boolean isAutoSsoUserVoice()
  {
    return autoSsoUserVoice;
  }

  public void setAutoSsoUserVoice(boolean autoSsoUserVoice)
  {
    this.autoSsoUserVoice = autoSsoUserVoice;
  }

  public SynchroReminderPeriod getSynchroReminderPeriod()
  {
    return synchroReminderPeriod;
  }

  public void setSynchroReminderPeriod(SynchroReminderPeriod synchroReminderPeriod)
  {
    this.synchroReminderPeriod = synchroReminderPeriod;
  }

  public boolean isEmailVerified() {
    return isEmailVerified;
  }

  public void setEmailVerified(boolean emailVerified) {
    isEmailVerified = emailVerified;
  }
}
