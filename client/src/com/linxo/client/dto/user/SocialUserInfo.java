/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 12:19:15 PM by tarunmalhotra.
*/
package com.linxo.client.dto.user;

import com.linxo.client.dto.EntityInfo;

public final class SocialUserInfo extends EntityInfo
{

  private Long   expireTime;
  private String providerUserId;
  private String providerId;
  private String secret;
  private String displayName;
  private String profileUrl;
  private String imageUrl;
  private String accessToken;
  private String refreshToken;
  private int    rank;

  @SuppressWarnings("unused") // s11n
  public SocialUserInfo() // public to be used in the Android app
  {}

  public SocialUserInfo(long id)
  {
    setId(id);
  }


  public String getProviderId()
  {
    return providerId;
  }

  public void setProviderId(String providerId)
  {
    this.providerId = providerId;
  }

  public String getAccessToken()
  {
    return accessToken;
  }

  public void setAccessToken(String accessToken)
  {
    this.accessToken = accessToken;
  }

  public Long getExpireTime()
  {
    return expireTime;
  }

  public void setExpireTime(Long expireTime)
  {
    this.expireTime = expireTime;
  }

  public String getProviderUserId()
  {
    return providerUserId;
  }

  public void setProviderUserId(String providerUserId)
  {
    this.providerUserId = providerUserId;
  }

  public String getSecret()
  {
    return secret;
  }

  public void setSecret(String secret)
  {
    this.secret = secret;
  }

  public String getDisplayName()
  {
    return displayName;
  }

  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }

  public String getProfileUrl()
  {
    return profileUrl;
  }

  public void setProfileUrl(String profileUrl)
  {
    this.profileUrl = profileUrl;
  }

  public String getImageUrl()
  {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl)
  {
    this.imageUrl = imageUrl;
  }

  public String getRefreshToken()
  {
    return refreshToken;
  }

  public void setRefreshToken(String refreshToken)
  {
    this.refreshToken = refreshToken;
  }

  public int getRank()
  {
    return rank;
  }

  public void setRank(int rank)
  {
    this.rank = rank;
  }
}
