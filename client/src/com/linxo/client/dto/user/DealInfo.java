/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 1:26:23 PM by tarunmalhotra.
*/
package com.linxo.client.dto.user;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.subscriptions.BillingCycle;
import com.linxo.client.data.subscriptions.DealSource;
import com.linxo.client.dto.EntityInfo;

import java.util.Date;

public final class DealInfo
    extends EntityInfo
{
  private Date creationTime;
  private Date endTime;

  private boolean recurring;
  private BillingCycle billingCycle;
  private double cyclePrice;
  private String commercialName;
  private DealSource dealSource;
  private Date nextPaymentDate;
  private Date lastRenewalMailSentTime;
  private LinxoDate renewalSuggestionTime;

  @SuppressWarnings("unused") // s11n
  private DealInfo()
  {}

  public DealInfo(final Long id, final Date creationTime, final Date endTime, final boolean recurring,
                  final BillingCycle billingCycle, final double cyclePrice,
                  final String commercialName, final DealSource dealSource,
                  final Date nextPaymentDate, final LinxoDate renewalSuggestionTime)
  {
    setId(id);
    this.commercialName = commercialName;
    this.creationTime = creationTime;
    this.dealSource = dealSource;
    this.endTime = endTime;
    this.billingCycle = billingCycle;
    this.recurring = recurring;
    this.cyclePrice = cyclePrice;
    this.nextPaymentDate = nextPaymentDate;
    this.renewalSuggestionTime = renewalSuggestionTime;
  }

  public String getCommercialName()
  {
    return commercialName;
  }

  public Date getCreationTime()
  {
    return creationTime;
  }

  public Date getEndTime()
  {
    return endTime;
  }

  public BillingCycle getBillingCycle()
  {
    return billingCycle;
  }

  public DealSource getDealSource()
  {
    return dealSource;
  }

  public boolean isRecurring()
  {
    return recurring;
  }

  public double getCyclePrice()
  {
    return cyclePrice;
  }

  public Date getNextPaymentDate()
  {
    return nextPaymentDate;
  }

  public LinxoDate getRenewalSuggestionTime()
  {
    return renewalSuggestionTime;
  }

  public void setLastRenewalMailSentTime(Date lastRenewalMailSentTime)
  {
    this.lastRenewalMailSentTime = lastRenewalMailSentTime;
  }

  public Date getLastRenewalMailSentTime()
  {
    return lastRenewalMailSentTime;
  }

  @Override
  public String toString()
  {
    final StringBuilder sb = new StringBuilder();
    sb.append("DealInfo");
    sb.append("{commercialName='").append(commercialName).append('\'');
    sb.append(", creationTime=").append(creationTime);
    sb.append(", endTime=").append(endTime);
    sb.append(", billingCycle=").append(billingCycle);
    sb.append(", dealSource=").append(dealSource);
    sb.append(", recurring=").append(recurring);
    sb.append(", cyclePrice=").append(cyclePrice);
    sb.append(", nextPaymentDate=").append(nextPaymentDate);
    sb.append(", renewalSuggestionTime=").append(renewalSuggestionTime);
    sb.append(", lastRenewalMailSentTime=").append(lastRenewalMailSentTime);
    sb.append('}');
    return sb.toString();
  }

}
