/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.dto.user;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.subscriptions.BillingCycle;
import com.linxo.client.data.subscriptions.DealSource;
import com.linxo.client.dto.EntityInfo;

import java.util.HashSet;

public final class OfferInfo
    extends EntityInfo
{
  private String commercialName;
  private boolean recurring;
  private BillingCycle trialBillingCycle;
  private Integer trialTotalBillingCycles;
  private Double trialCyclePrice;
  private BillingCycle billingCycle;
  private Double cyclePrice;
  private DealSource dealSource;
  private String reference;
  private HashSet<FeatureValueInfo> featureValues;
  private LinxoDate subscriptionEndDate;

  @SuppressWarnings("unused")
  private OfferInfo()
  {
    this.commercialName = null;
    this.recurring = false;
    this.trialBillingCycle = null;
    this.trialTotalBillingCycles = 0;
    this.trialCyclePrice = 0.0;
    this.billingCycle = null;
    this.cyclePrice = 0.0;
    this.dealSource = null;
    this.reference = null;
    this.featureValues = null;
    this.subscriptionEndDate = null;
  }

  public OfferInfo(final String commercialName, final boolean recurring,
                   final BillingCycle trialBillingCycle, final Integer trialTotalBillingCycles,
                   final Double trialCyclePrice, final BillingCycle billingCycle,
                   final Double cyclePrice, final DealSource dealSource, final String reference,
                   final HashSet<FeatureValueInfo> featureValues,
                   final LinxoDate subscriptionEndDate)
  {
    this.commercialName = commercialName;
    this.recurring = recurring;
    this.trialBillingCycle = trialBillingCycle;
    this.trialTotalBillingCycles = trialTotalBillingCycles;
    this.trialCyclePrice = trialCyclePrice;
    this.billingCycle = billingCycle;
    this.cyclePrice = cyclePrice;
    this.dealSource = dealSource;
    this.reference = reference;
    this.featureValues = featureValues;
    this.subscriptionEndDate = subscriptionEndDate;
  }

  public String getCommercialName()
  {
    return commercialName;
  }

  public boolean isRecurring()
  {
    return recurring;
  }

  public BillingCycle getTrialBillingCycle()
  {
    return trialBillingCycle;
  }

  public Integer getTrialTotalBillingCycles()
  {
    return trialTotalBillingCycles;
  }

  public Double getTrialCyclePrice()
  {
    return trialCyclePrice;
  }

  public BillingCycle getBillingCycle()
  {
    return billingCycle;
  }

  public double getCyclePrice()
  {
    return cyclePrice;
  }

  public DealSource getDealSource()
  {
    return dealSource;
  }

  public String getReference()
  {
    return reference;
  }

  public HashSet<FeatureValueInfo> getFeatureValues()
  {
    return featureValues;
  }

  public LinxoDate getSubscriptionEndDate()
  {
    return subscriptionEndDate;
  }

  @Override
  public String toString()
  {
    return "OfferInfo{"
        + "commercialName='" + commercialName + '\''
        + ", recurring=" + recurring
        + ", trialBillingCycle=" + trialBillingCycle
        + ", trialTotalBillingCycles=" + trialTotalBillingCycles
        + ", trialCyclePrice=" + trialCyclePrice
        + ", billingCycle=" + billingCycle
        + ", cyclePrice=" + cyclePrice
        + ", dealSource=" + dealSource
        + ", reference='" + reference + '\''
        + ", featureValues=" + featureValues
        + ", subscriptionEndDate=" + subscriptionEndDate
        + '}';
  }

}
