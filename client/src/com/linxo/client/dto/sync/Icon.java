/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2016 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>


   Created by Alban on 10/01/2017.
 */
package com.linxo.client.dto.sync;

import java.io.Serializable;
import java.util.ArrayList;

public class Icon implements Serializable {

  private ArrayList<String> urls;
  private String position;

  public Icon(){
    this.position = "";
    this.urls = new ArrayList<String>();
  }

  public Icon(ArrayList<String> urls, String position){
    this.position = position;
    this.urls = urls;
  }

  public String getPosition(){
    return position;
  }

  public ArrayList<String> getUrls(){
    return urls;
  }

  @Override
  public String toString() {
    if(this.urls.size() == 0){
      return "icon=null";
    }
    String urls = "[";
    for (String url: this.urls) {
      urls += url + ",";
    }
    urls = urls.substring(0, urls.length() - 1 );
    urls += "]";
    return "icon={"
        + urls + ","
        + position
        + "}";
  }

}
