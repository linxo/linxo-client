/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 19/04/2011 by hugues.
 */
package com.linxo.client.dto.sync;

import com.linxo.client.data.pfm.bank.CredentialKeyboardType;

import java.io.Serializable;
import java.util.ArrayList;

public final class Key implements Serializable
{
  // IMPORTANT: make sure these constants are the same as in com.linxo.services.pfm.CredentialType
  public enum Type {
    TextField,          // The key should be entered through a simple text field
    Keyboard,           // The key should be entered through virtual keyboard
    PasswordField,      // The key should be entered through a password text field
    HiddenField,        // The key should be a hidden input field, not entered by the user (typically, the value of the label)
    Date,               // The key should be entered through 3 text fields (dd MM yyyy)
    OkCancel,           // 2 buttons. Label field contains the text of the Ok button and optionnally the text of the Cancel button: Ok|Cancel
    Info,               // Just text with elements that can be filled on the fly. Example label: 'Send SMS to {phone}'
    Error,              // Just text with elements that can be filled on the fly. Example label: 'Send SMS to {phone}'
    Timeout,            // Works as an Info field with a special 'timeout' argument (in seconds). The UI should substitute the {timeout} arg with a count down.
    Oauth,              // The key should be entered through the bank API interface
    Boxes,              // The key should be entered through boxes where only some have to be filled.
    Checkbox,           // The key should be entered through a checkbox / switch. The label of the key may contain a <u> tag which may link to a given url.

    // This key will be obtained displaying a webview to the client. The url to load in the webview
    // is given through the url parameter of this key. At the end of the process in the webview,
    // the client will send the value for this key using a json following this format:
    //   {
    //     "url" : ''xxxxxx",
    //     "content" : "yyyyyy"
    //   }
    // Where the url is the url reached at the end of the webview process and the content
    // is the content of the page at the end of the webview process.
    Webview,
  }


  private long   id;

  private String key;

  private String label;

  private Type type;

  private String regexp;

  private String boxesScheme; // 0xxx0x.x0

  private String value;

  // the type of keyboard to display in mobile applications to enter this credential
  private CredentialKeyboardType keyboardType;

  // potential url associated to the <u> tag of the label of a credential of type Checkbox
  private String url;

  // format of the date for a credential of type Date
  private String dateFormat;

  // potential text that may help users understand what they have to enter
  private String hint;

  // potential url that may help users understand what they have to enter (can be a web link or an application link linxo://xxx)
  private String hintUrl;

  // error message to display when the value entered does not match the regexp
  private String validationErrorMessage;

  // parent element in the case of a dropdown element
  private Long parent;

  // icon element
  private Icon icon;

  @SuppressWarnings("unused")// s11n
  public Key(){}

  public Key(long id, String key, String label, Type type, String regexp)
  {
    this.id = id;
    this.key = key;
    this.label = label;
    this.regexp = regexp;
    this.type = type;
  }

  public Key(long id, String key, String label, Type type, String regexp, String boxesScheme, String value,
             CredentialKeyboardType keyboardType, String url, String dateFormat, String hint, String hintUrl,
             String validationErrorMessage)
  {
    this(id, key, label, type, regexp);
    this.boxesScheme = boxesScheme;
    this.value = value;
    this.keyboardType = keyboardType;
    this.url = url;
    this.dateFormat = dateFormat;
    this.hint = hint;
    this.hintUrl = hintUrl;
    this.validationErrorMessage = validationErrorMessage;
  }

  public Key(long id, String key, String label, Type type, String regexp, String boxesScheme, String value,
             CredentialKeyboardType keyboardType, String url, String dateFormat, String hint, String hintUrl,
             String validationErrorMessage, Long parent, ArrayList<String> iconUrls, String iconPosition){
    this(id,key,label,type,regexp,boxesScheme,value,keyboardType,url,dateFormat,hint,hintUrl,validationErrorMessage);
    this.parent = parent;
    this.icon = new Icon(iconUrls, iconPosition);

  }

  public long getId()
  {
    return id;
  }

  public String getKey()
  {
    return key;
  }

  public String getLabel()
  {
    return label;
  }

  public String getRegexp()
  {
    return regexp;
  }

  public Type getType()
  {
    return type;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public String getBoxesScheme()
  {
    return boxesScheme;
  }

  public String getValue()
  {
    return value;
  }

  public CredentialKeyboardType getKeyboardType()
  {
    return keyboardType;
  }

  public String getUrl()
  {
    return url;
  }

  public String getDateFormat()
  {
    return dateFormat;
  }

  public String getHint()
  {
    return hint;
  }

  public String getHintUrl()
  {
    return hintUrl;
  }

  public String getValidationErrorMessage()
  {
    return validationErrorMessage;
  }

  public Long getParent() {
    return parent;
  }

  public Icon getIcon() {
    return icon;
  }

  @Override
  public String toString()
  {
    return "Key{id=" + getId()
           + ", key=" + key
           + ", label=" + label
           + ", type=" + type
           + ", regexp=" + regexp
           + ", boxesScheme=" + boxesScheme
           + ", value=" + (value == null ? "null" : "not-null")
           + ", keyboardType=" + keyboardType
           + ", url=" + url
           + ", dateFormat=" + dateFormat
           + ", hint=" + hint
           + ", hintUrl=" + hintUrl
           + ", validationErrorMessage=" + validationErrorMessage
           + '}';
  }

}
