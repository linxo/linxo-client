/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.dto.sync;

import com.linxo.client.dto.EntityInfo;

import java.util.ArrayList;

public final class FinancialInstitutionInfo extends EntityInfo
  implements Comparable<FinancialInstitutionInfo>
{
  private String name;
  private String branchName;
  private String logoUrl;
  private String bankUrl;

  private boolean sync;
  private boolean show;
  private Boolean fiAuthentication;
  private String unAvailableMessage;
  private String supportURL;
  private String supportText;
  private ArrayList<Key> keys = new ArrayList<Key>();

  // Instance Initializers

  protected FinancialInstitutionInfo(){}

  public FinancialInstitutionInfo(String name, long id)
  {
    this.name = name;
    setId(id);
  }

  public FinancialInstitutionInfo( long id )
  {
    this();
    setId( id );
  }

  public String getName()
  {
    return name;
  }

  public void setName( String name )
  {
    this.name = name;
  }

  public String getBranchName()
  {
    return branchName;
  }

  public void setBranchName(String branchName)
  {
    this.branchName = branchName;
  }

  public String getLogoUrl()
  {
    return logoUrl;
  }

  public void setLogoUrl( String logoUrl )
  {
    this.logoUrl = logoUrl;
  }

  public String getBankUrl()
  {
    return bankUrl;
  }

  public void setBankUrl( String bankUrl )
  {
    this.bankUrl = bankUrl;
  }

  public boolean isSync()
  {
    return sync;
  }

  public void setSync( boolean sync )
  {
    this.sync = sync;
  }

  public ArrayList<Key> getKeys()
  {
    return keys;
  }

  public void setKeys(ArrayList<Key> keys)
  {
    this.keys = keys;
  }

  public boolean isShow()
  {
    return show;
  }

  public void setShow(boolean show)
  {
    this.show = show;
  }

  public String getUnAvailableMessage()
  {
    return unAvailableMessage;
  }

  public void setUnAvailableMessage(String unAvailableMessage)
  {
    this.unAvailableMessage = unAvailableMessage;
  }

  public String getSupportText()
  {
    return supportText;
  }

  public void setSupportText(String supportText)
  {
    this.supportText = supportText;
  }

  public String getSupportURL()
  {
    return supportURL;
  }

  public void setSupportURL(String supportURL)
  {
    this.supportURL = supportURL;
  }

  public Boolean isFiAuthentication()
  {
    return fiAuthentication;
  }

  public void setFiAuthentication(Boolean fiAuthentication)
  {
    this.fiAuthentication = fiAuthentication;
  }

  @Override
  public int compareTo(FinancialInstitutionInfo financialInstitutionInfo)
  {
    if (name.equals(financialInstitutionInfo.name)) {
      if (branchName == null && financialInstitutionInfo.branchName == null) {
        return 0;
      }
      if (branchName == null) {
        return -1;
      }
      if (financialInstitutionInfo.branchName == null) {
        return 1;
      }
      return branchName.compareToIgnoreCase(financialInstitutionInfo.branchName);
    }
    return name.compareToIgnoreCase(financialInstitutionInfo.name);
  }

  @Override
  public boolean equals( Object o )
  {
    if ( this == o ) return true;
    if ( !( o instanceof FinancialInstitutionInfo ) ) return false;

    FinancialInstitutionInfo fiInfo = (FinancialInstitutionInfo) o;

    if ( getId() != null ? !getId().equals( fiInfo.getId() ) : fiInfo.getId() != null ) return false;
    if ( name != null ? !name.equals( fiInfo.name ) : fiInfo.name != null ) return false;
    if ( logoUrl != null ? !logoUrl.equals( fiInfo.logoUrl ) : fiInfo.logoUrl != null ) return false;
    if ( bankUrl != null ? !bankUrl.equals( fiInfo.bankUrl ) : fiInfo.bankUrl != null ) return false;
    if ( sync != fiInfo.sync ) return false;
    if ( show != fiInfo.show ) return false;
    //noinspection RedundantIfStatement
    if ( keys != null ? !keys.equals( fiInfo.keys ) : fiInfo.keys != null ) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result;
    result = ( getId() != null ? getId().hashCode() : 0 );
    result = 31 * result + ( name != null ? name.hashCode() : 0 );
    result = 31 * result + ( bankUrl != null ? bankUrl.hashCode() : 0 );
    result = 31 * result + ( logoUrl != null ? logoUrl.hashCode() : 0 );
    result = 31 * result + ( keys != null ? keys.hashCode() : 0 );
    return result;
  }

  @Override
  public String toString()
  {
    return "FinancialInstitutionInfo{"
           + "id=" + getId()
           + ", name=" + name
           + ", logoUrl=" + logoUrl
           + ", bankUrl=" + bankUrl
           + ", sync=" + sync
           + ", show=" + show
           + ", fiAuthentication=" + fiAuthentication
           + ", unAvailableMessage=" + unAvailableMessage
           + ", keys=" + keys
           + '}';
  }

}
