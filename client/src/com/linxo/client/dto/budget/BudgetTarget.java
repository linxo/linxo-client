/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 17/09/2015 by hugues.
 */
package com.linxo.client.dto.budget;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.upcoming.IntervalUnit;
import com.linxo.client.dto.EntityInfo;

/**
 * <p>
 * This object defines a BudgetTarget a user would create or the server would suggest for a given
 * CategoryId, account (one or all the user's accounts), starting at a given Date
 * for every intervalValue IntervalUnit.
 * </p>
 * <p>
 * For instance the User may ask the server to suggest a budget amount for the DailyLife expenses
 * every two weeks on all the accounts including the subcategories (like supermarket, giftOffered etc...).
 * </p>
 * <pre>
 *   {
 *     "id" : null,                 // the user _queried the server_, no target currently stored
 *     "bankAccountId" : null,
 *     "categoryId" : 400100,
 *     "includeSubCategories" : true,
 *     "amount" : 123.45,             // the user _queried the server_, default amount is the same than the suggested amount
 *     "suggestedAmount" : 123.45,
 *     "intervalUnit" : "week",
 *     "intervalValue" : 2,
 *     "startDate" : { "day" : 1, "month" : 9, "year" : 2015 }
 *   }
 * </pre>
 * <p>
 * The User may also allocate a given amount to the category Sport every 3 months since this is a
 * Fixed amount that is directly debited from the personal user's account. This amount is only
 * allocated to the personal user account.
 * </p>
 * <pre>
 *   {
 *     "id" : 1234,                 // the user stored the target: it has an id
 *     "bankAccountId" : 12357,     // the bankAccountId of the user's personal account
 *     "categoryId" : 400710,
 *     "includeSubCategories" : false,
 *     "amount" : 75,               // the currently allocated amount by the user
 *     "suggestedAmount" : 75.45,   // the server still suggests an amount to the user
 *     "intervalUnit" : "month",
 *     "intervalValue" : 3,
 *     "startDate" : { "day" : 1, "month" : 1, "year" : 2015 }
 *   }
 * </pre>
 */
public class BudgetTarget
    extends EntityInfo
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  public static final Integer DEFAULT_INTERVAL_VALUE = 1;

  // Static Initializers

  // Static Methods

  // Instance Fields

  /**
   * Id of the account to which this budget target is defined.
   * If this fields is null, this target applies to all accounts
   * for this user.
   */
  private Long bankAccountId;

  /**
   * Reference of the account to which this budget target is defined.
   *
   * /!\ This field may be used only at budget target creation. /!\
   *
   * It will be used if the bankAccountId is null. If both the
   * bankAccountId and this field are null, this target budget
   * applies to all accounts for this user.
   */
  private String accountReference;

  /**
   * The {@link com.linxo.client.dto.tx.CatInfo#getId() Id of the Category}
   * of this BudgetTarget
   */
  private Long categoryId;
  private boolean includeSubCategories;

  private double amount;
  private double currentAmount;

  private Double suggestedAmount;

  private IntervalUnit intervalUnit;
  private Integer intervalValue;

  private LinxoDate startDate;

  // Instance Initializers

  // Constructors

  /**
   * Constructor that does not set the ID
   */
  public BudgetTarget() {}

  /**
   * Constructor that sets the ID
   * @param id the internal id of this BudgetTarget
   */
  public BudgetTarget(Long id)
  {
    setId(id);
  }

  // Instance Methods


  /**
   * Id of the account to which this budget target is defined.
   * If this fields is null, this target applies to all accounts
   * for this user.
   * @return the Id of the account to which this budget target is defined
   */
  public Long getBankAccountId()
  {
    return bankAccountId;
  }

  /**
   * Sets the {@link com.linxo.client.dto.account.BankAccountInfo#getId()}  ID of the BankAccountInfo}
   * to which this target applies.
   * @param bankAccountId the id, or null if this budget target applies to all the accounts.
   */
  public void setBankAccountId(Long bankAccountId)
  {
    this.bankAccountId = bankAccountId;
  }

  /**
   * Reference of the account to which this budget target is defined.
   *
   * /!\ This field may be used only at budget target creation. /!\
   *
   * It will be used if the bankAccountId is null. If both the
   * bankAccountId and this field are null, this budget target
   * applies to all accounts for this user.
   *
   * @return The reference of the account to which this budget target
   *         is defined.
   */
  public String getAccountReference()
  {
    return accountReference;
  }

  /**
   * Sets the {@link com.linxo.client.dto.account.BankAccountInfo#getReference()}
   * reference of the account to which this budget target is defined.
   *
   * /!\ This field may be used only at budget target creation. /!\
   *
   * It will be used if the bankAccountId is null. If both the
   * bankAccountId and this field are null, this target applies
   * to all accounts for this user.
   *
   * @param accountReference The reference, or null if this budget target applies to all
   *                         the accounts.
   */
  public void setAccountReference(String accountReference)
  {
    this.accountReference = accountReference;
  }

  /**
   * @return the {@link com.linxo.client.dto.tx.CatInfo#getId() Id of the Category}
   * of this UpcomingTransaction
   */
  public Long getCategoryId()
  {
    return categoryId;
  }

  public void setCategoryId(Long categoryId)
  {
    this.categoryId = categoryId;
  }

  /**
   * Indicates if this budget should also use sub-categories of the given category for
   * the allocated/suggested amount
   * @return {@code true} if the sub-categories must be considered, or {@code false} if
   * this BudgetTarget applies only to the given category.
   * @see #getCategoryId()
   */
  public boolean isIncludeSubCategories()
  {
    return includeSubCategories;
  }

  public void setIncludeSubCategories(boolean includeSubCategories)
  {
    this.includeSubCategories = includeSubCategories;
  }

  /**
   * This amount is signed: i.e. negative amounts are debits, and positive amounts are credits,
   * whatever the category is.
   * @return the allocated amount for this BudgetTarget.
   */
  public double getAmount()
  {
    return amount;
  }

  public void setAmount(double amount)
  {
    this.amount = amount;
  }

  /**
   * @return The current amount for this BudgetTarget.
   */
  public double getCurrentAmount()
  {
    return currentAmount;
  }

  public void setCurrentAmount(double currentAmount)
  {
    this.currentAmount = currentAmount;
  }

  /**
   * @return the suggested amount for this BudgetTarget.
   */
  public Double getSuggestedAmount()
  {
    return suggestedAmount;
  }

  public void setSuggestedAmount(Double suggestedAmount)
  {
    this.suggestedAmount = suggestedAmount;
  }


  public IntervalUnit getIntervalUnit()
  {
    return intervalUnit;
  }

  public void setIntervalUnit(IntervalUnit intervalUnit)
  {
    this.intervalUnit = intervalUnit;
  }

  /**
   * Number of intervalUnit considered in this BudgetTarget
   * defaults to 1 unit
   * @return the number of intervalUnit considered in this BudgetTarget
   */
  public Integer getIntervalValue()
  {
    // The intervalValue might be null when de-serialized, so we default
    // to the value of DEFAULT_INTERVAL_VALUE (ie. "1") when null.
    return intervalValue==null
        ? DEFAULT_INTERVAL_VALUE
        : intervalValue;
  }

  public void setIntervalValue(Integer intervalValue)
  {
    this.intervalValue = intervalValue;
  }

  /**
   * Mandatory field (not null).
   * The date from which to compute the first included date.
   * This date may or may not be included depending on other fields
   * @return the startDate
   */
  public LinxoDate getStartDate()
  {
    return startDate;
  }

  public void setStartDate(LinxoDate startDate)
  {
    this.startDate = startDate;
  }


  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("BudgetTarget{");
    sb.append("super=").append(super.toString());
    sb.append(", bankAccountId=").append(bankAccountId);
    sb.append(", accountReference=").append(accountReference);
    sb.append(", categoryId=").append(categoryId);
    sb.append(", includeSubCategories=").append(includeSubCategories);
    sb.append(", amount=").append(amount);
    sb.append(", currentAmount=").append(currentAmount);
    sb.append(", suggestedAmount=").append(suggestedAmount);
    sb.append(", intervalUnit=").append(intervalUnit);
    sb.append(", intervalValue=").append(intervalValue);
    sb.append(", startDate=").append(startDate);
    sb.append('}');
    return sb.toString();
  }
}
