/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 08/06/2011 by hugues.
 */
package com.linxo.client.dto.announce;

import com.linxo.client.dto.EntityInfo;

public final class AnnounceInfo extends EntityInfo
{

  // Nested Types (mixing inner and static classes is okay)

  /**
   * see AnnounceAction.Action
   */
  //LINXO-CLIENT: make sure there is no space around "Action"
  public enum Action{
    Display,
    Click,
  }

  /**
   * see com.linxo.services.pfm.hibernate.affiliation.Announce.Size
   */
  public enum Size {
    Banner120x600,
    Banner160x600,
    Banner300x250,
    Banner468x60,
    Banner728x90,
    Banner1000x90,
    Banner600,
    EmailOverview,
    MobileBanner320
  }
  
  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private String announce;
  private Size size;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  public AnnounceInfo()
  {}

  public AnnounceInfo(long id, String announce, Size size)
  {
    setId(id);
    this.announce = announce;
    this.size = size;
  }

  // Instance Methods

  public String getAnnounce()
  {
    return announce;
  }

  public Size getSize()
  {
    return size;
  }
}
