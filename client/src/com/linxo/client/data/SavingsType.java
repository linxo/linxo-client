/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data;

/**
 * This enum used to be defining types of Savings Account and infer Savings Family.
 *
 * For more flexibility, the pairing (type vs. family) will be done in a table in the
 * Service, and will be made transparent to the API. Instead of using this enum, new
 * development should be using values (static String objects) in the interface
 * {@link SavingsAccountTypes}
 *
 * @see SavingsAccountTypes
 * @deprecated use SavingsAccountTypes instead
 */
@Deprecated
public enum SavingsType
{
  // Epargne liquide
  CEL(SavingsFamily.LIV),
  LIVA(SavingsFamily.LIV),
  LDD(SavingsFamily.LIV),
  LEP(SavingsFamily.LIV),
  LIVJ(SavingsFamily.LIV),
  LIV(SavingsFamily.LIV),

  // Epargne à terme
  PEL(SavingsFamily.TERME),
  CAT(SavingsFamily.TERME),

  // Epargne boursière
  TIT(SavingsFamily.BOURSE),
  ESP(SavingsFamily.BOURSE),
  PEA(SavingsFamily.BOURSE),
  PEAE(SavingsFamily.BOURSE),

  // Assurance Vie et Capitalisation
  AVIE(SavingsFamily.AVIECAPI),
  CAPI(SavingsFamily.AVIECAPI),
  PEP(SavingsFamily.AVIECAPI),

  // Epargne Retraite et Salariale
  PERP(SavingsFamily.RETRAITE),
  PEE(SavingsFamily.RETRAITE),
  PERE(SavingsFamily.RETRAITE),
  PERCO(SavingsFamily.RETRAITE),
  MADELIN(SavingsFamily.RETRAITE),
  PREFON(SavingsFamily.RETRAITE),
  COREM(SavingsFamily.RETRAITE),
  CRH(SavingsFamily.RETRAITE),

  // Default at the end
  AUTRE(SavingsFamily.AUTRE);

  private final SavingsFamily savingsFamily;

  SavingsType(final SavingsFamily savingsFamily)
  {
    this.savingsFamily = savingsFamily;
  }

  public SavingsFamily getSavingsFamily()
  {
    return savingsFamily;
  }

}
