/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 12/12/2011 by hugues.
 */
package com.linxo.client.data.pfm.alert;

import com.linxo.client.data.support.Version;

  /*
   * DO NOT CHANGE THE ORDER, It IS STORED AS "ORDINAL" IN THE DATABASE.
   *
   * A change would also require a change in the client AlertInfo
   */
public enum AlertType {
    /**
     * News alert type. In the case, Alert#getMessage()
     * contains the information to display/send.
     */
    @Deprecated
    News(Version.NO_API_VERSION),

    /**
     * Generic alert type. In the case, Alert#getMessage()
     * contains the information to display/send.
     */
    @Deprecated
    GenericAlert(Version.NO_API_VERSION),

    /**
     * Low balance alert type.
     * Alert#getMessage() contains a string
     * representation of the accountId.
     */
    //@see com.linxo.services.pfm.impl.alert.BankAccountAlertHandler
    LowBalance(Version.NO_API_VERSION),

    /**
     * High spending alert type.
     * Alert#getMessage() contains a String
     * representation of the transactionId.
     */
    // @see com.linxo.services.pfm.impl.alert.TransactionAlertHandler
    HighSpending(Version.NO_API_VERSION),

    /**
     * High deposit alert type.
     * Alert#getMessage() contains a String
     * representation of the transactionId.
     */
    // @see com.linxo.services.pfm.impl.alert.TransactionAlertHandler
    HighDeposit(Version.NO_API_VERSION),

    /**
     * BankFee alert type.
     * Alert#getMessage() contains a String
     * representation of the transactionId.
     */
    // @see com.linxo.services.pfm.impl.alert.TransactionAlertHandler
    BankFee(Version.NO_API_VERSION),

    /**
     * TooManyAttempts have been performed with bad identifiers
     * Connection is blocked until the user provides his
     * credentials again.
     */
    TooManyAttempts(Version.NO_API_VERSION),

    /**
     * The web site requires that the user changes his password
     * Connection is blocked until the user provides his
     * credentials again.
     */
    PasswordChangeRequired(Version.NO_API_VERSION),

    /**
     * The web site requires that the user does something: fill
     * a form, validate some information, etc. Connection is blocked
     * until the user does what he is asked to do.
     */
    UserActionRequired(Version.NO_API_VERSION);

    private Version requiredVersion;

    private AlertType(Version requiredVersion)
    {
      this.requiredVersion = requiredVersion;
    }

    public Version getRequiredVersion()
    {
      return requiredVersion;
    }

}
