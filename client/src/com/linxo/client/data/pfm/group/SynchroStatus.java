/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.pfm.group;

public enum SynchroStatus
{
  None,                   // manual account group
  Never,                  // never synchronized yet
  Running,                // running
  PartialSuccess,         // some of the accounts failed
  Success,                // success
  Failed,                 // failed
  AuthFailed,             // failed because of authentication
  TooManyAttempts,        // failed because of authentication and max failed attempts reached
  ServiceUnavailable,     // failed because the web site was not available
  PasswordChangeRequired, // failed because the web site required a password change
  UserActionRequired,     // failed because the user has to do something before to be able to access his accounts
  ChallengeRequired,      // failed because a challenge/response is required and the user was not assisting the process
  ChallengeTimedOut,      // failed because a challenge/response is required and the user did not answer in the alloted time
  ChallengeFailed,        // failed because a challenge/response is required and the user responded incorrectly
  ChallengeCancelled,     // failed because a challenge/response is required and the user cancelled the process
  NeedsSecret,
  Closed,                 // The user decides to manually close the account group
}
