/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.pfm.bank;

public enum AccountSynchroStatus
{
  None,            // manual account
  Never,           // never synchronized yet
  Running,         // running
  Success,         // success
  FailedSingle,    // failed but the whole synchro didn't fail, the user may decide to close it
  Unavailable,     // The account was not found on the financial institution website, the user may decide to close it
  Failed,          // the whole synchro for the group failed
  Closed,          // closed
}
