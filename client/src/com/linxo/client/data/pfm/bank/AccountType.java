/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.pfm.bank;

public enum AccountType
{
  Checkings,
  CreditCard,
  Savings,
  Loan
}
