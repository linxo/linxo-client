/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.pfm.bank;

/**
 * An enum for the different types of keyboards a mobile
 * application can display to enter a credential.
 */
public enum CredentialKeyboardType
{
  Default,
  DigitsOnly,
  Email,
  Date
}
