/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2012 by hugues.
 */
package com.linxo.client.data.pfm.bank;

import java.io.Serializable;

public enum TransactionType implements Serializable {
  /** generic credit. */
  Credit,

  /** generic debit. */
  Debit,

  /** interest paid or earned (depends on amount sign, paid: &lt;0, earned: &gt;0) */
  Interest,

  /** dividend */
  Dividend,

  /** bank fee */
  BankFee,

  /** deposit. */
  Deposit,

  /**
   * Atm transaction debit or credit (depends on amount sign, debit &lt;0, credit &gt;0)
   * Can be a cash withdrawal, but in an Atm/DAB, not a branch
   * @see #Cash for cash withdrawal in a branch
   */
  Atm,

  /**
   * point of sale,
   * credit card transactions
   */
  PointOfSale,

  /**
   * Payment of a credit card due amount, complete or partial
   */
  CreditCardPayment,

  /**
   * transfer identified within the provider
   * - 'virement recu/emis' locally in a provider group
   */
  InternalTransfer,

  /**
   * potential transfer
   * - 'virement recu/emis' but not identified within this provider group
   */
  PotentialTransfer,

  /** check */
  Check,

  /**
   * Electronic payment
   * includes:
   * - TIP
   * - Telereglement
   * @see #DirectDebit for "prelevements"
   */
  ElectronicPayment,

  /** cash withdrawal in a branch */
  Cash,

  /** direct deposit (for instance: salary) */
  DirectDeposit,

  /** merchant-initiated debit
   * for instance:
   * - "prelevements"
   *@see #ElectronicPayment for TIP, telereglement
   */
  DirectDebit,

  /**
   * Repeating payment
   * for instance 'virement permanent'
   */
  RepeatingPayment,

  /**
   * other, unknown
   */
  Other
}
