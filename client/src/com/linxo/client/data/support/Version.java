/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>


   Created on : 23/08/2011 by hugues.
 */
package com.linxo.client.data.support;

import java.io.Serializable;
import java.util.Arrays;

/**
 * This class represents the API/GWT version that is required by a client.
 * The Version string as implemented by Linxo today is structured as:
 * {major}.{minor}.{micro}-{patch}
 * The micro and patch versions are optional
 * This class is immutable.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class Version implements Comparable<Version>, Serializable {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // When there was no API version, it was actually v 1.0.
  public static final Version NO_API_VERSION = new Version("1.0.0-0");

  // API v 1.1 requires a timeStamp check, and introduces the
  // CHALLENGE statuses
  public static final Version MIN_CHALLENGE_VERSION = new Version("1.1");
  public static final Version MIN_TIMESTAMP_CHECK_VERSION = new Version("1.1");

  public static final Version HAS_MANUAL_ACCOUNT = new Version("1.2");

  // /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
  //
  // API VERSIONS RELATED TO PASSWORD VALIDATION ARE DELEGATED TO THE PASSWORD
  // VALIDATION CLASSES (see AbstractPasswordValidator and the daughter classes).
  //
  // /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\

  // API v 1.3 introduces :
  //  - the semi auto groups management
  public static final Version MIN_SEMI_AUTO_VERSION = new Version("1.3");
  //  - the notification when an account has not been synchronized for a certain time (week or month)
  public static final Version MIN_TOO_OLD_SYNC_NOTIFICATION = new Version("1.3");

  //- -//
  public static final Version HAS_STANDARD_DATE_FORMAT_VERSION = new Version("1.4");

  // Budget Date added //
  public static final Version HAS_BUDGET_DATE = new Version("1.4.1");

  // Linxo Date added //
  public static final Version HAS_LINXO_DATE = new Version("1.4.1");

  /**
   * LastFailure date added in AccountGroup
   */
  public static final Version HAS_LAST_END_DATE = new Version("1.5");

  public static final Version HAS_FILTERED_CLOSED_ACCOUNT_PARAM = new Version ("1.5");

  public static final Version HAS_ACCOUNT_SUB_TYPES = new Version("1.5.1");

  public static final Version HAS_CLOSED_ACCOUNT_GROUPS = new Version("1.5.1");

  public static final Version HAS_UPDATED_DATES_INITIAL_INFO = new Version("1.6.0");

  public static final Version HAS_DEALS_EVENTS = new Version("1.6.0");

  public static final Version HAS_PERMISSIONS = new Version("1.6.0");

  // AXA API Provider has been removed
  // public static final Version HAS_AXA_API = new Version("1.6.1");

  public static final Version HAS_VERIFIED_EMAIL = new Version("1.6.2");

  public static final Version HAS_USER_PROPERTIES = new Version("1.6.3");

  public static final Version HAS_ALERT_INFO_SCHEME_FIX = new Version("1.6.3");

  public static final Version HAS_CUSTOM_CATEGORIES = new Version("1.6.4");

  public static final Version HAS_DAILY_OFFERS = new Version("1.6.5");

  public static final Version HAS_UPCOMING_TRANSACTIONS = new Version("1.6.6");

  // before this version, BankAccountInfo had the field LastSuccessfulUpdate
  public static final Version HAS_NO_LAST_LAST_SUCCESSFUL_UPDATE_IN_ACCOUNT = new Version("1.6.6");

  /**
   * Indicate the minimum version that has access to the {@code includedInXXX}
   * in the BankAccountInfo.
   *
   * When using previous versions ( < {@code HAS_DETAILED_ACCOUNT_VIEWS_SETTINGS}
   * one should only fill the value of {@code includedInTrends} in the
   * BankAccountInfo. An update to this value will update both
   * {@code includedInBudget} and {@code includedInSavings}.
   *
   */
  public static final Version HAS_DETAILED_ACCOUNT_VIEWS_SETTINGS = new Version("1.7.0");

  public static final Version HAS_DETAILED_ACCOUNT_DATA = new Version("1.7.1");

  public static final Version HAS_SPECIALIZED_TYPES = new Version("1.8.0");

  /**
   *
   */
  public static final Version CURRENT_VERSION = new Version("1.8.0");



  // Static Initializers

  // Static Methods

  // Instance Fields
  /**
   * version numbers are ordered into this array with
   * MAJOR at pos = 0
   * MINOR at pos = 1
   * MICRO at pos = 2
   * PATCH at pos = 3
   */
  private final int[] version;

  // Instance Initializers

  // Constructors

  /**
   * @param versionString the version string provided by a client
   * @throws IllegalArgumentException if the version string is not correctly formatted
   */
  public Version(String versionString)
      throws IllegalArgumentException
  {
    this.version = parseVersion(versionString);
  }

  public static Version getVersion(final String versionString)
  {
    return new Version(versionString);
  }

  // Instance Methods

  public boolean hasChallenge()
  {
    return compareTo(MIN_CHALLENGE_VERSION) >= 0;
  }

  public boolean hasTimeStampCheck()
  {
    return compareTo(MIN_TIMESTAMP_CHECK_VERSION) >= 0;
  }

  public boolean hasManualAccount()
  {
    return compareTo(HAS_MANUAL_ACCOUNT) >= 0;
  }

  public boolean hasSemiAuto()
  {
    return compareTo(MIN_SEMI_AUTO_VERSION) >= 0;
  }

  public boolean hasStandardDateFormat()
  {
    return compareTo(HAS_STANDARD_DATE_FORMAT_VERSION) >=0;
  }

  public boolean hasBudgetDate()
  {
    return compareTo(HAS_BUDGET_DATE) >= 0;
  }

  public boolean hasLinxoDate()
  {
    return compareTo(HAS_LINXO_DATE) >= 0;
  }

  public boolean hasEndDate()
  {
    return compareTo(HAS_LAST_END_DATE) >= 0;
  }


  public boolean hasAccountSubTypes()
  {
    return compareTo(HAS_ACCOUNT_SUB_TYPES) >= 0;
  }


  public boolean hasFilteredCloseAccountParam()
  {
    return compareTo(HAS_FILTERED_CLOSED_ACCOUNT_PARAM) <= 0;
  }

  public boolean hasClosedAccountGroups()
  {
    return compareTo(HAS_CLOSED_ACCOUNT_GROUPS) >= 0;
  }

  public boolean hasUpdatedDatesInitialInfo()
  {
    return compareTo(HAS_UPDATED_DATES_INITIAL_INFO) >= 0;
  }

  public boolean hasDealEvents()
  {
    return compareTo(HAS_DEALS_EVENTS) >= 0;
  }

  public boolean hasPermissions()
  {
    return compareTo(HAS_PERMISSIONS) >= 0;
  }

  public boolean hasVerifiedEmail()
  {
    return compareTo(HAS_VERIFIED_EMAIL) >= 0;
  }

  public boolean hasUserProperties()
  {
    return compareTo(HAS_USER_PROPERTIES) >= 0;
  }

  public boolean hasAlertInfoSchemeFix()
  {
    return compareTo(HAS_ALERT_INFO_SCHEME_FIX) >= 0;
  }

  public boolean hasCustomCategories()
  {
    return compareTo(HAS_CUSTOM_CATEGORIES) >= 0;
  }

  public boolean hasUpcomingTransactions()
  {
    return compareTo(HAS_UPCOMING_TRANSACTIONS) >= 0;
  }

  public boolean hasDetailedAccountViewsSettings(){
    return compareTo(HAS_DETAILED_ACCOUNT_VIEWS_SETTINGS) >= 0;
  }

  public boolean hasDailyOffers()
  {
    return compareTo(HAS_DAILY_OFFERS) >= 0;
  }

  public boolean hasDetailedAccountData()
  {
    return compareTo(HAS_DETAILED_ACCOUNT_DATA) >= 0;
  }

  public boolean hasSpecializedTypes() {
    return compareTo(HAS_SPECIALIZED_TYPES) >= 0;
  }

  public String getVersionString()
  {
    return version[0] + "." + version[1] + "." + version[2] + "-" + version[3];
  }

  @Override
  public int compareTo(Version other)
  {
    for(int i=0; i<4; i++) {
      if(this.version[i] != other.version[i]){
        return this.version[i] - other.version[i];
      }
    }

    return 0;
  }

  private int[] parseVersion(String versionString)
    throws IllegalArgumentException
  {
    if (versionString == null) {
      return NO_API_VERSION.version;
    }
    final int[] result = new int[4];

    // The Pattern class is not available in GWT. So we need to parse this manually.
    // Below is the regexp we're trying to parse.
    //    Pattern p = Pattern.compile("(\\d+)\\.(\\d+)(?:\\.(\\d+)(?:-(\\d+))?)?(?:-SNAPSHOT)?");
    if(!versionString.matches("(\\d+)\\.(\\d+)(?:\\.(\\d+)(?:-(\\d+))?)?(?:-SNAPSHOT)?")){
      throw new IllegalArgumentException("Incompatible version number provided: "+versionString);
    }

    String[] groups = versionString.split("[\\.\\-]");
    for(int i = 0; i<Math.min(groups.length,4); i++) {
      int number = 0;
      try{
        number = Integer.parseInt(groups[i]);
      }
      catch(NumberFormatException nfe) {
        // if this is the last group, check if this is a 'SNAPSHOT'
        if(i != (groups.length -1) || !"SNAPSHOT".equals(groups[i])){
          throw new IllegalArgumentException("Incompatible version number provided while parsing group("+i+")='"+groups[i]+"' :"+versionString);
        }
      }
      result[i] = number;
    }

    return result;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Version other = (Version) o;
    return Arrays.equals(version, other.version);
  }

  @Override
  public int hashCode()
  {
    return Arrays.hashCode(version);
  }

  @Override
  public String toString()
  {
    return version[0] + "." + version[1] + "." + version[2] + "-" + version[3];
  }
}
