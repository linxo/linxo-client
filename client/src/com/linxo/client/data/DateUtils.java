/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */

package com.linxo.client.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Date utils used in DTOs and other inheriting GWT modules.
 *
 * This Class is compatible with the GWT JRE emulation.
 */
@SuppressWarnings("deprecation")
public class DateUtils {

  public static final long SECOND_MS = 1000L;
  public static final long MINUTE_MS = 60L * SECOND_MS;
  public static final long HOUR_MS = 60L * MINUTE_MS;
  public static final long DAY_MS = 24L * HOUR_MS;
  public static final long WEEK_MS = 7L * DAY_MS;

  /**
   * {@link Date} year shift
   * @see Date
   */
  public static final int MAGIC_YEAR = 1900;
  public static final int MAGIC_MONTH = 1;


  /**
   * @return a Date object representing midnight, at the beginning
   * of the day today.
   */
  public static Date todaysDate()
  {
    final Date now = new Date(); // now contains hours/minutes/seconds, we want midnight
    return new Date(now.getYear(), now.getMonth(), now.getDate());
  }

  public static Date getDateOnly(final Date date)
  {
    return new Date(date.getYear(), date.getMonth(), date.getDate());
  }

  /**
   * @return int the value of the current year, i.e : 2008, 2012...
   */
  public static int getCurrentYear()
  {
    return DateUtils.MAGIC_YEAR + new Date().getYear();
  }

  /**
   * @param date the date to start counting
   * @param days_count the number of days to add
   * @return a Date object at the same time of the day, but days_count
   *         after (or before if days_count is negative) the given day.
   */
  public static Date addDays(final Date date, final int days_count)
  {
    long epoch = date.getTime();
    epoch+=days_count*DAY_MS;
    Date result = new Date(epoch);

    // Check if we have switched the Standard Time vs. DST :
    // In this case we add (or remove) the difference to keep the time of the day.
    if(date.getTimezoneOffset() != result.getTimezoneOffset()) {
      result = new Date(epoch + (result.getTimezoneOffset() - date.getTimezoneOffset()) * MINUTE_MS);
    }
    return result;
  }

  /**
   * @param date the date to start counting
   * @param days_count the number of days to add
   * @return a Date object representing midnight, at the beginning of
   * the day right after given date + days_count days.
   */
  public static LinxoDate addDays(final LinxoDate date, final int days_count)
  {
    return getLinxoDate(addDays(getDate(date), days_count));
  }

  /**
   * @param date the date to start counting
   * @param weeks_count the number of days to add
   * @return a Date object representing midnight, at the beginning of
   * the day right after the given date + weeks_count weeks.
   */
  @SuppressWarnings("UnusedDeclaration")
  public static Date addWeeks( Date date, int weeks_count )
  {
    long time = date.getTime();
    Date next_week = new Date( time + ( WEEK_MS * weeks_count ) );

    return new Date( next_week.getYear(), next_week.getMonth(), next_week.getDate() );
  }

  /**
   * Adds the given number of months to a date.
   *
   * @param date the date
   * @param months number of months
   */
  public static void addMonthsToDate(Date date, int months) {
    if (months != 0) {
      int month = date.getMonth();
      int year = date.getYear();

      int resultMonthCount = year * 12 + month + months;
      int resultYear = resultMonthCount / 12;
      int resultMonth = resultMonthCount - resultYear * 12;

      date.setMonth(resultMonth);
      date.setYear(resultYear);
    }
  }

  /**
   * @param date the date to start counting
   * @param years_count the number of years to add
   * @return a Date object representing midnight, at the beginning of
   *         the day right after the given date + years_count years.
   */
  public static Date addYears( Date date, int years_count )
  {
    return new Date( date.getYear() + years_count, date.getMonth(), date.getDate() );
  }

  /**
   * @param date the date to start counting
   * @param years_count the number of years to add
   * @return a LinxoDate object representing the day right after the
   *         given date + years_count years.
   * @see #addYears(Date,int)
   */
  public static LinxoDate addYears( LinxoDate date, int years_count )
  {
    return new LinxoDate(date.getYear() + years_count, date.getMonth(), date.getDay());
  }

  public static boolean isSameDay( Date aDate, Date anotherDate )
  {
    return
        aDate.getYear() == anotherDate.getYear() &&
        aDate.getMonth() == anotherDate.getMonth() &&
        aDate.getDate() == anotherDate.getDate();
  }

  public static boolean isSameMonth( Date aDate, Date anotherDate )
  {
    return
        aDate.getYear() == anotherDate.getYear() &&
        aDate.getMonth() == anotherDate.getMonth();
  }

  public static int getMonthCount(final Date start, final Date end)
  {
    Date loop = new Date(start.getTime());
    int result = 0;
    while (!isSameMonth(loop, end)) {
      result++;
      // add one month to the loop month
      loop.setDate(1);
      if (loop.getMonth() == 11) {
        loop.setMonth(0);
        loop.setYear(loop.getYear() + 1);
      } else {
        loop.setMonth(loop.getMonth() + 1);
      }
    }
    return result + 1;
  }

  /**
   * @param date the reference date
   * @return the date representing the first day of the previous month
   * represented by the given Date.
   */
  public static Date firstDayOfPreviousMonth(Date date)
  {
    return date.getMonth()!=0 
        ? new Date(date.getYear(), date.getMonth()-1, 1)
        : new Date(date.getYear()-1,11,1);
  }

  /**
   * @param date the reference date
   * @return the date representing the first day of the previous month
   * represented by the given Date.
   */
  public static Date firstDayOfNextMonth(Date date)
  {
    return date.getMonth()!=11
            ? new Date(date.getYear(), date.getMonth()+1, 1)
            : new Date(date.getYear()+1,0,1);
  }

  /**
   * @param year - the year minus {@link #MAGIC_YEAR 1900}.
   * @param month - the month between 0-11.
   * @return A Date object that represents the first day of the given month and
   *         year at 0:00.
   */
  public static Date firstDayOfMonth(int month, int year)
  {
    return new Date(year, month, 1);
  }

  /**
   * This method is appropriate to be used with {@link Date} objects. For {@link LinxoDate},
   * You will have to remove {@link #MAGIC_YEAR 1900} and {@link #MAGIC_MONTH}
   *
   * @param year - the year minus {@link #MAGIC_YEAR 1900}.
   * @param month - the month between 0-11.
   * @return A Date object that represents the last day of the given month and
   *         year at 23:59.59.
   */
  public static Date lastDayOfMonth(int month, int year)
  {
    Date firstDayOfNextMonth = (month == 11)
        ?firstDayOfMonth(0, year + 1)
        :firstDayOfMonth(month+1, year);

    return new Date(firstDayOfNextMonth.getTime() - SECOND_MS);
  }



  public static Set<Date> getMonthsBetween(final Date start, final Date end)
  {
    if (start.getYear() > end.getYear()
        || (start.getYear() == end.getYear()
            && start.getMonth() > end.getMonth())) {
      throw new RuntimeException("Cannot get months between [" + start + "] and ["
                                 + end + "] as start date is after end date");
    }
    Set<Date> dates = new HashSet<Date>();
    dates.add(start);
    Date loop = new Date(start.getTime());

    while (!isSameMonth(loop, end)) {
      // add one month to the loop month
      loop.setDate(1);
      if (loop.getMonth() == 11) {
        loop.setMonth(0);
        loop.setYear(loop.getYear() + 1);
      } else {
        loop.setMonth(loop.getMonth() + 1);
      }
      dates.add(new Date(loop.getTime()));
    }
    return dates;
  }

  public static int compareMonthsYears(Date date1, Date date2) {
    int year1 = date1.getYear(), month1 = date1.getMonth();
    int year2 = date2.getYear(), month2 = date2.getMonth();
    return (year1 != year2) ? compareInt(year1, year2) : compareInt(month1, month2);
  }

  public static boolean equalsMonthsYears(Date date1, Date date2) {
    return compareMonthsYears(date1, date2) == 0;
  }

  private static int compareInt(int a, int b) {
    return a < b ? -1 : (a == b ? 0 : 1);
  }


  public static Date getStartData(Date startData) {
    if (startData != null) {
      return firstDayOfMonth(startData.getMonth(), startData.getYear());
    } else {
      return null;
    }
  }


  // LINXO DATE stuff

  public static Date getDate(LinxoDate date)
  {
    return
        date == null
            ?null
            :new Date(date.getYear() - DateUtils.MAGIC_YEAR, date.getMonth() - DateUtils.MAGIC_MONTH, date.getDay());
  }

  public static LinxoDate getLinxoDate(Date date)
  {
    return
        date == null
            ?null
            :new LinxoDate(date.getYear() + DateUtils.MAGIC_YEAR, date.getMonth() + DateUtils.MAGIC_MONTH, date.getDate());
  }

  // /!\ DO NOT USE THIS METHOD IF YOU COUNT ON DAYS /!\
  // APP-4078 - DateUtils.addMonths(new LinxoDate(2013, 5, 31), 1)
  //            returns 31/06/2013, which does not exist.
  @SuppressWarnings("UnusedDeclaration") // used in Android
  public static LinxoDate addMonths(int monthSpan)
  {
    return addMonths(LinxoDate.today(), monthSpan);
  }

  // /!\ DO NOT USE THIS METHOD IF YOU COUNT ON DAYS /!\
  // APP-4078 - DateUtils.addMonths(new LinxoDate(2013, 5, 31), 1)
  //            returns 31/06/2013, which does not exist.
  public static LinxoDate addMonths(LinxoDate date, int monthSpan)
  {
    int year = date.getYear();
    int month = date.getMonth();

    month += monthSpan;

    if(monthSpan >= 0) {
      while(month > 12) {
        month -= 12;
        year++;
      }
    }
    else {
      while(month < 1) {
        month += 12;
        year--;
      }
    }

    return new LinxoDate(year, month, date.getDay());
  }

  @SuppressWarnings("UnusedDeclaration") // used in Android
  public static int monthSpan(LinxoDate date1, LinxoDate date2)
  {
    if (date1 == null || date2 == null) {
      return 0;
    }

    int span;

    LinxoDate oldestDate;
    LinxoDate newestDate;

    boolean correctOrder = (date1.compareTo(date2) <= 0);
    if (correctOrder) {
      oldestDate = date1;
      newestDate = date2;
    } else {
      oldestDate = date2;
      newestDate = date1;
    }

    if (oldestDate.getYear() == newestDate.getYear()) {
      span = newestDate.getMonth() - oldestDate.getMonth();
    } else {
      int fullYears = newestDate.getYear() - oldestDate.getYear() - 1;
      span = (fullYears * 12) + (newestDate.getMonth() - 1) + (13 - oldestDate.getMonth());
    }

    if (!correctOrder) {
      span *= -1;
    }

    return span;
  }

  /**
   * Return the {@link #addDays(LinxoDate, int) number of days to add} to {@code that} to obtain {@code other}.
   *
   * @param that -
   * @param other -
   * @return the {@link #addDays(LinxoDate, int) number of days to add} to {@code that} to obtain {@code other}.
   * @throws java.lang.NullPointerException if that or other is {@code null}
   * @see #addDays(LinxoDate, int)
   */
  public static int daySpan(LinxoDate that, LinxoDate other)
  {
    if(that == null || other == null) {
      throw new NullPointerException("Arguments cannot be null");
    }

    int increment = (that.compareTo(other) < 0)
        ? +1
        : -1;

    int span = 0;
    LinxoDate current = that;
    while(!current.equals(other)) {
      span += increment;
      current = addDays(current, increment);
    }

    return span;
  }

  /**
   * @param date the original date
   * @return the Date of the monday that happened just before the give Date
   * The time element of this date remains the same.
   */
  public static Date getStartOfWeek(Date date)
  {
    Date currentDate = date;
    while (currentDate.getDay() != 1 /*MONDAY*/)
    {
      currentDate = addDays(currentDate, -1);
    }

    return currentDate;
  }

  /**
   * Returns the latest date. If dates are equals, the first one is returned.
   *
   * @param date1 The first date.
   * @param date2 The second date.
   *
   * @return The latest date. If dates are equals, the first one is returned.
   */
  public static Date max(final Date date1, final Date date2)
  {
    return date1.compareTo(date2) >= 0 ? date1 : date2;
  }

  /**
   * Returns the oldest date. If dates are equals, the first one is returned.
   *
   * @param date1 The first date.
   * @param date2 The second date.
   *
   * @return The oldest date. If dates are equals, the first one is returned.
   */
  public static Date min(final Date date1, final Date date2)
  {
    return date1.compareTo(date2) <= 0 ? date1 : date2;
  }

}
