/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.sponsor;

public enum SocialProvider
{
  /*
   * /!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
   *     THESE ENUMS MUST BE ALL LOWER CASE
   * /!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\
   *
   * If they are not, the ConnectionFactoryLocator just do not find the correct
   * ConnectionFactory.
   */
  facebook,
  twitter,
}
