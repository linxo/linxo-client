/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.sponsor;

public enum SocialAction
{
  Share("share"),
  Like("like"),
  Follow("follow"),
  Birthday("birthday"),
  ;

  private final String value;

  private SocialAction(final String value)
  {
    this.value = value;
  }

  public final String getValue()
  {
    return value;
  }

  public static SocialAction getSocialAction(final String value)
  {
    for (SocialAction sa : values()) {
      if (sa.value.equals(value)) {
        return sa;
      }
    }
    return null;
  }

}
