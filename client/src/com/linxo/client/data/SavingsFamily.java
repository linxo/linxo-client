/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data;

/**
 * This enum used to be defining families of Savings Account Types.
 *
 * For more flexibility, the pairing (type vs. family) will be done in a table in the
 * Service, and will be made transparent to the API. Instead of using this enum, new
 * development should be using values (static String objects) in the interface
 * {@link SavingsAccountsFamilies}
 *
 * @see SavingsAccountsFamilies
 * @deprecated use SavingsAccountsFamilies instead
 */
@Deprecated
public enum SavingsFamily
{
  LIV(0),
  TERME(1),
  BOURSE(2),
  AVIECAPI(3),
  RETRAITE(4),
  AUTRE(5);

  private final int displayOrder;

  SavingsFamily(int displayOrder)
  {
    this.displayOrder = displayOrder;
  }

  public final int getDisplayOrder()
  {
    return displayOrder;
  }

}
