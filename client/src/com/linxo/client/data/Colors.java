/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 28/01/2014 by hugues.
 */
package com.linxo.client.data;

public class Colors
{

  // Nested Types (mixing inner and static classes is okay)

  private static class RGB
  {
    int r, g, b;
  }

  private static class HSV
  {
    double hue, saturation, value;
  }

  // Static Fields

  private static String HEX_DIGITS = "0123456789ABCDEF";

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  public static String[] findAnalogous(String color, double lTint, double dShade)
  {
    if (color == null || color.length() != 6) {
      throw new RuntimeException("color string is null or is not a color (length != 6)");
    }

    for (int i = 0; i < 6; i++) {
      if (HEX_DIGITS.indexOf(color.toUpperCase().charAt(i)) == -1) {
        throw new RuntimeException("color string is not a color (char[" + i + "] not in " + HEX_DIGITS + ")");
      }
    }

    final RGB thisRgb = new RGB();
    thisRgb.r = hex2Dec(color.substring(0, 2));
    thisRgb.g = hex2Dec(color.substring(2, 4));
    thisRgb.b = hex2Dec(color.substring(4, 6));

    String d
        = dec2Hex((int) Math.round(thisRgb.r * dShade))
        + dec2Hex((int) Math.round(thisRgb.g * dShade))
        + dec2Hex((int) Math.round(thisRgb.b * dShade));

    String l
        = dec2Hex((int) Math.round(thisRgb.r + (255. - thisRgb.r) * lTint))
        + dec2Hex((int) Math.round(thisRgb.g + (255. - thisRgb.g) * lTint))
        + dec2Hex((int) Math.round(thisRgb.b + (255. - thisRgb.b) * lTint));


    return new String[]{
        color,
        l,
        d,
    };
  }

  private static String dec2Hex(int d)
  {
    return
        String.valueOf(HEX_DIGITS.charAt((d - (d % 16)) / 16))
            + String.valueOf(HEX_DIGITS.charAt(d % 16));
  }

  private static int hex2Dec(String h)
  {
    h = h.toUpperCase();
    int d = 0;
    for (int i = 0; i < h.length(); i++) {
      d = d * 16;
      d += HEX_DIGITS.indexOf(h.charAt(i));
    }
    return d;
  }

}
