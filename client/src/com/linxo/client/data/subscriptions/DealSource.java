/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.subscriptions;

public enum DealSource
{
  Paypal,          // Paypal Payments
  PaypalPromos,    // Paypal Promotions
  ItunesIphone,    // Apple InApp's
  ItunesIpad,      // Apple InApp's
  GooglePlay,      // Google InApp's
  Support,         // Added by support (payed by check, etc)
  FreeSupport,     // Offered by Linxo
  FreeSponsorship, // Offered by Linxo
  AccountGroup,    // Not used in Linxo
  WindowsPhone,    // Microsoft InApp's
  Windows8,        // Microsoft InApp's
  WindowsUniversal,// Microsoft InApp's
  Partnership,     // Offered because of a partnership
}
