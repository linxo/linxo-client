/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.data.subscriptions;

// https://developer.paypal.com/webapps/developer/applications/ipn_simulator
// PayPal statuses for Express Checkout:
//  - Canceled_Reversal
//  - Completed
//  - Denied
//  - Expired
//  - Failed
//  - In-Progress
//  - Partially_Refunded
//  - Pending
//  - Processed
//  - Refunded
//  - Reversed
//  - Voided

// Android statuses:
// The purchase state of the order. Possible values are 0 (purchased), 1 (canceled), or 2 (refunded).

// iOS statuses (only xxxPurchased and xxxRestored can be sent by the app):
// SKPaymentTransactionStatePurchasing,    // Transaction is being added to the server queue.
// SKPaymentTransactionStatePurchased,     // Transaction is in queue, user has been charged.  Client should complete the transaction.
// SKPaymentTransactionStateFailed,        // Transaction was cancelled or failed before being added to the server queue.
// SKPaymentTransactionStateRestored       // Transaction was restored from user's purchase history.  Client should complete the transaction.

public enum PaymentStatus
{
  Canceled("canceled", "cancelled"),
  CanceledReversal("canceled-reversal", "canceled_reversal"),
  Denied("denied"),
  Expired("expired"),
  Failed("failed"),
  InProgress("in-progress", "in_progress"),
  PartiallyRefunded("partially-refunded", "partially_refunded"),
  Pending("pending"),
  Processing("processing", "purchasing"),
  Refunded("refunded"),
  Restored("restored"),
  Reversed("reversed"),
  Success("completed", "purchased", "processed"),
  Voided("voided");

  private String[] matchingStrings;

  private PaymentStatus(String... matchingStrings)
  {
    this.matchingStrings = matchingStrings;
  }

  public static PaymentStatus getStatus(final String payPalStatus)
  {
    for (PaymentStatus paymentStatus : PaymentStatus.values()) {
      for (String matching : paymentStatus.matchingStrings) {
        if (payPalStatus.trim().toLowerCase().equals(matching)) {
          return paymentStatus;
        }
      }
    }
    return null;
  }

}
