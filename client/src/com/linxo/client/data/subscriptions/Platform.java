/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2017 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.data.subscriptions;

/**
 * Enum for different platform available
 */
public enum Platform {

  /**
   * Web platform. A user on this platform can receive offers from these deal sources:
   *  - Paypal
   *  - PaypalPromos
   *  - Partnership
   *  - Sogenactif
   */
  web,

  /**
   * Web platform. A user on this platform can receive offers from these deal sources:
   *  - ItunesIphone
   *  - Partnership
   *
   *  Note: The ItunesIpad deal source is not used since the iOS application is universal.
   *        The old iPad application will keep using GetOffersAction with the deal source
   *        parameter. So we consider than for the ios platform, the only itunes-related
   *        deals source is ItunesIphone.
   */
  ios,

  /**
   * Android platform. A user on this platform can receive offers from these deal sources:
   *  - GooglePlay
   *  - Partnership
   */
  android,

  /**
   * Windows platform. A user on this platform can receive offers from these deal sources:
   *  - WindowsUniversal
   *  - Partnership
   *
   *  Note: The WindowsPhone and Windows8 deal sources are not used since the Windows application
   *        is universal. The old Windows applications will keep using GetOffersAction with the
   *        deal source parameter. So we consider than for the windows platform, the only
   *        windows-related deals source is WindowsUniversal.
   */
  windows,

  /**
   * Blackberry platform. There is currently no offer available on this platform.
   */
  blackberry,

  /**
   * Amazon platform. There is currently no offer available on this platform.
   */
  amazon,

}
