/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.subscriptions;


public enum BillingCycle
{
  Daily,
  Monthly,
  Yearly,
  NoCycle, // infinite deals added at the same time than Partnership deal source
}
