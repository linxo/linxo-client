/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.subscriptions;

public enum PurchaseProperties
{
  // The property for the receipt received from iOS apps
  // and that the server has to verify with Apple.
  iTunesReceiptData,

  // The property for the receipt received from Android apps
  // and that the server has to verify.
  GooglePlayPurchaseData,
  GooglePlaySignature,

  // The property for the receipt received from Windows Phone AND Windows8 apps
  // and that the server has to verify.
  WindowsPhoneReceiptData,
}
