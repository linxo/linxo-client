/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 26/07/2010 by hugues.
 */
package com.linxo.client.data;

public enum ViewToken
{
  // auth
  Login("Login", Page.Auth),

  // overview
  Overview("Overview", Page.Overview),

  // history
  Search("Search", Page.History),

  // trends
  Trends("Trends", Page.Trends),

  // profile
  Accounts("Accounts", Page.Profile),
  BankSelection("BankSelection", Page.Profile),
  BankConnection("Connection", Page.Profile),
  BankConnectionUpdate("BankConnectionUpdate", Page.Profile),
  Subscription("Subscription", Page.Profile),
  Notifications("Reports", Page.Profile),
  MyProfile("Profile", Page.Profile),
  MyTrends("Trends", Page.Profile),
  Tags("Tags", Page.Profile),
  Coupons("Coupons", Page.Profile),
  Categories("Categories", Page.Profile),
  PersonalizedViews("PersonalizedViews", Page.Profile)
  ;

  public static final String LABEL_SEPARATOR = "/";
  public static final String TYPE_SEPARATOR = ",";
  public static final String HISTORY_TOKEN = "#";
  public static final String PARAM_SEPARATOR = ";";
  public static final String VALUE_SEPARATOR = "=";
  public static final String MESSAGE_VALUE_SUCCESS = "SUCCESS";
  public static final String DATE_FORMAT_STRING = "M/d/yy";

  public enum Page
  {
    Auth,
    Overview,
    History,
    Trends,
    Profile
  }

  private final String token;

  private final Page page;

  ViewToken(final String token, final Page page)
  {
    this.token = token;
    this.page = page;
  }

  public final String getToken()
  {
    return token;
  }

  public final Page getPage()
  {
    return page;
  }

  public static ViewToken getViewToken(final String token)
  {
    for (ViewToken viewToken : values()) {
      if (viewToken.getToken().equals(token)) {
        return viewToken;
      }
    }
    return null;
  }

}
