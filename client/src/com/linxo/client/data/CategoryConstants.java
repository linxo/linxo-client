/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public interface CategoryConstants
{
  // this is used in ClassificationServiceImpl and ClassificationManagerImpl
  // Please check before making any change
  public final static long UNCATEGORIZED_THEME_ID = 0;
  public final static long POTENTIAL_TRANSFER_ID = 100;
  public final static long CHECK_ID = 200;
  public final static long ATM_ID = 300;

  public final static Long[] UNCATEGORIZED_IDS = new Long[] {
      CategoryConstants.UNCATEGORIZED_THEME_ID,
      CategoryConstants.POTENTIAL_TRANSFER_ID,
      CategoryConstants.CHECK_ID,
      CategoryConstants.ATM_ID
  };

  public final static long INCOME_THEME_ID = 200000;
  public final static long EXPENSE_THEME_ID = 400000;

  public final static long INCOME_CAT_ID = 200100;

  public final static long EXCLUDE_FROM_BUDGET_THEME_ID = 600000;
  public final static long INTERNAL_TRANSFER_ID = 600110;

  public final static String INCOME_THEME_KEY = "incomeTheme";
  public final static String INCOME_CAT_KEY = "incomeCat";
  public final static String EXPENSE_THEME_KEY = "expenseTheme";
  public final static String UNCATEGORIZED_THEME_KEY = "uncategorized";
  public final static String EXCLUDE_FROM_BUDGET_THEME_KEY = "excludeFromBudgetTheme";

  // the maximum number of categories a user can add to the same category
  public static final long MAX_CUSTOM_CATEGORIES_PER_PARENT = 10;
  public static final long CUSTOM_CATEGORIES_NAME_MAX_LENGTH = 100;

  public final static List<String> ICONS = Arrays.asList(
      "custom-0", "custom-1", "custom-2", "custom-3", "custom-4", "custom-5", "custom-6", "custom-7",
      "custom-8", "custom-9", "custom-10", "custom-11", "custom-12", "custom-13",
      "0", "100", "200", "300",
      "200100", "200110", "200120", "200130", "200140", "200150", "200160", "200170", "200180", "200190",
      "400000", "400100", "400110", "400120", "400130", "400140", "400150", "400160", "400170", "400180", "400190", "400111", "400112",
      "400200", "400205", "400210", "400220", "400230", "400240", "400250", "400260", "400270", "400280", "400290",
      "400300", "400310", "400320", "400330", "400340", "400350",
      "400400", "400410", "400420", "400430", "400440", "400450", "400460", "400470",
      "400500", "400510", "400520", "400530", "400540",
      "400600", "400610", "400620",
      "400700", "400710", "400720", "400730", "400740", "400750", "400760",
      "400800", "400810", "400820", "400830", "400840", "400850",
      "400900", "400910", "400920", "400930", "400940",
      "401000", "401010", "401020", "401030", "401040", "401050", "401060", "401070", "401080",
      "600100", "600110", "600120", "600130", "600140", "600150", "600160", "600170"
  );

  public final static Map<Long, String> OLD_COLOR_PER_CATEGORY_ID = new HashMap<Long, String>() {
    {
      // uncategorized

      this.put(0L, "777777");
      this.put(100L, "888888");
      this.put(200L, "999999");
      this.put(300L, "AAAAAA");

      // income

      this.put(200000L, "e28772");
      this.put(200100L, "e28772");
      this.put(200110L, "e2ac6d");
      this.put(200120L, "e7d282");
      this.put(200130L, "bfc984");
      this.put(200140L, "8ab67f");
      this.put(200150L, "64ada3");
      this.put(200160L, "886d9b");
      this.put(200170L, "63739b");
      this.put(200180L, "d377a9");
      this.put(200190L, "c26276");

      // expenses

      this.put(400000L, "e28772");

      this.put(400100L, "e28772");
      this.put(400110L, "e2ac6d");
      this.put(400120L, "e7d282");
      this.put(400130L, "bfc984");
      this.put(400140L, "8ab67f");
      this.put(400150L, "64ada3");
      this.put(400160L, "886d9b");
      this.put(400170L, "63739b");
      this.put(400180L, "d377a9");
      this.put(400190L, "c26276");
      this.put(400111L, "8f7902");
      this.put(400112L, "0032ad");

      this.put(400200L, "e2ac6d");
      this.put(400205L, "0032ad");
      this.put(400210L, "e7d282");
      this.put(400220L, "bfc984");
      this.put(400230L, "8ab67f");
      this.put(400240L, "64ada3");
      this.put(400250L, "886d9b");
      this.put(400260L, "63739b");
      this.put(400270L, "d377a9");
      this.put(400280L, "c26276");
      this.put(400290L, "7d725f");

      this.put(400300L, "e7d282");
      this.put(400310L, "e2ac6d");
      this.put(400320L, "c26276");
      this.put(400330L, "bfc984");
      this.put(400340L, "8ab67f");
      this.put(400350L, "64ada3");

      this.put(400400L, "bfc984");
      this.put(400410L, "e2ac6d");
      this.put(400420L, "e7d282");
      this.put(400430L, "0032ad");
      this.put(400440L, "8ab67f");
      this.put(400450L, "64ada3");
      this.put(400460L, "886d9b");
      this.put(400470L, "63739b");

      this.put(400500L, "8ab67f");
      this.put(400510L, "e2ac6d");
      this.put(400520L, "e7d282");
      this.put(400530L, "bfc984");
      this.put(400540L, "0032ad");

      this.put(400600L, "64ada3");
      this.put(400610L, "e2ac6d");
      this.put(400620L, "e7d282");

      this.put(400700L, "886d9b");
      this.put(400710L, "e2ac6d");
      this.put(400720L, "e7d282");
      this.put(400730L, "bfc984");
      this.put(400740L, "8ab67f");
      this.put(400750L, "64ada3");
      this.put(400760L, "0032ad");

      this.put(400800L, "63739b");
      this.put(400810L, "e2ac6d");
      this.put(400820L, "e7d282");
      this.put(400830L, "bfc984");
      this.put(400840L, "8ab67f");
      this.put(400850L, "64ada3");

      this.put(400900L, "d377a9");
      this.put(400910L, "e2ac6d");
      this.put(400920L, "e7d282");
      this.put(400930L, "bfc984");
      this.put(400940L, "8ab67f");

      this.put(401000L, "c26276");
      this.put(401010L, "e2ac6d");
      this.put(401020L, "e7d282");
      this.put(401030L, "bfc984");
      this.put(401040L, "8ab67f");
      this.put(401050L, "64ada3");
      this.put(401060L, "886d9b");
      this.put(401070L, "63739b");
      this.put(401080L, "d377a9");

      // out of budget

      this.put(600000L, "e28772");
      this.put(600100L, "e28772");
      this.put(600110L, "e2ac6d");
      this.put(600120L, "e7d282");
      this.put(600130L, "bfc984");
      this.put(600140L, "8ab67f");
      this.put(600150L, "64ada3");
      this.put(600160L, "886d9b");
      this.put(600170L, "d377a9");
    }
  };

}
