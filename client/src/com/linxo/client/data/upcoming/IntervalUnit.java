/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.upcoming;

/**
 * Describes the unit used to compute time intervals.
 */
public enum IntervalUnit
{
  once,
  day,
  week,
  month,
  year,
  not_supported // for future use. TBD
}
