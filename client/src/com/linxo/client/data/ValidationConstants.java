/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data;

/**
 *
 */
public final class ValidationConstants
{
  /**
   * Extracted the hostname part of the email validator in {@link LoginValidator}
   */
  public static final String URL_VALIDATION_REGEXP
      = "http(s)?://(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(:[0-9]*)?(/.*)*";

  public static final int EMAIL_FRIENDS_CUSTOM_MESSAGE_MAX_LENGTH = 300;

  /**
   * format is 'M/d/yy' => 2 + '/' + 2 + '/' + 2 = 8 characters
   * Leave two more if the user put the full date
   */
  public static final int TRANSACTION_SEARCH_DATE_MAX_LENGTH = 10;

  public static final int TRANSACTION_MEMO_MAX_LENGTH = 512;

  public static final double PRECISION = 1.E-4;
}
