/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data;

/**
 * Category types for the charts.
 */
public enum CategoryType
{
  Income,
  Spending,
  ExclOutBudget // excludes from search what is in the Category theme "out-of-budget"
                // (so, it actually also includes the un-categorized amounts)
}
