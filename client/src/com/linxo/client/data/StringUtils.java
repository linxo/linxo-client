/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */

package com.linxo.client.data;


import java.util.HashSet;
import java.util.Set;

/**
 * String Utils that can be used by Gwt classes.
 * This class is compatible with GWT JRE emulation AND the server code.
 */
public class StringUtils {

  /**
   * <p>Compares two Strings, returning <code>true</code> if they are equal.</p>
   *
   * <p><code>null</code>s are handled without exceptions. Two <code>null</code>
   * references are considered to be equal. The comparison is case sensitive.</p>
   *
   * <pre>
   * StringUtils.equals(null, null)   = true
   * StringUtils.equals(null, "abc")  = false
   * StringUtils.equals("abc", null)  = false
   * StringUtils.equals("abc", "abc") = true
   * StringUtils.equals("abc", "ABC") = false
   * </pre>
   *
   * @see java.lang.String#equals(Object)
   * @param str  the first String, may be null
   * @param other  the second String, may be null
   * @return <code>true</code> if the Strings are equal, case sensitive, or
   *  both <code>null</code>
   */
  public static boolean equals(String str, String other)
  {
    return (str == null)
        ?(other == null)
        :str.equals(other);
  }

  /**
   * <p>Compares two Strings, returning <code>true</code> if they are equal ignoring
   * the case.</p>
   *
   * <p><code>null</code>s are handled without exceptions. Two <code>null</code>
   * references are considered equal. Comparison is case insensitive.</p>
   *
   * <pre>
   * StringUtils.equalsIgnoreCase(null, null)   = true
   * StringUtils.equalsIgnoreCase(null, "abc")  = false
   * StringUtils.equalsIgnoreCase("abc", null)  = false
   * StringUtils.equalsIgnoreCase("abc", "abc") = true
   * StringUtils.equalsIgnoreCase("abc", "ABC") = true
   * </pre>
   *
   * @see java.lang.String#equalsIgnoreCase(String)
   * @param str the first String, may be null
   * @param other  the second String, may be null
   * @return <code>true</code> if the Strings are equal, case insensitive, or
   *  both <code>null</code>
   */
  public static boolean equalsIgnoreCase(String str, String other)
  {
    return (str == null)
        ?(other == null)
        :str.equalsIgnoreCase(other);
  }

  /**
   * Returns true if the String is null or 0-sized
   * @param str the string object to check
   * @return true if the String is null or 0-sized
   */
  public static boolean isEmpty( String str )
  {
    return str == null || str.length() == 0;
  }

  public static boolean isEmptyTrimmed(final String str)
  {
    return str == null || str.trim().length() == 0;
  }

  /**
   * <p>Abbreviates a String using ellipses. This will turn
   * "Now is the time for all good men" into "Now is the time for..."</p>
   *
   * <p>Specifically:
   * </p>
   * <ul>
   *   <li>If <code>str</code> is less than <code>maxWidth</code> characters
   *       long, return it.</li>
   *   <li>Else abbreviate it to <code>(substring(str, 0, max-3) + "...")</code>.</li>
   *   <li>If <code>maxWidth</code> is less than <code>4</code>, throw an
   *       <code>IllegalArgumentException</code>.</li>
   *   <li>In no case will it return a String of length greater than
   *       <code>maxWidth</code>.</li>
   * </ul>
   *
   * <pre>
   * StringUtils.abbreviate(null, *)      = null
   * StringUtils.abbreviate("", 4)        = ""
   * StringUtils.abbreviate("abcdefg", 6) = "abc..."
   * StringUtils.abbreviate("abcdefg", 7) = "abcdefg"
   * StringUtils.abbreviate("abcdefg", 8) = "abcdefg"
   * StringUtils.abbreviate("abcdefg", 4) = "a..."
   * StringUtils.abbreviate("abcdefg", 3) = IllegalArgumentException
   * </pre>
   *
   * @param str  the String to check, may be null
   * @param maxWidth  maximum length of result String, must be at least 4
   * @param trimWhiteSpace whether or not the abbreviated string should be
   *                       trim'ed before concatenating "..."
   * @return abbreviated String, <code>null</code> if null String input
   * @throws IllegalArgumentException if the width is too small
   */
  public static String abbreviate( String str, int maxWidth, boolean trimWhiteSpace )
  {
    if ( str == null ) return null;

    if ( maxWidth < 4 ) throw new IllegalArgumentException( "width is too small: " + maxWidth );

    if ( str.length() <= maxWidth )
    {
      return str;
    }

    if ( trimWhiteSpace ) {
      return str.substring( 0, maxWidth - 3).trim() + "...";
    }

    return str.substring( 0, maxWidth - 3) + "...";
  }

  /**
   * Copied and adapted from apache commons lang WordUtils.java
   *
   * <p>Capitalizes all the delimiter separated words in a String.
   * Only the first letter of each word is changed.</p>
   *
   * <p>The delimiters represent a set of characters understood to separate words.
   * The first string character and the first non-delimiter character after a
   * delimiter will be capitalized. </p>
   *
   * <p>A <code>null</code> input String returns <code>null</code>.
   * Capitalization uses the unicode title case, normally equivalent to
   * upper case.</p>
   *
   * <pre>
   * StringUtils.capitalizeWords(null)        = null
   * StringUtils.capitalizeWords("")          = ""
   * StringUtils.capitalizeWords("i am FINE") = "I Am FINE"
   * </pre>
   *
   * @param str  the String to capitalize, may be null
   * @param delimiters  set of characters to determine capitalization
   * @return capitalized String, <code>null</code> if null String input
   */
  public static String capitalizeWords(String str, char[] delimiters) {
    if (str == null || str.length() == 0) {
      return str;
    }
    int strLen = str.length();
    final StringBuilder buffer = new StringBuilder(strLen);

    int delimitersLen = 0;
    if(delimiters != null) {
      delimitersLen = delimiters.length;
    }

    boolean capitalizeNext = true;
    for (int i = 0; i < strLen; i++) {
      char ch = str.charAt(i);

      boolean isDelimiter = false;
      if(delimiters == null) {
        // GWT JRE implementation only provider this deprecated method to test for "space"
        // original: isDelimiter = Character.isWhitespace(ch);
        isDelimiter = Character.isSpace(ch);
      } else {
        for(int j=0; j < delimitersLen; j++) {
          if(ch == delimiters[j]) {
            isDelimiter = true;
            break;
          }
        }
      }

      if (isDelimiter) {
        buffer.append(ch);
        capitalizeNext = true;
      } else if (capitalizeNext) {
        // GWT JRE implementation only provider this method to transform into "Title" case
        // original: buffer.append(Character.toTitleCase(ch));
        buffer.append(Character.toUpperCase(ch));
        capitalizeNext = false;
      } else {
        buffer.append(ch);
      }
    }
    return buffer.toString();
  }

  public static String capitalizeWords(String str) {
      return capitalizeWords(str, null);
  }

  public static String getFullName(String title, String fName, String lName)
  {
    boolean rtl = false;
    title = (isEmpty(title)?"":title);
    if(rtl) {
      return capitalizeWords(lName+ " " + fName +  " " + title);
    }
    else {
      return capitalizeWords(title + " " + fName + " " + lName);
    }
  }

  public static String stripWhiteSpace(String s){
    return s.replaceAll("[\\s]*","");
  }

  /**
   * very very simple replacement for java.text.MessageFormat.format()
   *
   * @param format - a format String link "my message is {0}"
   * @param args - the list of arguments for the format
   * @return the formatted String
   */
  public static String format(final String format, final Object... args) {
    StringBuilder sb = new StringBuilder();
    int cur = 0;
    int len = format.length();
    while (cur < len) {
      int fi = format.indexOf('{', cur);
      if (fi != -1) {
        sb.append(format.substring(cur, fi));
        int si = format.indexOf('}', fi);
        if (si != -1) {
          String nStr = format.substring(fi + 1, si);
          int i = Integer.parseInt(nStr);
          sb.append(args[i]);
          cur = si + 1;
        } else {
          sb.append(format.substring(fi));
          break;
        }
      } else {
        sb.append(format.substring(cur, len));
        break;
      }
    }
    return sb.toString();
  }

  /**
   * Note: this implementation is case-sensitive.
   * @param s1 -
   * @param s2 -
   * @return Computes Dice's Coefficient between two strings: 1 means strings are equals,
   * 0 means nothing in common
   * @see <a href="http://en.wikipedia.org/wiki/Dice%27s_coefficient">Dice's Coefficient</a>
   */
  public static double diceCoefficient(String s1, String s2) {
    Set<String> nx = new HashSet<String>();
    Set<String> ny = new HashSet<String>();

    for (int i=0; i < s1.length()-1; i++) {
      char x1 = s1.charAt(i);
      char x2 = s1.charAt(i+1);
      String tmp = "" + x1 + x2;
      nx.add(tmp);
    }
    for (int j=0; j < s2.length()-1; j++) {
      char y1 = s2.charAt(j);
      char y2 = s2.charAt(j+1);
      String tmp = "" + y1 + y2;
      ny.add(tmp);
    }

    Set<String> intersection = new HashSet<String>(nx);
    intersection.retainAll(ny);
    double bigramCount = intersection.size();

    return (2*bigramCount) / (nx.size()+ny.size());
  }

  private static int minimum(int a, int b, int c) {
    return Math.min(Math.min(a, b), c);
  }

  /**
   * Note: this implementation is case-sensitive.
   * @param s1 -
   * @param s2 -
   * @return Computes Levenshtein's Distance between two strings.
   * 0 means Strings are equal().
   * @see <a href="http://en.wikipedia.org/wiki/Levenshtein_distance">Levenshtein's Distance</a>
   */
  public static int levenshteinDistance(CharSequence s1, CharSequence s2)
  {
    int[][] distance = new int[s1.length() + 1][s2.length() + 1];

    for (int i = 0; i <= s1.length(); i++)
      distance[i][0] = i;
    for (int j = 1; j <= s2.length(); j++)
      distance[0][j] = j;

    for (int i = 1; i <= s1.length(); i++)
      for (int j = 1; j <= s2.length(); j++)
        distance[i][j] = minimum(
            distance[i - 1][j] + 1,
            distance[i][j - 1] + 1,
            distance[i - 1][j - 1]
                + ((s1.charAt(i - 1) == s2.charAt(j - 1)) ? 0
                : 1));

    return distance[s1.length()][s2.length()];
  }

  public static String filter(final String s)
  {
    return !StringUtils.isEmpty(s) ? s.replaceAll("[0-9]", "X").replace("\n", "\\n") : s;
  }

  /**
   * @param s - the URI string to escape
   * @return an encoded version of the given Uri. This implementation is
   * based in the 'encodeURI' javascript function and does not encode the
   * following characters: , / ? : @ &amp; = + $ #.
   * @see <a href='http://www.w3schools.com/jsref/jsref_encodeuri.asp'>http://www.w3schools.com/jsref/jsref_encodeuri.asp</a>
   * @see #DecodeUri(String)
   */
  public static native String encodeUri(String s)
    /*-{
      return encodeURI(s);
    }-*/;

  /**
   * @param s - the URI string to decode
   * @return a decode version of the given URI string. This implementation is
   * based in the 'decodeURI' javascript function.
   * @see #encodeUri(String)
   */
  public static native String DecodeUri(String s)
    /*-{
      return decodeURI(s);
    }-*/;

  /**
   * @param s - the URI string to escape
   * @return an encoded version of the given Uri. This implementation is
   * based in the 'encodeURIComponent' javascript function and does not encode the
   * following characters: , / ? : @ &amp; = + $ #.
   * @see <a href='http://www.w3schools.com/jsref/jsref_encodeuricomponent.asp'>http://www.w3schools.com/jsref/jsref_encodeuricomponent.asp</a>
   */
  public static native String encodeUriComponent(String s)
  /*-{
    return encodeURIComponent(s);
  }-*/;

  public static String formatWithNonBreakingSpace(String s)
  {
    return s.replace(' ', '\u00a0');
  }

}
