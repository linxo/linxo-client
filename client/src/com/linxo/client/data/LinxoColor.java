/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.data;

import java.io.Serializable;

/**
 * Simple class to hold a color code and color name.
 */
public class LinxoColor
    implements Serializable,Comparable<LinxoColor>
{
  private String name;
  private String code;


  public LinxoColor() {}

  public LinxoColor(String code, String name)
  {
    this.name = name;
    this.code = code;
  }


  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getCode()
  {
    return code;
  }

  public void setCode(String code)
  {
    this.code = code;
  }

  @Override
  public int compareTo(final LinxoColor linxoColor)
  {
    return this.name.compareToIgnoreCase(linxoColor.getName());
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    LinxoColor color = (LinxoColor) o;

    //noinspection SimplifiableIfStatement
    if (name != null ? !name.equals(color.name) : color.name != null) {
      return false;
    }
    return code != null ? code.equals(color.code) : color.code == null;
  }

  @Override
  public int hashCode()
  {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (code != null ? code.hashCode() : 0);
    return result;
  }

  @Override
  public String toString()
  {
    return "LinxoColor{"
           + "name='" + name + '\''
           + ", code='" + code + '\''
           + '}';
  }

}
