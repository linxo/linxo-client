/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.permissions;

public enum Feature
{
  VisibleHistory,
  ApiUtilization,
  PrioritizedSupport,
  BlockAdvertisement,
  EnhancedLogo,
  ExtendedExports,
  CustomCategories,
  UpcomingTransactions,
  PersonalizedViews
}
