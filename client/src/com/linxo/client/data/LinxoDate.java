/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 </pre>
 
 Created on : 5:21:19 PM by tarunmalhotra.
 */
package com.linxo.client.data;

import java.io.Serializable;

import java.util.Date;

public class LinxoDate
    implements Serializable,Comparable<LinxoDate>
{
  /**
   * NOTE: this date may be "invalid" ("29032005" where 2005 is not a leap year)
   * @param date - a String formatted as ddMMyyyy
   * @return the associated date
   */
  public static LinxoDate makeDate(String date){
    int day = Integer.parseInt(date.substring(0,2));
    int month = Integer.parseInt(date.substring(2,4));
    int year = Integer.parseInt(date.substring(4));

    return new LinxoDate(year, month, day);
  }

  /**
   * @return the linxoDate that represents today.
   */
  public static LinxoDate today() {
    return DateUtils.getLinxoDate(new Date());
  }

  private int year;
  private int month;
  private int day;

  // internal transient date to accelerate comparisons
  private transient Date internalDate;

  @SuppressWarnings("unused")
  private LinxoDate()
  {}

  public LinxoDate(int year, int month, int day)
  {
    this.year = year;
    this.month = month;
    this.day = day;
  }

  public int getDay()
  {
    return day;
  }

  public int getMonth()
  {
    return month;
  }

  public int getYear()
  {
    return year;
  }

  /**
   * Create a copy of the current object. We do not override the {@link #clone()} method as it is not
   * available in the GWT jre replacement.
   *
   * @return a copy of the current object
   */
  public final LinxoDate copy()
  {
    return new LinxoDate(this.getYear(), this.getMonth(), this.getDay());
  }

  @SuppressWarnings({ "NullableProblems", "deprecation" })
  @Override
  public int compareTo(LinxoDate other)
  {
    if(other == null)
    {
      throw new NullPointerException("Cannot compare with null object");
    }

    if(internalDate == null){
      internalDate = DateUtils.getDate(this);
    }
    if(other.internalDate == null){
      other.internalDate = DateUtils.getDate(other);
    }
    return internalDate.compareTo(other.internalDate);
  }

  public boolean isValid()
  {
    return this.compareTo(DateUtils.getLinxoDate(
        DateUtils.lastDayOfMonth(this.month - DateUtils.MAGIC_MONTH, this.year - DateUtils.MAGIC_YEAR))) <= 0;
  }

  @SuppressWarnings("SimplifiableIfStatement") // let's keep this human readable
  public boolean isBetween(LinxoDate startDate, LinxoDate endDate, boolean inclusive)
  {
    if(startDate == null || endDate == null)
    {
      return false;
    }

    if(inclusive && (this.equals(startDate) || this.equals(endDate)))
    {
      return true;
    }

    return (this.compareTo(startDate) > 0 && this.compareTo(endDate) < 0);
  }

  @Override
  public String toString()
  {
    return "LinxoDate{"
           + "year=" + year
           + ", month=" + month
           + ", day=" + day
           + '}';
  }

  @SuppressWarnings("RedundantIfStatement")
  @Override
  public boolean equals( Object o )
  {
    if (this == o) { return true; }
    if (!(o instanceof LinxoDate)) { return false; }

    final LinxoDate that = (LinxoDate) o;
    if (getDay()!=that.getDay()) {return false;}
    if (getMonth()!=that.getMonth()) {return false;}
    if (getYear()!=that.getYear()) {return false;}

    return true;
  }

  @Override
  public int hashCode()
  {
    int result;
    result = getDay();
    result = 31 * result + getMonth();
    result = 31 * result + getYear();
    return result;
  }
}
