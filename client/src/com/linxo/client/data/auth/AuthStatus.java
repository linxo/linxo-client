/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data.auth;

/**
 * RPC enum for com.linxo.services.auth.AuthBean.AuthStatus;
 */
public enum AuthStatus
{
  /** An user has already registered this identifier */
  IdentifierAlreadyInUse,
  /** A user has already registered a device with this identifier */
  DeviceAlreadyInUse,
  /** User is blocked */
  Blocked,
  /** User is not activated */
  Inactive,
  /** Invalid credentials */
  InvalidCredentials,
  /** Error checking the credentials */
  Error,
  /** All is okay for login */
  Success,
  /** Need to revalidate T&amp;Cs */
  AskForTermsAndConditions,
  /** DeviceLimitReached */
  DeviceLimitReached,
  /** More credentials are required. */
  CredentialsRequired,

  /**
   * CLIENT ONLY. NOT RETURNED BY SERVER
   * The PIN entered does not match
   */
  PinInvalid,

  /**
   * CLIENT ONLY. NOT RETURNED BY SERVER
   * The PIN entered does not match and too many attempts.
   * The token and device id have been cleared from the device
   */
  PinBlocked,

  /**
   * CLIENT ONLY. NOT RETURNED BY SERVER
   * There was an active session
   */
  SessionRestored,

  /**
   * Used for FI Authentication only, when a user B logs in
   * using a device previously attached to user A.
   */
  LogoutRequired,
}
