/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.data.auth;

/**
 * The different reasons why a login attempt can be blocking.
 */
public enum BlockedReason
{
  /**
   * The password received was invalid (for the 3rd time in a row).
   */
  InvalidPassword,

  /**
   * The identifier received was invalid.
   */
  InvalidIdentifier,

  /**
   * A device with the same id already existed for another user.
   */
  DeviceAlreadyInUse,

  /**
   * The received token did not match the device token.
   */
  InvalidDeviceToken,
}
