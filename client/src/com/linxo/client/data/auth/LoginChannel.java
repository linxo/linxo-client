/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.data.auth;

/**
 * The different channels for the login attempts.
 */
public enum LoginChannel
{
  /**
   * When a LoginDeviceAction is received. A known device re-connects.
   */
  AuthenticateDevice,

  /**
   * When an AuthorizeDeviceAction is received. A new device authenticates.
   */
  AuthorizeDevice,

  /**
   * When a user logs in on the web.
   */
  AuthenticateWeb,

  /**
   * We save a ResetPassword login attempt each time the password is reset, it
   * allow us to not block a password after only one error right after a reset.
   * Plus it's better for traceability.
   */
  ResetPassword,

  /**
   * Stored when an admin logs in as a user.
   */
  AdminAuthenticateWeb,
}
