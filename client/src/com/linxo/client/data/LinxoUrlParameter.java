/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.data;

public enum LinxoUrlParameter
{
  // auth
  @Deprecated // prefer User and Domain
  Email("e"),
  ZipCode("pc"),
  SponsorCode("sc"),
  NewLoginEmail("email"),
  NewLoginPassword("password"),
  NewLoginRememberEmail("rememberEmail"),
  NewLoginSecretMode("secretMode"),
  NewLoginConfirmPassword("confirmPassword"),
  NewLoginThemeReferer("themeReferer"),
  NewLoginCgu("cgu"),
  Next("next"),
  Tref("tref"),

  // user and domain are used together to build the email user@domain
  User("user"),
  Domain("domain"),

  // trends
  ParentCategory("parentCategory"),
  ChartType("chartType"),

  // trends and history
  AccountType("accountType"),
  AccountTypes("accountTypes"),
  StartDate("startDate"),
  EndDate("endDate"),

  // credit cards
  DebitDate("debitDate"),

  // amount
  Amount("amount"),
  MinAmount("minAmount"),
  MaxAmount("maxAmount"),

  // history
  CategoryId("categoryId"),
  LoadSubCategories("loadSubCategories"),
  CategoryType("categoryType"),
  UseBudgetDate("useBudgetDate"),
  SelectedViewIds("selectedViewIds"),
  UseBudgetAccounts("useBudgetAccounts"),
  UseSavingsAccounts("useSavingsAccounts"),
  UseAssetsAccounts("useAssetsAccounts"),
  AccountId("accountId"),
  Labels("labels"),
  TaggedOnly("taggedOnly"),
  TagId("tagId"),
  PageNumber("pageNumber"),
  ExtCustomType("extCustomType"),

  // profile
  Fid("fid"),
  GroupId("groupId"), // admin too

  // promos
  EncryptedUserId("u"),
  EncryptedOffer("o"),
  Promo("promo"),
  Offer("offer"),
  PaypalPromo("paypalPromo"),
  PaypalToken("token"),

  // reset password
  Key("key"),
  Rpk("rpk"),

  // verification
  @Deprecated // to be removed soon, replaced by VerificationKey
  ActivationKey("ak"),
  VerificationKey("vk"),
  VerificationHash("vh"),
  Redirect("redirect"),
  EncryptedIdParam("encryptedIdParam"),
  SponsorCodeParam("sponsorCodeParam"),

  // exports
  FileName("fileName"),
  FileExtension("extension"),
  Data("data"),

  SelectedTab("selectedTab"),
  ShowLoggedView("showLoggedView"),
  ShowMainHeaders("showMainHeaders"),
  ShowPremiumLogo("showPremiumLogo"),
  LogoUrl("logoUrl"),
  Message("m"),
  MessageType("mt"),
  CustomLink("cl"),
  CustomTitle("cl"),
  Action("action"),
  Header("header"),

  // Social
  SocialActionAfterConnect("actionAfterConnect"),
  SocialRedirectAfterConnect("redirectAfterConnect"),
  SocialProviderId("socialProviderId"),
  SocialShareServiceFreeDays("socialShareServiceFreeDays"),
  SocialSponsorshipFreeMonths("socialShareSponsorFreeMonths"),

  // OAuth
  OAuthState("state"),
  OAuthUser("seed"),
  OAuthGroupId("group_id"),
  OAuthStatus("status"),

  // Admin
  AdminUserEmail("userEmail"),
  AdminUserEmailILike("emailILike"),
  AdminUserId("userId"),
  AdminAccountReference("accountReference"),
  AdminTransactionReference("transactionReference"),
  AdminPasswordParam("password"),
  AdminDeviceId("deviceId"),
  AdminDebugWorkerName("debugWorkerName"),
  AdminPhoneNumber("phoneNumber"),
  AdminUserAddress("userAddress"),
  AdminUserAddress2("userAddress2"),
  AdminUserZipCode("zipCode"),
  AdminUserCity("city"),
  AdminUserCountry("country"),
  AdminDealId("dealId"),
  AdminPaymentId("paymentId"),
  AdminNotLoggedFor("notLoggedForDays"),
  AdminRunningForHours("runningForHours"),
  AdminStopGroupId("groupIdToStop"),
  AdminLoginIdentifier("loginIdentifier"),
  AdminLoginAttemptsSearchBy("loginAttemptSearchBy"),
  AdminRelaunchGroupId("launchGroupId"),
  AdminRelaunchOperation("launchOperation"),
  AdminRelaunchFid("launchFId"),
  AdminFid("fid"),
  AdminFids("fids"),
  AdminFiDetailsSumOnly("fiDetailsSumOnly"),
  AdminFidsToUpdate("fidsToUpdate"),
  AdminFiDisabledMessage("fidDisabledMessage"),
  AdminGetFis("getFis"),
  AdminRecentPeriodParam("recentPeriod"),
  AdminBlockedRecentPeriod("blockedRecentPeriod"),
  AdminFilename("filename"),
  AdminMinExpirationDate("minExpirationDate"),
  AdminMaxExpirationDate("maxExpirationDate"),
  AdminActivateUsersIds("activateUsersIdsAttribute"),
  AdminIsicEmail("emailIsic"),
  AdminIsicNumber("numberIsic"),
  AdminLoginReason("adminLoginReason"),
  // support deals
  AdminSupportDealsCount("supportDealsCount"),
  AdminSupportDealsComment("supportDealsComment"),
  AdminSupportDealsDays("supportDealsDays"),
  AdminSupportDealsFree("supportDealsFree"),
  // categorisation cache
  AdminCategorisationCacheLabel("categorisationCacheLabel"),
  AdminCategorisationCacheDebitsCategoryId("categorisationCacheDebitsCategoryId"),
  AdminCategorisationCacheCreditsCategoryId("categorisationCacheCreditsCategoryId"),
  AdminCategorisationCacheRemoveFor("categorisationCacheRemoveFor"),
  // bforbank imports
  AdminBforBankImportPath("bforbankImportPath"),
  AdminBforBankImportFile("bforbankImportFile"),
  AdminBforBankImportDate("bforbankImportDate"),
  AdminBforBankImportType("bforbankImportType"),

  // JSP Headers
  JspContext("JspContext"),
  ;

  private final String parameter;

  LinxoUrlParameter(final String parameter)
  {
    this.parameter = parameter;
  }

  public final String getParameter()
  {
    return parameter;
  }

}
