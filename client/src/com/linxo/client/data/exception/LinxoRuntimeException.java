/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2017 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 23/02/2017 by hugues.
 */
package com.linxo.client.data.exception;

import java.io.Serializable;

/**
 * A RuntimeException in the Linxo Platform
 */
public class LinxoRuntimeException extends RuntimeException implements Serializable {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors


  public LinxoRuntimeException() {
  }

  public LinxoRuntimeException(String message) {
    super(message);
  }

  public LinxoRuntimeException(Throwable cause) {
    super(cause.getMessage());
  }

  public LinxoRuntimeException(String message, Throwable cause) {
    super(message, cause);
  }

  // Instance Methods
}
