package com.linxo.client.net;

/**
 * Created by hermann on 24/10/2017.
 */
public class LinxoClientBuilder {

    static final int THREAD_POOL_SIZE = 5;
    static final int TIMEOUT = 10;

    String domain;
    String apiKey;
    String apiSecret;
    LinxoClient.UserCredentials userCredentials;
    LinxoClient.DeviceCredentials deviceCredentials;
    int threadPoolSize;
    int timeout;

    public LinxoClientBuilder(String domain, String apiKey, String apiSecret) {
        this.domain = domain;
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
        this.threadPoolSize = THREAD_POOL_SIZE;
        this.timeout = TIMEOUT;
    }

    /**
     * Host domain
     * @param domain Host
     * @return LinxoClientBuilder
     */
    public LinxoClientBuilder domain(String domain) {
        this.domain = domain;
        return this;
    }

    /**
     * API key
     * @param apiKey API key
     * @return LinxoClientBuilder
     */
    public LinxoClientBuilder apiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    /**
     * API secret
     * @param apiSecret API secret
     * @return LinxoClientBuilder
     */
    public LinxoClientBuilder apiSecret(String apiSecret) {
        this.apiSecret = apiSecret;
        return this;
    }

    /**
     * User credentails (email, password)
     * @param userCredentials User credentails
     * @return LinxoClientBuilder
     */
    public LinxoClientBuilder userCredentials(LinxoClient.UserCredentials userCredentials) {
        this.userCredentials = userCredentials;
        return this;
    }

    /**
     * Device credentails (deviceId, deviceToken, appId, appVersion)
     * @param deviceCredentials Device credentails
     * @return LinxoClientBuilder
     */
    public LinxoClientBuilder deviceCredentials(LinxoClient.DeviceCredentials deviceCredentials) {
        this.deviceCredentials = deviceCredentials;
        return this;
    }

    /**
     * Number of threads in the pool
     * @param threadPool Number of threads
     * @return LinxoClientBuilder
     */
    public LinxoClientBuilder threadPool(int threadPool) {
        this.threadPoolSize = threadPool;
        return this;
    }

    /**
     * HTTP request timeout expressed in seconds
     * @param timeout Number of seconds
     * @return LinxoClientBuilder
     */
    public LinxoClientBuilder timeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    public LinxoClient build() {
        return new LinxoClient(this);
    }
}
