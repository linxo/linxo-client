/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 30/09/2014 by hugues.
 */
package com.linxo.client.net;

import com.linxo.client.actions.*;
import com.linxo.client.actions.auth.AuthCookies;
import com.linxo.client.data.support.Version;
import com.linxo.client.json.LinxoJsonSerializer;
import com.linxo.infrastructure.exceptions.TechnicalException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.FutureRequestExecutionService;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class LinxoClient
{

  // Nested Types (mixing inner and static classes is okay)

  public static final class UserCredentials
  {
    private final String identifier;
    private final String password;

    public UserCredentials(String identifier, String password)
    {
      this.identifier = identifier;
      this.password = password;
    }

    @Override
    public String toString()
    {
      //noinspection StringBufferReplaceableByString
      final StringBuilder sb = new StringBuilder("UserCredentials{");
      sb.append("identifier='").append(identifier).append('\'');
      if(logger.isTraceEnabled()) {
        sb.append(", password='").append(password).append('\'');
      }
      else{
        sb.append(", password='").append("OBFUSCATED").append('\'');
      }
      sb.append('}');
      return sb.toString();
    }
  }

  public static final class DeviceCredentials
  {
    private String deviceId;
    private String token;
    private String appIdentifier;   // These fields come in replacement of the AppInfo object when the
    private String appVersion;      // stateless device login occurs.

    public DeviceCredentials(String deviceId, String token, String appIdentifier, String appVersion)
    {
      this.appIdentifier = appIdentifier;
      this.appVersion = appVersion;
      this.deviceId = deviceId;
      this.token = token;
    }

    @Override
    public String toString()
    {
      //noinspection StringBufferReplaceableByString
      final StringBuilder sb = new StringBuilder("DeviceCredentials{");
      sb.append("deviceId='").append(deviceId).append('\'');
      if(logger.isTraceEnabled()) {
        sb.append(", token='").append(token).append('\'');
      }
      else {
        sb.append(", token='").append("OBFUSCATED").append('\'');
      }
      sb.append(", appVersion='").append(appVersion).append('\'');
      sb.append(", appIdentifier='").append(appIdentifier).append('\'');
      sb.append('}');
      return sb.toString();
    }
  }


  // Static Fields

  // Static Initializers

  private static final Logger logger = Logger.getLogger(LinxoClient.class);

  private static final String API_VERSION = Version.CURRENT_VERSION.getVersionString();
  private static final String PATH_AUTH = "/auth.page";
  private static final String PATH_JSON = "/json";
  private static final String PATH_JSON_SUDO = "/sudojson";
  private static final String LINXO_SESSION_COOKIE_NAME = AuthCookies.SESSION_COOKIE_NAME;

  private static final String SERVER_DATE_TIME_FORMAT = "EEE, d MMM yyyy HH:mm:ss z";

  private static final String USER_AGENT = "LinxoClient/@VERSION@ (Api Version "+API_VERSION+")";
  private static final String TARGET_USER_HEADER = "TARGET_USER";

  // Static Methods

  // Instance Fields

  private final String apiKey;
  private final String apiSecret;
  private final URI jsonUri;
  private final URI jsonSudoUri;
  private final URI authUri;
  private int threadPoolSize = LinxoClientBuilder.THREAD_POOL_SIZE;
  private int timeout = LinxoClientBuilder.TIMEOUT;

  // provides the user credentials when in stateless mode.
  private final UserCredentials userCredentials;
  // provides the device credentials when in stateless mode.
  private final DeviceCredentials deviceCredentials;

  private CloseableHttpClient httpClient;
  private BasicCookieStore cookieStore;

  private FutureRequestExecutionService futureRequestExecutionService;

  private long offset;

  // Instance Initializers

  // Constructors
  @Deprecated
  public LinxoClient(String domain, String apiKey, String apiSecret)
      throws LinxoClientException
  {
    this(domain, apiKey, apiSecret, null, null);
  }
  @Deprecated
  public LinxoClient(String domain, String apiKey, String apiSecret,
                     UserCredentials userCredentials)
      throws LinxoClientException
  {
    this(domain, apiKey, apiSecret, userCredentials, null);
  }
  @Deprecated
  public LinxoClient(String domain, String apiKey, String apiSecret,
                     DeviceCredentials deviceCredentials)
      throws LinxoClientException
  {
    this(domain, apiKey, apiSecret, null, deviceCredentials);
  }

  /**
   * LinxoClientBuilder
   * @param builder LinxoClientBuilder
   */
  public LinxoClient(LinxoClientBuilder builder) {
    this(builder.domain,
            builder.apiKey,
            builder.apiSecret,
            builder.userCredentials,
            builder.deviceCredentials);

    threadPoolSize = builder.threadPoolSize;
    timeout = builder.timeout;
  }

  private LinxoClient(String domain, String apiKey, String apiSecret,
                     UserCredentials userCredentials,
                     DeviceCredentials deviceCredentials)
      throws LinxoClientException
  {
    this.apiKey = apiKey;
    this.apiSecret = apiSecret;

    // stateful mode is not-null
    this.userCredentials = userCredentials;
    this.deviceCredentials = deviceCredentials;

    try {
      jsonUri = new URI("https://" + domain + PATH_JSON);
      jsonSudoUri = new URI("https://" + domain + PATH_JSON_SUDO);
      authUri = new URI("https://" + domain + PATH_AUTH);
    }
    catch(URISyntaxException e) {
      throw new LinxoClientException(e);
    }
  }


  // Instance Methods

  /////////////////////////////////////////////////////////////////////////////
  //
  //                               Public API
  //
  /////////////////////////////////////////////////////////////////////////////


  public void initialize() throws LinxoClientException
  {
    initialize(null);
  }
  public void initialize(final List<Cookie> cookies) throws LinxoClientException
  {

    if (httpClient != null) {
      return; // initialized
    }

    cookieStore = new BasicCookieStore();

    // if we got cookies, add the cookies into the store
    if (cookies != null && !cookies.isEmpty()) {
      for (Cookie cookie : cookies) {
        cookieStore.addCookie(cookie);
      }
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Initializing HttpClient...");
    }
    long start = System.currentTimeMillis();

    RequestConfig httpGlobalConfig = RequestConfig.custom()
        .setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY)
        .setConnectTimeout(timeout * 1000)
        .setConnectionRequestTimeout(timeout * 1000)
        .setSocketTimeout(timeout * 1000)
        .build();

    PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
    connectionManager.setMaxTotal(threadPoolSize);
    connectionManager.setDefaultMaxPerRoute(threadPoolSize);

    httpClient = HttpClients.custom()
        .setDefaultRequestConfig(httpGlobalConfig)
        .setDefaultCookieStore(cookieStore)
        .setConnectionManager(connectionManager)
        .setDefaultHeaders(Arrays.asList(
                new BasicHeader("User-Agent", USER_AGENT)
                // todo: Accept-Encoding: gzip, deflate
        ))
        .build();

    ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);
    futureRequestExecutionService = new FutureRequestExecutionService(httpClient, executorService);

    if (userCredentials == null && deviceCredentials == null) {
      if (logger.isDebugEnabled()) {
        logger.debug("Initializing HttpClient Session (Stateful mode)...");
      }

      statefulInit();
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Initialized HttpClient in ["+(System.currentTimeMillis() - start)+"]ms...");
    }
  }

  private void statefulInit()
  {
    // First call to get the LinxoSession and JSESSIONID cookies
    HttpGet httpGet = new HttpGet(authUri);
    CloseableHttpResponse response = null;
    try {
      try {
        if(logger.isTraceEnabled()){
          logger.trace("Initial HttpClient getRequest..." + httpGet);
        }

        response = httpClient.execute(httpGet);

        if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
          throw new LinxoClientException("Could not initialize LinxoClient on " + authUri + " status:" + response.getStatusLine());
        }

        if(logger.isTraceEnabled()){
          logger.trace("Initial HttpClient response..." + response);
        }

        computerServerTimeOffset(response);

      }
      catch (ClientProtocolException cpe) {
        httpClient = null;
        throw new LinxoClientException("Could not initialize LinxoClient", cpe);
      }
      finally {
        if (response != null) {
          response.close();
        }
      }
    }
    catch (IOException ioe) {
      httpClient = null;
      throw new LinxoClientException("Could not initialize LinxoClient", ioe);
    }

    if(logger.isDebugEnabled()) {
      logger.debug("LinxoClient initialized");
    }
    if(logger.isTraceEnabled()) {
      logger.trace("LinxoSession: " + findLinxoSession());
      for(Cookie cookie : cookieStore.getCookies()){
        logger.trace(
            "cookieName=" + cookie.getName() +
                ", value=" + cookie.getValue() +
                ", path=" + cookie.getPath() +
                ", domain=" + cookie.getDomain());
      }
    }

  }

  public void close()
      throws LinxoClientException
  {
    if(httpClient != null) {
      try {
        futureRequestExecutionService.close();
        httpClient.close();
      }
      catch(IOException ioe) {
        throw new LinxoClientException("error while closing");
      }
      finally {
        futureRequestExecutionService = null;
        httpClient = null;

        cookieStore.clear();
        cookieStore = null;
      }
    }
  }

  public <R extends LinxoResult> LinxoRequest<R> createRequest(LinxoAction<R> action)
      throws TechnicalException
  {
    LinxoRequest<R> request;
    try {
      final String nonce = HashUtils.generateNonce();
      final long timeStamp = getServerTime();
      final String message = nonce + timeStamp + apiSecret;

      final Hash hash =
          (userCredentials != null)
              // stateless w/ userCredentials
              ? new Hash(nonce, Long.toString(timeStamp), apiKey, HashUtils.hashSha1Base64String(message),
                         userCredentials.identifier, userCredentials.password)
              : (
              (deviceCredentials != null)
                  // stateless w/ deviceCredentials
                  ? new Hash(nonce, Long.toString(timeStamp), apiKey, HashUtils.hashSha1Base64String(message),
                             deviceCredentials.deviceId, deviceCredentials.token, deviceCredentials.appIdentifier, deviceCredentials.appVersion)
                  // stateful
                  : new Hash(nonce, Long.toString(timeStamp), apiKey, HashUtils.hashSha1Base64String(message)));

      request = new LinxoRequest<R>(
          action,
          LinxoJsonSerializer.getJsonNameFromType(action.getClass().getName()),
          hash
      );
    }
    catch(Exception e) {
      throw new LinxoClientException("Error while creating LinxoRequest for Action["+action+"]", e);
    }

    if(action instanceof HasSecret) {
      ((HasSecret)action).setSharedSecret(findLinxoSession());
    }

    return request;
  }

  public <R extends LinxoResult> R sendAction(LinxoAction<R> action)
  {
    return sendAction(action, 0L);
  }

  public <R extends LinxoResult> R sendAction(LinxoAction<R> action, Long userId)
  {
    return doSendAction(action, userId);
  }

  /**
   * This method sends the given action and return the result we got from
   * the server. It also returns the list of cookies currently in the cookie
   * store used by the client. They can be used later on to send an action
   * for the same session (linxo server session).
   *
   * @param action The action to send
   * @param <R> The result we expect from the server.
   *
   * @return The result we got from the server. And the list of cookies
   *         currently in the cookie store used by the client. They can
   *         be used later on to send an action for the same session
   *         (linxo server session).
   */
  public <R extends LinxoResult> LinxoClientResult<R> sendActionAndGetCookies(LinxoAction<R> action)
  {
    // send the action and get the result
    final R result = doSendAction(action, 0L);

    // return the result and the cookies currently in the store
    return new LinxoClientResult<R>(cookieStore.getCookies(), result);
  }

  private  <R extends LinxoResult> R doSendAction(LinxoAction<R> action, Long userId)
      throws TechnicalException
  {
    if (logger.isDebugEnabled()) {
      logger.debug("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv - Sending action()");
    }
    long start;

    start = System.currentTimeMillis();
    LinxoRequest<R> req = createRequest(action);
    if (logger.isDebugEnabled()) {
      logger.debug("Created Request    in [" + (System.currentTimeMillis() - start) + "]ms");
    }

    start = System.currentTimeMillis();
    String json = LinxoJsonSerializer.serialize(req);
    if (logger.isDebugEnabled()) {
      logger.debug("Serialized Request in [" + (System.currentTimeMillis() - start) + "]ms");
    }

    if (logger.isTraceEnabled()) {
      logger.trace("Invoking Action[" + json + "]");
    }

    start = System.currentTimeMillis();
    HttpEntity entity = EntityBuilder.create()
        .setContentEncoding("utf-8")
        .setContentType(ContentType.APPLICATION_JSON)
        .setText(json)
        .build();
    if (logger.isDebugEnabled()) {
      logger.debug("Prepared HttpEnti. in [" + (System.currentTimeMillis() - start) + "]ms");
    }

    start = System.currentTimeMillis();
    final HttpPost httpPost = new HttpPost(userId != 0L ? jsonSudoUri : jsonUri);
//    httpPost.addHeader("Content-Type", "application/json");
    httpPost.addHeader("X-Linxo-API-Version", API_VERSION);
    httpPost.addHeader("Accept-Charset", "utf-8");
    if(userId != 0L){
      httpPost.addHeader(TARGET_USER_HEADER, userId.toString());
    }


    httpPost.setEntity(entity);
    if (logger.isDebugEnabled()) {
      logger.debug("Prepared HttpPost  in [" + (System.currentTimeMillis() - start) + "]ms");
    }

    start = System.currentTimeMillis();
    Future<R> futureResult = futureRequestExecutionService.execute(httpPost, HttpClientContext.create(),
        new ResponseHandler<R>() {
          @Override
          public R handleResponse(HttpResponse httpResponse)
              throws IOException
          {
            try {

              if (logger.isDebugEnabled()) {
                logger.debug("vovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovovo - Receiving response()");
              }

              if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new ClientProtocolException(
                    "Unexpected response status [" + httpResponse.getStatusLine() + "] while connecting to [" + jsonUri + "]");
              }

              String content = EntityUtils.toString(httpResponse.getEntity());

              if (logger.isTraceEnabled()) {
                logger.trace("Content received [" + content + "]");
              }

              LinxoResponse response = LinxoJsonSerializer.deSerialize(content);

              if (response.getResultName().endsWith("ErrorResult")) {
                throw new ClientProtocolException(new LinxoClientException((ErrorResult) response.getResult()));
              }
              //noinspection unchecked
              return (R) response.getResult();
            }
            finally {
              if (logger.isDebugEnabled()) {
                logger.debug("^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o^o - returned response()");
              }
            }
          }
        });
    if (logger.isDebugEnabled()) {
      logger.debug("Request Sent      in [" + (System.currentTimeMillis() - start) + "]ms");
    }

    try {
      start = System.currentTimeMillis();
      return futureResult.get();
    }
    catch (ExecutionException exc) {
      throw new LinxoClientException("Caught exception while collecting the result of " + action, exc);
    }
    catch (InterruptedException ir) {
      throw new LinxoClientException("Interrupted while collecting the result of " + action, ir);
    }
    finally {
      if (logger.isDebugEnabled()) {
        logger.debug("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ - Request Received in [" + (System.currentTimeMillis() - start) + "]ms");
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  //
  //                               Helpers
  //
  /////////////////////////////////////////////////////////////////////////////

  private void computerServerTimeOffset(CloseableHttpResponse response)
  {
    final SimpleDateFormat dateFormat = new SimpleDateFormat(SERVER_DATE_TIME_FORMAT, Locale.US);
    try {
      Header dateHeader = response.getFirstHeader("Date");
      Date serverTime = dateFormat.parse(dateHeader.getValue());
      offset = (serverTime.getTime() - System.currentTimeMillis())/1000;
    }
    catch(ParseException pe) {
      logger.warn("Error while parsing the Date header ["+response.getFirstHeader("Date")+"] : offset is 0, format is ["+dateFormat+"]", pe);
      offset = 0;
    }
  }

  public long getServerTime() { return System.currentTimeMillis()/1000 + offset;}

  public LinxoClientException getClientException(Throwable t)
  {
    Throwable internal = t;
    while(internal != null && ! (internal instanceof LinxoClientException) && internal.getCause() != null)
    {
      internal = internal.getCause();
    }

    if(internal == null || ! (internal instanceof LinxoClientException))
    {
      return null;
    }

    return (LinxoClientException) internal;
  }

  private String findLinxoSession()
  {
    if(httpClient == null){
      return null;
    }

    for(Cookie cookie : cookieStore.getCookies())
    {
      if(cookie.getName().equals(LINXO_SESSION_COOKIE_NAME))
      {
        return cookie.getValue();
      }
    }

    return null;
  }


  /////////////////////////////////////////////////////////////////////////////
  //
  //                               toString
  //
  /////////////////////////////////////////////////////////////////////////////

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("LinxoClient{");
    sb.append("apiKey='").append(apiKey).append('\'');
    if(logger.isTraceEnabled()) {
      sb.append(", apiSecret='").append(apiSecret).append('\'');
    }
    else {
      sb.append(", apiSecret='").append("OBFUSCATED").append('\'');
    }
    sb.append(", jsonUri=").append(jsonUri);
    sb.append(", authUri=").append(authUri);
    sb.append(", offset=").append(offset);

    sb.append(", userCredentials=").append(userCredentials);
    sb.append(", deviceCredentials=").append(deviceCredentials);

    sb.append(", currentLinxoSession=").append(findLinxoSession());
    sb.append(", httpClient=").append(httpClient);

    sb.append('}');
    return sb.toString();
  }
}
