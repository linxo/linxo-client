/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 30/09/2014 by hugues.
 */
package com.linxo.client.net;

import com.linxo.client.actions.ErrorResult;
import com.linxo.infrastructure.exceptions.TechnicalException;

public class LinxoClientException
    extends TechnicalException
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private ErrorResult errorResult;

  // Instance Initializers

  // Constructors

  public LinxoClientException(String s)
  {
    super(s);
  }

  public LinxoClientException(String s, Throwable throwable)
  {
    super(s, throwable);
  }

  public LinxoClientException(Throwable throwable)
  {
    super(throwable);
  }

  public LinxoClientException(ErrorResult errorResult)
  {
    this.errorResult = errorResult;
  }

  // Instance Methods

  @Override
  public String getMessage()
  {
    return super.getMessage() + ": " + (errorResult == null?null:errorResult.toString());
  }
}
