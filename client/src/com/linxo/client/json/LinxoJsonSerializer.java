/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 30/09/2014 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.LinxoRequest;
import com.linxo.client.actions.LinxoResponse;
import com.linxo.client.actions.LinxoResult;
import com.linxo.client.actions.classification.GetCategoriesResult;
import com.linxo.client.actions.events.GetNextEventsResult;
import com.linxo.client.actions.pfm.GetTransactionsAction;
import com.linxo.client.actions.pfm.UpdateTransactionAction;
import com.linxo.client.actions.pfm.sync.*;
import com.linxo.client.actions.pfm.upcoming.FindUpcomingTransactionsResult;
import com.linxo.client.actions.pfm.upcoming.GetUpcomingTransactionsResult;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.json.upcoming.FindUpcomingTransactionsResultAdapter;
import com.linxo.client.json.upcoming.GetUpcomingTransactionsResultAdapter;
import com.linxo.client.json.upcoming.UpcomingTransactionAdapterFactory;
import org.apache.log4j.Logger;

import java.util.Date;

public class LinxoJsonSerializer
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(LinxoJsonSerializer.class);

  private static final String CLIENT_BASE_PACKAGE = "com.linxo.client.actions.";
  private static final String SERVER_BASE_PACKAGE = "com.linxo.gwt.rpc.client.";

  public static final String ERROR_RESULT_CLASS_NAME = CLIENT_BASE_PACKAGE + "ErrorResult";

  private static final Gson parser;

  // Static Initializers

  static {
    GsonBuilder builder = new GsonBuilder()
        .generateNonExecutableJson()
        .enableComplexMapKeySerialization()
        .registerTypeAdapter(Date.class, new DateTypeAdapter())
        .registerTypeAdapter(java.sql.Date.class, new DateTypeAdapter())
        .registerTypeAdapter(java.sql.Timestamp.class, new DateTypeAdapter())
        .registerTypeAdapter(java.sql.Time.class, new DateTypeAdapter())
        .registerTypeAdapter(LinxoRequest.class, new LinxoRequestAdapter())
        .registerTypeAdapter(LinxoResponse.class, new LinxoResponseAdapter())
        .registerTypeAdapter(BankAccountInfo.class, new BankAccountInfoHierarchyAdapter())
        .registerTypeAdapter(GetCategoriesResult.class, new GetCategoriesResultAdapter())
        .registerTypeAdapter(GetNextEventsResult.class, new GetNextEventsResultAdapter())
        .registerTypeAdapter(ListAccountsInFinancialInstitutionAction.class, new ListAccountsInFinancialInstitutionActionAdapter())
//        .registerTypeAdapter(GetFinancialInstitutionAndListAccountsAction.class, new GetFinancialInstitutionAndListAccountsActionAdapter())
        .registerTypeAdapter(UpdateCredentialsAction.class, new UpdateCredentialsActionAdapter())
        .registerTypeAdapter(SelectAccountsForSynchronizationAction.class, new SelectAccountsForSynchronizationActionAdapter())
        .registerTypeAdapter(UpdateAccountListAction.class, new UpdateAccountListActionAdapter())
        .registerTypeAdapter(UpdateTransactionAction.class, new UpdateTransactionActionAdapter())
        .registerTypeAdapter(ChallengeResponseAction.class, new ChallengeResponseActionAdapter())
//        .registerTypeAdapter(AddSemiSyncCredentialsAction.class, new AddSemiSyncCredentialsActionAdapter())
//        .registerTypeAdapter(GetTrendsOverviewAction.class, new GetTrendsOverviewActionAdapter())
        .registerTypeAdapter(GetTransactionsAction.class, new GetTransactionsActionAdapter())
//          .setDateFormat() // replaced by above *.Date and *.Time* to have epoch time in seconds as a String
            // manage results for collecting Upcoming Transactions
        .registerTypeAdapter(GetUpcomingTransactionsResult.class, new GetUpcomingTransactionsResultAdapter())
        .registerTypeAdapter(FindUpcomingTransactionsResult.class, new FindUpcomingTransactionsResultAdapter())
            // Removes and add the upcoming transactions from their modified occurrences
        .registerTypeAdapterFactory(new UpcomingTransactionAdapterFactory())
//        .serializeNulls()
        .setLongSerializationPolicy(LongSerializationPolicy.STRING)
        // .serializeSpecialFloatingPointValues() // NaN, +Infinity, -Infinity
        ;

    if(logger.isDebugEnabled()) {
      logger.debug("JSON Pretty printing enabled");
      builder.setPrettyPrinting();
    }

    parser =  builder.create();
  }

  // Static Methods



  public static String getJsonNameFromType(String className)
      throws Exception
  {
    if (!className.startsWith(CLIENT_BASE_PACKAGE)) {
      throw new Exception("Can not determine actionName for type " + className);
    }

    String shortName = className.substring(CLIENT_BASE_PACKAGE.length());
/*
    Not needed in Java

    // We change 'Auth.LoginResult' => 'auth.LoginResult'
    // We put a lower case char for all packages
    int lastDot = shortName.lastIndexOf(".");
    char[] res = shortName.toCharArray();
    char[] lower = shortName.toLowerCase().toCharArray();
    System.arraycopy(lower, 0, res, 0, lastDot);

    return SERVER_BASE_PACKAGE + new String(res);
*/

    return SERVER_BASE_PACKAGE + shortName;
  }

  public static String getTypeFromJsonName(String jsonName)
      throws Exception
  {
    // First we take care of the well known
    if (jsonName.equals("com.linxo.gwt.server.support.json.ErrorResult"))
    {
      return ERROR_RESULT_CLASS_NAME;
    }

    // Then we do automatic package conversion
    if (!jsonName.startsWith(SERVER_BASE_PACKAGE))
    {
      throw new Exception("Can not determine type for jsonName " + jsonName);
    }
    String shortName = jsonName.substring(SERVER_BASE_PACKAGE.length());
/*
    NOT NEEDED IN JAVA

    int lastDot = shortName.lastIndexOf(".", StringComparison.Ordinal);

    // We will change 'auth.LoginAction' => 'Auth.LoginAction
    // We put an upper char after each dot except last
    char[] upper = shortName.ToUpper().ToCharArray();
    char[] res = shortName.ToCharArray();
    bool afterDot = true;
    for (int i = 0; i < lastDot; i++)
    {
      if (afterDot) res[i] = upper[i];
      afterDot = (res[i] == '.');
    }
*/
    return CLIENT_BASE_PACKAGE + shortName;
  }


  public static <R extends LinxoResult> String serialize(LinxoRequest<R> request)
  {
    return parser.toJson(request);
  }


  public static LinxoResponse deSerialize(String resultString)
  {
    return parser.fromJson(resultString, LinxoResponse.class);
  }

  static ClassLoader getClassLoader(Object object)
  {
    ClassLoader result = object.getClass().getClassLoader();

    if(result == null){
      return ClassLoader.getSystemClassLoader();
    }

    return result;
  }



  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods



}
