/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.pfm.sync.AddSemiSyncCredentialsAction;
import com.linxo.client.actions.pfm.sync.Credential;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 *
 */
final class AddSemiSyncCredentialsActionAdapter
  implements JsonDeserializer<AddSemiSyncCredentialsAction>
{
  @Override
  public AddSemiSyncCredentialsAction deserialize(final JsonElement json, final Type typeOfT,
                                                  final JsonDeserializationContext context)
    throws JsonActionParseException
  {
    // get the secret
    final JsonElement secretElt = json.getAsJsonObject().get(AddSemiSyncCredentialsAction.SECRET);
    if (secretElt == null) {
      throw new JsonActionParseException("Invalid AddSemiSyncCredentialsAction: no secret found");
    }
    final String secret = secretElt.getAsString();

    // get the group id
    final JsonElement groupIdElt = json.getAsJsonObject().get(AddSemiSyncCredentialsAction.ACCOUNT_GROUP_ID);
    if (groupIdElt == null) {
      throw new JsonActionParseException("Invalid AddSemiSyncCredentialsAction: no group id found");
    }
    final long groupId = groupIdElt.getAsLong();

    // get the remember boolean
    final JsonElement rememberElt = json.getAsJsonObject().get(AddSemiSyncCredentialsAction.REMEMBER_CREDENTIALS);
    if (rememberElt == null) {
      throw new JsonActionParseException("Invalid AddSemiSyncCredentialsAction: no remember found");
    }
    final boolean remember = rememberElt.getAsBoolean();

    // get the credentials
    final JsonElement credentialsElt = json.getAsJsonObject().get(AddSemiSyncCredentialsAction.CREDENTIALS);
    if (credentialsElt == null) {
      throw new JsonActionParseException("Invalid AddSemiSyncCredentialsAction: no credentials found");
    }
    final JsonArray credentialsArray = credentialsElt.getAsJsonArray();
    final ArrayList<Credential> credentials = new ArrayList<Credential>(credentialsArray.size());
    for (JsonElement jsonCredential : credentialsArray) {
      credentials.add(context.<Credential>deserialize(jsonCredential, Credential.class));
    }

    final AddSemiSyncCredentialsAction action =
        new AddSemiSyncCredentialsAction(groupId, credentials, remember);
    action.setSharedSecret(secret);
    return action;
  }

}
