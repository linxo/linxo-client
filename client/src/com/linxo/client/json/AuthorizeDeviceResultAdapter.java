/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.data.auth.AuthStatus;
import com.linxo.client.actions.auth.AuthorizeDeviceResult;
import com.linxo.client.dto.device.AppInfo;
import com.linxo.client.dto.sync.Key;
import com.linxo.client.dto.user.DealInfo;
import com.linxo.client.dto.user.PermissionInfo;
import com.linxo.client.dto.user.UserProfileInfo;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Adapter to deserialize a {@link AuthorizeDeviceResult}.
 */
@SuppressWarnings("WeakerAccess") // class has to be public in android
public final class AuthorizeDeviceResultAdapter
    implements JsonDeserializer<AuthorizeDeviceResult>
{
  @Override
  public final AuthorizeDeviceResult deserialize(final JsonElement jsonElement, final Type type,
                                                 final JsonDeserializationContext context)
      throws JsonParseException
  {
    final String token = getString(jsonElement, "token", false);
    final boolean blocked = getBoolean(jsonElement, "blocked");
    final String blockedAddress = getString(jsonElement, "blockedAddress", false);
    final String firstName = getString(jsonElement, "firstName", false);
    final String lastName = getString(jsonElement, "lastName", false);
    final boolean isActivated = getBoolean(jsonElement, "isActivated");
    final boolean askForTermsAndConditions = getBoolean(jsonElement, "askForTermsAndConditions");
    final AuthStatus authStatus = getAuthStatus(jsonElement, "status");
    final DealInfo deal = getObject(jsonElement, "dealInfo", false, context, DealInfo.class);
    final HashSet<PermissionInfo> permissions = getObjectSet(jsonElement, "permissions", false,
                                                             context, PermissionInfo.class);
    final AppInfo appInfo = getObject(jsonElement, "appInfo", false, context, AppInfo.class);
    final UserProfileInfo userProfileInfo = getObject(jsonElement, "userProfileInfo", false,
                                                      context, UserProfileInfo.class);
    final ArrayList<Key> requestedKeys = getObjectList(jsonElement, "requestedKeys", false,
                                                       context, Key.class);
    final String publicKey = getString(jsonElement, "publicKey", false);
    final String errorCode = getString(jsonElement, "errorCode", false);
    final String errorMessage = getString(jsonElement, "errorMessage", false);
    final Long groupId = getLong(jsonElement, "groupId");

    return new AuthorizeDeviceResult(token,
                                     blocked,
                                     blockedAddress,
                                     firstName,
                                     lastName,
                                     isActivated,
                                     askForTermsAndConditions,
                                     authStatus,
                                     deal,
                                     permissions,
                                     appInfo,
                                     userProfileInfo,
                                     publicKey,
                                     requestedKeys,
                                     errorCode,
                                     errorMessage,
                                     groupId);
  }

  private <T> T getObject(final JsonElement element, final String key, final boolean expected,
                          final JsonDeserializationContext context, final Class<T> clazz)
  {
    final JsonElement keyElement = element.getAsJsonObject().get(key);
    if (keyElement == null && expected) {
      throw new JsonActionParseException("Missing key [" + key + "] in AuthorizeDeviceResult json");
    }
    return context.deserialize(keyElement, clazz);
  }

  private <T> HashSet<T> getObjectSet(final JsonElement element, final String key, final boolean expected,
                                      final JsonDeserializationContext context, final Class<T> clazz)
  {
    final ArrayList<T> list = getObjectList(element, key, expected, context, clazz);
    if (list == null) {
      return null;
    }
    return new HashSet<>(list);
  }

  private <T> ArrayList<T> getObjectList(final JsonElement element, final String key, final boolean expected,
                                         final JsonDeserializationContext context, final Class<T> clazz)
  {
    final JsonElement keyElement = element.getAsJsonObject().get(key);
    if (keyElement == null && expected) {
      throw new JsonActionParseException("Missing key [" + key + "] in AuthorizeDeviceResult json");
    }
    if (keyElement == null) {
      return null;
    }
    final JsonArray array = keyElement.getAsJsonArray();
    final ArrayList<T> result = new ArrayList<>(array.size());

    for (JsonElement arrayElement : array) {
      final T object = context.deserialize(arrayElement, clazz);
      result.add(object);
    }
    return result;
  }

  private AuthStatus getAuthStatus(final JsonElement element,final String key)
  {
    final JsonElement authStatusElement = getElement(element, key, false);
    if (authStatusElement == null) {
      return null;
    }
    return AuthStatus.valueOf(authStatusElement.getAsString());
  }

  private JsonElement getElement(final JsonElement element, final String key, final boolean expected)
  {
    final JsonElement keyElement = element.getAsJsonObject().get(key);
    if (keyElement == null && expected) {
      throw new JsonActionParseException("Missing key [" + key + "] in AuthorizeDeviceResult json");
    }
    return keyElement;
  }

  private String getString(final JsonElement element, final String key, final boolean expected)
  {
    final JsonElement keyElement = getElement(element, key, expected);
    if (keyElement == null) {
      return null;
    }
    return keyElement.getAsString();
  }

  private boolean getBoolean(final JsonElement element, final String key)
  {
    // this method returns a boolean and not a Boolean so expected = true
    return getElement(element, key, true).getAsBoolean();
  }

  private Long getLong(final JsonElement element, final String key)
  {
    // this method returns a Long and not a long so expected = false
    final JsonElement keyElement = getElement(element, key, false);
    if (keyElement == null) {
      return null;
    }
    return keyElement.getAsLong();
  }

}
