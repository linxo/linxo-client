/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>

   Created on : 2:35:49 PM by tarunmalhotra.
*/
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.data.CategoryType;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.actions.builders.GetTransactionsActionBuilder;
import com.linxo.client.actions.pfm.GetTransactionsAction;

import java.lang.reflect.Type;
import java.util.ArrayList;

final class GetTransactionsActionAdapter
    implements JsonDeserializer<GetTransactionsAction>
{

  private static final String START_ROW = "startRow";
  private static final String NUMBER_OF_ROWS = "numRows";

  private static final String ACCOUNT_ID = "accountId";
  private static final String ACCOUNT_REFERENCE = "accountReference";
  private static final String LABELS = "labels";
  private static final String CATEGORY_ID = "categoryId";
  private static final String TAG_ID = "tagId";
  private static final String START_DATE = "startDate";
  private static final String END_DATE = "endDate";
  private static final String DEBIT_DATE = "debitDate";
  private static final String AMOUNT = "amount";
  private static final String MIN_AMOUNT = "minAmount";
  private static final String MAX_AMOUNT = "maxAmount";
  private static final String USE_BUDGET_DATE = "useBudgetDate";
  private static final String USE_TREND_ACCOUNTS = "useTrendsAccounts";
  private static final String USE_BUDGET_ACCOUNTS = "useBudgetAccounts";
  private static final String USE_SAVINGS_ACCOUNTS = "useSavingsAccounts";
  private static final String USE_ASSETS_ACCOUNTS = "useAssetsAccounts";
  private static final String SELECTED_VIEW_IDS = "selectedViewIds";
  private static final String LOAD_SUBCATEGORIES = "loadSubCategories";
  private static final String LOAD_ONLY_TAGGED_TRANSACTIONS = "loadOnlyTaggedTransactions";
  private static final String ACCOUNT_TYPE = "accountType";
  private static final String ACCOUNT_TYPES = "accountTypes";
  private static final String CATEGORY_TYPE = "categoryType";
  private static final String EXT_CUSTOM_TYPE = "extCustomType";
  private static final String INCLUDE_AVAILABLE_SUMS = "includeAvailableSums";
  private static final String ACCOUNT_GROUP_ID = "accountGroupId";
  private static final String IMPORT_DATE = "importDate";

  @Override
  public GetTransactionsAction deserialize(final JsonElement json, final Type typeOfT,
                                           final JsonDeserializationContext context)
      throws JsonActionParseException
  {
    final JsonElement secretElement = getJsonElement(json, GetTransactionsAction.SECRET);
    final JsonElement accountIdElement = getJsonElement(json, ACCOUNT_ID);
    final JsonElement accountReferenceElement = getJsonElement(json, ACCOUNT_REFERENCE);
    final JsonElement categoryIdElement = getJsonElement(json, CATEGORY_ID);
    final JsonElement tagIdElement = getJsonElement(json, TAG_ID);
    final JsonElement startRowElement = getJsonElement(json, START_ROW);
    final JsonElement numRowElement = getJsonElement(json, NUMBER_OF_ROWS);
    final JsonElement startDateElement = getJsonElement(json, START_DATE);
    final JsonElement endDateElement = getJsonElement(json, END_DATE);
    final JsonElement useBudgetDateElement = getJsonElement(json, USE_BUDGET_DATE);
    final JsonElement useTrendsAccountsElement = getJsonElement(json, USE_TREND_ACCOUNTS);
    final JsonElement useBudgetAccountsElement = getJsonElement(json, USE_BUDGET_ACCOUNTS);
    final JsonElement useSavingsAccountsElement = getJsonElement(json, USE_SAVINGS_ACCOUNTS);
    final JsonElement useAssetsAccountsElement = getJsonElement(json, USE_ASSETS_ACCOUNTS);
    final JsonArray selectedViewIdsElements = getJsonElement(json, SELECTED_VIEW_IDS) == null
                                              ? null : getJsonElement(json, SELECTED_VIEW_IDS).getAsJsonArray();
    final JsonElement loadSubCatsElement = getJsonElement(json, LOAD_SUBCATEGORIES);
    final JsonElement loadOnlyTaggedTransactionElement = getJsonElement(json, LOAD_ONLY_TAGGED_TRANSACTIONS);
    final JsonElement accountTypeElement = getJsonElement(json, ACCOUNT_TYPE);
    final JsonArray accountTypesElement = getJsonElement(json, ACCOUNT_TYPES) == null
                                          ? null : getJsonElement(json, ACCOUNT_TYPES).getAsJsonArray();
    final JsonElement categoryTypeElement = getJsonElement(json, CATEGORY_TYPE);
    final JsonArray labelElements = getJsonElement(json, LABELS) == null
                                    ? null : getJsonElement(json, LABELS).getAsJsonArray();
    final JsonElement debitDateElement = getJsonElement(json, DEBIT_DATE);
    final JsonElement amountElement = getJsonElement(json, AMOUNT);
    final JsonElement minAmountElement = getJsonElement(json, MIN_AMOUNT);
    final JsonElement maxAmountElement = getJsonElement(json, MAX_AMOUNT);
    final JsonElement extCustomTypeElement = getJsonElement(json, EXT_CUSTOM_TYPE);
    final JsonElement includeAvailableSumsElement = getJsonElement(json, INCLUDE_AVAILABLE_SUMS);
    final JsonElement accountGroupIdElement = getJsonElement(json, ACCOUNT_GROUP_ID);
    final JsonElement importDateElement = getJsonElement(json, IMPORT_DATE);

    final String secret = secretElement == null ? null : secretElement.getAsString();
    final Long accountId = accountIdElement == null ? null : accountIdElement.getAsLong();
    final String accountReference = accountReferenceElement == null ? null : accountReferenceElement.getAsString();
    final Long categoryId = categoryIdElement == null ? null : categoryIdElement.getAsLong();
    final Long tagId = tagIdElement == null ? null : tagIdElement.getAsLong();
    final int startRow = startRowElement == null ? 0 : startRowElement.getAsInt();
    final int numRows = numRowElement == null ? 0 : numRowElement.getAsInt();
    final String startDate = startDateElement == null ? null : startDateElement.getAsString();
    final String endDate = endDateElement == null ? null : endDateElement.getAsString();
    final String debitDate = debitDateElement == null ? null : debitDateElement.getAsString();
    final Double amount = amountElement == null ? null : amountElement.getAsDouble();
    final Double minAmount = minAmountElement == null ? null : minAmountElement.getAsDouble();
    final Double maxAmount = maxAmountElement == null ? null : maxAmountElement.getAsDouble();
    final boolean useBudgetDate = useBudgetDateElement != null && useBudgetDateElement.getAsBoolean();
    final boolean useTrendsAccounts = useTrendsAccountsElement != null && useTrendsAccountsElement.getAsBoolean();
    final boolean useBudgetAccounts = useBudgetAccountsElement != null && useBudgetAccountsElement.getAsBoolean();
    final boolean useSavingsAccounts = useSavingsAccountsElement != null && useSavingsAccountsElement.getAsBoolean();
    final boolean useAssetsAccounts = useAssetsAccountsElement != null && useAssetsAccountsElement.getAsBoolean();
    final boolean loadSubCats = loadSubCatsElement != null && loadSubCatsElement.getAsBoolean();
    final boolean loadOnlyTaggedTransactions = loadOnlyTaggedTransactionElement != null
                                               && loadOnlyTaggedTransactionElement.getAsBoolean();
    final String extCustomType = extCustomTypeElement == null ? null : extCustomTypeElement.getAsString();
    final boolean includeAvailableSums = includeAvailableSumsElement != null
                                         && includeAvailableSumsElement.getAsBoolean();
    final Long accountGroupId = accountGroupIdElement == null ? null : accountGroupIdElement.getAsLong();
    final Long importDate = importDateElement == null ? null : importDateElement.getAsLong();

    AccountType accountType = null;
    if (accountTypeElement != null) {
      try {
        accountType = AccountType.valueOf(accountTypeElement.getAsString());
      }
      catch (IllegalArgumentException iae) {
      }
    }

    ArrayList<AccountType> accountTypes = null;
    if (accountTypesElement != null) {
      accountTypes = new ArrayList<>();
      for (JsonElement loopElement : accountTypesElement) {
        try {
          accountTypes.add(AccountType.valueOf(loopElement.getAsString()));
        }
        catch (IllegalArgumentException iae) {
        }
      }
    }

    CategoryType categoryType = null;
    if (categoryTypeElement != null) {
      try {
        categoryType = CategoryType.valueOf(categoryTypeElement.getAsString());
      }
      catch (IllegalArgumentException e) {
      }
    }
    ArrayList<String> labels = null;
    if (labelElements != null) {
      labels = new ArrayList<>();
      for (JsonElement labelElement : labelElements) {
        labels.add(labelElement.getAsString());
      }
    }

    ArrayList<Long> selectedViewIds = null;
    if (selectedViewIdsElements != null) {
      selectedViewIds = new ArrayList<>();
      for (JsonElement selectedViewIdElement : selectedViewIdsElements) {
        selectedViewIds.add(selectedViewIdElement.getAsLong());
      }
    }

    final GetTransactionsAction action = new GetTransactionsActionBuilder()
            .withAccountType(accountType)
            .withAccountTypes(accountTypes)
            .withAccountId(accountId)
            .withAccountReference(accountReference)
            .withLabels(labels)
            .withStartDate(startDate)
            .withEndDate(endDate)
            .withDebitDate(debitDate)
            .withAmount(amount)
            .withMinAmount(minAmount)
            .withMaxAmount(maxAmount)
            .withUseBudgetDate(useBudgetDate)
            .withCategoryId(categoryId)
            .withLoadSubCategories(loadSubCats)
            .withCategoryType(categoryType)
            .withTagId(tagId)
            .withLoadOnlyTaggedTransactions(loadOnlyTaggedTransactions)
            .withExtCustomType(extCustomType)
            .withIncludeAvailableSums(includeAvailableSums)
            .withUseTrendsAccounts(useTrendsAccounts)
            .withUseBudgetAccounts(useBudgetAccounts)
            .withUseSavingsAccounts(useSavingsAccounts)
            .withUseAssetsAccounts(useAssetsAccounts)
            .withSelectedViewIds(selectedViewIds)
            .withStartRow(startRow)
            .withNumRows(numRows)
            .withAccountGroupId(accountGroupId)
            .withImportDate(importDate)
        .build();

    action.setSharedSecret(secret);

    return action;
  }

  private JsonElement getJsonElement(final JsonElement json, final String element)
  {
    return json.getAsJsonObject().get(element) == null
           || json.getAsJsonObject().get(element).isJsonNull()
           ? null : json.getAsJsonObject().get(element);
  }

}

