/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/05/2011 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.dto.account.CheckingsProviderAccountInfo;
import com.linxo.client.dto.account.CreditCardProviderAccountInfo;
import com.linxo.client.dto.account.LoanProviderAccountInfo;
import com.linxo.client.dto.account.ProviderAccountInfo;
import com.linxo.client.dto.account.SavingsProviderAccountInfo;
import com.linxo.client.actions.pfm.sync.SelectAccountsForSynchronizationAction;

import java.lang.reflect.Type;
import java.util.ArrayList;

final class SelectAccountsForSynchronizationActionAdapter
  implements JsonDeserializer<SelectAccountsForSynchronizationAction>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods


  @Override
  public SelectAccountsForSynchronizationAction deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
  {
    // process the List ProviderAccountInfo
    final JsonArray jsonAccounts = json.getAsJsonObject()
        .get(SelectAccountsForSynchronizationAction.ACCOUNTS_SELECTED).getAsJsonArray();
    final ArrayList<ProviderAccountInfo> accounts = new ArrayList<>();
    for (JsonElement jsonAccount : jsonAccounts) {
      final JsonElement typeElement = jsonAccount.getAsJsonObject().get("type");
      final String typeString = typeElement.getAsString();

      final Class klass;
      if (AccountType.Checkings.name().equals(typeString)) {
        klass = CheckingsProviderAccountInfo.class;
      }
      else if (AccountType.Savings.name().equals(typeString)) {
        klass = SavingsProviderAccountInfo.class;
      }
      else if (AccountType.CreditCard.name().equals(typeString)) {
        klass = CreditCardProviderAccountInfo.class;
      }
      else if (AccountType.Loan.name().equals(typeString)) {
        klass = LoanProviderAccountInfo.class;
      }
      else {
        throw new JsonActionParseException("Unknown subtype of ProviderAccountInfo [" + typeString + "]");
      }
      accounts.add(context.<ProviderAccountInfo>deserialize(jsonAccount, klass));
    }

    // get the secret
    JsonElement secretElement = json.getAsJsonObject().get(SelectAccountsForSynchronizationAction.SECRET);
    final String secret =  secretElement == null ? null : secretElement.getAsString();

    final SelectAccountsForSynchronizationAction result = new SelectAccountsForSynchronizationAction(accounts);
    result.setSharedSecret(secret);

    return result;
  }
}
