/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 19/01/2011 by hugues.
 */
package com.linxo.client.json;

import com.linxo.client.actions.LinxoAction;

/**
 * composite of a String (the action class name, and the action itself)
 */
public final class JsonAction {

  // Nested Types (mixing inner and static classes is okay)

  /**
   * Basic container for the JSON hash signature information
   */
  public static final class Hash
  {
    private String nonce;
    private String timeStamp;
    private String apiKey;
    private String signature;

    // Stateless identification with the email/password
    private String identifier;
    private String password;


    // Stateless identification with the deviceId/token
    private String deviceId;
    private String token;
    private String appIdentifier;   // These fields come in replacement of the AppInfo object when the
    private String appVersion;      // stateless device login occurs.

    @SuppressWarnings("UnusedDeclaration") // s11n
    public Hash() {}

    @SuppressWarnings("UnusedDeclaration") // API
    public Hash(String nonce, String timeStamp, String apiKey, String signature,
                String identifier, String password,
                String deviceId, String token, String appIdentifier, String appVersion)
    {
      this.nonce = nonce;
      this.timeStamp = timeStamp;
      this.apiKey = apiKey;
      this.signature = signature;
      this.identifier = identifier;
      this.password = password;
      this.deviceId = deviceId;
      this.token = token;
      this.appIdentifier = appIdentifier;
      this.appVersion = appVersion;
    }

    public String getApiKey()
    {
      return apiKey;
    }

    public void setApiKey(String apiKey)
    {
      this.apiKey = apiKey;
    }

    public String getTimeStamp()
    {
      return timeStamp;
    }

    public String getNonce()
    {
      return nonce;
    }

    public String getSignature()
    {
      return signature;
    }

    public void setSignature(String signature)
    {
      this.signature = signature;
    }

    public String getIdentifier()
    {
      return identifier;
    }

    public void setIdentifier(String identifier)
    {
      this.identifier = identifier;
    }

    public String getPassword()
    {
      return password;
    }

    public void setPassword(String password)
    {
      this.password = password;
    }

    public String getDeviceId()
    {
      return deviceId;
    }

    public String getToken()
    {
      return token;
    }

    public String getAppIdentifier()
    {
      return appIdentifier;
    }

    public String getAppVersion()
    {
      return appVersion;
    }

    @Override
    public String toString()
    {
      //noinspection StringBufferReplaceableByString,StringBufferMayBeStringBuilder
      final StringBuffer sb = new StringBuffer();
      sb.append("Hash");
      sb.append("{apiKey='").append(apiKey).append('\'');
      sb.append(", nonce='").append(nonce).append('\'');
      sb.append(", timeStamp='").append(timeStamp).append('\'');
      sb.append(", signature='").append(signature).append('\'');
      sb.append(", identifier='").append(identifier).append('\'');
      sb.append(", password='").append("OBFUSCATED").append('\'');
      sb.append(", deviceId='").append(deviceId).append('\'');
      sb.append(", token='").append("OBFUSCATED").append('\'');
      sb.append(", appIdentifier='").append(appIdentifier).append('\'');
      sb.append(", appVersion='").append(appVersion).append('\'');
      sb.append('}');
      return sb.toString();
    }
  }

  // Static Fields

  public static final String ACTION_NAME = "actionName";
  public static final String ACTION = "action";
  public static final String HASH = "hash";

  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_NONCE = "nonce";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_TIMESTAMP = "timeStamp";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_API_KEY = "apiKey";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_SIGNATURE = "signature";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_IDENTIFIER = "identifier";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_PASSWORD = "password";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_DEVICE_ID= "devideId";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_TOKEN = "token";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_APP_IDENTIFIER = "appIdentifier";
  @SuppressWarnings("UnusedDeclaration")
  public static final String HASH_APP_VERSION = "appVersion";

  // Static Initializers

  // Static Methods

  // Instance Fields
  private String actionName;
  private LinxoAction<?> action;
  private Hash hash;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // required by Gson
  public JsonAction(){}

  public JsonAction(LinxoAction<?> action, Hash hash)
  {
    this.action = action;
    this.actionName = action.getClass().getName();
    this.hash = hash;
  }


  // Instance Methods


  LinxoAction<?> getAction()
  {
    return action;
  }

  String getActionName()
  {
    return actionName;
  }

  Hash getHash()
  {
    return hash;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString,StringBufferMayBeStringBuilder
    final StringBuffer sb = new StringBuffer();
    sb.append("JsonAction");
    sb.append("{action=").append(action);
    sb.append(", actionName='").append(actionName).append('\'');
    sb.append(", hash=").append(hash);
    sb.append('}');
    return sb.toString();
  }
}
