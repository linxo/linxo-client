/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 01/10/2014 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.Hash;
import com.linxo.client.actions.LinxoAction;
import com.linxo.client.actions.LinxoRequest;
import com.linxo.client.actions.LinxoResult;

import java.lang.reflect.Type;

class LinxoRequestAdapter<R extends LinxoResult>
  implements
    JsonSerializer<LinxoRequest<R>>,
    JsonDeserializer<LinxoRequest<R>>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final String ACTION_NAME = "actionName";
  private static final String ACTION = "action";
  private static final String HASH = "hash";

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods


  @Override
  public JsonElement serialize(LinxoRequest<R> linxoRequest, Type type,
                               JsonSerializationContext context)
  {

    JsonObject result = new JsonObject();
    String actionName = linxoRequest.getActionName();

    result.add(ACTION_NAME, new JsonPrimitive(actionName));
    result.add(ACTION, context.serialize(linxoRequest.getAction()));
    result.add(HASH, context.serialize(linxoRequest.getHash()));

    return result;
  }

  @Override
  public LinxoRequest<R> deserialize(JsonElement jsonElement, Type type,
                                     JsonDeserializationContext context)
      throws JsonParseException
  {

    final JsonElement classNameElement = jsonElement.getAsJsonObject().get(ACTION_NAME);
    if (classNameElement == null) {
      throw new JsonParseException("Cannot deserialize JsonAction since action name is null");
    }


    String className = classNameElement.getAsString();

    final LinxoAction<R> action;
    try {
      final Class clazz = LinxoJsonSerializer.getClassLoader(this).loadClass(className);

      final JsonElement actionElement = jsonElement.getAsJsonObject().get(ACTION);
      if (actionElement == null) {
        throw new JsonParseException("Cannot deserialize JsonAction of class [" + className
            + "] since its action is null");
      }
      action = context.deserialize(actionElement, clazz);
    }
    catch (ClassNotFoundException cnf) {
      throw new JsonParseException("Cannot deserialize JsonAction of unknown class ["
          + className + "]", cnf);
    }

    final Hash hash = context.deserialize(jsonElement.getAsJsonObject().get(HASH), Hash.class);

    return new LinxoRequest<R>(action, className, hash);
  }

}


