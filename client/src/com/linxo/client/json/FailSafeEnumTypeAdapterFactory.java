/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 07/11/2014 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Create Generic TypeAdapter for enums that will return {@code null} when
 * a value of the enum is unknown.
 */
public class FailSafeEnumTypeAdapterFactory
    implements TypeAdapterFactory
{

  // Nested Types (mixing inner and static classes is okay)

  public static class FailSafeEnumTypeAdapter<T extends Enum<T>>
      extends TypeAdapter<T>
  {
    private final Map<String, T> nameToConstant = new HashMap<String,T>();
    private final Map<T, String> constantToName = new HashMap<T, String>();

    public FailSafeEnumTypeAdapter(Class<T> classOfT) {
      try {
        T[] e = classOfT.getEnumConstants();
        for (T constant : e) {
          String name = constant.name();
          SerializedName annotation = classOfT.getField(name).getAnnotation(SerializedName.class);
          if (annotation != null) {
            name = annotation.value();
          }

          this.nameToConstant.put(name, constant);
          this.constantToName.put(constant, name);
        }
      }
      catch (NoSuchFieldException e)
      {
        throw new AssertionError(e);
      }
    }

    @Override
    public T read(JsonReader in) throws IOException {
      if(in.peek() == JsonToken.NULL) {
        in.nextNull();
        return null;
      } else {
        // returns null if the type is not found...
        return this.nameToConstant.get(in.nextString());
      }
    }

    @Override
    public void write(JsonWriter out, T value) throws IOException {
      out.value(value == null?null:this.constantToName.get(value));
    }
  }


  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods


  /**
   * @return A TypeAdapter for the given type when this type is an enum.
   */
  @Override
  public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken)
  {
    Class<? super T> rawType = typeToken.getRawType();
    if(rawType.isEnum()) {
      //noinspection unchecked
      return new FailSafeEnumTypeAdapter(rawType);
    }

    return null;
  }

}
