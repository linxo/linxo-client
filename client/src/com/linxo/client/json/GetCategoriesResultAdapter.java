/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 2:15:38 PM by tarunmalhotra.
*/
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.classification.GetCategoriesResult;
import com.linxo.client.dto.tx.CatInfo;

import java.lang.reflect.Type;
import java.util.ArrayList;

public final class GetCategoriesResultAdapter
    implements JsonSerializer<GetCategoriesResult>, JsonDeserializer<GetCategoriesResult>
{
  public GetCategoriesResultAdapter()
  {
  }

  private ThreadLocal<ArrayList<JsonObject>> jsonObjects = new ThreadLocal<ArrayList<JsonObject>>();

  @Override
  public JsonElement serialize(final GetCategoriesResult result, final Type type,
                               final JsonSerializationContext jsonSerializationContext)
  {
    // serialize the categories
    jsonObjects.set(new ArrayList<JsonObject>());
    final JsonArray jsonArray = new JsonArray();
    for (CatInfo catInfo :result.getCatInfos()) {
      serializeCatInfo(catInfo);
    }
    for (JsonObject object : jsonObjects.get()) {
      jsonArray.add(object);
    }
    final JsonObject jsonResult = new JsonObject();
    jsonResult.add(GetCategoriesResult.CAT_INFOS, jsonArray);

    // serialize the next color (if present)
    if (result.getNextColor() != null) {
      final JsonElement nextColorElement = new JsonPrimitive(result.getNextColor());
      jsonResult.add(GetCategoriesResult.NEXT_COLOR, nextColorElement);
    }

    return jsonResult;
  }

  private void serializeCatInfo(final CatInfo catInfo)
  {
    final JsonObject result = new JsonObject();
    if (catInfo.getKey() != null) {
      result.add(CatInfo.KEY, new JsonPrimitive(catInfo.getKey()));
    }
    if (catInfo.getName() != null) {
      result.add(CatInfo.NAME, new JsonPrimitive(catInfo.getName()));
    }
    if (catInfo.getComment() != null) {
      result.add(CatInfo.COMMENT, new JsonPrimitive(catInfo.getComment()));
    }
    if (catInfo.getColor() != null) {
      result.add(CatInfo.COLOR, new JsonPrimitive(catInfo.getColor()));
    }
    if (catInfo.getIcon() != null) {
      result.add(CatInfo.ICON, new JsonPrimitive(catInfo.getIcon()));
    }

    if (catInfo.getTheme() != null) {
      result.add(CatInfo.THEME_ID, new JsonPrimitive(String.valueOf(catInfo.getTheme().getId())));
    }
    if (catInfo.getParent() != null) {
      result.add(CatInfo.PARENT_ID, new JsonPrimitive(String.valueOf(catInfo.getParent().getId())));
    }

    result.add(CatInfo.TYPE, new JsonPrimitive(catInfo.getType().toString()));
    result.add(CatInfo.ID, new JsonPrimitive(String.valueOf(catInfo.getId())));
    result.add(CatInfo.CUSTOM, new JsonPrimitive(catInfo.isCustom()));
    result.add(CatInfo.MASKED, new JsonPrimitive(catInfo.isMasked()));
    result.add(CatInfo.DISABLED, new JsonPrimitive(catInfo.isDisabled()));

    if (catInfo.getSubCategories() != null) {
      final JsonArray subCatIdArray = new JsonArray();
      for (CatInfo subCatInfo : catInfo.getSubCategories()) {
        serializeCatInfo(subCatInfo);
        subCatIdArray.add(new JsonPrimitive(String.valueOf(subCatInfo.getId())));
      }
      result.add(CatInfo.SUB_CATEGORIES, subCatIdArray);
    }
    jsonObjects.get().add(result);
  }

  @Override
  public GetCategoriesResult deserialize(final JsonElement jsonElement, final Type type,
                                         final JsonDeserializationContext jsonDeserializationContext)
  {
    // deserialize the categories
    final ArrayList<ArrayList<CatInfo>> catInfo =
        getCategories(jsonElement.getAsJsonObject().get(GetCategoriesResult.CAT_INFOS));
    final ArrayList<CatInfo> finalCatInfo = sortCatInfo(catInfo);

    // deserialize the next color
    final String nextColor =
        jsonElement.getAsJsonObject().get(GetCategoriesResult.NEXT_COLOR) == null
        ? null : jsonElement.getAsJsonObject().get(GetCategoriesResult.NEXT_COLOR).getAsString();

    return new GetCategoriesResult(finalCatInfo, nextColor);
  }

  private ArrayList<CatInfo> sortCatInfo(final ArrayList<ArrayList<CatInfo>> catInfo)
  {
    final ArrayList<CatInfo> themeInfo = catInfo.get(CatInfo.Type.Theme.getType());
    final ArrayList<CatInfo> categoryInfo = catInfo.get(CatInfo.Type.Category.getType());
    final ArrayList<CatInfo> subcategoryInfo = catInfo.get(CatInfo.Type.SubCategory.getType());

    for (CatInfo theme : themeInfo) {
      for (CatInfo category : categoryInfo) {
        if (category.getParent().getIdAsString().equals(theme.getIdAsString())) {
          for (CatInfo subcategory : subcategoryInfo) {
            if (subcategory.getParent().getIdAsString().equals(category.getIdAsString())) {
              category.getSubCategories().add(subcategory);
            }
          }
          theme.getSubCategories().add(category);
        }
      }
    }
    return themeInfo;
  }

  private ArrayList<ArrayList<CatInfo>> getCategories(JsonElement catInfos)
  {
    final ArrayList<ArrayList<CatInfo>> cat = new ArrayList<ArrayList<CatInfo>>();
    for (int i = 0; i < 3; i++) {
      cat.add(new ArrayList<CatInfo>());
    }
    for (int i = 0; i < catInfos.getAsJsonArray().size(); i++) {
      CatInfo tmpCat = extractCatInfo(catInfos.getAsJsonArray().get(i));
      cat.get(tmpCat.getType().getType()).add(tmpCat);
    }
    return cat;
  }

  private CatInfo extractCatInfo(JsonElement je)
  {
    final String key = je.getAsJsonObject().get(CatInfo.KEY) == null
                       ? null : je.getAsJsonObject().get(CatInfo.KEY).getAsString();
    final String name = je.getAsJsonObject().get(CatInfo.NAME) == null
                        ? null : je.getAsJsonObject().get(CatInfo.NAME).getAsString();
    final String comment = je.getAsJsonObject().get(CatInfo.COMMENT) == null
                           ? null : je.getAsJsonObject().get(CatInfo.COMMENT).getAsString();
    final CatInfo.Type type = je.getAsJsonObject().get(CatInfo.TYPE) == null
                              ? null : CatInfo.Type.getType(je.getAsJsonObject().get(CatInfo.TYPE).getAsString());
    final Long parent = (type != CatInfo.Type.Theme)
                        ? (je.getAsJsonObject().get(CatInfo.PARENT_ID) == null
                           ? null : je.getAsJsonObject().get(CatInfo.PARENT_ID).getAsLong()) : null;
    final Long theme = (type != CatInfo.Type.Theme)
                       ? (je.getAsJsonObject().get(CatInfo.THEME_ID) == null
                          ? null : je.getAsJsonObject().get(CatInfo.THEME_ID).getAsLong()) : null;
    final Long id = je.getAsJsonObject().get(CatInfo.ID) == null
                    ? null : je.getAsJsonObject().get(CatInfo.ID).getAsLong();
    final String color = je.getAsJsonObject().get(CatInfo.COLOR) == null
                         ? null : je.getAsJsonObject().get(CatInfo.COLOR).getAsString();
    final String url = je.getAsJsonObject().get(CatInfo.ICON) == null
                       ? null : je.getAsJsonObject().get(CatInfo.ICON).getAsString();
    final boolean custom = je.getAsJsonObject().get(CatInfo.CUSTOM) != null
                           && je.getAsJsonObject().get(CatInfo.CUSTOM).getAsBoolean();
    final boolean masked = je.getAsJsonObject().get(CatInfo.MASKED) != null
                           && je.getAsJsonObject().get(CatInfo.MASKED).getAsBoolean();
    final boolean disabled = je.getAsJsonObject().get(CatInfo.DISABLED) != null
                             && je.getAsJsonObject().get(CatInfo.DISABLED).getAsBoolean();

    final CatInfo parentCat = parent == null ? null : new CatInfo(parent);
    final CatInfo themeCat = theme == null ? null : new CatInfo(theme);

    return new CatInfo(id, key, name, comment, type, themeCat, parentCat, color, url, custom,
                       masked, disabled, new ArrayList<CatInfo>());
  }

}
