/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 01/10/2014 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.LinxoResponse;
import com.linxo.client.actions.LinxoResult;
import org.apache.log4j.Logger;

import java.lang.reflect.Type;

public class LinxoResponseAdapter
    implements
    JsonSerializer<LinxoResponse>,
    JsonDeserializer<LinxoResponse>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(LinxoResponseAdapter.class);

  // Static Initializers
  private static final String RESULT_NAME = "resultName";
  private static final String RESULT      = "result";

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods


  @Override
  public LinxoResponse deserialize(JsonElement jsonElement, Type type,
                                   JsonDeserializationContext context)
      throws JsonParseException
  {
    String className = jsonElement.getAsJsonObject().get(RESULT_NAME).getAsString();

    final Class clazz;
    try {
      clazz = LinxoJsonSerializer.getClassLoader(this).loadClass(
          LinxoJsonSerializer.getTypeFromJsonName(className)
      );
    } catch (ClassNotFoundException cnfe) {
      throw new JsonParseException("Could not find Action/Result class for [" + className + "]", cnfe);
    } catch (Exception e) {
      throw new JsonParseException("Could not parse Action/Result className for [" + className + "]", e);
    }

    if(logger.isTraceEnabled()) {
      logger.trace("About to de-serialize object of type ["+className+"]: " +  jsonElement.getAsJsonObject().get(RESULT));
    }
    final LinxoResult result = context.deserialize(
        jsonElement.getAsJsonObject().get(RESULT), clazz);

    if(logger.isTraceEnabled()) {
      logger.trace("De-serialized object of type ["+className+"]: " +  result);
    }

    return new LinxoResponse(className, result);
  }

  @Override
  public JsonElement serialize(LinxoResponse linxoResponse, Type type,
                               JsonSerializationContext context)
  {
    final JsonObject result = new JsonObject();

    String resultName = linxoResponse.getResultName();
    result.add(RESULT_NAME, new JsonPrimitive(resultName));
    result.add(RESULT, context.serialize(linxoResponse.getResult()));
    return result;
  }
}
