/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/05/2011 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.dto.account.ProviderAccountInfo;
import com.linxo.client.actions.pfm.sync.UpdateAccountListAction;

import java.lang.reflect.Type;
import java.util.ArrayList;

final class UpdateAccountListActionAdapter
  implements JsonDeserializer<UpdateAccountListAction>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  @Override
  public UpdateAccountListAction deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonActionParseException
  {
    // get the gid
    final JsonElement groupIdElt = json.getAsJsonObject().get(UpdateAccountListAction.GROUP_ID);
    if (groupIdElt == null) {
      throw new JsonActionParseException("No group id element");
    }
    final long groupId = groupIdElt.getAsLong();

    // process the List ProviderAccountInfo
    final JsonElement toAddElt = json.getAsJsonObject().get(UpdateAccountListAction.TO_ADD);
    final ArrayList<ProviderAccountInfo> toAdd = new ArrayList<ProviderAccountInfo>();
    if (toAddElt != null) {
      final JsonArray jsonAccountsToAdd = toAddElt.getAsJsonArray();
      for (JsonElement jsonAccount : jsonAccountsToAdd) {
        toAdd.add(context.<ProviderAccountInfo>deserialize(jsonAccount, ProviderAccountInfo.class));
      }
    }

    // process the list of uid to remove
    final JsonElement toRemoveElt = json.getAsJsonObject().get(UpdateAccountListAction.TO_REMOVE);
    final ArrayList<String> toRemove = new ArrayList<String>();
    if (toRemoveElt != null) {
      final JsonArray jsonAccountsToRemove = toRemoveElt.getAsJsonArray();
      for (JsonElement jsonAccount : jsonAccountsToRemove) {
        toRemove.add(context.<String>deserialize(jsonAccount, String.class));
      }
    }

    // process the list of uid to close
    final JsonElement toCloseElt = json.getAsJsonObject().get(UpdateAccountListAction.TO_CLOSE);
    final ArrayList<String> toClose = new ArrayList<String>();
    if (toCloseElt != null) {
      final JsonArray jsonAccountsToClose = toCloseElt.getAsJsonArray();
      for (JsonElement jsonAccount : jsonAccountsToClose) {
        toClose.add(context.<String>deserialize(jsonAccount, String.class));
      }
    }

    // process the list of uid missing
    final JsonElement missingElt = json.getAsJsonObject().get(UpdateAccountListAction.MISSING_ACCOUNTS);
    final ArrayList<String> missingAccounts = new ArrayList<String>();
    if (missingElt != null) {
      final JsonArray jsonAccountsMissing = missingElt.getAsJsonArray();
      for (JsonElement jsonAccount : jsonAccountsMissing) {
        missingAccounts.add(context.<String>deserialize(jsonAccount, String.class));
      }
    }

    // get the secret
    JsonElement secretElement = json.getAsJsonObject().get(UpdateAccountListAction.SECRET);
    final String secret =  secretElement == null ? null : secretElement.getAsString();

    final UpdateAccountListAction result
        = new UpdateAccountListAction(groupId, toAdd, toRemove, toClose, missingAccounts);
    result.setSharedSecret(secret);

    return result;

  }
}
