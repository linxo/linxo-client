/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.data.DateUtils;
import com.linxo.client.actions.pfm.UpdateTransactionAction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 */
final class UpdateTransactionActionAdapter
  implements JsonDeserializer<UpdateTransactionAction>
{

  @SuppressWarnings({"deprecation"})
  @Override
  public UpdateTransactionAction deserialize(final JsonElement json, final Type typeOfT,
                                             final JsonDeserializationContext context)
    throws JsonActionParseException
  {
    // txn id
    final JsonElement txIdElt = json.getAsJsonObject().get(UpdateTransactionAction.TX_ID);
    if (txIdElt == null || txIdElt.isJsonNull()) {
      throw new JsonActionParseException("No txn id element");
    }
    final long txnId = txIdElt.getAsLong();
    // label
    final JsonElement newLabelElt = json.getAsJsonObject().get(UpdateTransactionAction.NEW_LABEL);
    String newLabel = null;
    if (newLabelElt != null && ! newLabelElt.isJsonNull()) {
      newLabel = newLabelElt.getAsString();
    }
    // cat id
    final JsonElement newCatIdElt = json.getAsJsonObject().get(UpdateTransactionAction.NEW_CAT_ID);
    long newCatId = -1;
    if (newLabelElt != null && !newLabelElt.isJsonNull()) {
      newCatId = newCatIdElt.getAsLong();
    }
    // check number
    final JsonElement newCheckNumberElt = json.getAsJsonObject().get(UpdateTransactionAction.NEW_CHECK_NUMBER);
    String newCheckNumber = null;
    if (newCheckNumberElt != null && !newCheckNumberElt.isJsonNull()) {
      newCheckNumber = newCheckNumberElt.getAsString();
    }
    // notes
    final JsonElement notesElt = json.getAsJsonObject().get(UpdateTransactionAction.NOTES);
    String notes = null;
    if (notesElt != null && !notesElt.isJsonNull()) {
      notes = notesElt.getAsString();
    }
    // tagIDs
    final JsonElement tagIDsElt = json.getAsJsonObject().get(UpdateTransactionAction.TAG_IDS);
    final ArrayList<Long> tagIDs = new ArrayList<Long>();
    if (tagIDsElt != null && tagIDsElt.isJsonArray()) {
      final JsonArray jsonTagIDs = tagIDsElt.getAsJsonArray();
      for (JsonElement jsonTagID : jsonTagIDs) {
        tagIDs.add(context.<Long>deserialize(jsonTagID, Long.class));
      }
    }
    // duplicate txn
    final JsonElement dupTxnElt = json.getAsJsonObject().get(UpdateTransactionAction.DUPLICATE_TXN);
    boolean dupTxn = false;
    if (dupTxnElt != null && !dupTxnElt.isJsonNull()) {
      dupTxn = dupTxnElt.getAsBoolean();
    }

    final JsonElement budgetDayElt = json.getAsJsonObject().get(UpdateTransactionAction.BUDGET_DAY);
    int budgetDay = -1;
    if (budgetDayElt != null && !budgetDayElt.isJsonNull()) {
      budgetDay = budgetDayElt.getAsInt();
    }

    final JsonElement budgetMonthElt = json.getAsJsonObject().get(UpdateTransactionAction.BUDGET_MONTH);
    int budgetMonth = -1;
    if (budgetMonthElt != null && !budgetMonthElt.isJsonNull()) {
      budgetMonth = budgetMonthElt.getAsInt() - DateUtils.MAGIC_MONTH;
    }

    final JsonElement budgetYearElt = json.getAsJsonObject().get(UpdateTransactionAction.BUDGET_YEAR);
    int budgetYear = -1;
    if (budgetYearElt != null && !budgetYearElt.isJsonNull()) {
      budgetYear = budgetYearElt.getAsInt() - DateUtils.MAGIC_YEAR;
    }

    Date budgetDate;
    if (budgetDay < 0 || budgetMonth < 0 || budgetYear < 0) {
      budgetDate = null;
    } else {
      budgetDate = new Date(budgetYear, budgetMonth, budgetDay);
    }

    // get the secret
    JsonElement secretElement = json.getAsJsonObject().get(UpdateTransactionAction.SECRET);
    final String secret =  secretElement == null ? null : secretElement.getAsString();


    final UpdateTransactionAction action =
        new UpdateTransactionAction(txnId, newLabel, newCatId, newCheckNumber, notes, tagIDs, dupTxn, budgetDate);
    action.setSharedSecret(secret);
    return action;
  }

}
