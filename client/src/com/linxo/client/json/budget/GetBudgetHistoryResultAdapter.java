/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 10/11/2015 by hugues.
 */
package com.linxo.client.json.budget;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.dto.budget.BudgetTarget;
import com.linxo.client.actions.pfm.budget.GetBudgetHistoryResult;
import com.linxo.client.json.JsonActionParseException;

import java.lang.reflect.Type;
import java.util.HashMap;

public class GetBudgetHistoryResultAdapter
    implements JsonDeserializer<GetBudgetHistoryResult>, JsonSerializer<GetBudgetHistoryResult>

{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields


  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods


  @Override
  public GetBudgetHistoryResult deserialize(JsonElement jsonElement, Type typeOfT, JsonDeserializationContext context) throws JsonActionParseException
  {

    Type myType = new TypeToken<HashMap<BudgetTarget, HashMap<LinxoDate, Double>>>(){}.getType();

    // Instantiate the object to get its class object
    HashMap<BudgetTarget, HashMap<LinxoDate, Double>> budgetHistory = context.deserialize(jsonElement, myType);


    return new GetBudgetHistoryResult(budgetHistory);
  }

  @Override
  public JsonElement serialize(GetBudgetHistoryResult result, Type typeOfSrc, JsonSerializationContext context)
  {

    Type myType = new TypeToken<HashMap<BudgetTarget, HashMap<LinxoDate, Double>>>(){}.getType();
    JsonElement jsonResult = context.serialize(result.getBudgetHistory(), myType);


    return jsonResult;
  }
}
