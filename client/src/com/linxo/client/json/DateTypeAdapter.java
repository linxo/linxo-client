/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 19/01/2011 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Used to serialize/de-serialize dates through the use of seconds
 */
public final class DateTypeAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final long S_TO_MS = 1000;

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods


  @Override
  public Date deserialize(JsonElement jsonElement, Type type,
                          JsonDeserializationContext jsonDeserializationContext)
      throws JsonActionParseException
  {
    String stringDate = jsonElement.getAsString(); // the date in milliseconds
    try {
      long epoch = Long.parseLong(stringDate);
      epoch*=S_TO_MS;
      return new Date(epoch);
    }
    catch(NumberFormatException nfe){
      throw new JsonActionParseException("Could not parse epoch time in["+stringDate+"]", nfe);
    }
  }

  @Override
  public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext)
  {
    // make sure we send a String as long might not be correctly represented in JavaScript
    return new JsonPrimitive(String.valueOf(date.getTime()/S_TO_MS));
  }

}
