/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 29/04/2011 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.pfm.sync.Credential;
import com.linxo.client.actions.pfm.sync.ListAccountsInFinancialInstitutionAction;

import java.lang.reflect.Type;
import java.util.ArrayList;

final class ListAccountsInFinancialInstitutionActionAdapter
    implements JsonDeserializer<ListAccountsInFinancialInstitutionAction>
{
  @Override
  public ListAccountsInFinancialInstitutionAction deserialize(JsonElement json,
                                                              Type typeOfT,
                                                              JsonDeserializationContext context)
      throws JsonActionParseException
  {
    // get the fid
    final JsonElement fidElt = json.getAsJsonObject().get(ListAccountsInFinancialInstitutionAction.FINANCIAL_INSTITUTION_ID);
    if (fidElt == null) {
      throw new JsonActionParseException("Invalid ListAccountsInFinancialInstitutionAction: no fid found");
    }
    final long financialInstitutionId = fidElt.getAsLong();

    // get the credentials
    final JsonElement credentialsElt = json.getAsJsonObject().get(ListAccountsInFinancialInstitutionAction.CREDENTIALS);
    if (credentialsElt == null) {
      throw new JsonActionParseException("Invalid ListAccountsInFinancialInstitutionAction: no credentials found");
    }
    final JsonArray credentialsArray = credentialsElt.getAsJsonArray();
    final ArrayList<Credential> credentials = new ArrayList<Credential>(credentialsArray.size());
    for(JsonElement jsonCredential : credentialsArray) {
      credentials.add(context.<Credential>deserialize(jsonCredential, Credential.class));
    }

    // semi auto may not be in the json depending on api version...
    Boolean semiAuto = null;
    final JsonElement semiAutoElt = json.getAsJsonObject().get(ListAccountsInFinancialInstitutionAction.SEMI_AUTO);
    if (semiAutoElt != null) {
      semiAuto = semiAutoElt.getAsBoolean();
    }

    // get the secret
    final JsonElement secretElt = json.getAsJsonObject().get(ListAccountsInFinancialInstitutionAction.SECRET);
    if (secretElt == null) {
      throw new JsonActionParseException("Invalid ListAccountsInFinancialInstitutionAction: no secret found");
    }
    final String secret = secretElt.getAsString();

    final ListAccountsInFinancialInstitutionAction result =
        new ListAccountsInFinancialInstitutionAction(financialInstitutionId, credentials, semiAuto);
    result.setSharedSecret(secret);
    return result;
  }

}
