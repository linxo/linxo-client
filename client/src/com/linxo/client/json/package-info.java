/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 06/08/2015 by hugues.
 */

/**
 * This package and its sub-packages contain all Action/Result
 * Adapter classes that implement either
 * {@link com.google.gson.JsonDeserializer} or/and
 * {@link com.google.gson.JsonDeserializer}.
 */
package com.linxo.client.json;