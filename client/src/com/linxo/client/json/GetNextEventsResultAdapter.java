/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 4:09:21 PM by tarunmalhotra.
*/
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.events.GetNextEventsResult;
import com.linxo.client.actions.pfm.events.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public final class GetNextEventsResultAdapter
    implements JsonSerializer<GetNextEventsResult>, JsonDeserializer<GetNextEventsResult>
{
  private ThreadLocal<ArrayList<JsonObject>> jsonObjects = new ThreadLocal<ArrayList<JsonObject>>();

  public GetNextEventsResultAdapter()
  {
  }

  @Override
  public JsonElement serialize(GetNextEventsResult getNextEventsResult, Type type,
                               JsonSerializationContext jsonSerializationContext)
  {
    jsonObjects.set(new ArrayList<JsonObject>());

    final List<AbstractSyncEvent> events = getNextEventsResult.getEvents();
    for (AbstractSyncEvent event : events) {
      JsonObject jsonObject = jsonSerializationContext.serialize(event).getAsJsonObject();
      if (event instanceof AccountListUpdateEvent) {
        jsonObject.add("type", new JsonPrimitive("accountListUpdateEvent"));
      }
      else if (event instanceof SynchronizationUpdateEvent) {
        jsonObject.add("type", new JsonPrimitive("synchronizationUpdateEvent"));
      }
      else if (event instanceof DealsUpdateEvent) {
        jsonObject.add("type", new JsonPrimitive("dealsUpdateEvent"));
      }
      else if (event instanceof PermissionsUpdateEvent) {
        jsonObject.add("type", new JsonPrimitive("permissionsUpdateEvent"));
      }
      else if (event instanceof OauthTokenEvent) {
        jsonObject.add("type", new JsonPrimitive("oauthTokenEvent"));
      }
      else if (event instanceof UserInfoUpdatedEvent) {
        jsonObject.add("type", new JsonPrimitive("userInfoUpdatedEvent"));
      }
      jsonObjects.get().add(jsonObject);
    }
    final JsonObject result = new JsonObject();
    final JsonArray jsonArray = new JsonArray();
    for (JsonObject object : jsonObjects.get()) {
      jsonArray.add(object);
    }
    result.add(GetNextEventsResult.EVENTS, jsonArray);
    return result;
  }

  @Override
  public GetNextEventsResult deserialize(JsonElement jsonElement, Type type,
                                         JsonDeserializationContext jsonDeserializationContext)
      throws JsonActionParseException
  {
    final ArrayList<AbstractSyncEvent> result = new ArrayList<AbstractSyncEvent>();

    if (jsonElement.getAsJsonObject().get("events").isJsonArray()) {
      final JsonArray events = jsonElement.getAsJsonObject().get("events").getAsJsonArray();
      for (int i = 0; i < events.size(); i++) {
        final String typeEvent = events.get(i).getAsJsonObject().get("type").toString().replace("\"", "");

        final JsonObject je = events.get(i).getAsJsonObject();
        je.remove("type");
        AbstractSyncEvent evt = null;

        if (typeEvent.equalsIgnoreCase("synchronizationUpdateEvent")) {
          //noinspection unchecked
          evt = (AbstractSyncEvent<SynchronizationUpdateEventHandler>)
              jsonDeserializationContext.deserialize(je, SynchronizationUpdateEvent.class);
        }
        else if (typeEvent.equalsIgnoreCase("accountListUpdateEvent")) {
          //noinspection unchecked
          evt = (AbstractSyncEvent<AccountListUpdateEventHandler>)
              jsonDeserializationContext.deserialize(je, AccountListUpdateEvent.class);
        }
        else if (typeEvent.equalsIgnoreCase("dealsUpdateEvent")) {
          //noinspection unchecked
          evt = (AbstractSyncEvent<DealsUpdateEventHandler>)
              jsonDeserializationContext.deserialize(je, DealsUpdateEvent.class);
        }
        else if (typeEvent.equalsIgnoreCase("permissionsUpdateEvent")) {
          //noinspection unchecked
          evt = (AbstractSyncEvent<PermissionsUpdateEventHandler>)
              jsonDeserializationContext.deserialize(je, PermissionsUpdateEvent.class);
        }
        else if (typeEvent.equalsIgnoreCase("oauthTokenEvent")) {
          //noinspection unchecked
          evt = (AbstractSyncEvent<OauthTokenEventHandler>)
              jsonDeserializationContext.deserialize(je, OauthTokenEvent.class);
        }
        else if (typeEvent.equalsIgnoreCase("userInfoUpdatedEvent")) {
          //noinspection unchecked
          evt = (AbstractSyncEvent<UserInfoUpdatedEventHandler>)
              jsonDeserializationContext.deserialize(je, UserInfoUpdatedEvent.class);
        }

        if (evt != null) {
          result.add(evt);
        }
      }
    }
    return new GetNextEventsResult(result);
  }

}
