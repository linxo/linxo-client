/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 29/04/2011 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.pfm.sync.Credential;
import com.linxo.client.actions.pfm.sync.UpdateCredentialsAction;

import java.lang.reflect.Type;
import java.util.ArrayList;

final class UpdateCredentialsActionAdapter implements JsonDeserializer<UpdateCredentialsAction>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  @Override
  public UpdateCredentialsAction deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonActionParseException
  {
    // get the gid
    final long groupId = json.getAsJsonObject()
        .get(UpdateCredentialsAction.ACCOUNT_GROUP_ID).getAsLong();

    // get the credentials
    final JsonArray jsonCredentials = json.getAsJsonObject()
        .get(UpdateCredentialsAction.CREDENTIALS).getAsJsonArray();
    final ArrayList<Credential> credentials = new ArrayList<Credential>();
    for(JsonElement jsonCredential : jsonCredentials)
    {
      credentials.add(context.<Credential>deserialize(jsonCredential, Credential.class));
    }

    // get the secret
    JsonElement secretElement = json.getAsJsonObject().get(UpdateCredentialsAction.SECRET);
    final String secret =  secretElement == null ? null : secretElement.getAsString();

    final UpdateCredentialsAction result = new UpdateCredentialsAction(groupId, credentials);
    result.setSharedSecret(secret);

    return result;
  }
}
