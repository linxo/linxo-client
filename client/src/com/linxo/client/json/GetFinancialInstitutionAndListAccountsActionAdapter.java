/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.json;


import com.google.gson.*;
import com.linxo.client.actions.pfm.sync.Credential;
import com.linxo.client.actions.pfm.sync.GetFinancialInstitutionAndListAccountsAction;

import java.lang.reflect.Type;
import java.util.ArrayList;

final class GetFinancialInstitutionAndListAccountsActionAdapter
  implements JsonDeserializer<GetFinancialInstitutionAndListAccountsAction>
{
  @Override
  public GetFinancialInstitutionAndListAccountsAction deserialize(final JsonElement json,
                                                                  final Type typeOfT,
                                                                  final JsonDeserializationContext context)
    throws JsonActionParseException
  {
    // get the group id
    final JsonElement groupIdElt = json.getAsJsonObject().get(GetFinancialInstitutionAndListAccountsAction.ACCOUNT_GROUP_ID);
    if (groupIdElt == null || groupIdElt.isJsonNull()) {
      throw new JsonActionParseException("Invalid GetFinancialInstitutionAndListAccountsAction: no group id found");
    }
    final long groupId = groupIdElt.getAsLong();

    // get the secrets (may not be there)
    final JsonElement secretsElt = json.getAsJsonObject().get(GetFinancialInstitutionAndListAccountsAction.SECRETS);
    ArrayList<Credential> secrets = null;
    if (secretsElt != null && secretsElt.isJsonArray()) {
      final JsonArray secretsArray = secretsElt.getAsJsonArray();
      secrets = new ArrayList<Credential>(secretsArray.size());
      for (JsonElement jsonCredential : secretsArray) {
        secrets.add(context.<Credential>deserialize(jsonCredential, Credential.class));
      }
    }

    // get the secret
    final JsonElement secretElt = json.getAsJsonObject().get(GetFinancialInstitutionAndListAccountsAction.SECRET);
    final String secret =
        (secretElt == null || secretElt.isJsonNull())
            ? null
            : secretElt.getAsString();

    final GetFinancialInstitutionAndListAccountsAction result =
        new GetFinancialInstitutionAndListAccountsAction(groupId, secrets);
    result.setSharedSecret(secret);
    return result;
  }

}
