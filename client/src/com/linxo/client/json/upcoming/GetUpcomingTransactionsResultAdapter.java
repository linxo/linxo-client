/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 25/09/2014 by hugues.
 */
package com.linxo.client.json.upcoming;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.linxo.client.dto.upcoming.UpcomingTransaction;
import com.linxo.client.actions.pfm.upcoming.GetUpcomingTransactionsResult;
import com.linxo.client.json.JsonActionParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public final class GetUpcomingTransactionsResultAdapter
    implements JsonDeserializer<GetUpcomingTransactionsResult>, JsonSerializer<GetUpcomingTransactionsResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields


  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods


  @Override
  public GetUpcomingTransactionsResult deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context)
      throws JsonActionParseException
  {


    Type myType = new TypeToken<HashMap<Long, ArrayList<UpcomingTransaction>>>(){}.getType();

    // Instantiate the object to get its class object
    HashMap<Long, ArrayList<UpcomingTransaction>> transactions = context.deserialize(jsonElement, myType);


    return new GetUpcomingTransactionsResult(transactions);
  }

  @Override
  public JsonElement serialize(GetUpcomingTransactionsResult result, Type type, JsonSerializationContext context)
  {


    JsonElement jsonResult = context.serialize(result.getTransactions());


    return jsonResult;
  }

}
