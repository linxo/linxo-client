/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 23/01/2015 by hugues.
 */
package com.linxo.client.json.upcoming;

import com.linxo.client.dto.upcoming.UpcomingTransaction;
import com.linxo.client.dto.upcoming.UpcomingTransactionOccurrence;
import com.linxo.client.actions.support.json.CustomizedTypeAdapterFactory;

/**
 * This AdapterFactory fills the UpcomingTransaction into its
 * {@link com.linxo.client.dto.upcoming.UpcomingTransaction#modifiedOccurrences}
 * after it's been parsed.
 *
 * The UpcomingTransactionOccurrences are cleared from their UpcomingTransaction during the
 * serialization process to make sure that we do not have infinite loop as they are
 * cross referenced. The removal is performed via the {@link com.linxo.client.actions.support.json.Exclude} annotation.
 *
 * @see com.linxo.client.actions.support.json.Exclude
 * @see com.linxo.client.dto.upcoming.UpcomingTransaction
 */
public final class UpcomingTransactionAdapterFactory
    extends CustomizedTypeAdapterFactory<UpcomingTransaction>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors
  public UpcomingTransactionAdapterFactory() {
    super(UpcomingTransaction.class);
  }

  // Instance Methods

  @Override
  protected void afterParse(UpcomingTransaction deserialized)
  {
    // Here, we need to fill the UpcomingTransactionOccurrence with the containing UpcomingTransaction.
    for(UpcomingTransactionOccurrence occurrence : deserialized.getModifiedOccurrences())
    {
      occurrence.setUpcomingTransaction(deserialized);
    }
  }

}
