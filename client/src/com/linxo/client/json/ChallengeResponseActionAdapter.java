/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.pfm.sync.ChallengeResponseAction;
import com.linxo.client.actions.pfm.sync.Credential;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 *
 */
final class ChallengeResponseActionAdapter
  implements JsonDeserializer<ChallengeResponseAction>
{
  @Override
  public ChallengeResponseAction deserialize(final JsonElement json, final Type typeOfT,
                                             final JsonDeserializationContext context)
    throws JsonActionParseException
  {
    // group id
    final JsonElement groupIdElt = json.getAsJsonObject().get(ChallengeResponseAction.ACCOUNT_GROUP_ID);
    if (groupIdElt == null) {
      throw new JsonActionParseException("No group id element");
    }
    final long groupId = groupIdElt.getAsLong();

    // credentials
    final JsonArray credentialsElt = json.getAsJsonObject()
        .get(ChallengeResponseAction.CREDENTIALS).getAsJsonArray();
    final ArrayList<Credential> credentials = new ArrayList<Credential>();
    for (JsonElement jsonCredential : credentialsElt) {
      credentials.add(context.<Credential>deserialize(jsonCredential, Credential.class));
    }

    // get the secret
    JsonElement secretElement = json.getAsJsonObject().get(ChallengeResponseAction.SECRET);
    final String secret =  secretElement == null ? null : secretElement.getAsString();

    final ChallengeResponseAction action = new ChallengeResponseAction(groupId, credentials);
    action.setSharedSecret(secret);
    return action;
  }

}
