/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 19/01/2011 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.actions.LinxoAction;

import java.lang.reflect.Type;

public final class JsonActionAdapter implements JsonSerializer<JsonAction>, JsonDeserializer<JsonAction> {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  @Override
  public JsonAction deserialize(JsonElement jsonElement, Type type,
                                JsonDeserializationContext jsonDeserializationContext)
      throws JsonActionParseException
  {
    final JsonElement classNameElement = jsonElement.getAsJsonObject().get(JsonAction.ACTION_NAME);
    if (classNameElement == null) {
      throw new JsonActionParseException("Cannot deserialize JsonAction since action name is null");
    }
    String className = classNameElement.getAsString();

    final LinxoAction<?> action;
    try {
      final Class clazz = getClassLoader().loadClass(className);

      final JsonElement actionElement = jsonElement.getAsJsonObject().get(JsonAction.ACTION);
      if (actionElement == null) {
        throw new JsonActionParseException("Cannot deserialize JsonAction of class [" + className
                                     + "] since its action is null");
      }
      action = jsonDeserializationContext.deserialize(actionElement, clazz);
    }
    catch (ClassNotFoundException cnf) {
      throw new JsonActionParseException("Cannot deserialize JsonAction : name unknown ["
                                   + className + "]", cnf);
    }

    final JsonAction.Hash hash =
        jsonDeserializationContext.deserialize(jsonElement.getAsJsonObject().get(JsonAction.HASH),
                                               JsonAction.Hash.class);

    return new JsonAction(action, hash);
  }


  @Override
  public JsonElement serialize(JsonAction jsonAction, Type type,
                               JsonSerializationContext jsonSerializationContext)
  {
    JsonObject result = new JsonObject();
    String actionName = jsonAction.getActionName();

    result.add(JsonAction.ACTION_NAME, new JsonPrimitive(actionName));
    result.add(JsonAction.ACTION, jsonSerializationContext.serialize(jsonAction.getAction()));
    result.add(JsonAction.HASH, jsonSerializationContext.serialize(jsonAction.getHash()));

    return result;
  }



  private ClassLoader getClassLoader()
  {
    ClassLoader result = this.getClass().getClassLoader();

    if(result == null){
      return ClassLoader.getSystemClassLoader();
    }

    return result;
  }

}
