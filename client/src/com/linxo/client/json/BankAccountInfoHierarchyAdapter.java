/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 02/10/2015 by hugues.
 */
package com.linxo.client.json;

import com.google.gson.*;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.dto.account.CheckingsAccountInfo;
import com.linxo.client.dto.account.CreditCardAccountInfo;
import com.linxo.client.dto.account.LoanAccountInfo;
import com.linxo.client.dto.account.SavingsAccountInfo;
import java.lang.reflect.Type;

public class BankAccountInfoHierarchyAdapter
    implements JsonSerializer<BankAccountInfo>, JsonDeserializer<BankAccountInfo>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  @Override
  public JsonElement serialize(BankAccountInfo src, Type typeOfSrc, JsonSerializationContext context)
  {
    final JsonElement retVal;

    switch(src.getType()){
      case Checkings:{
        retVal = context.serialize(src, CheckingsAccountInfo.class);
      }
      break;

      case Savings:{
        retVal = context.serialize(src, SavingsAccountInfo.class);
      }
      break;

      case CreditCard:{
        retVal = context.serialize(src, CreditCardAccountInfo.class);
      }
      break;

      case Loan:{
        retVal = context.serialize(src, LoanAccountInfo.class);
      }
      break;

      default:{
        throw new JsonActionParseException("Unknown subtype of BankAccountInfo ["+src.getType()+"]");
      }
    }


    return retVal;
  }

  @Override
  public BankAccountInfo deserialize(JsonElement json, Type typeOfT,
                                     JsonDeserializationContext context) throws JsonActionParseException
  {
    JsonObject jsonObject =  json.getAsJsonObject();

    JsonPrimitive prim = (JsonPrimitive) jsonObject.get("type");
    String type = prim.getAsString();

    final Class klass;
    if(AccountType.Checkings.name().equals(type)) klass = CheckingsAccountInfo.class;
    else if (AccountType.Savings.name().equals(type)) klass = SavingsAccountInfo.class;
    else if (AccountType.CreditCard.name().equals(type)) klass = CreditCardAccountInfo.class;
    else if (AccountType.Loan.name().equals(type)) klass = LoanAccountInfo.class;
    else throw new JsonActionParseException("Unknown subtype of BankAccountInfo ["+type+"]");

    return context.deserialize(jsonObject, klass);
  }

}
