/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;

import java.lang.reflect.Type;

/**
 * An abstract class adapters may extend to use utility some methods.
 */
public abstract class AbstractAdapter
{
  private Boolean getBoolean(final JsonElement jsonElement, final String memberName)
  {
    return jsonElement.getAsJsonObject().get(memberName) == null ?
           null : jsonElement.getAsJsonObject().get(memberName).getAsBoolean();
  }

  private Integer getInteger(final JsonElement jsonElement, final String memberName)
  {
    return jsonElement.getAsJsonObject().get(memberName) == null ?
           null : jsonElement.getAsJsonObject().get(memberName).getAsInt();
  }

  private Long getLong(final JsonElement jsonElement, final String memberName)
  {
    return jsonElement.getAsJsonObject().get(memberName) == null ?
           null : jsonElement.getAsJsonObject().get(memberName).getAsLong();
  }

  private Double getDouble(final JsonElement jsonElement, final String memberName)
  {
    return jsonElement.getAsJsonObject().get(memberName) == null ?
           null : jsonElement.getAsJsonObject().get(memberName).getAsDouble();
  }

  private String getString(final JsonElement jsonElement, final String memberName)
  {
    return jsonElement.getAsJsonObject().get(memberName) == null ?
           null : jsonElement.getAsJsonObject().get(memberName).getAsString();
  }

  private Object getObject(final JsonElement jsonElement, final String memberName,
                           final Type clazz, final JsonDeserializationContext context)
  {
    return jsonElement.getAsJsonObject().get(memberName) == null ?
           null : context.deserialize(jsonElement.getAsJsonObject().get(memberName), clazz);
  }

}
