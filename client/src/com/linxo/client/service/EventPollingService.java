/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.actions.pfm.events.*;
import com.linxo.client.actions.events.EventRegisterAction;
import com.linxo.client.actions.events.EventRegisterResult;
import com.linxo.client.actions.events.GetNextEventsAction;
import com.linxo.client.actions.events.GetNextEventsResult;
import com.linxo.client.net.LinxoClient;
import com.linxo.client.net.LinxoClientException;
import com.linxo.infrastructure.exceptions.TechnicalException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.*;

public class EventPollingService
  implements Runnable
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(EventPollingService.class);

  // Static Initializers

  // Static Methods

  // Instance Fields

  private LinxoClient linxoClient;
  private AuthenticationService authenticationService;
  private TransactionService transactionService;
  private AccountService accountService;

  private boolean cancelled = true;
  private boolean registered;

  // Polling fields
  private long minEventNumber;
  private int queueId;

  public DealsUpdateEventHandler dealUpdatedHandler;

  private ScheduledExecutorService scheduler;
  private int threadCount;

  // Instance Initializers

  // Constructors

  // Instance Methods

  ////////////////////////////////////////////////////////////////////
  //
  //                        Setters
  //
  ////////////////////////////////////////////////////////////////////

  public void setLinxoClient(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;
  }

  public void setAuthenticationService(AuthenticationService authenticationService)
  {
    this.authenticationService = authenticationService;
  }

  public void setTransactionService(TransactionService transactionService)
  {
    this.transactionService = transactionService;
  }

  public void setAccountService(AccountService accountService)
  {
    this.accountService = accountService;
  }

  ////////////////////////////////////////////////////////////////////
  //
  //                        Public API
  //
  ////////////////////////////////////////////////////////////////////

  void initialize()
  {}

  private void fireDealUpdated(DealsUpdateEvent e)
  {
    dealUpdatedHandler.onDeal(e);
  }

  void clear()
  {
    cancelled = true;
    registered = false;
  }

  private void register() throws TechnicalException
  {
    if (registered)
      return;

    if(logger.isTraceEnabled()) {
      logger.trace("Sending EventRegisterAction");
    }
    try {
      EventRegisterResult res = linxoClient.sendAction(new EventRegisterAction());
      queueId = res.getQueueId();
      minEventNumber = 0;
      if (res.getSynchronizationUpdateEvents() != null)
      {
        if(logger.isTraceEnabled()) {
          logger.trace(
              String.format("EventRegisterAction got %1$s events", res.getSynchronizationUpdateEvents().size()));
        }
        for(SynchronizationUpdateEvent evt : res.getSynchronizationUpdateEvents())
        {
          processEvent(evt);
        }
      }
      registered = true;
    }
    catch(LinxoClientException lce) {
      throw new TechnicalException("Caught error while registering", lce);
    }
  }


  void start() throws TechnicalException
  {
    if(logger.isDebugEnabled()) {
      logger.debug("EventPolling Starting");
    }

    if (!cancelled)
      return;

    cancelled = false;

    register();

    scheduler = Executors.newScheduledThreadPool(1, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable runnable)
      {
        return new Thread("EventPolling-" + threadCount++);
      }
    });
    scheduler.scheduleAtFixedRate(this, 0, 2, TimeUnit.SECONDS);

  }

  @Override
  public void run()
  {
    if(cancelled) {
      List<Runnable> waitingTasks = scheduler.shutdownNow();
      scheduler = null;

      if(logger.isDebugEnabled()){
        logger.debug("EventPolling Stopped !!!!!");
        logger.debug("===> ["+waitingTasks.size()+"] were running.");
      }

      return;
    }

    try {
      // Send event polling request
      if(logger.isTraceEnabled())
      {
        logger.trace("Sending GetNextEventsAction");
      }

      GetNextEventsResult r = linxoClient.sendAction(new GetNextEventsAction(queueId, minEventNumber));

      // Process results
      if (r.getEvents() != null)
      {
        if(logger.isTraceEnabled())
        {
          logger.trace(String.format("GetNextEventsResult got %1$s events", r.getEvents().size()));
        }
        for(AbstractSyncEvent evt : r.getEvents())
        {
          processEvent(evt);
        }
      }
    }
    catch (TechnicalException e) {
      // No need to log event if the task was cancelled
      if (cancelled) return;

      logger.error(String.format("Event polling raised exception: %1$s", e.getMessage()), e);
    }
  }

  private void processEvent(AbstractSyncEvent evt)
  {
    try
    {
      minEventNumber = Math.max(minEventNumber, evt.getEventNumber());
      if(logger.isDebugEnabled()){
        logger.debug(
            String.format("EventPollingService processing %1$s: %2$s",
                evt.getClass(), evt));
      }

      if(evt instanceof AccountListUpdateEvent)
      {
        accountService.processEvent((AccountListUpdateEvent) evt);
        //FireAccountListUpdated(accountListUpdateEvent);
        return;
      }

      if(evt instanceof DealsUpdateEvent)
      {
        DealsUpdateEvent dealsUpdateEvent = (DealsUpdateEvent)evt;
        authenticationService.setDeal(dealsUpdateEvent.getDealInfo());
        transactionService.loadDates();
        fireDealUpdated(dealsUpdateEvent);
        return;
      }
      if(evt instanceof PermissionsUpdateEvent)
      {
        PermissionsUpdateEvent permissionsUpdateEvent = (PermissionsUpdateEvent)evt;
        authenticationService.setPermissions(permissionsUpdateEvent.getPermissionInfoSet());
        //FirePermissionsUpdated(permissionsUpdateEvent);
        return;
      }
      if(evt instanceof SynchronizationUpdateEvent)
      {
        SynchronizationUpdateEvent synchronizationUpdateEvent = (SynchronizationUpdateEvent)evt;
        accountService.processEvent(synchronizationUpdateEvent);
        //FireSynchronizationUpdated(synchronizationUpdateEvent);
        return;
      }
      if(evt instanceof UploadUpdateEvent)
      {
        UploadUpdateEvent uploadUpdateEvent = (UploadUpdateEvent)evt;
        //FireUploadUpdated(uploadUpdateEvent);
        return;
      }
      if(evt instanceof UserInfoUpdatedEvent)
      {
        UserInfoUpdatedEvent userInfoUpdatedEvent = (UserInfoUpdatedEvent)evt;
        authenticationService.setUserProfile(userInfoUpdatedEvent.getUserProfile());
        //FireUserInfoUpdated(userInfoUpdatedEvent);
        //return;
      }
      //todo log warning if event not recognized
    }
    catch (Exception e)
    {
      logger.error(
          String.format("ProcessEvent raised exc=%1$s",e.getMessage()), e);
    }
  }

  public void stop()
  {
    if(logger.isDebugEnabled()){
      logger.debug("EventPolling Stopping");
    }
    cancelled = true;
  }
}
