/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.actions.pfm.*;
import com.linxo.client.actions.pfm.events.AccountListUpdateEvent;
import com.linxo.client.actions.pfm.events.AccountListUpdateEventHandler;
import com.linxo.client.actions.pfm.events.SynchronizationUpdateEvent;
import com.linxo.client.actions.pfm.sync.*;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.data.pfm.group.SynchroStatus;
import com.linxo.client.dto.account.AccountGroupInfo;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.dto.account.SavingsAccountInfo;
import com.linxo.client.dto.tx.TransactionInfo;
import com.linxo.client.net.LinxoClient;
import com.linxo.crypto.CryptoException;
import com.linxo.crypto.Encryption;
import com.linxo.infrastructure.exceptions.TechnicalException;
import org.apache.log4j.Logger;

import java.util.*;


// todo
public class AccountService
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(AccountService.class);

  // Static Initializers

  // Static Methods

  // Instance Fields

  private LinxoClient linxoClient;
  private FinancialInstitutionService financialInstitutionService;
  private TransactionService transactionService;
  private EventPollingService eventPollingService;

  private HashMap<Long, BankAccountInfo> bankAccountMap = new HashMap<Long, BankAccountInfo>();
  private HashMap<Long, AccountGroupInfo> accountGroupMap = new HashMap<Long, AccountGroupInfo>();

  // ID of account group that is in creation
  private Long addAccountGroupId;

  /// <summary>
  /// The list of account groups (banks) for the user currently logged in
  /// </summary>
  public ArrayList<AccountGroupInfo> accountGroups;


  // Instance Initializers

  // Constructors

  // Instance Methods

  ////////////////////////////////////////////////////////////////////
  //
  //                        Setters
  //
  ////////////////////////////////////////////////////////////////////

  public void setLinxoClient(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;
  }

  public void setFinancialInstitutionService(FinancialInstitutionService financialInstitutionService)
  {
    this.financialInstitutionService = financialInstitutionService;
  }

  public void setTransactionService(TransactionService transactionService)
  {
    this.transactionService = transactionService;
  }

  public void setEventPollingService(EventPollingService eventPollingService)
  {
    this.eventPollingService = eventPollingService;
  }


  ////////////////////////////////////////////////////////////////////
  //
  //                        Getters
  //
  ////////////////////////////////////////////////////////////////////

  public HashMap<Long, BankAccountInfo> getBankAccountMap()
  {
    return bankAccountMap;
  }

  public HashMap<Long, AccountGroupInfo> getAccountGroupMap()
  {
    return accountGroupMap;
  }


  ////////////////////////////////////////////////////////////////////
  //
  //                        Event Handling
  //
  ////////////////////////////////////////////////////////////////////

  public AccountListUpdateEventHandler accountListUpdateHandler;

  protected void fireAccountGroupUpdated(AccountListUpdateEvent e)
  {
    AccountListUpdateEventHandler handler = accountListUpdateHandler;
    if (handler != null) handler.onAccountListUpdate(e);
  }

//  public AccountGroupInfoHandler accountGroupAdded;

  protected void fireAccountGroupAdded(AccountGroupInfo accountGroup)
  {
//  AccountGroupInfoHandler handler = AccountGroupAdded;
//  if (handler != null) handler(this, accountGroup);
  }

//  public event EventHandler<AccountGroupInfo> AccountGroupUpdated;

  protected void fireAccountGroupUpdated(AccountGroupInfo accountGroup)
  {
//  EventHandler<AccountGroupInfo> handler = AccountGroupUpdated;
//  if (handler != null) handler(this, accountGroup);
  }

//  public event EventHandler<long> AccountGroupRemoved;

  protected void fireAccountGroupRemoved(long accountGroupId)
  {
//  EventHandler<long> handler = AccountGroupRemoved;
//  if (handler != null) handler(this, accountGroupId);
  }

//  public event EventHandler<long> AccountGroupUpdating;

  protected void fireAccountGroupUpdating(long accountGroupId)
  {
//  EventHandler<long> handler = AccountGroupUpdating;
//  if (handler != null) handler(this, accountGroupId);
  }


  ////////////////////////////////////////////////////////////////////
  //
  //                        Public API
  //
  ////////////////////////////////////////////////////////////////////


  void initialize()
  {}


  void load()
  {
    if(logger.isDebugEnabled()) {
      logger.debug("Load start");
    }
    try {
      GetAccountGroupsResult res = linxoClient.sendAction(new GetAccountGroupsAction());
      filterClosed(res.getAccountGroups());
// todo: needed in java ?
//      financialInstitutionService.fill(res.getAccountGroups());
      update(res.getAccountGroups());
    }
    catch (TechnicalException e){
      logger.error("Error while loading Account Groups", e);
      return;
    }
    if(logger.isDebugEnabled()) {
      logger.debug("Loaded");
    }
  }


  private AccountGroupInfo loadAccountGroup(long accountGroupId)
  {

    AccountGroupInfo accountGroup = null;
    try {
      GetAccountGroupResult res = linxoClient.sendAction(new GetAccountGroupAction(accountGroupId));
      accountGroup = res.getAccountGroup();
    }
    catch(TechnicalException e){
      logger.error("Could not load the accountGroup ["+accountGroupId+"]", e);
      return null;
    }
    filterClosed(accountGroup);
// todo: needed in java ?
//    financialInstitutionService.fill(accountGroup);

    boolean newGroup = !accountGroupMap.containsKey(accountGroupId);
    accountGroupMap.put(accountGroup.getId(), accountGroup);
    for(BankAccountInfo bankAccount : accountGroup.getBankAccounts().values())
    {
      bankAccountMap.put(bankAccount.getId(), bankAccount);
    }

    if (newGroup)
    {
      fireAccountGroupAdded(accountGroup);
      transactionService.loadDates();
    }
    else
    {
      fireAccountGroupUpdated(accountGroup);
    }

    return accountGroup;
  }

  private void filterClosed(ArrayList<AccountGroupInfo> accountGroups)
  {
    for(AccountGroupInfo accountGroup : accountGroups)
    {
      filterClosed(accountGroup);
    }
  }

  private void filterClosed(AccountGroupInfo accountGroup)
  {
    Iterator<Long> iterator = accountGroup.getBankAccounts().keySet().iterator();

    while(iterator.hasNext())
    {
      Long key = iterator.next();
      if(accountGroup.getBankAccounts().get(key).getCloseDate() != null){
        iterator.remove();
      }
    }
  }

  void clear()
  {
    bankAccountMap.clear();
    accountGroupMap.clear();
    addAccountGroupId = null;
    accountGroups = null;
  }

  private void update(ArrayList<AccountGroupInfo> accountGroups)
  {
    clear();
    this.accountGroups = accountGroups;
    for (AccountGroupInfo accountGroupInfo : accountGroups)
    {
      accountGroupMap.put(accountGroupInfo.getId(), accountGroupInfo);
      if (accountGroupInfo.getBankAccounts() != null)
      {
        for (BankAccountInfo bankAccount : accountGroupInfo.getBankAccounts().values())
        {
          bankAccountMap.put(bankAccount.getId(), bankAccount);
        }
      }
    }
  }

  /**
   * Starts the process for adding an account group (bank)
   *
   * @param financialInstitutionId - ID of financial institution
   * @param credentials - All credentials should be passed in clear. This method will take care of PGP encryption.
   */
  public void addAccountGroup(long financialInstitutionId, ArrayList<Credential> credentials)
      throws TechnicalException
  {
    if(logger.isDebugEnabled()){
      logger.debug("AddAccountGroup start: financialInstitutionId = " + financialInstitutionId);
    }

    addAccountGroupId = null;

    // Get a fresh PGP public key from server
    try {
      GetFinancialInstitutionResult getFiResult =
          linxoClient.sendAction(new GetFinancialInstitutionAction(financialInstitutionId));

      // Encrypt credentials
      for (Credential credential : credentials) {
        if (credential.getKey().contains("secret")) {
          credential.setValue(Encryption.encryptLikeJavaScript(credential.getValue(), getFiResult.getPublicKey()));
        }
      }

      // Send action to start the process.
      // The list of accounts will come as an event processed in ProcessEvent that will
      // send SelectAccountsForSynchronizationAction in order to start the second phase of the process.
      if (logger.isDebugEnabled()) {
        logger.debug("Sending ListAccountsInFinancialInstitutionAction");
      }
      ListAccountsInFinancialInstitutionResult listAccountsResult =
          // todo : handle the semi-auto case
          linxoClient.sendAction(new ListAccountsInFinancialInstitutionAction(financialInstitutionId, credentials, true));
      addAccountGroupId = listAccountsResult.getGroupId();
      if(logger.isDebugEnabled()){
        logger.debug("ListAccountsInFinancialInstitutionResult received with groupId = " + addAccountGroupId);
      }
    }
    catch(TechnicalException lce)
    {
      throw new TechnicalException("Error during communication", lce);
    }
    catch(CryptoException ce) {
      throw new TechnicalException("Error during encryption", ce);
    }
  }

  public void cancelAddAccountGroup()
  {
    try {
      linxoClient.sendAction(new CancelAddAccountGroupAction());
    }
    catch(TechnicalException e) {
      logger.error("Error while cancelling AddAccountGroup");
    }
  }

  public void fill(ArrayList<TransactionInfo> txInfos)
  {
    for (TransactionInfo txInfo : txInfos)
    {
      fill(txInfo);
    }
  }

  public void fill(TransactionInfo txInfo)
  {
    if (bankAccountMap.containsKey(txInfo.getBankAccountId()))
    {
      //todo check about where the fill-ing is needed
//      txInfo.setBankAccount(bankAccountMap.get(txInfo.getBankAccountId()));
    }
    else
    {
      logger.error(
          String.format(
              "Could not Fill tx.BankAccount, tx.Id=%1$s, tx.BankAccountId=%2$s",
              txInfo.getId(), txInfo.getBankAccountId()));
    }
  }

  public void processEvent(SynchronizationUpdateEvent e)
  {
    Long accountGroupId = e.getAccountGroupId();

    if (e.getUpdateType() == SynchronizationUpdateEvent.UpdateType.Update)
    {
      if (accountGroupMap.containsKey(accountGroupId))
      {
        accountGroupMap.get(accountGroupId).setStatus(SynchroStatus.Running);
        fireAccountGroupUpdating(accountGroupId);
      }
      return;
    }
    if (e.getUpdateType() == SynchronizationUpdateEvent.UpdateType.Success ||
        e.getUpdateType() == SynchronizationUpdateEvent.UpdateType.Failure)
    {
      loadAccountGroup(accountGroupId);
    }
  }


  public void processEvent(AccountListUpdateEvent e)
  {
    // Send event to UI
    fireAccountGroupUpdated(e);

    // Receive the list of bank accounts for the group account that we are adding
    // and selecting them all for addition.
    if (e.getUpdateType() == AccountListUpdateEvent.UpdateType.Success)
    {
      try {
        logger.debug("ProcessEvent : AccountListUpdateEvent.UpdateTypes.Success... Sending SelectAccountsForSynchronizationAction");
        linxoClient.sendAction(new SelectAccountsForSynchronizationAction(e.getProviderAccounts()));
        //todo check result of SendAction(new SelectAccountsForSynchronizationAction)
      }
      catch(TechnicalException exc) {
        logger.error("Error while selecting accounts", exc);
      }
    }
  }

  public UpdateBankAccountResult closeBankAccount(BankAccountInfo bankAccount)
  {
    // this is not logical but we need to include all the following fields in the action
    UpdateBankAccountAction action = new UpdateBankAccountAction(
        bankAccount.getId(),
        bankAccount.getName(),
        true,
        bankAccount.getCurrentBalance(),
        bankAccount.getType()
    );

    if(logger.isDebugEnabled()){
      logger.debug(
          String.format(
              "CloseBankAccount bankAccountId=%1$s, name=%2$s",
              bankAccount.getId(), bankAccount.getName()));
    }
    try {
      UpdateBankAccountResult res = linxoClient.sendAction(action);
      if(logger.isDebugEnabled()){
        logger.debug("  => res=" + res.getStatus());
      }
      return res;
    }
    catch(TechnicalException e) {
      logger.error("Caught exception while updating account (closing it) : " + bankAccount.getId());
      return null;
    }
  }

  public void deleteAccountGroup(long accountGroupId)
  {
    try {
      // DeleteAccountGroupResult does not contain any status field so nothing to check
      linxoClient.sendAction(new DeleteAccountGroupAction(accountGroupId));
    }
    catch(TechnicalException exc) {
      logger.error("Error while deleting accountGroup:" + accountGroupId);
    }
  }

  public void deleteBankAccount(long accountGroupId, long bankAccountId)
  {
    try {
      // DeleteBankAccountResult does not contain any status field so nothing to check
      linxoClient.sendAction(new DeleteBankAccountAction(accountGroupId, bankAccountId));
    }
    catch(TechnicalException exc) {
      logger.error("Caught error while deleting bank account["+bankAccountId+"]");
    }
  }

  public UpdateAccountGroupResult setAccountGroupName(long accountGroupId, String name)
  {
    if(logger.isDebugEnabled()) {
      logger.debug(
          String.format("SetAccountGroupName accountGroupId=%1$s, name=%2$s", accountGroupId, name));
    }
    try {
      UpdateAccountGroupResult res = linxoClient.sendAction(
          new UpdateAccountGroupAction(accountGroupId, name));

      logger.debug("  => res=" + res.getStatus());
      return res;
    }
    catch(TechnicalException exc) {
      logger.error("Error while changing group["+accountGroupId+"] name["+name+"]");
      return null;
    }
  }

  /**
   * Changes the name of the bank account
   * @param bankAccount - BankAccount to modify
   * @param newName - New name
   * @return -
   */
  public UpdateBankAccountResult setBankAccountName(BankAccountInfo bankAccount, String newName)
  {
    // this is not logical but we need to include all the following fields in the action
    UpdateBankAccountAction action = new UpdateBankAccountAction(
        bankAccount.getId(),
        newName,
        false,
        bankAccount.getCurrentBalance(),
        bankAccount.getType(),
        (bankAccount.getType() == AccountType.Savings)
            ?((SavingsAccountInfo)bankAccount).getSavingsType()
            :null
    );

    if(logger.isDebugEnabled()) {
      logger.debug(
          String.format("SetBankAccountName bankAccountId=%1$s, name=%2$s",
              bankAccount.getId(), newName));
    }
    try {
      UpdateBankAccountResult res = linxoClient.sendAction(action);
      if(logger.isDebugEnabled()) {
        logger.debug("  => res=" + res.getStatus());
      }
      return res;
    }
    catch(TechnicalException exc){
      logger.error("Error while changing account name", exc);
      return null;
    }
  }

  /**
   * Launch synchronization for all accounts
   */
  public void SynchronizeAllAccountGroups()
  {
    try {
      linxoClient.sendAction(new SynchronizeAllAccountGroupsAction());
    }
    catch(TechnicalException exc) {
      logger.error("Error while launching synchronization", exc);
    }
  }

  public void synchronizeAccountGroup(long accountGroupId)
  {
    try {
      if(logger.isDebugEnabled())
      {
        logger.debug("SynchronizeAccountGroup accountGroupId=" + accountGroupId);
      }
      linxoClient.sendAction(new SynchronizeAccountGroupAction(accountGroupId));
    }
    catch(TechnicalException exc) {
      logger.error("Caught error while launching synchronization", exc);
    }
  }

}
