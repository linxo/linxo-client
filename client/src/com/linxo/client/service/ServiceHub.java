/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 14/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.net.LinxoClient;
import com.linxo.client.net.LinxoClientException;
import com.linxo.infrastructure.exceptions.FatalException;
import com.linxo.infrastructure.interfaces.AppDeviceConfig;
import com.linxo.infrastructure.interfaces.AppStorage;
import org.apache.log4j.Logger;

public class ServiceHub {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(ServiceHub.class);

  // Static Initializers

  // Static Methods

  // Instance Fields

  private LinxoClient linxoClient;
  private EventPollingService eventPollingService;
  private AccountService accountService;
  private CategoryService categoryService;
  private FinancialInstitutionService financialInstitutionService;
  private AlertService alertService;
  private AuthenticationService authenticationService;
  private TransactionService transactionService;
  private UserPropertiesService userPropertiesService;

  // Instance Initializers

  // Constructors

  /**
   * This constructor will initialize all services to enable communication with Linxo API.
   * Method Initialize should be called after constructor.
   * @param appDeviceConfig -
   * @param appStorage -
   * @throws FatalException if something goes wrong during the initialization
   */
  public ServiceHub(AppDeviceConfig appDeviceConfig, AppStorage appStorage)
      throws FatalException
  {
    try{
      linxoClient = new LinxoClient(
          appDeviceConfig.getDomain(),
          appDeviceConfig.getApiKey(),
          appDeviceConfig.getApiSecret());
    }
    catch(LinxoClientException lce) {
      final String message = "Error while configuring LinxoClient";
      logger.error(message);
      throw new FatalException(message, lce);
    }

    // Create services
    eventPollingService = new EventPollingService();
    accountService = new AccountService();
    categoryService = new CategoryService();
    financialInstitutionService = new FinancialInstitutionService();
    alertService = new AlertService();
    authenticationService = new AuthenticationService();
    transactionService = new TransactionService();
    userPropertiesService = new UserPropertiesService();

    // Link services
    eventPollingService.setLinxoClient(linxoClient);
    eventPollingService.setAuthenticationService(authenticationService);
    eventPollingService.setTransactionService(transactionService);
    eventPollingService.setAccountService(accountService);

    accountService.setLinxoClient(linxoClient);
    accountService.setFinancialInstitutionService(financialInstitutionService);
    accountService.setTransactionService(transactionService);
    accountService.setEventPollingService(eventPollingService);

    categoryService.setLinxoClient(linxoClient);

    financialInstitutionService.setLinxoClient(linxoClient);

    alertService.setLinxoClient(linxoClient);

    authenticationService.setAppDeviceConfig(appDeviceConfig);
    authenticationService.setAppStorage(appStorage);
    authenticationService.setLinxoClient(linxoClient);
    authenticationService.setEventPollingService(eventPollingService);
    authenticationService.setCategoryService(categoryService);
    authenticationService.setFinancialInstitutionService(financialInstitutionService);
    authenticationService.setAccountService(accountService);
    authenticationService.setAlertService(alertService);
    authenticationService.setTransactionService(transactionService);
    authenticationService.setUserPropertiesService(userPropertiesService);

    transactionService.setLinxoClient(linxoClient);
    transactionService.setCategoryService(categoryService);
    transactionService.setAccountService(accountService);

    userPropertiesService.setLinxoClient(linxoClient);

  }


  // Instance Methods


  ////////////////////////////////////////////////////////////////////
  //
  //                        Getters
  //
  ////////////////////////////////////////////////////////////////////

  public EventPollingService getEventPollingService()
  {
    return eventPollingService;
  }

  public AccountService getAccountService()
  {
    return accountService;
  }

  public CategoryService getCategoryService()
  {
    return categoryService;
  }

  public FinancialInstitutionService getFinancialInstitutionService()
  {
    return financialInstitutionService;
  }

  public AlertService getAlertService()
  {
    return alertService;
  }

  public AuthenticationService getAuthenticationService()
  {
    return authenticationService;
  }

  public TransactionService getTransactionService()
  {
    return transactionService;
  }

  public UserPropertiesService getUserPropertiesService()
  {
    return userPropertiesService;
  }


  ////////////////////////////////////////////////////////////////////
  //
  //                        Public API
  //
  ////////////////////////////////////////////////////////////////////

  public void initialize()
      throws FatalException
  {
    logger.debug("Initializing services...");
    // Initialization
    try {
      linxoClient.initialize();
    }
    catch (LinxoClientException lce) {
      throw new FatalException("Caught exception during initialization", lce);
    }
    logger.debug("Initializing Event polling...");
    eventPollingService.initialize();
    logger.debug("Initializing Accounts...");
    accountService.initialize();
    logger.debug("Initializing Categories...");
    categoryService.initialize();
    logger.debug("Initializing Financial Institutions...");
    financialInstitutionService.initialize();
    logger.debug("Initializing Alerts...");
    alertService.initialize();
    logger.debug("Initializing Authentication...");
    authenticationService.initialize();
    logger.debug("Initializing transactions...");
    transactionService.initialize();
    logger.debug("Initializing user properties...");
    userPropertiesService.initialize();
    logger.debug("Initialization completed !!");
  }


  public void clear()
      throws FatalException
  {
    logger.debug("Clearing services...");
    // Initialization
    try {
      linxoClient.close();
    }
    catch (LinxoClientException lce) {
      throw new FatalException("Caught exception during initialization", lce);
    }
    logger.debug("Clearing Event polling...");
    eventPollingService.clear();
    logger.debug("Clearing Accounts...");
    accountService.clear();
    logger.debug("Clearing Categories...");
    categoryService.clear();
    logger.debug("Clearing Financial Institutions...");
    financialInstitutionService.clear();
    logger.debug("Clearing Alerts...");
    alertService.clear();
    logger.debug("Clearing Authentication...");
    authenticationService.clear();
    logger.debug("Clearing transactions...");
    transactionService.clear();
    logger.debug("Clearing user properties...");
    userPropertiesService.clear();
    logger.debug("Clearing completed !!");
  }

}
