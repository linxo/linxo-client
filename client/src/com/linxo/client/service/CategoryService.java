/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 07/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.actions.classification.*;
import com.linxo.client.dto.tx.CatInfo;
import com.linxo.client.dto.tx.TransactionInfo;
import com.linxo.client.net.LinxoClient;
import com.linxo.infrastructure.exceptions.TechnicalException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class CategoryService
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(CategoryService.class);

  public static final long Theme_Uncategorized     = 0;
  public static final long Theme_Income            = 200000;
  public static final long Theme_Expenses          = 400000;
  public static final long Theme_ExcludeFromBudget = 600000;

  // Static Initializers

  // Static Methods

  // Instance Fields

  private LinxoClient linxoClient;

  private HashMap<Long, CatInfo> catMap = new HashMap<Long, CatInfo>();

  public ArrayList<CatInfo> themes = new ArrayList<CatInfo>();

  public ArrayList<String> icons;

  public String nextColor;

  private final Object lock = new Object();

  // Instance Initializers

  // Constructors

  // Instance Methods

  ////////////////////////////////////////////////////////////////////
  //
  //                        Setters
  //
  ////////////////////////////////////////////////////////////////////

  public void setLinxoClient(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;
  }


  ////////////////////////////////////////////////////////////////////
  //
  //                        Public API
  //
  ////////////////////////////////////////////////////////////////////

  void initialize()
  {}

  void load()
  {
    try {
      // Thanks to the adapter, the Result contains the categories as a tree
      GetCategoriesResult res = linxoClient.sendAction(new GetCategoriesAction());
      synchronized (lock)
      {
        buildCategoryTree(res.getCatInfos());
        addOtherCategories();
        icons = buildIconList();
        nextColor = res.getNextColor();
      }
    }
    catch(TechnicalException exc) {
      logger.error("Caught exception while loading categories", exc);
      return;
    }
    if(logger.isDebugEnabled()) {
      logger.debug("Categories loaded");
    }
  }

  private ArrayList<String> buildIconList()
  {
    ArrayList<String> icons = new ArrayList<String>();
    for(CatInfo cat : catMap.values())
    {
      //HACK we exclude cat 200000 to avoid duplicate icon...
      if (!icons.contains(cat.getIcon()) && !cat.getIcon().equals("200000"))
      {
        icons.add(cat.getIcon());
      }
    }
    for (int i = 0; i < 14; i++)
    {
      String s = "custom-" + i;
      if (!icons.contains(s))
      {
        icons.add(s);
      }
    }
    return icons;
  }

  public Long addCategory(Long parentId, String name, String color, String icon)
  {
    final AddCategoryAction action = new AddCategoryAction(name, color, icon, parentId);

    AddCategoryResult res;
    try {
      res = linxoClient.sendAction(action);
    }
    catch(TechnicalException exc) {
      logger.error("Exception caught while adding a new category ["+action+"]", exc);
      return null;
    }

    if (res.getStatus() != AddCategoryResult.Status.Success)
    {
      throw new RuntimeException(" AddCategory res.Status=Error");
    }
    load();
    return res.getCategoryId();
  }



  public void editCategory(long categoryId, String name, String icon)
  {
    final EditCategoryAction action = new EditCategoryAction(categoryId, name, icon);

    EditCategoryResult res;
    try {
      res = linxoClient.sendAction(action);
    }
    catch(TechnicalException exc) {
      logger.error("Exception caught while editing a category ["+action+"]", exc);
      return;
    }

    if (res.getStatus() != EditCategoryResult.Status.Success)
    {
      throw new RuntimeException(" EditCategory res.Status=Error");
    }
    load();

  }


  public void hideCategory(long categoryId, long destinationId)
  {
    final HideCategoryAction action = new HideCategoryAction(categoryId, destinationId);

    HideCategoryResult res;
    try {
      res = linxoClient.sendAction(action);
    }
    catch(TechnicalException exc) {
      logger.error("Exception caught while hiding a category ["+action+"]", exc);
      return;
    }

    if (res.getStatus() != HideCategoryResult.Status.Success)
    {
      throw new RuntimeException(" HideCategory res.Status=Error");
    }
    load();
  }

  public void deleteCategory(long categoryId, long destinationId)
  {
    final DeleteCategoryAction action = new DeleteCategoryAction(categoryId, destinationId);

    DeleteCategoryResult res;
    try {
      res = linxoClient.sendAction(action);
    }
    catch(TechnicalException exc) {
      logger.error("Exception caught while deleting a category ["+action+"]", exc);
      return;
    }

    if (res.getStatus() != DeleteCategoryResult.Status.Success)
    {
      throw new RuntimeException(" DeleteCategory res.Status=Error");
    }
    load();
  }

  public void showCategory(long categoryId)
  {
    final ShowCategoryAction action = new ShowCategoryAction(categoryId);

    ShowCategoryResult res;
    try {
      res = linxoClient.sendAction(action);
    }
    catch(TechnicalException exc) {
      logger.error("Exception caught while showing a category ["+action+"]", exc);
      return;
    }

    if (res.getStatus() != ShowCategoryResult.Status.Success)
    {
      throw new RuntimeException(" ShowCategory res.Status=Error");
    }
    load();
  }


  /**
   * Adding an 'other' category as a subcategory for each category that can be set for a
   * transaction and that can have child categories.
   * The expense categories has an extra level of nesting compared to other themes.
   */
  private void addOtherCategories()
  {
    for (CatInfo theme : themes)
    {
      // Theme Expenses : we have categories and subcategories. We will create an 'other' fake sub-category as a child of each category
      if (theme.getId() == Theme_Expenses)
      {
        for (CatInfo category : theme.getSubCategories())
        {

          Collections.sort(category.getSubCategories());
          category.getSubCategories().add(createOther(category, CatInfo.Type.SubCategory));
        }
        Collections.sort(theme.getSubCategories());
      }
      // Theme Uncategorized : we have categories only. So we just add 1 'other' fake category
      else if (theme.getId() == Theme_Uncategorized)
      {
        Collections.sort(theme.getSubCategories());
        theme.getSubCategories().add(createOther(theme, CatInfo.Type.Category));
      }
      // Theme Exclude From Budget and Theme Income : We have for each theme only one category and then subcategories.
      // We remove the extra level of nesting
      else if (theme.getId() == Theme_ExcludeFromBudget || theme.getId() == Theme_Income)
      {
        if (theme.getSubCategories().size() == 1) //should always be the case but trying to be robust if we remove this level in DB
        {
          CatInfo uselessCategory = theme.getSubCategories().get(0);
          theme.setSubCategories(uselessCategory.getSubCategories());
          uselessCategory.setSubCategories(new ArrayList<CatInfo>());
          Collections.sort(theme.getSubCategories());
          theme.getSubCategories().add(uselessCategory);
          uselessCategory.setName(String.format("autre : %1$s" ,uselessCategory.getName()));
          //todo : may need to change the CategoryType ?
        }
      }
    }
  }


  /**
   * Creating a copy of a category to be used as a fake child to the given category.
   * This fake transaction is used to assign transactions to this category.
   * @param category -
   * @param catType -
   * @return -
   */
  private CatInfo createOther(CatInfo category, CatInfo.Type catType)
  {
    return new CatInfo(category.getId(), category.getKey(), category.getName(), category.getComment(),
        catType, category.getTheme(), category.getParent(), category.getColor(),
        category.getIcon(), category.isCustom(), category.isMasked(), category.isDisabled());
  }

  public CatInfo categoryById(long id)
  {
    return catMap.containsKey(id) ? catMap.get(id) : null;
  }

  void clear()
  {
    synchronized (lock)
    {
      catMap.clear();
      themes.clear();
    }
  }

  private void buildCategoryTree(ArrayList<CatInfo> catInfos)
  {
    clear();

    // First put all categories in a Dictionary, indexed by category id
    for (CatInfo catInfo : catInfos) {
      catMap.put(catInfo.getId(), catInfo);
    }

    // Then reference each category parent, theme and children
    // In Java, this is performed while de-marshalling the data.
    for (CatInfo catInfo : catInfos) {
//      // Fill the category theme
//      if (catInfo.getTheme() != null)
//      {
//        if (catMap.containsKey(catInfo.getThemeId.Value))
//        {
//          catInfo.Theme = _catDictionary[catInfo.ThemeId.Value];
//        }
//        else
//        {
//          Log.Error("Could not fill cat.Theme: cat.Id={0}, cat.ThemeId={1}", catInfo.Id, catInfo.ThemeId);
//        }
//      }
//
//      // Fill the category parent
//      if (catInfo.ParentId.HasValue)
//      {
//        if (_catDictionary.ContainsKey(catInfo.ParentId.Value))
//        {
//          catInfo.Parent = _catDictionary[catInfo.ParentId.Value];
//        }
//        else
//        {
//          Log.Error("Could not fill cat.Parent: cat.Id={0}, cat.ParentId={1}", catInfo.Id, catInfo.ParentId);
//        }
//
//      }
//
//      // Fill the category children
//      if (catInfo.SubCategoryIds != null)
//      {
//        foreach (var id in catInfo.SubCategoryIds)
//        {
//          if (_catDictionary.ContainsKey(id))
//          {
//            catInfo.SubCategories.Add(_catDictionary[id]);
//          }
//          else
//          {
//            Log.Error("Could not fill cat.SubCategories: cat.Id={0}, SubCategoryId={1}",catInfo.Id,id);
//          }
//        }
//      }
      if (catInfo.getTheme() == null) {
        themes.add(catInfo);
      }
    }
  }


  public void fill(ArrayList<TransactionInfo> txInfos)
{
  synchronized (lock)
  {
    for (TransactionInfo txInfo : txInfos)
    {
      fill(txInfo);
    }
  }
}

  public void fill(TransactionInfo txInfo)
{
  synchronized (lock)
  {
    if (!catMap.containsKey(txInfo.getCategoryId()))
    {
      logger.error(
          String.format(
              "Did not find category for txInfo.Category: txInfo.Id=%1$s, txInfo.CategoryId=%2$s",
              txInfo.getId(), txInfo.getCategoryId()));
    }
  }
}

}
