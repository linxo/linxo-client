/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.actions.auth.*;
import com.linxo.client.actions.auth.signup.IsEmailAvailableAction;
import com.linxo.client.actions.auth.signup.IsEmailAvailableResult;
import com.linxo.client.actions.auth.signup.SignUpAction;
import com.linxo.client.actions.auth.signup.SignUpResult;
import com.linxo.client.actions.user.*;
import com.linxo.client.data.StringUtils;
import com.linxo.client.data.auth.AuthStatus;
import com.linxo.client.data.permissions.Feature;
import com.linxo.client.dto.alerts.AlertInfo;
import com.linxo.client.dto.device.AppInfo;
import com.linxo.client.dto.user.DealInfo;
import com.linxo.client.dto.user.PermissionInfo;
import com.linxo.client.dto.user.UserProfileInfo;
import com.linxo.client.net.LinxoClient;
import com.linxo.client.net.LinxoClientException;
import com.linxo.infrastructure.exceptions.FatalException;
import com.linxo.infrastructure.exceptions.TechnicalException;
import com.linxo.infrastructure.interfaces.AppDeviceConfig;
import com.linxo.infrastructure.interfaces.AppStorage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

/**
 * Usage
 * <pre>
 * if (authService.HasToken)
 *   Ask User to enter code pin showing number of attempts as authService.PinAttempts
 *   status = authService.LoginDevice(PIN);
 *   if (status == AuthStatus.Success)
 *     authStatus.LoadSession
 *     show logged in UI
 *     end
 *   else if (status == AuthStatus.InvalidCredentials)
 *     show very first screen asking for email address
 *   else if (status == AuthStatus.PinInvalid)
 *     Ask user to enter PIN again showing authService.PinAttempts
 *   else if (status == AuthStatus.PinBlocked)
 *     tell user that PIN was tried too many times and he has to authenticate with email/password
 *     show very first screen asking for email address
 *   else if (status == AuthStatus.DeviceLimitReached)
 *     that should not really happen for us. show error message and redirect to first screen.
 *   if exc is raised in LoginDevice =&gt; go to first screen showing email address
 *
 * if (!authService.HasToken)
 *   ask user to enter email address
 *   If email available
 *     Show create account screen and ask for password
 *     authService.SignupUser(email,pass)
 *     authService.AuthorizeDevice(email, pass)
 *     If success ask to define PIN and confirm PIN
 *     authService.Pin = pin
 *   else
 *     ask password for existing user
 *     authService.AuthorizeDevice(email, pass)
 *     If success ask to define PIN and confirm PIN
 *     authService.Pin = pin
 * </pre>
 */

// todo : review exceptions, looks bad :(
public class AuthenticationService
{

  // Nested Types (mixing inner and static classes is okay

  // Static Fields

  private static final Logger logger = Logger.getLogger(AuthenticationService.class);


  // Constants for safe storage
  private static final String SAFE_KEY_INSTALL_ID = "install_id";
  private static final String SAFE_KEY_DEVICE_ID = "device_id";
  private static final String SAFE_KEY_TOKEN = "token";
  private static final String SAFE_KEY_PIN = "pin";
  private static final String SAFE_KEY_PIN_ATTEMPTS = "pin_attempts";

  public static final int MAX_PIN_ATTEMPTS = 3;
  public static final int DEFAULT_SESSION_TIMEOUT = 15;

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Contains info on last version of app available and whether an upgrade is required
  public AppInfo app;

  public AppDeviceConfig appDeviceConfig;
  public AppStorage appStorage;
  public LinxoClient linxoClient;
  public EventPollingService eventPollingService;
  public CategoryService categoryService;
  public FinancialInstitutionService financialInstitutionService;
  public AccountService accountService;
  public AlertService alertService;
  public TransactionService transactionService;
  public UserPropertiesService userPropertiesService;


  public boolean askForTermsAndConditions;
  public String blockedAddress;
  public DealInfo deal;
  public HashSet<PermissionInfo> permissions;
  public UserProfileInfo userProfile;

  private String termsAndConditionsUrl;


  public boolean sessionLoaded;


  // Instance Initializers

  // Constructors

  // Instance Methods

  ////////////////////////////////////////////////////////////////////
  //
  //                        Setters
  //
  ////////////////////////////////////////////////////////////////////

  public void setAppDeviceConfig(AppDeviceConfig appDeviceConfig)
  {
    this.appDeviceConfig = appDeviceConfig;
  }

  public void setAppStorage(AppStorage appStorage)
  {
    this.appStorage = appStorage;
  }

  public void setLinxoClient(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;
  }

  public void setEventPollingService(EventPollingService eventPollingService)
  {
    this.eventPollingService = eventPollingService;
  }

  public void setCategoryService(CategoryService categoryService)
  {
    this.categoryService = categoryService;
  }

  public void setFinancialInstitutionService(FinancialInstitutionService financialInstitutionService)
  {
    this.financialInstitutionService = financialInstitutionService;
  }

  public void setAccountService(AccountService accountService)
  {
    this.accountService = accountService;
  }

  public void setAlertService(AlertService alertService)
  {
    this.alertService = alertService;
  }

  public void setTransactionService(TransactionService transactionService)
  {
    this.transactionService = transactionService;
  }

  public void setUserPropertiesService(UserPropertiesService userPropertiesService)
  {
    this.userPropertiesService = userPropertiesService;
  }

  public void setDeal(DealInfo deal)
  {
    this.deal = deal;
  }

  public void setPermissions(HashSet<PermissionInfo> permissions)
  {
    this.permissions = permissions;
  }

  public void setUserProfile(UserProfileInfo userProfile)
  {
    this.userProfile = userProfile;
  }

  ////////////////////////////////////////////////////////////////////
  //
  //                        Public API
  //
  ////////////////////////////////////////////////////////////////////


  public void initialize()
  {}

  /**
   * We generate a new one when calling AuthorizeDevice using this format : timestamp + '/' + install_id
   * @return the device id that is used to uniquely identify this device in the service
   */
  public String getDeviceId()
  {
    return appStorage.getStringDataProtected(SAFE_KEY_DEVICE_ID);
  }

  public Boolean setDeviceId(String deviceId)
  {
    return appStorage.saveProtected(SAFE_KEY_DEVICE_ID, deviceId);
  }

  /**
   * Token is returned by the server when calling AuthorizeDevice.
   * A user session can be established with LoginDevice(DeviceId, Token)
   * @return the auth token
   */
  private String getToken()
  {
    return appStorage.getStringDataProtected(SAFE_KEY_TOKEN);
  }

  private Boolean setToken(String token)
  {
    return appStorage.saveProtected(SAFE_KEY_TOKEN, token);
  }

  public Boolean hasToken()
  {
    return getToken() != null
        && getDeviceId() != null
        && getPin() != null;
  }


  public boolean isPremium()
  {
    return deal != null;
  }


  /**
   * @return 4 digit PIN code chosen by user
   */
  private String getPin()
  {
    return appStorage.getStringDataProtected(SAFE_KEY_PIN);
  }

  public Boolean setPin(String pin)
  {
    return appStorage.saveProtected(SAFE_KEY_PIN, pin);
  }


  /**
   * @return The number of attempts remaining
   */
  public Integer getRemainingPinAttempts()
  {
    return MAX_PIN_ATTEMPTS - getPinAttempts();
  }

  public Integer getPinAttempts()
  {
    return appStorage.getIntegerDataProtected(SAFE_KEY_PIN_ATTEMPTS);
  }

  private Boolean setPinAttempts(Integer pin)
  {
    return appStorage.saveProtected(SAFE_KEY_PIN_ATTEMPTS, Integer.toString(pin));
  }

  ////////////////////////////////////////////////////////////////////
  //
  //                        Private
  //
  ////////////////////////////////////////////////////////////////////

  private String getInstallId()
  {
    String res = appStorage.getStringDataProtected(SAFE_KEY_INSTALL_ID);
    if (res == null)
    {
      res = UUID.randomUUID().toString();
      appStorage.saveProtected(SAFE_KEY_INSTALL_ID, res);
    }
    return res;
  }

  private String generateDeviceId()
  {
    String res = (String.format("%1$s/%2$s",
        System.currentTimeMillis()/1000, // epoch in seconds
        getInstallId()));                // InstallId

    //log removed for security
//  if(logger.isDebugEnabled()) {
//    logger.debug("GenerateDeviceId -> " + res);
//  }
    return res;
  }

  /**
   * Checks if the pin given in param matches the pin code stored on the device
   * @param pin -
   * @return 0 if the pin matches, the number (&gt;0) of unsuccessful attempts otherwise
   */
  private int checkPin(String pin)
  {
    String currentPin = getPin();
    Integer currentPinAttempts = getPinAttempts();

    boolean result = pin != null && StringUtils.equals(pin, currentPin);

    currentPinAttempts = result
        ? 0
        : currentPinAttempts + 1;

    setPinAttempts(currentPinAttempts);

    return currentPinAttempts;
  }

  public boolean isEmailAvailable(String email)
  {
    try {
      IsEmailAvailableResult res = linxoClient.sendAction(new IsEmailAvailableAction(email));
      return (res.getStatus() == IsEmailAvailableResult.Status.Success);
    }
    catch(TechnicalException exc) {
      logger.error("Caught exception while checking for email ["+email+"]", exc);
      return false;
    }
  }


  /**
   * Attempts to login the user.
   * If the attempt is successful, the UserId property is set and a LoggedIn event will be fired.
   * Other services may listen to this event to start loading content as needed and register to event queue.
   *
   * Property AskForTermsAndConditions is set to true on the service if TaC have changed and need user approval. In this case,
   * the client should get approval from user and send AcceptTermsAndConditionsAction before sending
   * any other action to the server.
   *
   * @param email - User email
   * @param password - User password
   * @return AuthStatus.Success if logged in successfully
   *         AuthStatus.InvalidCredentials if credentials are invalid
   *         AuthStatus.Blocked - If 3 invalid login attempts
   * @throws FatalException if an error arises during the login process
   */
  public AuthStatus loginUser(String email, String password)
      throws FatalException
  {
    if(logger.isDebugEnabled()){
      logger.debug("LoginUser start");
    }

    // First, some cleanup
    clear();
    if (getToken() != null && getDeviceId() != null)
    {
      if(logger.isDebugEnabled()) {
        logger.debug("A Token exists. De-authorizing");
      }
      deAuthorizeDevice();
    }

    LoginResult loginResult;
    try {
      // Sending action
      loginResult = linxoClient.sendAction(new LoginAction(email, password, false, DEFAULT_SESSION_TIMEOUT));
    }
    catch(TechnicalException exc) {
      logger.error("Caught Exception while performing login");
      throw new FatalException(exc.getMessage(), exc);
    }


    if (loginResult.isBlocked())
    {
      if(logger.isDebugEnabled()){
        logger.debug("LoginUser -> AuthStatus.Blocked");
      }
      return AuthStatus.Blocked;
    }

    long userId = loginResult.getUserId() != null ? loginResult.getUserId() : 0;
    askForTermsAndConditions = loginResult.isAskForTermsAndConditions();
    if(logger.isDebugEnabled()){
      logger.debug("AskForTermsAndConditions = " + askForTermsAndConditions);
    }

    if (userId <= 0)
    {
      if(logger.isDebugEnabled()){
        logger.debug("LoginUser -> AuthStatus.InvalidCredentials");
      }
      return AuthStatus.InvalidCredentials;
    }

    try {
      GetUserProfileResult getUserProfileResult = linxoClient.sendAction(new GetUserProfileAction());
      userProfile = getUserProfileResult.getUserInfo();
      if(logger.isDebugEnabled()){
        logger.debug("UserProfile loaded");
      }

      GetDealResult getDealResult = linxoClient.sendAction(new GetDealAction());
      deal = getDealResult.getDealInfo();
      if(logger.isDebugEnabled()){
        logger.debug("Deal loaded");
      }

      GetPermissionsResult getPermissionsResult = linxoClient.sendAction(new GetPermissionsAction());
      permissions = getPermissionsResult.getPermissions();
      if(logger.isDebugEnabled()){
        logger.debug("Permissions loaded");
      }

      if(logger.isDebugEnabled()) {
        logger.debug("User "+userProfile.getId()+" logged in");
        logger.debug("LoginUser -> AuthStatus.Success");
      }

      return AuthStatus.Success;

    }
    catch(Exception exc) {
      final String message = "Caught Exception while gathering user information";
      logger.error(message);
      throw new FatalException(message, exc);
    }
  }


  public ArrayList<AlertInfo> loadAlerts()
  {
    try
    {
      if (!hasToken())
      {
        logger.info("LoadAlerts: no token => null");
        return null;
      }
      AuthStatus status = loginDevice(getPin());
      if (status == AuthStatus.Success || status == AuthStatus.Inactive)
      {
        return alertService.loadAlerts();
      }
      logger.info("LoadAlerts: LoginDevice returned: " + status);
      return null;
    }
    catch (Exception e)
    {
      logger.error("Exception raised in LoadAlerts", e);
      return null;
    }
  }


  private void loadSession() throws TechnicalException
  {
    if(logger.isDebugEnabled()){
      logger.debug("LoadSession start");
    }

    categoryService.load();
    if(logger.isDebugEnabled()){
      logger.debug("Categories loaded");
    }

    financialInstitutionService.load();
    if(logger.isDebugEnabled()){
      logger.debug("Financial institutions loaded");
    }

    accountService.load();
    if(logger.isDebugEnabled()){
      logger.debug("Accounts loaded");
    }

    eventPollingService.start();
    if(logger.isDebugEnabled()){
      logger.debug("Event Polling started");
    }

    transactionService.load(); //todo await maybe not needed here. TBD Stephane
    if(logger.isDebugEnabled()){
      logger.debug("TransactionService loaded");
    }

    //await UserPropertiesService.Load();
    //if(logger.isDebugEnabled()) logger.debug("Transactions start");

    sessionLoaded = true;
  }

  private void clearSession()
  {
    //todo if the LoadSession Task has not completed, we should probably cancel it
    //todo we should also check if we need to clear to avoid unnecessary calls
    sessionLoaded = false;
    eventPollingService.clear();
    alertService.clear();
    transactionService.clear();
    accountService.clear();
    categoryService.clear();
    financialInstitutionService.clear();
    userPropertiesService.clear();
  }

  public String loadTermsAndConditionsUrl()
      throws FatalException
  {
    if (termsAndConditionsUrl != null) return termsAndConditionsUrl;

    if(logger.isDebugEnabled()){
      logger.debug("Loading terms and conditions");
    }
    try {
      GetTermsAndConditionsResult res = linxoClient.sendAction(new GetTermsAndConditionsAction());
      termsAndConditionsUrl = res.getUrl();
      return termsAndConditionsUrl;
    }
    catch(TechnicalException exc) {
      throw new FatalException("Could not gather the TaC url", exc);
    }
  }

  /**
   * This method should be called when user is approving new Terms and Conditions.
   * The client app is informed that new Terms and Conditions should be approved
   * via the property AskForTermsAndConditions that is set to true after logging in
   * (LoginUser, LoginDevice or AuthorizeDevice)
   */
  public void acceptTermsAndConditions()
  {
    if(logger.isDebugEnabled()){
      logger.debug("AcceptTermsAndConditions start");
    }
    try {
      linxoClient.sendAction(new AcceptTermsAndConditionsAction());
    }
    catch(TechnicalException exc) {
      logger.error("Caught exception while accepting the TaC", exc);
    }
  }


  public boolean hasPermission(Feature feature)
  {
    if (permissions == null) return false;
    for (PermissionInfo permission : permissions)
    {
      if (permission.getFeature() == feature) return true;
    }
    return false;
  }

  /**
   * Number of months of visible history for any filtered list of transactions
   * @return -1 for unlimited or positive value to indicate the number of months
   */
  public int visibleHistoryInMonths()
  {
    if (permissions == null) return -1;
    for(PermissionInfo permission : permissions)
    {
      if (permission.getFeature() == Feature.VisibleHistory)
      {
        return permission.getValue() == null || permission.getValue() <= 0 ? -1 : permission.getValue();
      }
    }
    return -1;
  }

  public void switchToBackground()
  {
    eventPollingService.stop();
  }

  /**
   * @return true if there is an active session in place
   */
  private boolean tryRestoreSession()
  {
    if (sessionLoaded)
    {
      try
      {
        linxoClient.sendAction(new CheckSessionAction());
        eventPollingService.start();
        return true;
      }
      catch(LinxoClientException e){
        logger.error("SwitchToForeground raised exc while checking session", e);
        clear();
        return false;
      }
      catch(TechnicalException te){
        logger.error("SwitchToForeground raised exc while restarting polling", te);
        clear();
        return false;
      }
    }

    return false;
  }

  /**
   * This method will create a new user if it succeeds. It does not initialize the
   * session for the newly created user. So the logical next step is LoginUser() or
   * AuthorizeDevice()
   * @param email - User email
   * @param password - User password
   * @return the signUpResult that contains the outcome of the signUp
   * @throws FatalException - if a error occurs while sending/reading in the communication.
   */
  public SignUpResult signUpUser(String email, String password)
      throws FatalException, TechnicalException
  {
    if(logger.isDebugEnabled()){
      logger.debug("SignUpUser start");
    }

    // Some cleanup first
    clear();
    if (getToken() != null && getDeviceId() != null)
    {
      deAuthorizeDevice();
    }
    clearToken();

    try {
      return linxoClient.sendAction(new SignUpAction(email, password));
    }
    catch(TechnicalException exc) {
      throw new FatalException("Caught exception while signing-up user", exc);
    }
  }

  /**
   *
   * @param email - email address
   * @param password - password
   * @return - AuthStatus.Success if all is well
   *         - AuthStatus.Blocked if the user account is blocked (3 or more invalid login attempts)
   *         - AuthStatus.InvalidCredentials if email / password does not match
   *         - AuthStatus.DeviceLimitReached If the API key used only allows a limited number of connected apps/devices
   * @throws FatalException if an error occurs during the device authentication
   * @throws TechnicalException if an error occurs during the gathering of user information
   *         after the device is authorized
   */
  public AuthStatus authorizeDevice(String email, String password)
      throws FatalException, TechnicalException
  {
    if(logger.isDebugEnabled()){
      logger.debug("AuthorizeDevice start");
    }
    clear();

    String token = getToken();
    String deviceId = getDeviceId();

    if (token != null && deviceId != null)
    {
      if(logger.isDebugEnabled())logger.debug("A Token exists. De-authorizing");
      deAuthorizeDevice();
    }

    deviceId = generateDeviceId();

    AuthorizeDeviceAction action = new AuthorizeDeviceAction(
        appDeviceConfig.getDeviceFamily(),
        appDeviceConfig.getDeviceType(),
        deviceId,
        email, password,
        appDeviceConfig.getDeviceName(),
        new AppInfo(appDeviceConfig.getAppIdentifier(), appDeviceConfig.getAppVersion(), false));

    if(logger.isDebugEnabled()){
      logger.debug("Sending AuthorizeDeviceAction");
    }
    AuthorizeDeviceResult authorizeDeviceResult;
    try {
      authorizeDeviceResult = linxoClient.sendAction(action);
    }
    catch(TechnicalException lce) {
      throw new FatalException("Caught exception while authorizing device", lce);
    }

    if (authorizeDeviceResult.getStatus() == AuthStatus.Blocked)
    {
      blockedAddress = authorizeDeviceResult.getBlockedAddress();
      if(logger.isDebugEnabled()){
        logger.debug("AuthorizeDevice -> AuthStatus.Blocked");
      }
      return AuthStatus.Blocked;
    }

    if (authorizeDeviceResult.getStatus() == AuthStatus.InvalidCredentials)
    {
      clearToken();
      if(logger.isDebugEnabled()){
        logger.debug("AuthorizeDevice -> AuthStatus.InvalidCredentials");
      }
      return AuthStatus.InvalidCredentials;
    }

    if (authorizeDeviceResult.getStatus() == AuthStatus.DeviceLimitReached)
    {
      if(logger.isDebugEnabled()){
        logger.debug("AuthorizeDevice -> AuthStatus.DeviceLimitReached");
      }
      return AuthStatus.DeviceLimitReached;
    }

    if (authorizeDeviceResult.getStatus() == AuthStatus.Success
        || authorizeDeviceResult.getStatus() == AuthStatus.Inactive)
    {
      if(logger.isDebugEnabled())logger.debug("AuthorizeDevice -> AuthStatus.Success or Inactive");
      askForTermsAndConditions = authorizeDeviceResult.isAskForTermsAndConditions();
      if(logger.isDebugEnabled())logger.debug("AskForTermsAndConditions = " + askForTermsAndConditions);

      if (askForTermsAndConditions)
      {
        try {
          linxoClient.sendAction(new AcceptTermsAndConditionsAction());
        }
        catch(TechnicalException lce) {
          throw new TechnicalException("Caught while accepting the new TaC", lce);
        }
      }

      app = authorizeDeviceResult.getAppInfo();
      deal = authorizeDeviceResult.getDeal();
      permissions = authorizeDeviceResult.getPermissions();
      userProfile = authorizeDeviceResult.getUserProfileInfo();

      setDeviceId(deviceId);
      setToken(authorizeDeviceResult.getToken());

      if(logger.isDebugEnabled())logger.debug(String.format("User %1$s logged in", userProfile.getId()));
      loadSession();
      return AuthStatus.Success;
    }

    clearToken();

    logger.error("Server returned unknown AuthStatus=" + authorizeDeviceResult.getStatus());
    logger.error("AuthorizeDevice -> AuthStatus.TechnicalException");

    throw new TechnicalException(String.format("Invalid status in AuthorizeDevice: %1$s", authorizeDeviceResult.getStatus()));
  }

  /**
   * LoginDevice is used to create a session using an authentication token that has been obtained previously
   * using the AuthorizeDevice method. The token should be stored in safe storage between sessions.
   * @param pin - PIN code entered by the user
   * @return AuthStatus.Success if all is well
   *         AuthStatus.Blocked if the user account is blocked (3 or more invalid login attempts)
   *         AuthStatus.InvalidCredentials if email / password does not match
   *         AuthStatus.DeviceLimitReached If the API key used only allows a limited number of connected apps/devices
   * @throws FatalException - if an error occurs while login the device or accepting new TaC
   * @throws TechnicalException - if an error occurs while gathering data after the device is logged
   */
  public AuthStatus loginDevice(String pin)
      throws FatalException, TechnicalException
  {
    if(logger.isDebugEnabled()) logger.debug("LoginDevice start");

    if (!hasToken())
    {
      clear();
      if(logger.isDebugEnabled()) logger.debug("Invalid state : calling LoginDevice but HasToken=false");
      String token = getToken();
      String deviceId = getDeviceId();

      if (token != null && deviceId != null)
      {
        deAuthorizeDevice();
      }
      if(logger.isDebugEnabled()) logger.debug("LoginDevice -> AuthStatus.InvalidCredentials");
      return AuthStatus.InvalidCredentials;
    }

    int pinAttempts = checkPin(pin);

    if (pinAttempts != 0)
    {
      logger.info("PIN invalid. PinAttempts=" + pinAttempts);
      if (getPinAttempts() < MAX_PIN_ATTEMPTS)
      {
        if(logger.isDebugEnabled()) logger.debug("LoginDevice -> AuthStatus.PinInvalid");
        return AuthStatus.PinInvalid;
      }
      deAuthorizeDevice();
      if(logger.isDebugEnabled()) logger.debug("LoginDevice -> AuthStatus.PinBlocked");
      return AuthStatus.PinBlocked;
    }

    if(logger.isDebugEnabled()) logger.debug("PIN validated");

    boolean res = tryRestoreSession();
    if (res)
    {
      return AuthStatus.SessionRestored;
    }

    clear();

    LoginDeviceResult loginDeviceResult;
    try {
      loginDeviceResult= linxoClient.sendAction(
          new LoginDeviceAction(getDeviceId(), getToken(),
              new AppInfo(appDeviceConfig.getAppIdentifier(),
                  appDeviceConfig.getAppVersion(),
                  false)));
    }
    catch(TechnicalException lce){
      throw new FatalException("Caught exception while loging-in device", lce);
    }

    if (loginDeviceResult.getStatus() == AuthStatus.Blocked)
    {
      logger.warn("Server returned AuthStatus.Blocked");
      // todo should we clear the token here ?? => yes
      // Also, not sure if server can return this status since
      // server removes tokens when blocking account...
      if(logger.isDebugEnabled()) logger.debug("LoginDevice -> AuthStatus.Blocked");
      return AuthStatus.Blocked;
    }

    if (loginDeviceResult.getStatus() == AuthStatus.InvalidCredentials)
    {
      logger.info("Server returned AuthStatus.InvalidCredentials. Removing Token");
      clearToken();
      if(logger.isDebugEnabled()) logger.debug("LoginDevice -> AuthStatus.InvalidCredentials");
      return AuthStatus.InvalidCredentials;
    }

    if (loginDeviceResult.getStatus() == AuthStatus.DeviceLimitReached)
    {
      if(logger.isDebugEnabled()) logger.debug("LoginDevice -> AuthStatus.DeviceLimitReached");
      return AuthStatus.DeviceLimitReached;
    }

    if (loginDeviceResult.getStatus() == AuthStatus.Success
        || loginDeviceResult.getStatus() == AuthStatus.Inactive)
    {
      if(logger.isDebugEnabled()) logger.debug("Server returned Success (or Inactive)");

      askForTermsAndConditions = loginDeviceResult.isAskForTermsAndConditions();
      if(logger.isDebugEnabled()) logger.debug("AskForTermsAndConditions = " + askForTermsAndConditions);

      if (askForTermsAndConditions)
      {
        try {
          linxoClient.sendAction(new AcceptTermsAndConditionsAction());
        }
        catch(TechnicalException lce) {
          throw new FatalException("Caught exception while accepting the new TaC", lce);
        }
      }

      app = loginDeviceResult.getAppInfo();
      deal = loginDeviceResult.getDeal();
      permissions = loginDeviceResult.getPermissions();
      userProfile = loginDeviceResult.getUserProfileInfo();

      logger.info(String.format("User %1$s logged in", userProfile.getId()));

      if(logger.isDebugEnabled()) logger.debug("LoginDevice -> AuthStatus.Success");
      loadSession();
      return AuthStatus.Success;
    }

    logger.error(
        String.format("Server returned unknown AuthStatus=%1$s. TechnicalException will be thrown",
            loginDeviceResult.getStatus()));
    if(logger.isDebugEnabled()) logger.debug("LoginDevice -> TechnicalException");
    throw new TechnicalException(String.format("Invalid status in LoginDevice: %1$s",
        loginDeviceResult.getStatus()));
  }

  public void deleteUser(String reason) throws TechnicalException
  {
    if(logger.isDebugEnabled())logger.debug("DeleteUser: reason=" + reason);
    try {
      linxoClient.sendAction(new DeleteUserAction(reason));
    }
    catch(TechnicalException lce) {
      throw new TechnicalException(lce);
    }
  }

  /**
   * De-authorize currently logged in device
   * @return true if properly de-authorized a device
   */
  public boolean deAuthorizeDevice()
      throws TechnicalException
  {
    if(logger.isDebugEnabled())logger.debug("DeAuthorizeDevice start");
    clear();
    String deviceId = getDeviceId();
    String token = getToken();
    clearToken();
    return deAuthorizeDevice(deviceId, token);
  }

  public void clearToken()
  {
    if(logger.isDebugEnabled())logger.debug("ClearToken");
    setDeviceId(null);
    setToken(null);
    setPin (null);
    setPinAttempts(0);
  }

  /**
   * De-authorize device identified by deviceId, token
   * @param deviceId -
   * @param token -
   * @return {@code true} if the device was correctly logged-out, {@code false} otherwise
   * @throws TechnicalException if an error occurred during the communication
   */
  private boolean deAuthorizeDevice(String deviceId, String token)
      throws TechnicalException
  {
    if (deviceId != null && token != null)
    {
      if(logger.isDebugEnabled())logger.debug("Sending DeAuthorizeDeviceNotLoggedInAction");
      try {
        DeAuthorizeDeviceNotLoggedInResult r = linxoClient.sendAction(new DeAuthorizeDeviceNotLoggedInAction(deviceId, token, null));
        if(logger.isDebugEnabled())logger.debug("DeAuthorizeDeviceNotLoggedInResult -> " + r.isSuccess());
        return r.isSuccess();
      }
      catch(TechnicalException lce) {
        throw new TechnicalException("Caught exception while login-out", lce);
      }
    }
    return false;
  }

  void clear()
  {
    clearSession();
    askForTermsAndConditions = false;
    app = null;
    deal = null;
    permissions = null;
    userProfile = null;
    blockedAddress = null;
  }

}
