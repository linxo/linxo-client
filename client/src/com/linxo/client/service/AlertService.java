/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 04/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.actions.pfm.GetAlertsAction;
import com.linxo.client.actions.pfm.GetAlertsResult;
import com.linxo.client.actions.pfm.HideAlertAction;
import com.linxo.client.dto.alerts.AlertInfo;
import com.linxo.client.net.LinxoClient;
import com.linxo.infrastructure.exceptions.TechnicalException;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public class AlertService
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(AlertService.class);

  // Static Initializers

  // Static Methods

  // Instance Fields

  private LinxoClient linxoClient;

  public ArrayList<AlertInfo> alerts;

  // Instance Initializers

  // Constructors

  // Instance Methods


  public void setLinxoClient(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;
  }

  public ArrayList<AlertInfo> getAlerts()
  {
    return alerts;
  }

  public void setAlerts(ArrayList<AlertInfo> alerts)
  {
    this.alerts = alerts;
  }


  // Public API

  public ArrayList<AlertInfo> loadAlerts()
  {
    try {
      GetAlertsResult res = linxoClient.sendAction(new GetAlertsAction());
      alerts = res.getAlerts();
      return alerts;
    }
    catch(TechnicalException lce){
      logger.error("Could not load financial institutions");
      return null;
    }
  }

  void clear()
  {}

  void initialize()
  {}

  public boolean hideAlerts(long[] alertIds)
  {
    ArrayList<Long> list = new ArrayList<Long>();
    for(long alertId : alertIds) {
      list.add(alertId);
    }

    try {
      linxoClient.sendAction(new HideAlertAction(list));
    }
    catch (TechnicalException e)
    {
      logger.error("Error while hiding alerts["+list+"]", e);
      return false;
    }

    // remove from list
    for(long alertId : alertIds) {
      for (int i = alerts.size()-1; i >=0 ; i--)
      {
        if (alerts.get(i).getId().equals(alertId))
        {
          alerts.remove(i);
          break;
        }
      }
    }

    return false;
  }

  public void hideAlerts(long alertId)
  {
    hideAlerts(new long[]{ alertId });
  }
}
