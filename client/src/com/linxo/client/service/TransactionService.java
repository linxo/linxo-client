/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.actions.pfm.*;
import com.linxo.client.actions.pfm.trends.*;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.dto.tx.TransactionInfo;
import com.linxo.client.net.LinxoClient;
import com.linxo.infrastructure.exceptions.TechnicalException;
import org.apache.log4j.Logger;

import java.util.ArrayList;

// todo
public class TransactionService
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(TransactionService.class);

  // Static Initializers

  // Static Methods

  // Instance Fields

  private LinxoClient linxoClient;
  private CategoryService categoryService;
  private AccountService accountService;


  /**
   *  First day of the first month that can be used for search and charts
   */
  public LinxoDate startEnabled;

  /**
   * Last day of the current month
   */
  public LinxoDate endEnabled;

  /**
   * First day of the month of the oldest transaction in the user account.
   */
  public LinxoDate  startData;

  // Instance Initializers

  // Constructors

  // Instance Methods

  ////////////////////////////////////////////////////////////////////
  //
  //                        Setters
  //
  ////////////////////////////////////////////////////////////////////

  public void setLinxoClient(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;
  }

  public void setCategoryService(CategoryService categoryService)
  {
    this.categoryService = categoryService;
  }

  public void setAccountService(AccountService accountService)
  {
    this.accountService = accountService;
  }

  ////////////////////////////////////////////////////////////////////
  //
  //                        Public API
  //
  ////////////////////////////////////////////////////////////////////

  void initialize()
  {}

  void load()
  {
    //await LoadAllTransactions(0, 30);
    //await LoadUncategorizedSince(DateTime.Now.AddDays(-60), 0, 30);
    //await LoadTransactionsForAccountType(AccountType.Checkings, 0, 30);
    loadDates();
  }

  void clear()
  {}

  public void loadDates()
  {
    //todo this needs to be reloaded when a user subscribes Premium
    try {
      GetDatesInitialInfoResult res = linxoClient.sendAction(new GetDatesInitialInfoAction());
      startData = res.getStartData();
      startEnabled = res.getStartEnabled();
      endEnabled = res.getEndEnabled();
      if(logger.isDebugEnabled()){
        logger.debug("StartData=" + res.getStartData());
        logger.debug("EndEnabled=" + res.getEndEnabled());
        logger.debug("StartEnabled=" + res.getStartEnabled());
      }
    }
    catch(TechnicalException exc) {
      logger.error("Caught error while loading initial dates", exc);
      return;
    }

    //TODO : implement the handling below
//    fireDatesLoaded();
  }


  /**
   * @param startDate - Transactions of that date and after are included
   * @param endDate - Transactions with this date are included
   * @return the object that contains the Result data
   */
  public GetTotalsAndCountsResult loadBudgetPieChartData(LinxoDate startDate, LinxoDate endDate)
  {
    GetTotalsAndCountsAction action =
        new GetTotalsAndCountsAction(GetTotalsAndCountsAction.Per.Category,startDate, endDate);

    if(logger.isTraceEnabled()){
      logger.trace(
          String.format(
              "LoadBudgetPieChartData startDate=%1$s endDate=%2$s",
              action.getStartLinxoDate(), action.getEndLinxoDate()));
    }

    try {
      GetTotalsAndCountsResult res = linxoClient.sendAction(action);

      if(logger.isTraceEnabled())
      {
        logger.trace("GetTotalsAndCountsResult=" + res);
      }
      return res;
    }
    catch(TechnicalException exc){
      logger.error("Caught Exception while loading TotalsAndCounts per category", exc);
      return null;
    }
  }

  /**
   * @param startDate - Transactions of that date and after are included
   * @param endDate - Transactions with this date are included
   * @return the object that contains the Result data
   */
  public GetTotalsAndCountsResult loadNetIncomeBarChartData(LinxoDate startDate, LinxoDate endDate)
  {
    GetTotalsAndCountsAction action =
        new GetTotalsAndCountsAction(GetTotalsAndCountsAction.Per.Month, startDate, endDate);

    if(logger.isDebugEnabled())
    {
      logger.debug(
          String.format(
              "LoadNetIncomeBarChartData startDate=%1$s endDate=%2$s",
              action.getStartLinxoDate(), action.getEndLinxoDate()));
    }

    try {
      GetTotalsAndCountsResult res = linxoClient.sendAction(action);
      if(logger.isTraceEnabled())
      {
        logger.trace("GetTotalsAndCountsResult=" +res);
      }
      return res;
    }
    catch(TechnicalException exc) {
      logger.error("Caught Exception while loading TotalsAndCounts per month", exc);
      return null;
    }
  }


  /**
   * Update trends preferences
   * @param excludedAccounts the list of accounts that should be excluded from the Trends
   * @param includedAccounts the list of accounts that should be included to the Trends
   */
  public void updateTrendsPreferences(ArrayList<Long> includedAccounts, ArrayList<Long> excludedAccounts)
  {
    try {
      linxoClient.sendAction(new UpdateTrendsPreferencesAction(includedAccounts, excludedAccounts));
    }
    catch(Exception exc)
    {
      logger.error("Caught error while changing preferences", exc);
    }
  }

  /**
   * Load transactions in all accounts using pagination
   * @param startRow - start row
   * @param numRows - number of rows
   * @return the received GetTransactionsResult that contains the transactions and
   * some other meta-information
   */
  public GetTransactionsResult loadAllTransactions(int startRow, int numRows)
  {
    GetTransactionsAction action = new GetTransactionsAction(null, null, null, null, null, true, startRow, numRows);
    return loadTransactions(action);
  }

  /**
   * Load transactions for a given account type using pagination
   * @param accountType - Account type  (example: Checkings, Savings, CreditCard)
   * @param startRow - start row
   * @param numRows - number of rows
   * @return the received GetTransactionsResult that contains the transactions and
   * some other meta-information
   */
  public GetTransactionsResult loadTransactionsForAccountType(AccountType accountType, int startRow, int numRows)
  {
    GetTransactionsAction action = new GetTransactionsAction(accountType, null, null, null, null, true, startRow, numRows);
    return loadTransactions(action);
  }

  /**
   * Load transactions for one bank account, using pagination
   * @param accountId - Bank account id
   * @param startRow - start row
   * @param numRows - number of rows
   * @return the received GetTransactionsResult that contains the transactions and
   * some other meta-information
   */
  public GetTransactionsResult loadTransactionsForAccount(long accountId, int startRow, int numRows)
  {
    GetTransactionsAction action = new GetTransactionsAction(null, accountId, null, null, null, true, startRow, numRows);
    return loadTransactions(action);
  }

  /**
   * Loads uncategorized transactions since given date and using pagination
   * @param startDate - Use {@link com.linxo.client.data.DateUtils#addDays(java.util.Date, int) DateUtils#addDays(new Date, -60)}
   *                  to load transactions from last 60 days
   * @param startRow - start row
   * @param numRows - number of rows
   * @param loadSubCategories - Uncategorized has several sub-categories : checks, not categorized, wire transfers, cash withdrawal. Defaults to true
   * @return the received GetTransactionsResult that contains the transactions and
   * some other meta-information
   */
  public GetTransactionsResult loadUncategorizedSince(LinxoDate startDate, int startRow, int numRows, Boolean loadSubCategories)
  {
    if(loadSubCategories == null)
    {
      loadSubCategories = true;
    }

    GetTransactionsAction action = new GetTransactionsAction(
        null, // accountType
        null, // accountId
        null, // accountReference
        null, // labels
        startDate.toString(), // startDate
        null, // endDate
        null, // debitDate
        null, // amount
        null, // minAmount
        null, // maxAmount
        false, // useBudgetDate
        0l, // CategoryId
        loadSubCategories, // loadSubCategories
        null, // CategoryType
        null, // tagId
        false, // onlyTaggedTransactions
        false, // onlyTrendsAccounts
        startRow,
        numRows,
        null);

    return loadTransactions(action);
  }

  public GetTransactionsResult loadTransactions(GetTransactionsAction action)
  {
    return loadTransactions(action, 0L);
  }

  public GetTransactionsResult loadTransactions(GetTransactionsAction action, Long userId)
  {
    try {
      GetTransactionsResult res = linxoClient.sendAction(action, userId);
      categoryService.fill(res.getTransactions());
      accountService.fill(res.getTransactions());

      if(logger.isTraceEnabled())
      {
        logger.trace(
            String.format(
                "Caught exception while getting transactions action[%1$s]=>result=%2$s",
                action, res));
      }

      return res;
    }
    catch(TechnicalException exc){
      logger.error(
          String.format(
              "Caught exception while getting transactions action[%1$s]", action));
      return null;
    }

  }

  public TransactionInfo loadTransaction(long transactionId)
  {
    try {
      GetTransactionResult res = linxoClient.sendAction(new GetTransactionAction(transactionId));
      TransactionInfo transaction = res.getTransaction();
      categoryService.fill(transaction);
      accountService.fill(transaction);
      return transaction;
    }
    catch (TechnicalException exc) {
      logger.error("Caught exception while loading transaction["+transactionId+"]", exc);
      return null;
    }
  }

  public int transactionCountForCategory(long categoryId)
  {
    GetTransactionsAction action = new GetTransactionsAction(
        null, // accountType
        null, // accountId
        null, // accountReference
        null, // labels
        null, // startDate
        null, // endDate
        null, // debitDate
        null, // amount
        null, // minAmount
        null, // maxAmount
        false, // useBudgetDate
        categoryId, // CategoryId
        true, // loadSubCategories
        null, // CategoryType
        null, // tagId
        false, // onlyTaggedTransactions
        false, // onlyTrendsAccounts
        0, // startRow
        0, // numRows
        null); // extCustomType

    return loadTransactions(action).getAvailableCount();
  }

  public UpdateTransactionResult updateTransaction(TransactionInfo txInfo)
  {
    return updateTransaction(new UpdateTransactionAction(txInfo));
  }

  public UpdateTransactionResult updateTransaction(UpdateTransactionAction action)
  {
    try {
      return linxoClient.sendAction(action);
    }
    catch(TechnicalException exc){
      logger.error("Caught Exception while updating transaction action["+action+"]");
      return null;
    }
  }

  public ArrayList<GetBalanceHistoryResult.BalanceAndBalanceDate> loadBalanceDates(final long bankAccountId,
                                                                                   final LinxoDate startLinxoDate,
                                                                                   final LinxoDate endLinxoDate)
  {
    GetBalanceHistoryAction action = new GetBalanceHistoryAction(bankAccountId, startLinxoDate, endLinxoDate);
    try {
      return linxoClient.sendAction(action).getBalanceAndBalanceDates();
    }
    catch(TechnicalException exc){
      logger.error("Caught Exception while collecting history action["+action+"]", exc);
      return null;
    }
  }
}
