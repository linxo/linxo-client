/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 14/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.actions.properties.GetUserPropertiesAction;
import com.linxo.client.actions.properties.GetUserPropertiesResult;
import com.linxo.client.actions.properties.UpdateUserPropertiesAction;
import com.linxo.client.net.LinxoClient;
import org.apache.log4j.Logger;

import java.util.HashMap;

public class UserPropertiesService
{

  // Nested Types (mixing inner and static classes is okay)

  /**
   * Class that groups several properties related to a given alert
   */
  public static class AlertProperties
  {
    private final UserPropertiesService service;
    private final String name;

    public AlertProperties(UserPropertiesService service, String name)
    {
      this.service = service;
      this.name = name;
    }

    public void setSendEmail(boolean sendEmail)
    {
      setLongPropertyAsBool(service, keyAlertEmail(name), sendEmail);
    }

    public boolean isSendEmail()
    {
      return getLongPropertyAsBool(service, keyAlertEmail(name), 0);
    }

    public void setSendPush(boolean sendPush)
    {
      setLongPropertyAsBool(service, keyAlertPush(name), sendPush);
    }

    public boolean isSendPush()
    {
      return getLongPropertyAsBool(service, keyAlertPush(name), 0);
    }

    public long getThreshold()
    {
      return getLongProperty(service, keyAlertThreshold(name), 0);
    }

    public void setThreshold(long threshold)
    {
      setLongProperty(service, keyAlertThreshold(name), threshold);
    }


    @Override
    public String toString()
    {
      return String.format("IsSendEmail=%1$s, IsSendPush=%2$s, Threshold=%3$s",
          isSendEmail(), isSendPush(), getThreshold());
    }
  }


  // Static Fields

  private static final Logger logger = Logger.getLogger(UserPropertiesService.class);


  private static final String LOW_BALANCE_ALERT_NAME = "low.balance";
  private static final String HIGH_SPENDING_ALERT_NAME = "high.spending";
  private static final String HIGH_DEPOSIT_ALERT_NAME = "high.deposit";
  private static final String BANK_FEE_ALERT_NAME = "bank.fee";

  private static final String KEY_EMAIL_SYNCHRONIZATION_PROBLEMS = "alerts.synchronization.problems.email.long";
  private static final String KEY_PUSH_SYNCHRONIZATION_PROBLEMS = "alerts.synchronization.problems.push.long";
  private static final String KEY_EMAIL_WEEKLY_REPORT = "reports.weekly.email.long";
  public  static final String KEY_EMAIL_MONTHLY_REPORT = "reports.monthly.email.long";
  public  static final String KEY_SHOW_AMOUNTS_IN_EMAIL = "option.show.amounts.email.long";
  public  static final String KEY_SHOW_AMOUNTS_IN_PUSH = "option.show.amounts.push.long";
  private static final String KEY_GROUP_ALERTS_IN_PUSH = "option.group.alerts.push.long";

  // Static Initializers

  // Static Methods

  private static void setLongPropertyAsBool(UserPropertiesService service, String key, boolean value)
  {
    setLongProperty(service, key, value ? 1 : 0);
  }

  private static void setLongProperty(UserPropertiesService service, String key, long value)
  {
    final String stringValue = String.valueOf(value);
    service.properties.put(key, stringValue);
    try {
      service.linxoClient.sendAction(new UpdateUserPropertiesAction(key, stringValue));
    }
    catch(Exception exc) {
      //todo check result status
      logger.error("Caught error while updating property ["+key+"]=>["+value+"]");
    }
  }

  private static boolean getLongPropertyAsBool(UserPropertiesService service, String key, long def)
  {
    return getLongProperty(service, key, def) == 1;
  }


  private static long getLongProperty(UserPropertiesService service, String key, long def)
  {
    try
    {
      if (!service.properties.containsKey(key)) {
        logger.warn(String.format("Did not find key=%1$s", key));
      }
      return service.properties.containsKey(key) ? Long.parseLong(service.properties.get(key)) : def;
    }
    catch (Exception exc)
    {
      logger.error(
          String.format("longProperty raised exc: key=%1$s, exc=%2$s ", key, exc.getMessage()), exc);
      return def;
    }
  }

  private static String key(String name, String k)
  {
    return "alerts." + name + "." + k +".long";
  }

  private static String keyAlertPush(String name)
  {
    return key(name, "push");
  }

  private static String keyAlertEmail(String name)
  {
    return key(name, "email");
  }

  private static String keyAlertThreshold(String name)
  {
    return key(name, "threshold");
  }


  // Instance Fields

  private HashMap<String, String> properties;

  public AlertProperties lowBalanceAlert;
  public AlertProperties highSpendingAlert;
  public AlertProperties highDepositAlert;
  public AlertProperties bankFeeAlert;

  // LinxoClient
  private LinxoClient linxoClient;


  // Instance Initializers

  // Constructors

  public UserPropertiesService()
  {
    properties = new HashMap<String, String>();
  }

  // Instance Methods

  ////////////////////////////////////////////////////////////////////
  //
  //                        Setters
  //
  ////////////////////////////////////////////////////////////////////

  public void setLinxoClient(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;
  }

  ////////////////////////////////////////////////////////////////////
  //
  //                        Public API
  //
  ////////////////////////////////////////////////////////////////////

  void initialize()
  {}

  /**
   * Load user properties
   */
  void load()
  {
    //todo update UI

    try {
      GetUserPropertiesResult res = linxoClient.sendAction(new GetUserPropertiesAction());
      if (res.getUserProperties() != null)
      {
        properties = res.getUserProperties();
        lowBalanceAlert = new AlertProperties(this, LOW_BALANCE_ALERT_NAME);
        highDepositAlert = new AlertProperties(this, HIGH_DEPOSIT_ALERT_NAME);
        highSpendingAlert = new AlertProperties(this, HIGH_SPENDING_ALERT_NAME);
        bankFeeAlert = new AlertProperties(this, BANK_FEE_ALERT_NAME);
      }
    }
    catch(Exception exc) {
      logger.error("Caught error while collecting properties", exc);
      return;
    }

    //todo update UI
  }


  void clear()
  {
    properties.clear();
  }


  public boolean getEmailSynchronizationProblems()
  {
    return getLongProperty(this, KEY_EMAIL_SYNCHRONIZATION_PROBLEMS, 0) == 1;
  }

  public void setEmailSynchronizationProblems(boolean value)
  {
    setLongPropertyAsBool(this, KEY_EMAIL_SYNCHRONIZATION_PROBLEMS, value);
  }

  public boolean getPushSynchronizationProblems()
  {
    return getLongProperty(this, KEY_PUSH_SYNCHRONIZATION_PROBLEMS, 0) == 1;
  }

  public void setPushSynchronizationProblems(boolean value)
  {
    setLongPropertyAsBool(this, KEY_PUSH_SYNCHRONIZATION_PROBLEMS, value);
  }

  public boolean getEmailWeeklyReport()
  {
    return getLongProperty(this, KEY_EMAIL_WEEKLY_REPORT, 0) == 1;
  }

  public void setEmailWeeklyReport(boolean value)
  {
    setLongPropertyAsBool(this, KEY_EMAIL_WEEKLY_REPORT, value);
  }

  public boolean getEmailMonthlyReport()
  {
    return getLongProperty(this, KEY_EMAIL_MONTHLY_REPORT, 0) == 1;
  }

  public void setEmailMonthlyReport(boolean value)
  {
    setLongPropertyAsBool(this, KEY_EMAIL_MONTHLY_REPORT, value);
  }

  public boolean getShowAmountsInEmail()
  {
    return getLongProperty(this, KEY_SHOW_AMOUNTS_IN_EMAIL, 0) == 1;
  }

  public void setShowAmountsInEmail(boolean value)
  {
    setLongPropertyAsBool(this, KEY_SHOW_AMOUNTS_IN_EMAIL, value);
  }

  public boolean getShowAmountsInPush()
  {
    return getLongProperty(this, KEY_SHOW_AMOUNTS_IN_PUSH, 0) == 1;
  }

  public void setShowAmountsInPush(boolean value)
  {
    setLongPropertyAsBool(this, KEY_SHOW_AMOUNTS_IN_PUSH, value);
  }

  public boolean getGroupAlertsInPush()
  {
    return getLongProperty(this, KEY_GROUP_ALERTS_IN_PUSH, 0) == 1;
  }

  public void setGroupAlertsInPush(boolean value)
  {
    setLongPropertyAsBool(this, KEY_GROUP_ALERTS_IN_PUSH, value);
  }

}
