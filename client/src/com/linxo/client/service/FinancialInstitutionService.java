/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/10/2014 by hugues.
 */
package com.linxo.client.service;

import com.linxo.client.actions.pfm.sync.GetFinancialInstitutionsAction;
import com.linxo.client.actions.pfm.sync.GetFinancialInstitutionsResult;
import com.linxo.client.data.StringUtils;
import com.linxo.client.dto.sync.FinancialInstitutionInfo;
import com.linxo.client.dto.sync.Key;
import com.linxo.client.net.LinxoClient;
import com.linxo.infrastructure.exceptions.TechnicalException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class FinancialInstitutionService
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(FinancialInstitutionService.class);

  // Static Initializers

  // Static Methods

  // Instance Fields

  public LinxoClient linxoClient;

  public ArrayList<FinancialInstitutionInfo> financialInstitutions;
  public ArrayList<FinancialInstitutionInfo> synchronizedFinancialInstitutions;
  public ArrayList<FinancialInstitutionInfo> mainFinancialInstitutions;

  private HashMap<Long, FinancialInstitutionInfo> financialInstitutionMap = new HashMap<Long, FinancialInstitutionInfo>();

  private final Object lock = new Object();

  // Instance Initializers

  // Constructors

  public FinancialInstitutionService()
  {
    financialInstitutions = new ArrayList<FinancialInstitutionInfo>();
    synchronizedFinancialInstitutions = new ArrayList<FinancialInstitutionInfo>();
    mainFinancialInstitutions = new ArrayList<FinancialInstitutionInfo>();
  }


  // Instance Methods

  ////////////////////////////////////////////////////////////////////
  //
  //                        Setters
  //
  ////////////////////////////////////////////////////////////////////

  public void setLinxoClient(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;
  }

  ////////////////////////////////////////////////////////////////////
  //
  //                        Public API
  //
  ////////////////////////////////////////////////////////////////////

  void load()
  {
    try {
      GetFinancialInstitutionsResult res = linxoClient.sendAction(new GetFinancialInstitutionsAction());
      synchronized (lock) {
        update(res.getFinancialInstitutions());
      }
    }
    catch(TechnicalException lce) {
      logger.error("Could not load financial institutions");
    }
  }


  void clear()
  {
    synchronized (lock)
    {
      financialInstitutionMap.clear();
      financialInstitutions.clear();
      synchronizedFinancialInstitutions.clear();
    }
  }

  void initialize()
  {}

  private void update(ArrayList<FinancialInstitutionInfo> financialInstitutions)
  {
    clear();

    this.financialInstitutions = financialInstitutions;
    synchronizedFinancialInstitutions = new ArrayList<FinancialInstitutionInfo>();
    for(FinancialInstitutionInfo financialInstitution : financialInstitutions)
    {
      financialInstitutionMap.put(financialInstitution.getId(), financialInstitution);
      if (financialInstitution.isSync() && financialInstitution.isShow())
      {
        synchronizedFinancialInstitutions.add(financialInstitution);
      }
    }

    // Sort banks
    Collections.sort(synchronizedFinancialInstitutions,
        new Comparator<FinancialInstitutionInfo>() {
          @Override
          public int compare(FinancialInstitutionInfo that, FinancialInstitutionInfo other)
          {
            if(StringUtils.equals(that.getName(), other.getName())) {

              if(that.getBranchName() == null) {
                if (other.getBranchName() == null) return 0;
                else return 1;
              }
              else {
                if (other.getBranchName() == null) return -1;
                else return that.getBranchName().compareTo(other.getBranchName());
              }
            }

            return that.getName().compareTo(other.getName());
          }
        });

    buildMainList();
  }

  private void buildMainList()
  {
    mainFinancialInstitutions.clear();

    mainFinancialInstitutions.add(new FinancialInstitutionInfo("Crédit Agricole",-1l));
    addIfExists(82); // BNP Paribas
    addIfExists(1); // Caisse d'Epargne
    addIfExists(44); // La Banque Postale
    addIfExists(42); // Societe Generale
    mainFinancialInstitutions.add(new FinancialInstitutionInfo("Banque Populaire", -1l));
    mainFinancialInstitutions.add(new FinancialInstitutionInfo("Crédit Mutuel", -1l));
    addIfExists(63); // CIC
    mainFinancialInstitutions.add(new FinancialInstitutionInfo("LCL", -1l));
    addIfExists(86); // American Express
  }

  private void addIfExists(long fid)
  {
    if (financialInstitutionMap.containsKey(fid))
    {
      mainFinancialInstitutions.add(financialInstitutionMap.get(fid));
    }
  }


  public FinancialInstitutionInfo get(long financialInstitutionId)
  {
    return financialInstitutionMap.containsKey(financialInstitutionId)
        ? financialInstitutionMap.get(financialInstitutionId)
        : null;
  }

  /**
   * Get the credential keys for the given financial institution.
   * Keys for challenge/response are filtered out.
   *
   * @param financialInstitutionId -
   * @return null if financialInstitutionId is not found
   */
  public ArrayList<Key> credentialKeys(long financialInstitutionId)
  {
    FinancialInstitutionInfo fi = get(financialInstitutionId);
    if (fi == null) return null;

    ArrayList<Key> res = new ArrayList<Key>();
    for(Key key: fi.getKeys())
    {
      if (key.getKey().startsWith("credentials"))
      {
        res.add(key);
      }
    }
    return res;
  }

}
