/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/05/2014 by hugues.
 */
package com.linxo.client.actions.social;

import com.linxo.client.data.sponsor.SocialProvider;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.dto.user.SocialUserInfo;

public abstract class AbstractSocialAction extends SecuredAction<SocialServiceResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private SocialProvider provider;
  private SocialUserInfo socialUserInfo;


  // Instance Initializers

  // Constructors

  protected AbstractSocialAction()
  {}

  protected AbstractSocialAction(SocialProvider provider, SocialUserInfo socialUserInfo)
  {
    this.provider = provider;
    this.socialUserInfo = socialUserInfo;
  }


  // Instance Methods


  public SocialProvider getProvider()
  {
    return provider;
  }

  public SocialUserInfo getSocialUserInfo()
  {
    return socialUserInfo;
  }
}
