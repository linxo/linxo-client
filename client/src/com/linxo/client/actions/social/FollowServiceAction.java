/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 3:54:02 PM by tarunmalhotra.
*/
package com.linxo.client.actions.social;

import com.linxo.client.data.sponsor.SocialProvider;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.user.SocialUserInfo;

/**
 * Requests the server to send a follow/like whatever action for the currently logged in User.
 *
 * If the server does not have any connection to the given service, it expects a not-null
 * SocialUserInfo.
 *
 * If no SocialUser is provided when needed,or if the stored
 * SocialUSer is no more authorized to perform the action, it returns an error.
 *
 */
@GwtAction
@JsonAction
@SuppressWarnings({"DeserializableClassInSecureContext"})
public final class FollowServiceAction extends AbstractSocialAction
{
  @SuppressWarnings("unused")
  protected FollowServiceAction()
  {
    super();
  }

  public FollowServiceAction(SocialProvider provider, SocialUserInfo socialUserInfo)
  {
    super(provider, socialUserInfo);
  }
}
