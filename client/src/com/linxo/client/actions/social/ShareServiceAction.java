/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 3:54:02 PM by tarunmalhotra.
*/
package com.linxo.client.actions.social;

import com.linxo.client.data.sponsor.SocialProvider;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.user.SocialUserInfo;

@GwtAction
@JsonAction
@SuppressWarnings({"DeserializableClassInSecureContext"})
public final class ShareServiceAction extends AbstractSocialAction
{
  @SuppressWarnings("unused")
  protected ShareServiceAction()
  {
    super();
  }

  public ShareServiceAction(SocialProvider provider, SocialUserInfo socialUserInfo)
  {
    super(provider, socialUserInfo);
  }
}
