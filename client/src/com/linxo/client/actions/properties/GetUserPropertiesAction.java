/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.properties;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public final class GetUserPropertiesAction
    extends SecuredAction<GetUserPropertiesResult>
{
  public GetUserPropertiesAction()
  {
  }

}
