/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.properties;

import com.linxo.client.actions.SecuredResult;

import java.util.HashMap;

public final class GetUserPropertiesResult
    extends SecuredResult
{
  private HashMap<String, String> userProperties;

  @SuppressWarnings("unused")
  private GetUserPropertiesResult()
  {
  }

  public GetUserPropertiesResult(final HashMap<String, String> userProperties)
  {
    this.userProperties = userProperties;
  }

  public HashMap<String, String> getUserProperties()
  {
    return userProperties;
  }

}
