/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.properties;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.HashMap;
import java.util.List;

@GwtAction
@JsonAction
public final class UpdateUserPropertiesAction
    extends SecuredAction<UpdateUserPropertiesResult>
{
  private HashMap<String, String> propertiesToUpdate;

  private List<String> propertiesToDelete;

  @SuppressWarnings("unused")
  private UpdateUserPropertiesAction()
  {
  }

  public UpdateUserPropertiesAction(final HashMap<String, String> propertiesToUpdate,
                                    final List<String> propertiesToDelete)
  {
    this.propertiesToUpdate = propertiesToUpdate;
    this.propertiesToDelete = propertiesToDelete;
  }

  /**
   * Creates an action to update only the given property with the given value
   * @param key the key to add to the properties
   * @param value the value associated to the new key
   */
  public UpdateUserPropertiesAction(String key, String value)
  {
    propertiesToUpdate = new HashMap<String, String>(1);
    propertiesToUpdate.put(key, value);

  }

  public HashMap<String, String> getPropertiesToUpdate()
  {
    return propertiesToUpdate;
  }

  public List<String> getPropertiesToDelete()
  {
    return propertiesToDelete;
  }

}
