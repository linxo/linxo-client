/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.properties;

import com.linxo.client.actions.SecuredResult;

public final class UpdateUserPropertiesResult
    extends SecuredResult
{
  public enum Status {
    Failure,
    Success,
  }

  private Status status;

  @SuppressWarnings("unused")
  private UpdateUserPropertiesResult()
  {
  }

  public UpdateUserPropertiesResult(final Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

}
