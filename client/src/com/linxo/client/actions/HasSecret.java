/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 30/09/2014 by hugues.
 */
package com.linxo.client.actions;

public abstract class HasSecret
{
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  public static final String SECRET = "secret";

  // Instance Methods


  private String secret;

  public String getSharedSecret(){return secret;}
  public void setSharedSecret(String secret){this.secret = secret;}

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("HasSecret{");
    sb.append("secret='").append(secret).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
