/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 18/07/2013 by hugues.
 */
package com.linxo.client.actions.pfm.events;


import com.linxo.client.dto.user.UserProfileInfo;

/**
 * This event is broadcasted when the email verification has occured and all the clients have
 * to be updated.
 */
public final class UserInfoUpdatedEvent
  extends AbstractSyncEvent<UserInfoUpdatedEventHandler>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private UserProfileInfo userProfileInfo;

  // Instance Initializers

  // Constructors


  @SuppressWarnings({"unused"}) // s11n
  public UserInfoUpdatedEvent()
  {}

  public UserInfoUpdatedEvent(UserProfileInfo userProfileInfo)
  {
    this.userProfileInfo = userProfileInfo;
  }

  // Instance Methods

  public UserProfileInfo getUserProfile()
  {
    return userProfileInfo;
  }

  @Override
  protected void dispatch(UserInfoUpdatedEventHandler handler)
  {
    handler.onUserInfoUpdate(this);
  }
}
