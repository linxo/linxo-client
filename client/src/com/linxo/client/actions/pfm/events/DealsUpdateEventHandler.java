package com.linxo.client.actions.pfm.events;

import com.linxo.client.dto.events.EventHandler;

/**
 *
 */
public interface DealsUpdateEventHandler
  extends EventHandler
{
  void onDeal(DealsUpdateEvent evt);
}
