/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.pfm.events;


import com.linxo.client.data.support.Version;
import com.linxo.client.dto.user.PermissionInfo;

import java.util.HashSet;

public final class PermissionsUpdateEvent
    extends AbstractSyncEvent<PermissionsUpdateEventHandler>
{
  HashSet<PermissionInfo> permissionInfoSet = new HashSet<PermissionInfo>();
  private boolean reloadWeb;

  @Override
  protected void dispatch(final PermissionsUpdateEventHandler handler)
  {
    handler.onPermission(this);
  }

  public HashSet<PermissionInfo> getPermissionInfoSet() {
    return permissionInfoSet;
  }

  public void setPermissionInfoSet(HashSet<PermissionInfo> permissionInfoSet) {
    this.permissionInfoSet = permissionInfoSet;
  }

  public boolean isReloadWeb()
  {
    return reloadWeb;
  }

  public void setReloadWeb(boolean reloadWeb)
  {
    this.reloadWeb = reloadWeb;
  }

  @Override
  public AbstractSyncEvent prepareFor(Version version) {
    if (!version.hasDealEvents()) {
      return null;
    }
    return this;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("PermissionsUpdateEvent");
    sb.append("{permissionInfoSet=").append(permissionInfoSet);
    sb.append(", eventNumber=").append(eventNumber);
    sb.append('}');
    return sb.toString();
  }
}
