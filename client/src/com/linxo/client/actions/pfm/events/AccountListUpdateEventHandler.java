package com.linxo.client.actions.pfm.events;

import com.linxo.client.dto.events.EventHandler;

/**
 *
 */
public interface AccountListUpdateEventHandler
  extends EventHandler
{
  void onAccountListUpdate(AccountListUpdateEvent evt);
}
