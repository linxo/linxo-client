package com.linxo.client.actions.pfm.events;

import com.linxo.client.dto.events.EventHandler;

/**
 *
 */
public interface SynchronizationUpdateEventHandler
  extends EventHandler
{
  void onSynchronizationUpdate(SynchronizationUpdateEvent evt);
}
