package com.linxo.client.actions.pfm.events;


import com.linxo.client.dto.events.EventHandler;

/**
 *
 */
public interface PermissionsUpdateEventHandler
  extends EventHandler
{
  void onPermission(PermissionsUpdateEvent evt);
}
