package com.linxo.client.actions.pfm.events;

import com.linxo.client.data.support.Version;

/**
 *
 */
public interface SyncEvent
{
  /**
   * Prepare this event for being transmitted to the given version
   *
   * Note: if the event must be modified for a particular version,
   * it is important that a new object is created as the same event and
   * this event is left untouched as it could be used for several clients.
   *
   * Note 2: make sure that the eventNumber is preserved when creating the
   * new event bean as the event number of checked when the events are passed
   * to the clients.
   * 
   * @param version the version of the recipient for this event
   * @return an event compatible for the given version. If the returned value is
   * <code>null</code>, this event must not be transmitted.
   */
  public SyncEvent prepareFor(Version version);
  long getEventNumber();
  void setEventNumber(final long eventNumber);
}
