/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.pfm.events;

import com.linxo.client.data.support.Version;
import com.linxo.client.dto.user.DealInfo;

public final class DealsUpdateEvent
    extends AbstractSyncEvent<DealsUpdateEventHandler>
{
  private DealInfo dealInfo;
  private boolean reloadWeb;

  @Override
  protected final void dispatch(final DealsUpdateEventHandler handler)
  {
    handler.onDeal(this);
  }

  public final DealInfo getDealInfo()
  {
    return dealInfo;
  }

  public final void setDealInfo(final DealInfo dealInfo)
  {
    this.dealInfo = dealInfo;
  }

  public boolean isReloadWeb()
  {
    return reloadWeb;
  }

  public void setReloadWeb(boolean reloadWeb)
  {
    this.reloadWeb = reloadWeb;
  }

  @Override
  public AbstractSyncEvent prepareFor(Version version) {
    if (!version.hasDealEvents()) {
      return null;
    }
    return this;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    return new StringBuilder()
        .append("DealsUpdateEvent")
        .append("{dealInfo=").append(dealInfo)
        .append(", eventNumber=").append(eventNumber)
        .append('}')
        .toString();
  }

}
