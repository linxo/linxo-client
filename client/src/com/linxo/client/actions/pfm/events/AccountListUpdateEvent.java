package com.linxo.client.actions.pfm.events;

import com.linxo.client.data.support.Version;
import com.linxo.client.dto.account.OperationUpdateInfo;
import com.linxo.client.dto.account.ProviderAccountInfo;

import java.util.ArrayList;

/**
 *
 */
public final class AccountListUpdateEvent
  extends AbstractSyncEvent<AccountListUpdateEventHandler>
{

  public enum UpdateType
  {
    Update,
    Success,
    Failure
  }

  public enum ErrorType
  {
    Error,
    AuthError,
    InvalidCredentials,
    TooManyAttempts,
    ServiceUnavailable,
    PasswordChangeRequired,
    UserActionRequired,
    ChallengeRequired,      // failed because a challenge/response is required and the user was not assisting the process
    ChallengeTimedOut,      // failed because a challenge/response is required and the user did not answer in the alloted time
    ChallengeFailed,        // failed because a challenge/response is required and the user responded incorrectly
    ChallengeCancelled      // failed because a challenge/response is required and the user cancelled the process
  }

  private long accountGroupId;
  private UpdateType updateType;
  private ErrorType errorType;
  private OperationUpdateInfo operationUpdate;
  private ArrayList<ProviderAccountInfo> providerAccounts;

  @Override
  protected void dispatch(final AccountListUpdateEventHandler handler)
  {
    handler.onAccountListUpdate(this);
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  public void setAccountGroupId(final long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
  }

  public UpdateType getUpdateType()
  {
    return updateType;
  }

  public void setUpdateType(final UpdateType updateType)
  {
    this.updateType = updateType;
  }

  public OperationUpdateInfo getOperationUpdate()
  {
    return operationUpdate;
  }

  public void setOperationUpdate(final OperationUpdateInfo operationUpdate)
  {
    this.operationUpdate = operationUpdate;
  }

  public ArrayList<ProviderAccountInfo> getProviderAccounts()
  {
    return providerAccounts;
  }

  public void setProviderAccounts(final ArrayList<ProviderAccountInfo> providerAccounts)
  {
    this.providerAccounts = providerAccounts;
  }

  public ErrorType getErrorType()
  {
    return errorType;
  }

  public void setErrorType(final ErrorType errorType)
  {
    this.errorType = errorType;
  }

  /**
   * default implementation that returns this object
   */
  @Override
  public AccountListUpdateEvent prepareFor(Version version)
  {
    if(version.hasChallenge()) return this;

    if(updateType == UpdateType.Failure){

      if(errorType == ErrorType.ChallengeCancelled ||
         errorType == ErrorType.ChallengeFailed    ||
         errorType == ErrorType.ChallengeTimedOut  ||
         errorType == ErrorType.ChallengeRequired ) // This last case can appear during the review of account list
      {
        AccountListUpdateEvent result = new AccountListUpdateEvent();
        result.accountGroupId = accountGroupId;
        result.eventNumber = eventNumber;
        result.updateType = updateType;
        result.errorType = ErrorType.Error;
        result.operationUpdate = operationUpdate;
        result.providerAccounts = providerAccounts;
        return result;
      }
    }

    if(updateType == UpdateType.Update &&
       operationUpdate.getType() == OperationUpdateInfo.Type.ChallengeRequested)
    {
      return null;
    }

    return this;
  }


  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("AccountListUpdateEvent");
    sb.append("{accountGroupId=").append(accountGroupId);
    sb.append(", updateType=").append(updateType);
    sb.append(", eventNumber=").append(eventNumber);
    sb.append(", errorType=").append(errorType);
    sb.append(", operationUpdate=").append(operationUpdate);
    sb.append(", providerAccounts=").append(providerAccounts);
    sb.append('}');
    return sb.toString();
  }
}
