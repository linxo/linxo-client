package com.linxo.client.actions.pfm.events;


import com.linxo.client.data.support.Version;
import com.linxo.client.dto.account.OperationUpdateInfo;

/**
 *
 */
public final class SynchronizationUpdateEvent
  extends AbstractSyncEvent<SynchronizationUpdateEventHandler>
{
  public enum UpdateType
  {
    Update,
    Success,
    Failure
  }

  private long accountGroupId;
  private UpdateType updateType;
  private OperationUpdateInfo operationUpdate;

  @Override
  protected void dispatch(final SynchronizationUpdateEventHandler handler)
  {
    handler.onSynchronizationUpdate(this);
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  public void setAccountGroupId(final long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
  }

  public UpdateType getUpdateType()
  {
    return updateType;
  }

  public void setUpdateType(final UpdateType updateType)
  {
    this.updateType = updateType;
  }

  public OperationUpdateInfo getOperationUpdate()
  {
    return operationUpdate;
  }

  public void setOperationUpdate(final OperationUpdateInfo operationUpdate)
  {
    this.operationUpdate = operationUpdate;
  }

  /**
   * Filter events related to the Challenge* state
   */
  @Override
  public AbstractSyncEvent prepareFor(Version version)
  {
    if (updateType == UpdateType.Update
        && operationUpdate.getType() == OperationUpdateInfo.Type.ChallengeRequested
        && !version.hasChallenge()) {
      return null;
    }

    if (updateType == UpdateType.Update
        && operationUpdate.getType() == OperationUpdateInfo.Type.NeedsSecret
        && !version.hasSemiAuto()) {
      return null;
    }

    return this;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("SynchronizationUpdateEvent");
    sb.append("{accountGroupId=").append(accountGroupId);
    sb.append(", updateType=").append(updateType);
    sb.append(", eventNumber=").append(eventNumber);
    sb.append(", operationUpdate=").append(operationUpdate);
    sb.append('}');
    return sb.toString();
  }
}
