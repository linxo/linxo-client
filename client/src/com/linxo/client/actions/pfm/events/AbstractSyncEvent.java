package com.linxo.client.actions.pfm.events;

import com.linxo.client.data.support.Version;
import com.linxo.client.dto.events.Event;
import com.linxo.client.dto.events.EventHandler;

/**
 *
 */
public abstract class AbstractSyncEvent<H extends EventHandler>
  extends Event<H>
  implements SyncEvent
{
  protected long eventNumber;


  /**
   * default implementation that returns this object
   */
  @SuppressWarnings({ "DesignForExtension" })
  @Override
  public AbstractSyncEvent prepareFor(Version version)
  {
    return this;
  }

  @Override
  public final long getEventNumber()
  {
    return eventNumber;
  }

  @Override
  public final void setEventNumber(final long eventNumber)
  {
    this.eventNumber = eventNumber;
  }

}
