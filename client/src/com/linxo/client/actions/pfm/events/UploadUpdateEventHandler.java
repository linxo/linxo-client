package com.linxo.client.actions.pfm.events;


import com.linxo.client.dto.events.EventHandler;

/**
 *
 */
public interface UploadUpdateEventHandler
  extends EventHandler
{
  void onUploadUpdate(UploadUpdateEvent evt);
}
