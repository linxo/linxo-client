package com.linxo.client.actions.pfm.events;

import com.linxo.client.data.support.Version;
import com.linxo.client.dto.tx.TransactionInfo;

import java.util.List;

/**
 *
 */
public final class UploadUpdateEvent
  extends AbstractSyncEvent<UploadUpdateEventHandler>
{
  private long accountId;
  private List<TransactionInfo> transactions;

  @Override
  protected void dispatch(final UploadUpdateEventHandler handler)
  {
    handler.onUploadUpdate(this);
  }

  public void setTransactions(final List<TransactionInfo> transactions)
  {
    this.transactions = transactions;
  }

  public List<TransactionInfo> getTransactions()
  {
    return transactions;
  }

  public long getAccountId()
  {
    return accountId;
  }

  public void setAccountId(final long accountId)
  {
    this.accountId = accountId;
  }

  /**
   * returns null for version that do not Version.hasManualAccount
   */
  @Override
  public AbstractSyncEvent prepareFor(Version version)
  {
    return version.hasManualAccount() ? this : null;
  }

  @Override
  public String toString()
  {
    return new StringBuffer()
        .append("UploadUpdateEvent")
        .append("{accountId=").append(accountId)
        .append(", transactions=").append(transactions == null ? "null" : transactions.size())
        .append(", eventNumber=").append(eventNumber)
        .append('}')
        .toString();
  }

}
