package com.linxo.client.actions.pfm.events;

import com.linxo.client.dto.events.EventHandler;

public interface OauthTokenEventHandler
  extends EventHandler
{
  public void onSuccess(OauthTokenEvent evt);
}
