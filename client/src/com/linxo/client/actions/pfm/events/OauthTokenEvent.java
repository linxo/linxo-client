package com.linxo.client.actions.pfm.events;

/*
  Copyright (c) 2008-2010 Linxo, All Rights Reserved.

  COPYRIGHT:
       This software is the property of Linxo.
       It cannot be copied, used, or modified without obtaining an
       authorization from the authors or a person mandated by Linxo.
       If such an authorization is provided, any modified version
       or copy of the software has to contain this header.

  WARRANTIES:
       This software is made available by the authors in the hope
       that it will be useful, but without any warranty.
       Linxo is not liable for any consequence related to
       the use of the provided software.
  </pre>
   
   
  Created on : 5/28/13 by christophe.
*/
public final class OauthTokenEvent
    extends AbstractSyncEvent<OauthTokenEventHandler>
{

  public enum Status
  {
    Success,
    Cancelled,
    Error,
  }

  private Status status;

  // because it's serializable
  @SuppressWarnings("UnusedDeclaration")
  private OauthTokenEvent() {}

  public OauthTokenEvent(final Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

  @Override
  protected void dispatch(final OauthTokenEventHandler handler)
  {
    handler.onSuccess(this);
  }

  @Override
  public String toString()
  {
    return "OauthTokenEvent";
  }

}
