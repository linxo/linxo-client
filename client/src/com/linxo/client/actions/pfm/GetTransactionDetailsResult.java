/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.tx.DetailedTransactionInfo;

/**
 *
 */
@Deprecated
public final class GetTransactionDetailsResult
  extends SecuredResult
{
  private DetailedTransactionInfo dti;

  @SuppressWarnings("unused")
  private GetTransactionDetailsResult() {}

  public GetTransactionDetailsResult(final DetailedTransactionInfo dti)
  {
    this.dti = dti;
  }

  public DetailedTransactionInfo getDti()
  {
    return dti;
  }

}
