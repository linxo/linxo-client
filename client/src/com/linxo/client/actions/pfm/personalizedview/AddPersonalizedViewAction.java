/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.pfm.personalizedview;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.account.PersonalizedView;

/**
 * An action to add a {@link PersonalizedView}.
 */
@GwtAction
@JsonAction
public class AddPersonalizedViewAction
    extends SecuredAction<AddPersonalizedViewResult>
{
  /**
   * The {@link PersonalizedView} to add. It should not have an id yet.
   */
  private PersonalizedView personalizedView;


  @SuppressWarnings("unused") // s11n
  private AddPersonalizedViewAction() {}

  public AddPersonalizedViewAction(PersonalizedView personalizedView)
  {
    this.personalizedView = personalizedView;
  }


  public PersonalizedView getPersonalizedView()
  {
    return personalizedView;
  }

}
