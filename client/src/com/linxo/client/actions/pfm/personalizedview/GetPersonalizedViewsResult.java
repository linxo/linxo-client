/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.pfm.personalizedview;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.account.PersonalizedView;

import java.util.List;

/**
 * LinxoResult for the {@link GetPersonalizedViewsAction}.
 */
public class GetPersonalizedViewsResult
    extends SecuredResult
{
  /**
   * All the user's {@link PersonalizedView}.
   */
  private List<PersonalizedView> personalizedViews;


  @SuppressWarnings("unused") // s11n
  private GetPersonalizedViewsResult() {}

  public GetPersonalizedViewsResult(List<PersonalizedView> personalizedViews)
  {
    this.personalizedViews = personalizedViews;
  }


  public List<PersonalizedView> getPersonalizedViews()
  {
    return personalizedViews;
  }

}
