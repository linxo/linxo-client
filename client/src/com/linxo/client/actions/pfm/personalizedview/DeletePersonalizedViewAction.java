/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.pfm.personalizedview;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * An action to delete a {@link com.linxo.client.dto.account.PersonalizedView}
 */
@GwtAction
@JsonAction
public class DeletePersonalizedViewAction
    extends SecuredAction<DeletePersonalizedViewResult>
{
  /**
   * The id of the view to delete.
   */
  private long personalizedViewId;

  /**
   * The id of the view which will receive the accounts
   * currently associated with the view being deleted.
   */
  private long destinationViewId;


  @SuppressWarnings("unused") // s11n
  private DeletePersonalizedViewAction() {}

  public DeletePersonalizedViewAction(long personalizedViewId, long destinationViewId)
  {
    this.personalizedViewId = personalizedViewId;
    this.destinationViewId = destinationViewId;
  }


  public long getPersonalizedViewId()
  {
    return personalizedViewId;
  }

  public long getDestinationViewId()
  {
    return destinationViewId;
  }

}
