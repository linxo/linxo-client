/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.pfm.personalizedview;

import com.linxo.client.data.LinxoColor;
import com.linxo.client.actions.SecuredResult;

import java.util.List;

/**
 * LinxoResult for the {@link GetPersonalizedViewsColorsAction}.
 */
public class GetPersonalizedViewsColorsResult
    extends SecuredResult
{
  /**
   * The colors that can be used in a
   * {@link com.linxo.client.dto.account.PersonalizedView}.
   */
  private List<LinxoColor> colors;

  /**
   * The colors to use when all the views are selected.
   */
  private LinxoColor allViewsColor;


  @SuppressWarnings("unused") // s11n
  private GetPersonalizedViewsColorsResult() {}

  public GetPersonalizedViewsColorsResult(List<LinxoColor> colors, LinxoColor allViewsColor)
  {
    this.colors = colors;
    this.allViewsColor = allViewsColor;
  }


  public List<LinxoColor> getColors()
  {
    return this.colors;
  }

  public LinxoColor getAllViewsColor()
  {
    return allViewsColor;
  }

}
