/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.pfm.personalizedview;

import com.linxo.client.actions.SecuredResult;

/**
 * LinxoResult for the {@link DeletePersonalizedViewAction}.
 */
public class DeletePersonalizedViewResult
    extends SecuredResult
{
  public enum Status
  {
    Success,
    Error
  }

  /**
   * Whether the action was handled successfully.
   */
  private Status status;


  @SuppressWarnings("unused") // s11n
  public DeletePersonalizedViewResult() {}

  public DeletePersonalizedViewResult(Status status)
  {
    this.status = status;
  }


  public Status getStatus()
  {
    return status;
  }

}
