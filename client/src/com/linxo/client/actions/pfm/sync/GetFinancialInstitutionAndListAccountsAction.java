/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.ArrayList;

/**
 * This action should be used to review the account list on an existing group
 * (add new accounts, delete some, etc).
 *
 * Its parameters are :
 *  - a group id: id of the group we want to review the account list,
 *  - a list of Credential, for semi-auto groups only, containing the
 *    encrypted secret(s) for the group. It should be null for fully
 *    auto groups.
 *
 * When the server receives this action:
 *  - a "ListAccounts" job starts if the group is not semi-auto or if it is
 *    but all the required secrets were in the action. Clients should listen
 *    to AccountListUpdateEvent events to get the updates for this job. The
 *    final update will contain the list of accounts found on the bank web
 *    site, or an error status in case of problem.
 *    The result will contain the FinancialInstitutionInfo for the group
 *    we're reviewing the account list.
 *  - If the group is semi-auto and a required secret is missing, no job is
 *    started. The result will contain the FinancialInstitutionInfo for the
 *    group we want to review the account list, plus a "needsSecret" boolean
 *    set to true and a "publicKey" that should be used to encrypt the missing
 *    secret(s). The same action should then be sent, but with the encrypted
 *    secret(s).
 */
@GwtAction
@JsonAction
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetFinancialInstitutionAndListAccountsAction
  extends SecuredAction<GetFinancialInstitutionAndListAccountsResult>
{
  public static final String ACCOUNT_GROUP_ID = "accountGroupId";
  public static final String SECRETS = "secrets";

  private long accountGroupId;

  private ArrayList<Credential> secrets;

  @SuppressWarnings("unused")
  private GetFinancialInstitutionAndListAccountsAction() {}

  public GetFinancialInstitutionAndListAccountsAction(final long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
  }

  public GetFinancialInstitutionAndListAccountsAction(final long accountGroupId,
                                                      final ArrayList<Credential> secrets)
  {
    this.accountGroupId = accountGroupId;
    this.secrets = secrets;
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  public ArrayList<Credential> getSecrets()
  {
    return secrets;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("GetFinancialInstitutionAndListAccountsAction");
    sb.append("{accountGroupId=").append(accountGroupId);
    sb.append("{secrets=").append(secrets == null ? "null" : secrets.size());
    sb.append('}');
    return sb.toString();
  }

}
