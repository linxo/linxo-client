/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 19/07/2010 by hugues.
 */
package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action returns the credential keys for the financial institution.
 * It also returns the pgp rsa public key that must be used to encrypt the
 * credentials that contain the string 'secret'.
 *
 * This action must be performed with a logged in user, as the pgp rsa key
 * pair is stored in the session. The key can only be used once, for the given
 * financial institution id.
 */
@GwtAction
@JsonAction
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetFinancialInstitutionAction
    extends SecuredAction<GetFinancialInstitutionResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private long financialInstitutionId;

  // Instance Initializers

  // Constructors
  @SuppressWarnings("unused")
  private GetFinancialInstitutionAction()
  {}

  public GetFinancialInstitutionAction(long financialInstitutionId)
  {
    this.financialInstitutionId = financialInstitutionId;
  }

  // Instance Methods

  public long getFinancialInstitutionId()
  {
    return financialInstitutionId;
  }
}
