/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.HashMap;

@GwtAction
@JsonAction
public final class SaveOAuthAction
    extends SecuredAction<SaveOAuthResult>
{
  private long financialInstitutionId;
  private Long groupId;
  private HashMap<String, String> parameters;
  private String fragment;

  @SuppressWarnings("unused") // s11n
  private SaveOAuthAction() {}

  public SaveOAuthAction(final long financialInstitutionId,
                         final Long groupId,
                         final HashMap<String, String> parameters,
                         final String fragment)
  {
    this.financialInstitutionId = financialInstitutionId;
    this.groupId = groupId;
    this.parameters = parameters;
    this.fragment = fragment;
  }

  public final long getFinancialInstitutionId()
  {
    return financialInstitutionId;
  }

  public final Long getGroupId()
  {
    return groupId;
  }

  public final HashMap<String, String> getParameters()
  {
    return parameters;
  }

  public final String getFragment()
  {
    return fragment;
  }

  @Override
  public String toString()
  {
    return "SaveOAuthAction{"
           + "financialInstitutionId=" + financialInstitutionId
           + ", groupId=" + groupId
           + ", parameters=" + (parameters == null ? "null" : parameters.keySet())
           + ", fragment="
           + (fragment == null ? "null"
                               : fragment.replaceAll("[a-z]", "a")
                                         .replaceAll("[A-Z]", "A")
                                         .replaceAll("[0-9]", "0"))
           + '}';
  }

}
