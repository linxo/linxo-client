/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.dto.EntityInfo;

/**
 *
 */
public final class Credential
  extends EntityInfo
{
  private String key;
  private String value;

  @SuppressWarnings("unused")
  private Credential()
  {}

  public Credential(long id, String key, String value)
  {
    setId(id);
    this.value = value;
    this.key = key;
  }

  public void setValue(String value)
  {
    this.value = value;
  }

  public String getValue()
  {
    return value;
  }

  public String getKey()
  {
    return key;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer()
        .append("Credential")
        .append("{id=").append(getId())
        .append(", key=").append(key)
        .append('}');
    return sb.toString();
  }
}
