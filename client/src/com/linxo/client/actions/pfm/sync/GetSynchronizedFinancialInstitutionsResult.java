/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 20/04/2011 by hugues.
 */
package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.sync.FinancialInstitutionInfo;

import java.util.ArrayList;

public final class GetSynchronizedFinancialInstitutionsResult
  extends SecuredResult
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private ArrayList<FinancialInstitutionInfo> financialInstitutions;


  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private GetSynchronizedFinancialInstitutionsResult()
  {}

  public GetSynchronizedFinancialInstitutionsResult(ArrayList<FinancialInstitutionInfo> financialInstitutions)
  {
    this.financialInstitutions = financialInstitutions;
  }

  // Instance Methods

  @SuppressWarnings("unused") // @Json
  public ArrayList<FinancialInstitutionInfo> getFinancialInstitutions()
  {
    return financialInstitutions;
  }

  @Override
  public String toString() {
    return "GetSynchronizedFinancialInstitutionsResult{" +
        "size=" + financialInstitutions.size() +
        '}';
  }
}
