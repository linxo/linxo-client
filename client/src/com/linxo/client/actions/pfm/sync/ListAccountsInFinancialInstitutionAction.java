/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 19/07/2010 by hugues.
 */
package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.ArrayList;

/**
 * This action should be used for initial ListAccounts.
 *
 * Its parameters are :
 *  - a financial institution id: id of the bank we want to list the accounts for,
 *  - a list of Credential: the credentials the user entered. Any credential whose
 *    the key contains "secret" should already be encrypted in this action.
 *  - a boolean indicating whether or not the group we want to create has to be
 *    "semi-auto" (i.e. Linxo doesn't remember all the credentials). This parameter
 *    may be null (not appear in the json at all).
 *
 * When the server receives this action, a "ListAccounts" job starts. Clients
 * should listen to AccountListUpdateEvent events to get the updates for this
 * job. The final update will contain the list of accounts found on the bank
 * web site, or an error status in case of problem.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class ListAccountsInFinancialInstitutionAction
    extends SecuredAction<ListAccountsInFinancialInstitutionResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields
  public static final String FINANCIAL_INSTITUTION_ID = "financialInstitutionId";
  public static final String CREDENTIALS = "credentials";
  public static final String SEMI_AUTO = "semiAuto";

  // Static Initializers

  // Static Methods

  // Instance Fields
  private long financialInstitutionId;
  private ArrayList<Credential> credentials;
  private Boolean semiAuto;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused")
  private ListAccountsInFinancialInstitutionAction()
  {}

  public ListAccountsInFinancialInstitutionAction(final long financialInstitutionId,
                                                  final ArrayList<Credential> credentials,
                                                  final Boolean semiAuto)
  {
    this.credentials = credentials;
    this.financialInstitutionId = financialInstitutionId;
    this.semiAuto = semiAuto;
  }

  // Instance Methods

  public ArrayList<Credential> getCredentials()
  {
    return credentials;
  }

  public long getFinancialInstitutionId()
  {
    return financialInstitutionId;
  }

  public Boolean isSemiAuto()
  {
    return semiAuto;
  }

}
