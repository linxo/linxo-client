/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.ArrayList;

/**
 * An action to cancel adding an account for the current user.
 */
@GwtAction
@JsonAction
public class AddSemiSyncCredentialsAction
    extends SecuredAction<AddSemiSyncCredentialsResult>
{
  public static final String ACCOUNT_GROUP_ID = "accountGroupId";
  public static final String CREDENTIALS = "credentials";
  public static final String REMEMBER_CREDENTIALS = "rememberCredentials";

  private long accountGroupId;
  private ArrayList<Credential> credentials;
  private boolean rememberCredentials;

  @SuppressWarnings("unused")
  private AddSemiSyncCredentialsAction()
  {
  }

  public AddSemiSyncCredentialsAction(final long accountGroupId,
                                      final ArrayList<Credential> credentials,
                                      final boolean rememberCredentials)
  {
    this.credentials = credentials;
    this.accountGroupId = accountGroupId;
    this.rememberCredentials = rememberCredentials;
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  public ArrayList<Credential> getCredentials()
  {
    return credentials;
  }

  public boolean isRememberCredentials()
  {
    return rememberCredentials;
  }

}
