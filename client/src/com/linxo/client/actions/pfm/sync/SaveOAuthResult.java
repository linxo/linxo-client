/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredResult;

public final class SaveOAuthResult
    extends SecuredResult
{
  public enum Status
  {
    Success,
    Failure
  }

  private Status status;
  private Long groupId;

  @SuppressWarnings("unused") // s11n
  private SaveOAuthResult() {}

  public SaveOAuthResult(final Status status, final Long groupId)
  {
    this.status = status;
    this.groupId = groupId;
  }

  public final Status getStatus()
  {
    return status;
  }

  public final Long getGroupId()
  {
    return groupId;
  }

  @Override
  public String toString()
  {
    return "SaveOAuthResult{"
           + "status=" + status
           + "groupId=" + groupId
           + '}';
  }

}
