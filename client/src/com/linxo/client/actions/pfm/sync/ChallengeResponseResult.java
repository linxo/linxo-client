/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredResult;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public class ChallengeResponseResult
  extends SecuredResult
{
}
