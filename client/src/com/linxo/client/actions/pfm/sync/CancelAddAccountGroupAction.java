/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * An action to cancel adding an account for the current user.
 */
@GwtAction
@JsonAction
public class CancelAddAccountGroupAction
  extends SecuredAction<CancelAddAccountGroupResult>
{
}
