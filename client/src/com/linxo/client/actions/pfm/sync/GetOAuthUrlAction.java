/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public final class GetOAuthUrlAction
    extends SecuredAction<GetOAuthUrlResult>
{
  private long financialInstitutionId;

  @AssertStringSize(nullable = false, voidable = false)
  private String returnUrl;

  @SuppressWarnings("unused") // s11n
  private GetOAuthUrlAction() {}

  public GetOAuthUrlAction(final long financialInstitutionId, final String returnUrl)
  {
    this.financialInstitutionId = financialInstitutionId;
    this.returnUrl = returnUrl;
  }

  public final long getFinancialInstitutionId()
  {
    return financialInstitutionId;
  }

  public final String getReturnUrl()
  {
    return returnUrl;
  }

  @Override
  public final String toString()
  {
    return "GetOAuthUrlAction{"
           + "financialInstitutionId=" + financialInstitutionId
           + ", returnUrl='" + returnUrl + '\''
           + '}';
  }

}
