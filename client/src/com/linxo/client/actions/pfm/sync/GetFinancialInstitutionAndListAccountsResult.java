/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.sync.FinancialInstitutionInfo;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetFinancialInstitutionAndListAccountsResult
  extends SecuredResult
{
  private FinancialInstitutionInfo financialInstitution;

  private Boolean needsSecret;

  private String publicKey;

  private String groupName;

  @SuppressWarnings("unused")
  private GetFinancialInstitutionAndListAccountsResult() {}

  public GetFinancialInstitutionAndListAccountsResult(FinancialInstitutionInfo financialInstitution, final String groupName)
  {
    this.financialInstitution = financialInstitution;
    this.needsSecret = null;
    this.publicKey = null;
    this.groupName = groupName;
  }

  public GetFinancialInstitutionAndListAccountsResult(final FinancialInstitutionInfo financialInstitution,
                                                      final Boolean needsSecret,
                                                      final String publicKey, String groupName)
  {
    this.financialInstitution = financialInstitution;
    this.needsSecret = needsSecret;
    this.publicKey = publicKey;
    this.groupName = groupName;
  }

  public FinancialInstitutionInfo getFinancialInstitution()
  {
    return financialInstitution;
  }

  public Boolean getNeedsSecret()
  {
    return needsSecret;
  }

  public String getPublicKey()
  {
    return publicKey;
  }

  public String getGroupName()
  {
    return groupName;
  }
}
