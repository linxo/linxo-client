/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredResult;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public class UpdateAccountListResult
  extends SecuredResult
{
  private Long personalizedViewId;

  @SuppressWarnings("unused") // s11n
  public UpdateAccountListResult() {}

  public UpdateAccountListResult(Long personalizedViewId)
  {
    this.personalizedViewId = personalizedViewId;
  }

  public Long getPersonalizedViewId()
  {
    return personalizedViewId;
  }

}
