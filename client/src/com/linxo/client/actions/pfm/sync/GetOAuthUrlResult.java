/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredResult;

public final class GetOAuthUrlResult
    extends SecuredResult
{
  public enum Status
  {
    Success,
    Failure
  }

  private Status status;
  private String oauthUrl;

  @SuppressWarnings("unused") // s11n
  private GetOAuthUrlResult() {}

  public GetOAuthUrlResult(final Status status, final String oauthUrl)
  {
    this.status = status;
    this.oauthUrl = oauthUrl;
  }

  public final Status getStatus()
  {
    return status;
  }

  public final String getOauthUrl()
  {
    return oauthUrl;
  }

  @Override
  public final String toString()
  {
    return "GetOAuthUrlResult{"
           + "status=" + status
           + ", oauthUrl='" + oauthUrl + '\''
           + '}';
  }

}
