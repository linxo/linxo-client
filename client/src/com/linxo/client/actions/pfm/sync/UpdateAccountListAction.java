/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.account.ProviderAccountInfo;

import java.util.ArrayList;

/**
 * This action should be used to add/remove/close/mark as FailedSingle accounts after
 * getting the account list from the bank for an existing group (list accounts job).
 *
 * Use SelectAccountsForSynchronizationAction to add the accounts the client selected
 * for synchronization after it got the account list from the bank (at the end of the
 * initial list accounts job).
 */
@GwtAction
@JsonAction
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class UpdateAccountListAction
  extends SecuredAction<UpdateAccountListResult>
{
  public static final String GROUP_ID = "groupId";
  public static final String TO_ADD = "toAdd";
  public static final String TO_REMOVE = "toRemove";
  public static final String TO_CLOSE = "toClose";
  public static final String MISSING_ACCOUNTS = "missingAccounts";


  private long groupId;
  private ArrayList<ProviderAccountInfo> toAdd;
  private ArrayList<String> toRemove;
  private ArrayList<String> missingAccounts;
  private ArrayList<String> toClose;

  @SuppressWarnings("unused")
  private UpdateAccountListAction() {}

  public UpdateAccountListAction(final long groupId,
                                 final ArrayList<ProviderAccountInfo> toAdd,
                                 final ArrayList<String> toRemove,
                                 final ArrayList<String> toClose,
                                 final ArrayList<String> missingAccounts)
  {
    this.groupId = groupId;
    this.toAdd = toAdd;
    this.toRemove = toRemove;
    this.missingAccounts = missingAccounts;
    this.toClose = toClose;
  }

  public long getGroupId()
  {
    return groupId;
  }

  public ArrayList<ProviderAccountInfo> getAccountsToAdd()
  {
    return toAdd;
  }

  public ArrayList<String> getAccountsToRemove()
  {
    return toRemove;
  }

  public ArrayList<String> getMissingAccounts()
  {
    return missingAccounts;
  }

  public ArrayList<String> getAccountsToClose()
  {
    return toClose;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("UpdateAccountListAction");
    sb.append("{groupId=").append(groupId);
    sb.append('}');
    return sb.toString();
  }



}
