/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 19/07/2010 by hugues.
 */
package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.sync.FinancialInstitutionInfo;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetFinancialInstitutionResult extends SecuredResult
{

  // Nested Types (mixing inner and static classes is okay)


  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private FinancialInstitutionInfo financialInstitution;
  private String publicKey;

  // Instance Initializers

  // Constructors
  @SuppressWarnings("unused")
  private GetFinancialInstitutionResult()
  {}

  public GetFinancialInstitutionResult(FinancialInstitutionInfo financialInstitution, String publicKey)
  {
    this.financialInstitution = financialInstitution;
    this.publicKey = publicKey;
  }

  // Instance Methods


  public FinancialInstitutionInfo getFinancialInstitution()
  {
    return financialInstitution;
  }

  public String getPublicKey()
  {
    return publicKey;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("GetFinancialInstitutionResult");
    sb.append("{financialInstitution=").append(financialInstitution);
    sb.append(", publicKey='PGP PUBLIC KEY STRIPPED'");
    sb.append('}');
    return sb.toString();
  }
}
