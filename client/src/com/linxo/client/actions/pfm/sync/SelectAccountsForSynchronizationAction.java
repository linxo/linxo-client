/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 19/07/2010 by hugues.
 */
package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.account.ProviderAccountInfo;

import java.util.ArrayList;

/**
 * This action should be used to add the accounts the client selected for synchronization
 * after it got the account list from the bank (at the end of the initial list accounts job).
 *
 * Use UpdateAccountListAction to add/remove/close/mark as FailedSingle accounts after
 * getting the account list from the bank for an existing group (list accounts job).
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class SelectAccountsForSynchronizationAction
    extends SecuredAction<SelectAccountsForSynchronizationResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields
  public static final String ACCOUNTS_SELECTED = "accountsSelected";

  // Static Initializers

  // Static Methods

  // Instance Fields
  private ArrayList<ProviderAccountInfo> accountsSelected;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused")
  private SelectAccountsForSynchronizationAction(){}

  public SelectAccountsForSynchronizationAction(ArrayList<ProviderAccountInfo> accountsSelected)
  {
    this.accountsSelected = accountsSelected;
  }

  // Instance Methods

  public ArrayList<ProviderAccountInfo> getAccountsSelected()
  {
    return accountsSelected;
  }
}
