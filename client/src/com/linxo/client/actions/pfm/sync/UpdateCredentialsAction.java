/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.sync;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.ArrayList;

/**
 *
 */
@GwtAction
@JsonAction
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class UpdateCredentialsAction
  extends SecuredAction<UpdateCredentialsResult>
{
  public static final String ACCOUNT_GROUP_ID = "accountGroupId";
  public static final String CREDENTIALS = "credentials";
  

  private long accountGroupId;
  private ArrayList<Credential> credentials;

  @SuppressWarnings("unused")
  private UpdateCredentialsAction() {}

  public UpdateCredentialsAction(final long accountGroupId, final ArrayList<Credential> credentials)
  {
    this.accountGroupId = accountGroupId;
    this.credentials = credentials;
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  public ArrayList<Credential> getCredentials()
  {
    return credentials;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("UpdateCredentialsAction");
    sb.append("{accountGroupId=").append(accountGroupId);
    sb.append('}');
    return sb.toString();
  }

}
