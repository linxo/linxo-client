/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.tx.DetailedTransactionInfo;
import com.linxo.client.dto.tx.TransactionInfo;

import java.util.Map;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class UpdateMultipleTransactionsResult
    extends SecuredResult
{
  /**
   * A Map of rowIndex -> STI
   */
  private Map<Integer, TransactionInfo> transactionInfos;

  @SuppressWarnings("unused")
  private UpdateMultipleTransactionsResult(){}

  public UpdateMultipleTransactionsResult(Map<Integer, TransactionInfo> transactionInfos)
  {
    this.transactionInfos = transactionInfos;
  }

  public Map<Integer, TransactionInfo> getTransactionInfos()
  {
    return transactionInfos;
  }

}
