/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.data.DateUtils;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.actions.SecuredResult;

import java.util.Date;

/**
 *
 */
public final class GetDatesInitialInfoResult
  extends SecuredResult
{
  // These "dates" are deprecated. Use the LinxoDates instead.

  @Deprecated
  private int startEnabledDay;
  @Deprecated
  private int startEnabledMonth;
  @Deprecated
  private int startEnabledYear;

  @Deprecated
  private int startSelectedDay;
  @Deprecated
  private int startSelectedMonth;
  @Deprecated
  private int startSelectedYear;

  @Deprecated
  private int endSelectedDay;
  @Deprecated
  private int endSelectedMonth;
  @Deprecated
  private int endSelectedYear;

  /**
   * First day of the first month we have transactions for the user.
   * This is not based on user's permissions, just on user's transactions.
   */
  private LinxoDate startData;

  /**
   * First day of the first month the user can see graphs for.
   * This is based on user's transactions and user's permissions.
   */
  private LinxoDate startEnabled;

  /**
   * First day of the last month the user can see graphs for.
   * This is actually the last day of the current month.
   * Used only in the web client.
   */
  private LinxoDate endEnabled;

  /**
   * First day of the first month selected by default.
   * This is actually the first day of the current month.
   * Used only in the web client.
   */
  private LinxoDate startSelected;

  /**
   * First day of the last month selected by default.
   * This is actually the last day of the current month.
   * Used only in the web client.
   */
  private LinxoDate endSelected;

  /**
   * Maximum number of years displayed in the calendar
   * selector in the web client (only).
   */
  private int maxYears;

  @SuppressWarnings("unused")
  private GetDatesInitialInfoResult() {}

  @SuppressWarnings({ "deprecation" })
  public GetDatesInitialInfoResult(final Date startEnabled,
                                   final Date startSelected, final Date endSelected,
                                   final int maxYears)
  {
    this.startEnabledDay = startEnabled.getDate();
    this.startEnabledMonth = DateUtils.MAGIC_MONTH + startEnabled.getMonth();
    this.startEnabledYear = DateUtils.MAGIC_YEAR + startEnabled.getYear();
    this.startSelectedDay = startSelected.getDate();
    this.startSelectedMonth = DateUtils.MAGIC_MONTH + startSelected.getMonth();
    this.startSelectedYear = DateUtils.MAGIC_YEAR + startSelected.getYear();
    this.endSelectedDay = endSelected.getDate();
    this.endSelectedMonth = DateUtils.MAGIC_MONTH + endSelected.getMonth();
    this.endSelectedYear = DateUtils.MAGIC_YEAR + endSelected.getYear();
    this.maxYears = maxYears;
  }

  public GetDatesInitialInfoResult(final LinxoDate startEnabled, final LinxoDate startSelected,
                                   final LinxoDate endEnabled, final LinxoDate endSelected,
                                   final LinxoDate startData,
                                   final int maxYears)
  {
    this.startEnabled = startEnabled;
    this.startSelected = startSelected;
    this.endEnabled = endEnabled;
    this.endSelected = endSelected;
    this.startData = startData;
    this.maxYears = maxYears;
  }

  public int getStartEnabledDay()
  {
    return startEnabledDay;
  }

  public int getStartEnabledMonth()
  {
    return startEnabledMonth;
  }

  public int getStartEnabledYear()
  {
    return startEnabledYear;
  }

  public int getStartSelectedDay()
  {
    return startSelectedDay;
  }

  public int getStartSelectedMonth()
  {
    return startSelectedMonth;
  }

  public int getStartSelectedYear()
  {
    return startSelectedYear;
  }

  public int getEndSelectedDay()
  {
    return endSelectedDay;
  }

  public int getEndSelectedMonth()
  {
    return endSelectedMonth;
  }

  public int getEndSelectedYear()
  {
    return endSelectedYear;
  }

  public int getMaxYears()
  {
    return maxYears;
  }

  public LinxoDate getEndEnabled()
  {
    return endEnabled;
  }

  public LinxoDate getEndSelected()
  {
    return endSelected;
  }

  public LinxoDate getStartEnabled()
  {
    return startEnabled;
  }

  public LinxoDate getStartSelected()
  {
    return startSelected;
  }

  public LinxoDate getStartData()
  {
    return startData;
  }
}
