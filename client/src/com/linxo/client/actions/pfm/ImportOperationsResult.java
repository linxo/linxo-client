/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;

/**
 *
 */
@SuppressWarnings({"DeserializableClassInSecureContext"})
public final class ImportOperationsResult
    extends SecuredResult
{
  public ImportOperationsResult() {}
}
