/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.account.BankAccountInfo;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
@Deprecated
public final class UpdateBankAccountAction
  extends SecuredAction<UpdateBankAccountResult>
{
  private long bankAccountId;

  @AssertStringSize(nullable = false, voidable = false)
  private String newName;

  private AccountType newType;
  private boolean newClosed;
  private double newBalance;
  private BankAccountInfo accountInfo;
  private String savingsType;

  @SuppressWarnings("unused")// s11n
  private UpdateBankAccountAction()
  {}

  public UpdateBankAccountAction(final long bankAccountId, final String newName,
                                     final boolean newClosed, final double newBalance,
                                     final AccountType newType)
  {
    this(bankAccountId,newName,newClosed,newBalance,newType,null);
  }

  public UpdateBankAccountAction(final long bankAccountId, final String newName,
                                     final boolean newClosed, final double newBalance,
                                     final AccountType newType, String savingsType)
  {
    this.bankAccountId = bankAccountId;
    this.newName = newName;
    this.newType = newType;
    this.newClosed = newClosed;
    this.newBalance = newBalance;
    this.savingsType = savingsType;
  }

  public UpdateBankAccountAction(BankAccountInfo bankAccountInfo, boolean closed) {
    this.accountInfo = bankAccountInfo;
    this.newClosed = closed;
    this.bankAccountId = bankAccountInfo.getId();
    this.newType = bankAccountInfo.getType();
  }

  public long getBankAccountId()
  {
    return bankAccountId;
  }

  public String getNewName()
  {
    return newName;
  }

  public AccountType getNewType()
  {
    return newType;
  }

  public boolean isNewClosed()
  {
    return newClosed;
  }

  public double getNewBalance()
  {
    return newBalance;
  }

  public BankAccountInfo getAccountInfo()
  {
    return accountInfo;
  }

  public String getSavingsType()
  {
    return savingsType;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder();
    sb.append("UpdateBankAccountAction");
    sb.append("{bankAccountId=").append(bankAccountId);
    sb.append(", newName=").append(newName);
    sb.append(", newType=").append(newType);
    sb.append(", newClosed=").append(newClosed);
    sb.append(", newBalance=").append(newBalance);
    sb.append('}');
    return sb.toString();
  }

}
