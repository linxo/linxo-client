/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 1:24:14 PM by tarunmalhotra.
*/
package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.ArrayList;

@SuppressWarnings({"DeserializableClassInSecureContext"})
@GwtAction
@JsonAction
public final class HideAlertAction extends SecuredAction<HideAlertResult>
{
  private Long[] alertIds;
  private Long alertId;

  @SuppressWarnings("unused") // s11n
  private HideAlertAction()
  {
  }

  public HideAlertAction(Long... alertIds)
  {
    this.alertIds = alertIds;
  }

  public HideAlertAction(ArrayList<Long> alertIds)
  {
    this.alertIds = new Long[alertIds.size()];
    this.alertIds = alertIds.toArray(this.alertIds);
  }

  public HideAlertAction(long alertId)
  {
    this.alertId = alertId;
  }

  // Used for old mobile compatibility
  @Deprecated
  public Long getAlertId()
  {
    return alertId;
  }

  public Long[] getAlertIds()
  {
    return alertIds;
  }
}
