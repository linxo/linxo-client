/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 18/04/2011 by hugues.
 */
package com.linxo.client.actions.pfm.affiliation;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.announce.AnnounceInfo;

@GwtAction
@JsonAction
public final class AnnounceActionedAction extends SecuredAction<AnnounceActionedResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private Long announceId;

  //LINXO-CLIENT: make sure there is no space around "Action"
  private AnnounceInfo.Action action;


  // Instance Initializers

  // Constructors


  @SuppressWarnings("unused") // s11n
  private AnnounceActionedAction()
  {} // nothing

  public AnnounceActionedAction(Long announceId, AnnounceInfo.Action action)
  {
    this.action = action;
    this.announceId = announceId;
  }

  // Instance Methods


  public AnnounceInfo.Action getAction()
  {
    return action;
  }

  public Long getAnnounceId()
  {
    return announceId;
  }
}
