/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 08/06/2011 by hugues.
 */
package com.linxo.client.actions.pfm.affiliation;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.announce.AnnounceInfo;

@JsonAction
@GwtAction
public final class GetAnnounceAction extends SecuredAction<GetAnnounceResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private AnnounceInfo.Size size;

  @AssertStringSize
  private String keywords;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private GetAnnounceAction()
  {}

  public GetAnnounceAction(String keywords, AnnounceInfo.Size size)
  {
    this.keywords = keywords;
    this.size = size;
  }

  // Instance Methods


  public String getKeywords()
  {
    return keywords;
  }

  public AnnounceInfo.Size getSize()
  {
    return size;
  }
}
