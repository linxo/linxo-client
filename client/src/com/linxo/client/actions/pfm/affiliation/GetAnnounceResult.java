/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 08/06/2011 by hugues.
 */
package com.linxo.client.actions.pfm.affiliation;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.announce.AnnounceInfo;

public final class GetAnnounceResult extends SecuredResult
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private AnnounceInfo announce;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private GetAnnounceResult()
  {}

  public GetAnnounceResult(AnnounceInfo announce)
  {
    this.announce = announce;
  }

  // Instance Methods

  public boolean hasAnnounce(){
    return announce != null;
  }

  public AnnounceInfo getAnnounce()
  {
    return announce;
  }
}
