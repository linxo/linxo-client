/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.data.DateUtils;
import com.linxo.client.data.ValidationConstants;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.tx.TransactionInfo;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 */
@GwtAction
@JsonAction
public final class UpdateTransactionAction
  extends SecuredAction<UpdateTransactionResult>
{
  public static final String TX_ID = "txId";
  public static final String NEW_LABEL = "newLabel";
  public static final String NEW_CAT_ID = "newCatId";
  public static final String NEW_CHECK_NUMBER = "newCheckNumber";
  public static final String DUPLICATE_TXN = "duplicateTransaction";
  public static final String NOTES = "notes";
  public static final String TAG_IDS = "tagIDs";
  public static final String BUDGET_DAY = "budgetDay";
  public static final String BUDGET_MONTH = "budgetMonth";
  public static final String BUDGET_YEAR = "budgetYear";

  private long txId;

  @AssertStringSize(nullable = false, voidable = false)
  private String newLabel;

  private long newCatId;

  @AssertStringSize
  private String newCheckNumber;

  private boolean duplicateTransaction;

  private int budgetDay = -1;
  private int budgetMonth = -1;
  private int budgetYear = -1;

  @AssertStringSize(ValidationConstants.TRANSACTION_MEMO_MAX_LENGTH)
  private String notes;
  private ArrayList<Long> tagIDs;

  @SuppressWarnings("unused")
  private UpdateTransactionAction() {}

  @SuppressWarnings({"deprecation"})
  public UpdateTransactionAction(final long txId, final String newLabel,
                                       final long newCatId, final String newCheckNumber,
                                       boolean duplicateTransaction, Date budgetDate)
  {
    this.txId = txId;
    this.newLabel = newLabel;
    this.newCatId = newCatId;
    this.newCheckNumber = newCheckNumber;
    this.notes = null;
    this.tagIDs = null;
    this.duplicateTransaction = duplicateTransaction;
    if (budgetDate != null) {
      setBudgetDate(budgetDate.getDate(), budgetDate.getMonth() + DateUtils.MAGIC_MONTH, DateUtils.MAGIC_YEAR + budgetDate.getYear());
    }
  }

  @SuppressWarnings({"deprecation"})
  public UpdateTransactionAction(final long txId, final String newLabel,
                                       final long newCatId, final String newCheckNumber,
                                       final String notes, final ArrayList<Long> tagIDs,
                                       boolean duplicateTransaction, Date budgetDate)
  {
    this.txId = txId;
    this.newLabel = newLabel;
    this.newCatId = newCatId;
    this.newCheckNumber = newCheckNumber;
    this.notes = notes;
    this.tagIDs = tagIDs;
    this.duplicateTransaction = duplicateTransaction;
    if (budgetDate != null) {
      setBudgetDate(budgetDate.getDate(), budgetDate.getMonth() + DateUtils.MAGIC_MONTH, DateUtils.MAGIC_YEAR + budgetDate.getYear());
    }
  }

  @SuppressWarnings({"deprecation"}) // dates
  public UpdateTransactionAction(TransactionInfo txInfo)
  {
    this.txId = txInfo.getId();
    this.newCatId = txInfo.getCategoryId();
    this.newLabel = txInfo.getLabel();
    this.newCheckNumber = txInfo.getCheckNumber();
    this.duplicateTransaction = txInfo.isDuplicate();
    this.notes = txInfo.getNotes();
    this.tagIDs = txInfo.getTagIds();
    this.budgetDay = txInfo.getBudgetDate().getDay();
    this.budgetMonth = txInfo.getBudgetDate().getMonth();
    this.budgetYear = txInfo.getBudgetDate().getYear();
  }

  /**
   * @return the Id of the transaction to update.
   *
   * This fields cannot be null or the call returns
   * {@link UpdateTransactionResult.Status#BadParameterFailure an error}.
   */
  public long getTxId()
  {
    return txId;
  }

  /**
   * @return newLabel for this transaction.
   *
   * This fields cannot be null or the call returns
   * {@link UpdateTransactionResult.Status#BadParameterFailure an error}.
   */
  public String getNewLabel()
  {
    return newLabel;
  }

  /**
   * @return the id of the new Category for this transaction.
   *
   * This fields cannot be null and the category must be assignable
   * (not hidden or masked and owned by the user)
   * or the call returns a server error (HTTP Code 500).
   */
  public long getNewCatId()
  {
    return newCatId;
  }

  /**
   * @return the checkNumber for this transaction.
   * This fields can be left empty or null
   */
  public String getNewCheckNumber()
  {
    return newCheckNumber;
  }

  /**
   * @return the updated notes for this transaction.
   * This fields can be left empty or null
   */
  public String getNotes()
  {
    return notes;
  }

  /**
   * @return the updated list of tags for this transaction.
   * This fields can be left empty or null
   */
  public ArrayList<Long> getTagIDs()
  {
    return tagIDs;
  }

  /**
   * @return {@code true} if the user considers this transaction as a duplicate
   */
  public boolean isDuplicateTransaction()
  {
    return duplicateTransaction;
  }

  /**
   * @return the updated day of the budget Date (range: 1-31).
   * -1 means 'none'.
   * ok if all three budget fields are left empty.
   */
  public int getBudgetDay()
  {
    return budgetDay;
  }

  /**
   * @return the updated month of the budget Date (range: 1-12).
   * -1 means 'none'.
   * ok if all three budget fields are left empty.
   */
  public int getBudgetMonth()
  {
    return budgetMonth;
  }

  /**
   * @return the updated year of the budget Date (currently on 4 digits like '2015').
   * -1 means 'none'.
   * ok if all three budget fields are left empty.
   */
  public int getBudgetYear()
  {
    return budgetYear;
  }

  public void setBudgetDate(int date, int month, int year)
  {
    budgetDay = date;
    budgetMonth = month;
    budgetYear = year;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferMayBeStringBuilder
    final StringBuffer sb = new StringBuffer();
    sb.append("UpdateTransactionAction");
    sb.append("{txId=").append(txId);
    sb.append(", newLabel=").append(newLabel);
    sb.append(", newCatId=").append(newCatId);
    sb.append(", newCheckNumber=").append(newCheckNumber);
    sb.append(", notes=").append(notes);
    if (tagIDs != null) {
      for (Long tagId : tagIDs) {
        sb.append(", tagId=").append(tagId);
      }
    } else {
      sb.append(", tagId=null");
    }

    sb.append(", duplicateTransaction=").append(duplicateTransaction);
    sb.append(", budgetDay=").append(budgetDay);
    sb.append(", budgetMonth=").append(budgetMonth);
    sb.append(", budgetYear=").append(budgetYear);
    sb.append('}');
    return sb.toString();
  }

}
