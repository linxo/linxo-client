/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action queries for all the Account Groups of the currently logged-in user.
 */
@GwtAction
@JsonAction
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetAccountGroupsAction
  extends SecuredAction<GetAccountGroupsResult>
{}
