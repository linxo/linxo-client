/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class GetAccountsForGroupAction
  extends SecuredAction<GetAccountsForGroupResult>
{
  private long accountGroupId;

  @SuppressWarnings("unused") // used for s11n
  private GetAccountsForGroupAction()
  {}

  public GetAccountsForGroupAction(final long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("GetAccountsForGroupAction");
    sb.append("{accountGroupId=").append(accountGroupId);
    sb.append('}');
    return sb.toString();
  }

}
