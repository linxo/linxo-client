/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2016 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 17/10/2016 by hugues.
 */
package com.linxo.client.actions.pfm.account;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action closes the BankAccount reference to by the given ID.
 * The {@link CloseBankAccountResult} contains the fully updated
 * {@link com.linxo.client.dto.account.BankAccountInfo}.
 *
 * Note:
 * This action exists in
 * {@link com.linxo.client.data.support.Version#HAS_DETAILED_ACCOUNT_VIEWS_SETTINGS}
 * and superior,
 */
@GwtAction
@JsonAction
public class CloseBankAccountAction extends SecuredAction<CloseBankAccountResult> {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private long bankAccountId;

  // Instance Initializers

  // Constructors


  public CloseBankAccountAction() {}

  public CloseBankAccountAction(long bankAccountId) {
    this.bankAccountId = bankAccountId;
  }

  // Instance Methods

  public long getBankAccountId() {
    return bankAccountId;
  }

}
