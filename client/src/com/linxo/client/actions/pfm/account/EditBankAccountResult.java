/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2016 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 13/10/2016 by hugues.
 */
package com.linxo.client.actions.pfm.account;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.account.BankAccountInfo;

/**
 * Returns the result of the {@link EditBankAccountAction}.
 */
public class EditBankAccountResult extends SecuredResult {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private BankAccountInfo bankAccount;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // this class implements IsSerializable
  public EditBankAccountResult() {}

  public EditBankAccountResult(BankAccountInfo bankAccount) {
    this.bankAccount = bankAccount;
  }

  // Instance Methods

  public BankAccountInfo getBankAccount() {
    return bankAccount;
  }
}
