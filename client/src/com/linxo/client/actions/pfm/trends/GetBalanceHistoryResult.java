/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.trends;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.actions.SecuredResult;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * LinxoResult for the GetBalanceHistoryAction.
 */
public final class GetBalanceHistoryResult
    extends SecuredResult
{
  /**
   * Simple class to hold a balance and a balance date.
   */
  public static class BalanceAndBalanceDate
      implements Serializable
  {
    public LinxoDate balanceDate;
    public double balance;

    @SuppressWarnings("unused")
    private BalanceAndBalanceDate() {}

    public BalanceAndBalanceDate(final LinxoDate balanceDate, final double balance)
    {
      this.balanceDate = balanceDate;
      this.balance = balance;
    }
  }

  /** The balance history we wanted to get. It's a list of balances and balance dates. */
  private ArrayList<BalanceAndBalanceDate> balanceAndBalanceDates;

  @SuppressWarnings("unused")
  private GetBalanceHistoryResult() {} // used by s11n

  public GetBalanceHistoryResult(final ArrayList<BalanceAndBalanceDate> balanceAndBalanceDates)
  {
    this.balanceAndBalanceDates = balanceAndBalanceDates;
  }

  /**
   * Gets the balance history we wanted to get.
   * It's a list of balances and balance dates.
   *
   * @return The balance history we wanted to get. It's
   *         a list of balances and balance dates.
   */
  public final ArrayList<BalanceAndBalanceDate> getBalanceAndBalanceDates()
  {
    return balanceAndBalanceDates;
  }

}
