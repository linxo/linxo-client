/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.trends;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.actions.SecuredResult;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public final class GetTotalsAndCountsResult
  extends SecuredResult
{
  public static class MonthlyAmount
    implements Serializable
  {
    public Date month;
    public LinxoDate linxoDate;
    public double categorizedIncome;
    public double uncategorizedIncome;
    public double categorizedSpending;
    public double uncategorizedSpending;
  }

  public static class MonthlyAmountsByCategory
      implements Serializable
  {
    public LinxoDate linxoDate;
    public Map<Long, Long> debitsCountsByCategory;
    public Map<Long, Double> debitsSumsByCategory;
    public Map<Long, Long> creditsCountsByCategory;
    public Map<Long, Double> creditsSumsByCategory;
  }

  // ----- Used for Category and Label Graphs

  // will contain the sum of the transactions (credits and debits) for each category
  private HashMap<Long, Double> totals;
  // will contain the number of transactions (credits and debits) for each category
  private HashMap<Long, Integer> counts;

  // will contain the sum of the credits for each category
  private HashMap<Long, Double> credits;
  // will contain the number of credits for each category
  private HashMap<Long, Integer> creditCounts;

  // will contain the sum of the debits for each category
  private HashMap<Long, Double> debits;
  // will contain the number of debits for each category
  private HashMap<Long, Integer> debitCounts;

  // Contains the monthly expenses grouped by category
  private ArrayList<MonthlyAmountsByCategory> monthlyAmountsByCategories;

  // ----- Used for Monthly Graphs

  private ArrayList<MonthlyAmount> monthlyAmounts;

  @SuppressWarnings("unused")
  public GetTotalsAndCountsResult() {}

  public GetTotalsAndCountsResult(final HashMap<Long, Double> totals, final HashMap<Long, Integer> counts,
                                  final HashMap<Long, Double> credits, final HashMap<Long, Integer> creditCounts,
                                  final HashMap<Long, Double> debits, final HashMap<Long, Integer> debitCounts)
  {
    this.totals = totals;
    this.counts = counts;
    this.credits = credits;
    this.creditCounts = creditCounts;
    this.debits = debits;
    this.debitCounts = debitCounts;
  }

  public GetTotalsAndCountsResult(final ArrayList<MonthlyAmount> monthlyAmounts,
                                  final ArrayList<MonthlyAmountsByCategory> monthlyAmountsByCategory)
  {
    this.monthlyAmounts = monthlyAmounts;
    this.monthlyAmountsByCategories = monthlyAmountsByCategory;
  }

  public HashMap<Long, Double> getTotals()
  {
    return totals;
  }

  public HashMap<Long, Integer> getCounts()
  {
    return counts;
  }

  public HashMap<Long, Double> getCredits()
  {
    return credits;
  }

  public HashMap<Long, Integer> getCreditCounts()
  {
    return creditCounts;
  }

  public HashMap<Long, Double> getDebits()
  {
    return debits;
  }

  public HashMap<Long, Integer> getDebitCounts()
  {
    return debitCounts;
  }

  public ArrayList<MonthlyAmount> getMonthlyAmounts()
  {
    return monthlyAmounts;
  }

  public ArrayList<MonthlyAmountsByCategory> getMonthlyAmountsByCategories()
  {
    return monthlyAmountsByCategories;
  }
}
