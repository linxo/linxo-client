/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.trends;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.SavingsType;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.actions.SecuredResult;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 */
public final class GetMonthlyBalancesResult
  extends SecuredResult
{
  public static class MonthlyBalance
    implements Serializable
  {
    public double balance;
    public long accountId;
    public String accountName;
    public LinxoDate linxoDate;
    public AccountType accountType;
    public String savingsType;

    @SuppressWarnings("unused")
    public MonthlyBalance() {}

    public MonthlyBalance(final double balance, final long accountId, final String accountName,
                          final LinxoDate linxoDate, AccountType accountType, SavingsType savingsType)
    {
      this(balance, accountId ,accountName, linxoDate, accountType, savingsType.name());
    }

    public MonthlyBalance(final double balance, final long accountId, final String accountName,
                          final LinxoDate linxoDate, AccountType accountType, String savingsType)
    {
      this.balance = balance;
      this.accountId = accountId;
      this.accountName = accountName;
      this.linxoDate = linxoDate;
      this.accountType = accountType;
      this.savingsType = savingsType;
    }

    @Override
    public String toString()
    {
      final StringBuffer sb = new StringBuffer();
      sb.append("MonthlyBalance");
      sb.append("{balance=").append(balance);
      sb.append(", accountId=").append(accountId);
      sb.append(", accountName=").append(accountName);
      sb.append(", linxoDate=").append(linxoDate);
      sb.append(", accountType=").append(accountType);
      sb.append(", savingsType=").append(savingsType);
      sb.append('}');
      return sb.toString();
    }
  }

  public static class AccountsMonthlyBalances
    implements Serializable
  {
    public LinxoDate linxoDate;
    public ArrayList<MonthlyBalance> balances;

    @SuppressWarnings("unused")
    public AccountsMonthlyBalances() {}

    public AccountsMonthlyBalances(final LinxoDate linxoDate, final ArrayList<MonthlyBalance> balances)
    {
      this.linxoDate = linxoDate;
      this.balances = balances;
    }
  }

  private ArrayList<AccountsMonthlyBalances> monthlyBalances;

  @SuppressWarnings("unused")
  private GetMonthlyBalancesResult() {} // used by s11n

  public GetMonthlyBalancesResult(final ArrayList<AccountsMonthlyBalances> monthlyBalances)
  {
    this.monthlyBalances = monthlyBalances;
  }

  public ArrayList<AccountsMonthlyBalances> getMonthlyBalances()
  {
    return monthlyBalances;
  }

}
