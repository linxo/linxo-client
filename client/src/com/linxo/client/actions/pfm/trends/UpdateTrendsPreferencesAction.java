/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 20/04/2011 by hugues.
 */
package com.linxo.client.actions.pfm.trends;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.List;

/**
 * @deprecated This action should no more be used, and should be replaced by
 * explicit calls to
 * {@link com.linxo.client.actions.pfm.account.EditBankAccountAction} to
 * update the fields {@code includedInXXX} of the
 * {@link com.linxo.client.dto.account.BankAccountInfo} object.
 */
@Deprecated
@GwtAction
@JsonAction
public final class UpdateTrendsPreferencesAction
    extends SecuredAction<UpdateTrendsPreferencesResult>
{


  // Instance Methods

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers
  private List<Long> includedAccounts;
  private List<Long> excludedAccounts;

  @SuppressWarnings("unused")
  // s11n
  private UpdateTrendsPreferencesAction()
  {
  }

  // Constructors
  public UpdateTrendsPreferencesAction( List<Long> includedAccounts, List<Long> excludedAccounts)
  {
    this.includedAccounts = includedAccounts;
    this.excludedAccounts = excludedAccounts;
  }

  public List<Long> getExcludedAccounts()
  {
    return excludedAccounts;
  }

  public List<Long> getIncludedAccounts()
  {
    return includedAccounts;
  }
}
