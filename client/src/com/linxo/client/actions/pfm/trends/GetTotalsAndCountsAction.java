/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.trends;

import com.linxo.client.data.DateUtils;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 */
@GwtAction
@JsonAction
public final class GetTotalsAndCountsAction
  extends SecuredAction<GetTotalsAndCountsResult>
{
  public enum Per
  {
    Month,
    Category,
    Label
  }


  /**
   * Used to specify whether we want totals and counts per month, category or label.s
   */
  private Per per;


  /**
   * Day of the start date. Transactions that happen on that date will be included.
   */
  @Deprecated
  private int startDay;

  /**
   * Month of the start date. Transactions that happen on that date will be included.
   */
  @Deprecated
  private int startMonth;

  /**
   * Year of the start date. Transactions that happen on that date will be included.
   */
  @Deprecated
  private int startYear;


  /**
   * Day if the end date. Transactions that happen on that date WILL be included.
   * Use -1 if you do not want transactions until the last. For instance, if you
   * want to have the full month of july 2014 => use start = 1/7/2014, end = 31/7/2014
   */
  @Deprecated
  private int endDay;
  /**
   * Month of the end date. Transactions that happen on that date WILL be included.
   * Use -1 if you do not want transactions until the last. For instance, if you
   * want to have the full month of july 2014 => use start = 1/7/2014, end = 31/7/2014
   */
  @Deprecated
  private int endMonth;
  /**
   * Year of the end date. Transactions that happen on that date WILL be included.
   * Use -1 if you do not want transactions until the last. For instance, if you
   * want to have the full month of july 2014 => use start = 1/7/2014, end = 31/7/2014
   */
  @Deprecated
  private int endYear;


  /**
   * Start date. Transactions that happen on that date will be included.
   */
  private LinxoDate startLinxoDate;

  /**
   * End date. Transactions that happen on that date WILL be included. Use null
   * if you do not want transactions until the last. For instance, if you want
   * to have the full month of july 2014 => use start = 1/7/2014, end = 31/7/2014
   */
  private LinxoDate endLinxoDate;

  /**
   * You can specify this id if you want totals and counts for an account only.
   * You can also specify its reference instead, see below. Else this field
   * should be null.
   */
  private Long accountId;

  /**
   * You can specify this reference if you want totals and counts for an account only.
   * You can also specify its id instead, see abive. Else this field should be null.
   */
  @AssertStringSize
  private String accountReference;

  /**
   * The ids of the currently selected views ({@link com.linxo.client.dto.account.PersonalizedView}).
   * If accountId or accountReference is not null (or both), this field should be null. Else if
   * this field is null, all the accounts will be included. If not null, it should not be empty.
   */
  private ArrayList<Long> selectedViewIds;


  public GetTotalsAndCountsAction() {}

  @Deprecated
  public GetTotalsAndCountsAction(final Per per, final Date start, final Date end)
  {
    this.per = per;
    // we want to send dates with: 1 <= month <= 12 and 0 <= year <= XXXX so we need to
    // add 1 to the month and 1900 to the year as 'start' is a java.util.Date
    setStartDate(start.getDate(), start.getMonth() + DateUtils.MAGIC_MONTH, DateUtils.MAGIC_YEAR + start.getYear());
    if(end != null){
      // we want to send dates with: 1 <= month <= 12 and 0 <= year <= XXXX so we need to
      // add 1 to the month and 1900 to the year as 'start' is a java.util.Date
      setEndDate(end.getDate(), end.getMonth() + DateUtils.MAGIC_MONTH, DateUtils.MAGIC_YEAR + end.getYear());
    } else {
      setEndDate(-1, -1, -1);
    }
  }

  @Deprecated
  private GetTotalsAndCountsAction(final Per per, final LinxoDate start, final LinxoDate end,
                                   Long accountId, String accountReference)
  {
    this.per = per;
    this.startLinxoDate = start;
    this.endLinxoDate = end;
    this.accountId = accountId;
    this.accountReference = accountReference;
  }

  /**
   * Creates an action to query the totals and counts for the given date and with the associated {@code Per}.
   * The result will take into account all the bankAccounts that are currently selected for the Graphics.
   * The result will use the {@link com.linxo.client.dto.tx.TransactionInfo#getBudgetDate() budget date} to
   * group them per month.
   *
   * @param per -
   * @param start -  the oldest date that may include transactions, using budget dates
   * @param end -  the youngest date that may include transactions, using budget dates
   */
  @Deprecated
  public GetTotalsAndCountsAction(final Per per, final LinxoDate start, final LinxoDate end)
  {
    this(per, start, end, null, null);
  }

  /**
   * Creates an action to query the totals and counts for the given date and with the associated {@code Per}
   * for the given BankAccount.
   *
   * The result will take into account all the bankAccounts that are currently selected for the Graphics.
   * The result will use the {@link com.linxo.client.dto.tx.TransactionInfo#getBudgetDate() budget date} to
   * group them per month.
   *
   * @param bankAccountId - The id of the BankAccountInfo
   * @param per -
   * @param start -  the oldest date that may include transactions, using budget dates
   * @param end -  the youngest date that may include transactions, using budget dates
   * @throws NullPointerException if the bankAccountId is null.
   */
  @Deprecated
  public GetTotalsAndCountsAction(final Long bankAccountId, final Per per, final LinxoDate start, final LinxoDate end)
      throws  NullPointerException
  {
    this(per, start, end, bankAccountId, null);
    if(bankAccountId == null) throw new NullPointerException("bankAccountId must not be null");
  }

  /**
   * Creates an action to query the totals and counts for the given date and with the associated {@code Per}
   * for the given BankAccount.
   *
   * The result will take into account all the bankAccounts that are currently selected for the Graphics.
   * The result will use the {@link com.linxo.client.dto.tx.TransactionInfo#getBudgetDate() budget date} to
   * group them per month.
   *
   * @param accountReference - The external reference of the BankAccountInfo
   * @param per -
   * @param start -  the oldest date that may include transactions, using budget dates
   * @param end -  the youngest date that may include transactions, using budget dates
   * @throws NullPointerException if the accountReference is null.
   */
  @Deprecated
  public GetTotalsAndCountsAction(final String accountReference, final Per per, final LinxoDate start, final LinxoDate end)
  {
    this(per, start, end, null, accountReference);
    if(accountReference == null) throw new NullPointerException("accountReference must not be null");
  }


  @Deprecated
  public int getStartDay()
  {
    return startDay;
  }

  @Deprecated
  public int getStartMonth()
  {
    return startMonth;
  }

  @Deprecated
  public int getStartYear()
  {
    return startYear;
  }

  @Deprecated
  public int getEndDay()
  {
    return endDay;
  }

  @Deprecated
  public int getEndMonth()
  {
    return endMonth;
  }

  @Deprecated
  public int getEndYear()
  {
    return endYear;
  }

  public Per getPer()
  {
    return per;
  }

  public void setPer(Per per)
  {
    this.per = per;
  }

  @Deprecated
  public boolean hasEndDate()
  {
    return endDay != -1 && endMonth != -1 && endYear != -1;
  }

  @Deprecated
  public void setStartDate(final int date, final int month, final int year)
  {
    this.startDay = date;
    this.startMonth = month;
    this.startYear = year;
  }

  @Deprecated
  public void setEndDate(final int date, final int month, final int year)
  {
    this.endDay = date;
    this.endMonth = month;
    this.endYear = year;
  }

  public LinxoDate getEndLinxoDate()
  {
    return endLinxoDate;
  }

  public void setEndLinxoDate(LinxoDate endLinxoDate)
  {
    this.endLinxoDate = endLinxoDate;
  }

  public LinxoDate getStartLinxoDate()
  {
    return startLinxoDate;
  }

  public void setStartLinxoDate(LinxoDate startLinxoDate)
  {
    this.startLinxoDate = startLinxoDate;
  }

  public String getAccountReference()
  {
    return accountReference;
  }

  public void setAccountReference(String accountReference)
  {
    this.accountReference = accountReference;
  }

  public Long getAccountId()
  {
    return accountId;
  }

  public void setAccountId(Long accountId)
  {
    this.accountId = accountId;
  }

  public ArrayList<Long> getSelectedViewIds()
  {
    return selectedViewIds;
  }

  public void setSelectedViewIds(ArrayList<Long> selectedViewIds)
  {
    this.selectedViewIds = selectedViewIds;
  }


  @Override
  public String toString()
  {
    return "GetTotalsAndCountsAction{"
           + "per=" + per
           + ", startDay=" + startDay
           + ", startMonth=" + startMonth
           + ", startYear=" + startYear
           + ", endDay=" + endDay
           + ", endMonth=" + endMonth
           + ", endYear=" + endYear
           + ", startLinxoDate=" + startLinxoDate
           + ", endLinxoDate=" + endLinxoDate
           + ", accountId=" + accountId
           + ", accountReference=" + accountReference
           + ", selectedViewIds=" + selectedViewIds
           + '}';
  }

}
