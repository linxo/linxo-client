/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.trends;

import com.linxo.client.actions.SecuredResult;

/**
 */
public class GetTrendsOverviewResult
        extends SecuredResult
{
  private double categorizedIncome;
  private double categorizedExpenses;
  private double uncategorizedIncome;
  private double uncategorizedExpenses;

  private long uncategorizedIncomeCount;
  private long uncategorizedExpensesCount;

  public GetTrendsOverviewResult()
  {}


  public double getCategorizedIncome()
  {
    return categorizedIncome;
  }

  public void setCategorizedIncome(double categorizedIncome)
  {
    this.categorizedIncome = categorizedIncome;
  }

  public double getCategorizedExpenses()
  {
    return categorizedExpenses;
  }

  public void setCategorizedExpenses(double categorizedExpenses)
  {
    this.categorizedExpenses = categorizedExpenses;
  }

  public double getUncategorizedIncome()
  {
    return uncategorizedIncome;
  }

  public void setUncategorizedIncome(double uncategorizedIncome)
  {
    this.uncategorizedIncome = uncategorizedIncome;
  }

  public double getUncategorizedExpenses()
  {
    return uncategorizedExpenses;
  }

  public void setUncategorizedExpenses(double uncategorizedExpenses)
  {
    this.uncategorizedExpenses = uncategorizedExpenses;
  }

  public long getUncategorizedIncomeCount()
  {
    return uncategorizedIncomeCount;
  }

  public void setUncategorizedIncomeCount(final long uncategorizedIncomeCount)
  {
    this.uncategorizedIncomeCount = uncategorizedIncomeCount;
  }

  public long getUncategorizedExpensesCount()
  {
    return uncategorizedExpensesCount;
  }

  public void setUncategorizedExpensesCount(final long uncategorizedExpensesCount)
  {
    this.uncategorizedExpensesCount = uncategorizedExpensesCount;
  }

}
