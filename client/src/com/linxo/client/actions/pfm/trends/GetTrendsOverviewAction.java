/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.trends;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 */
@GwtAction
@JsonAction
public class GetTrendsOverviewAction
  extends SecuredAction<GetTrendsOverviewResult>
{
  // Transactions that happen on that date will be included
  private int startDay;
  private int startMonth;
  private int startYear;

  // Transactions that happen on that date will NOT be included. Use null if you do not want transactions until the last
  private int endDay;
  private int endMonth;
  private int endYear;

  private LinxoDate startLinxoDate;
  private LinxoDate endLinxoDate;

  @SuppressWarnings({ "deprecation" })
  public GetTrendsOverviewAction()
  {
  }

  public int getStartDay()
  {
    return startDay;
  }

  public int getStartMonth()
  {
    return startMonth;
  }

  public int getStartYear()
  {
    return startYear;
  }

  public int getEndDay()
  {
    return endDay;
  }

  public int getEndMonth()
  {
    return endMonth;
  }

  public int getEndYear()
  {
    return endYear;
  }

  public boolean hasEndDate()
  {
    return endDay != -1 && endMonth != -1 && endYear != -1;
  }

  public void setStartDate(final int date, final int month, final int year)
  {
    this.startDay = date;
    this.startMonth = month;
    this.startYear = year;
  }

  public void setEndDate(final int date, final int month, final int year)
  {
    this.endDay = date;
    this.endMonth = month;
    this.endYear = year;
  }

  public LinxoDate getEndLinxoDate()
  {
    return endLinxoDate;
  }

  public void setEndLinxoDate(LinxoDate endLinxoDate)
  {
    this.endLinxoDate = endLinxoDate;
  }

  public LinxoDate getStartLinxoDate()
  {
    return startLinxoDate;
  }

  public void setStartLinxoDate(LinxoDate startLinxoDate)
  {
    this.startLinxoDate = startLinxoDate;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("GetTrendsOverviewAction");
    sb.append("{startDate=").append(startDay);
    sb.append(", startMonth=").append(startMonth);
    sb.append(", startYear=").append(startYear);
    sb.append(", endDate=").append(endDay);
    sb.append(", endMonth=").append(endMonth);
    sb.append(", endYear=").append(endYear);
    sb.append('}');
    return sb.toString();
  }

}
