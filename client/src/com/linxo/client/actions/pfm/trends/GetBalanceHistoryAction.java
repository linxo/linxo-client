/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.trends;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * LinxoAction to get the balance history for a given account. The account
 * is specified using either a bank account id or a reference. You can
 * specify optional start and end date.
 */
@GwtAction
@JsonAction
public final class GetBalanceHistoryAction
    extends SecuredAction<GetBalanceHistoryResult>
{
  /**
   * The id of the account we want to get balance history.
   * If null, we'll use the reference.
   */
  private Long bankAccountId;

  /**
   * The reference of the account we want to get balance history.
   * Only used if the bank account id is null.
   */
  @AssertStringSize
  private String reference;

  /** Start date for the balance history we want to get. Optional. */
  private LinxoDate startLinxoDate;

  /** End date for the balance history we want to get. Optional. */
  private LinxoDate endLinxoDate;

  @SuppressWarnings("unused")
  private GetBalanceHistoryAction() {} // used by s11n

  public GetBalanceHistoryAction(final Long bankAccountId,
                                 final LinxoDate startLinxoDate,
                                 final LinxoDate endLinxoDate)
  {
    this.bankAccountId = bankAccountId;
    this.startLinxoDate = startLinxoDate;
    this.endLinxoDate = endLinxoDate;
  }

  @SuppressWarnings("UnusedDeclaration")
  public GetBalanceHistoryAction(final String reference,
                                 final LinxoDate startLinxoDate,
                                 final LinxoDate endLinxoDate)
  {
    this.reference = reference;
    this.startLinxoDate = startLinxoDate;
    this.endLinxoDate = endLinxoDate;
  }

  /**
   * Gets the id of the account we want to get balance history.
   * If null, we'll use the reference.
   *
   * @return The id of the account we want to get balance history.
   *         If null, we'll use the reference.
   */
  public final Long getBankAccountId()
  {
    return bankAccountId;
  }

  /**
   * Gets the reference of the account we want to get balance history.
   * Used only if the bank account id is null.
   *
   * @return The reference of the account we want to get balance history.
   *         Used only if the account id is null.
   */
  public final String getReference()
  {
    return reference;
  }

  /**
   * Gets the start date for the balance history
   * we want to get. It's optional.
   *
   * @return The start date for the balance history
   *         we want to get. It's optional.
   */
  public final LinxoDate getStartLinxoDate()
  {
    return startLinxoDate;
  }

  /**
   * Gets the end date for the balance history
   * we want to get. It's optional.
   *
   * @return The end date for the balance history
   *         we want to get. It's optional.
   */
  public final LinxoDate getEndLinxoDate()
  {
    return endLinxoDate;
  }

}
