/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.trends;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.ArrayList;

/**
 *
 */
@GwtAction
@JsonAction
public final class GetMonthlyBalancesAction
  extends SecuredAction<GetMonthlyBalancesResult>
{
  /**
   * First day of the first month we want monthly balances for.
   */
  private LinxoDate startLinxoDate;

  /**
   * Last day of the last month we want monthly balances for.
   */
  private LinxoDate endLinxoDate;

  /**
   * The ids of the currently selected views ({@link com.linxo.client.dto.account.PersonalizedView}).
   * If this field is null, all the accounts will be included. If not null, it should not be empty.
   */
  private ArrayList<Long> selectedViewIds;


  public GetMonthlyBalancesAction() {}

  public GetMonthlyBalancesAction(LinxoDate startLinxoDate, LinxoDate endLinxoDate)
  {
    this.startLinxoDate = startLinxoDate;
    this.endLinxoDate = endLinxoDate;
  }


  public LinxoDate getEndLinxoDate()
  {
    return endLinxoDate;
  }

  public void setEndLinxoDate(LinxoDate endLinxoDate)
  {
    this.endLinxoDate = endLinxoDate;
  }

  public LinxoDate getStartLinxoDate()
  {
    return startLinxoDate;
  }

  public void setStartLinxoDate(LinxoDate startLinxoDate)
  {
    this.startLinxoDate = startLinxoDate;
  }

  public ArrayList<Long> getSelectedViewIds()
  {
    return selectedViewIds;
  }

  public void setSelectedViewIds(ArrayList<Long> selectedViewIds)
  {
    this.selectedViewIds = selectedViewIds;
  }


  @Override
  public String toString()
  {
    return "GetMonthlyBalancesAction{"
           + "startLinxoDate=" + startLinxoDate
           + ", endLinxoDate=" + endLinxoDate
           + ", selectedViewIds=" + selectedViewIds
           + '}';
  }

}
