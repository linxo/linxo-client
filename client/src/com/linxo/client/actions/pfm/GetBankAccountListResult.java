/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 4:49:45 PM by tarunmalhotra.
*/
package com.linxo.client.actions.pfm;

import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.account.BankAccountInfo;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetBankAccountListResult extends SecuredResult
{
  private HashMap<AccountType, ArrayList<BankAccountInfo>> accountsByType;

  @SuppressWarnings("unused")
  private GetBankAccountListResult(){}

  public GetBankAccountListResult(HashMap<AccountType,ArrayList<BankAccountInfo>> accountsByType)
  {
    this.accountsByType = accountsByType;
  }

  public HashMap<AccountType, ArrayList<BankAccountInfo>> getAccountsByType()
  {
    return accountsByType;
  }
}
