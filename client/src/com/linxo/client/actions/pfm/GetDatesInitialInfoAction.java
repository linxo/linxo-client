/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 *
 */
@SuppressWarnings({"DeserializableClassInSecureContext"})
@GwtAction
@JsonAction
public final class GetDatesInitialInfoAction
  extends SecuredAction<GetDatesInitialInfoResult>
{
}
