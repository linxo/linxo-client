/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 11/07/2010 by hugues.
 */
package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.tx.TransactionInfo;

import java.util.ArrayList;

/**
 * This bean returns the result for the "get transactions":
 * - the transactions, including duplicates,
 * - the count of transactions visible and available, including the duplicate transactions,
 * - the debit and credit amounts, excluding the duplicate transactions.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetTransactionsResult extends SecuredResult
{
  private ArrayList<TransactionInfo> transactions;
  private int availableCount;
  private int visibleCount;
  private Double credits;
  private Double debits;
  @Deprecated
  private int totalCount;

  @SuppressWarnings("unused") // used by s11n
  private GetTransactionsResult()
  {}

  public GetTransactionsResult(ArrayList<TransactionInfo> transactions, int visibleCount,
                               int availableCount, Double credits, Double debits)
  {
    this.transactions = transactions;
    this.totalCount = visibleCount;
    this.visibleCount = visibleCount;
    this.availableCount = availableCount;
    this.credits = credits;
    this.debits = debits;
  }

  // Instance Methods


  /**
   * @return the total count of transactions matching the criteria
   * @deprecated use {@link #getAvailableCount() availableCount} instead
   */
  @Deprecated
  public int getTotalCount()
  {
    return totalCount;
  }

  /**
   * @return the count of available transactions matching the criteria (the transactions stored in the database)
   * including the duplicate transactions
   */
  public int getAvailableCount() {
    return availableCount;
  }

  /**
   * @return the count of visible transactions matching the criteria (the transactions stored in the database that are
   * accessible given the current Permission) including the duplicate transactions
   */
  public int getVisibleCount() {
    return visibleCount;
  }

  /**
   * @return The list of transactions associated to the criteria of the action
   */
  public ArrayList<TransactionInfo> getTransactions()
  {
    return transactions;
  }

  /**
   * @return the sum of the amount of the positive transactions
   * matching the criteria excluding the duplicate transactions if the
   * {@link GetTransactionsAction#isIncludeAvailableSums() includeAvailableSums field of the Action}
   * is {@code true}. This fields is {@code null} otherwise.
   */
  public Double getCredits()
  {
    return credits;
  }

  /**
   * @return the sum of the amount of the negative transactions
   * matching the criteria excluding the duplicate transactions if the
   * {@link GetTransactionsAction#isIncludeAvailableSums() includeAvailableSums field of the Action}
   * is {@code true}. This fields is {@code null} otherwise.
   */
  public Double getDebits()
  {
    return debits;
  }

}
