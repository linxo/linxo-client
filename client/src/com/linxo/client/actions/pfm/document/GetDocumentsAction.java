/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.document;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertNotNull;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public class GetDocumentsAction extends SecuredAction<GetDocumentsResult> {

    /**
     * Id a given document provider
     */
    @AssertNotNull
    private final Long accountGroupId;

    /**
     * Start row (used for infinite scroll)
     */
    private final Long startRow;

    /**
     * Number of docs per call
     */
    private final Long numRows;


    public GetDocumentsAction(Long accountGroupId, Long startRow, Long numRows) {
        this.accountGroupId = accountGroupId;
        this.startRow = startRow;
        this.numRows = numRows;
    }

    public Long getAccountGroupId() {
        return accountGroupId;
    }

    public Long getNumRows() {
        return numRows;
    }

    public Long getStartRow() {
        return startRow;
    }

}
