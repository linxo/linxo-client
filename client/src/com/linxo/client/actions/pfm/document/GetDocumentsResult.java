/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.document;

import java.util.List;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.document.Document;

public class GetDocumentsResult extends SecuredResult {

    private final List<Document> documents;

    public GetDocumentsResult(List<Document> documents) {
        this.documents = documents;
    }

    public List<Document> getDocuments() {
        return documents;
    }

}
