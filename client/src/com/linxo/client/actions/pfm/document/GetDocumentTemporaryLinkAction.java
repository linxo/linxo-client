/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.document;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public class GetDocumentTemporaryLinkAction extends SecuredAction<GetDocumentTemporaryLinkResult> {

    private final Long documentId;

    public GetDocumentTemporaryLinkAction(Long documentId, Long documentGroupId) {
        this.documentId = documentId;
    }

    public Long getDocumentId() {
        return documentId;
    }

}
