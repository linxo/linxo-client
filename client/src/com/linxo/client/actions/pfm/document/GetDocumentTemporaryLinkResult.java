/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm.document;

import com.linxo.client.actions.SecuredResult;

public class GetDocumentTemporaryLinkResult extends SecuredResult {

    /**
     * temporary/valid once link to the document. Pointing to a linxo.com/ url which will decrypt
     * and display PDF
     */
    private final String documentURL;

    public GetDocumentTemporaryLinkResult(String documentURL) {
        this.documentURL = documentURL;
    }

    public String getDocumentURL() {
        return documentURL;
    }
}
