/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 11/02/2015 by hugues.
 */
package com.linxo.client.actions.pfm.upcoming;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action requests to detect UpcomingTransactions using the user's history.
 *
 * The result will contain the new UT detected only.
 */
@GwtAction
@JsonAction
public class FindUpcomingTransactionsAction
    extends SecuredAction<FindUpcomingTransactionsResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  /**
   * When {@code true}, requests that the detected Upcoming Transactions are saved
   * before they are returned.
   *
   * When this member is not present in the JSON, it is considered as 'true'.
   * @see com.linxo.client.json.upcoming.FindUpcomingTransactionsActionAdapterFactory
   */
  @SuppressWarnings("JavadocReference")
  private boolean saveDetected;


  /**
   * The id of the account we want to detect upcoming transactions on.
   * If null, we'll try to use the reference.
   */
  private Long accountId;

  /**
   * The reference of the account we want to detect upcoming transactions on.
   * Only used if the account id is null.
   * If both the account id and the reference are null, we'll to detect upcoming
   * transactions on all the Checkings accounts included in the trends.
   */
  @AssertStringSize
  private String accountReference;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private FindUpcomingTransactionsAction(){}

  private FindUpcomingTransactionsAction(Long accountId, String accountReference, boolean saveDetected)
  {
    this.accountId = accountId;
    this.accountReference = accountReference;
    this.saveDetected = saveDetected;
  }

  public FindUpcomingTransactionsAction(boolean saveDetected)
  {
    this(null, null, saveDetected);
  }

  public FindUpcomingTransactionsAction(Long accountId, boolean saveDetected)
  {
    this(accountId, null, saveDetected);
  }

  public FindUpcomingTransactionsAction(String accountReference, boolean saveDetected)
  {
    this(null, accountReference, saveDetected);
  }

  // Instance Methods


  public boolean isSaveDetected()
  {
    return saveDetected;
  }

  public Long getAccountId()
  {
    return accountId;
  }

  public String getAccountReference()
  {
    return accountReference;
  }
}
