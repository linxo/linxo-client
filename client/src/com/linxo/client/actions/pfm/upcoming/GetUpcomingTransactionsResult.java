/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 24/09/2014 by hugues.
 */
package com.linxo.client.actions.pfm.upcoming;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.upcoming.UpcomingTransaction;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * LinxoResult for the GetUpcomingTransactionsAction.
 */
public class GetUpcomingTransactionsResult
    extends SecuredResult
{
  /**
   * Map of (accountId -> list of upcoming transactions with their modified occurrences)
   */
  private HashMap<Long, ArrayList<UpcomingTransaction>> transactions;

  protected GetUpcomingTransactionsResult(){}

  public GetUpcomingTransactionsResult(HashMap<Long, ArrayList<UpcomingTransaction>> transactions)
  {
    this.transactions = transactions;
  }

  /**
   * Gets an &lt;accountId, upcomings&gt; map.
   *
   * @return An &lt;accountId, upcomings&gt; map.
   */
  public HashMap<Long, ArrayList<UpcomingTransaction>> getTransactions()
  {
    return transactions;
  }


  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("GetUpcomingTransactionsResult{");
    sb.append(super.toString());
    sb.append(", transactions=").append(transactions);
    sb.append('}');
    return sb.toString();
  }

}
