/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 24/09/2014 by hugues.
 */
package com.linxo.client.actions.pfm.upcoming;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@JsonAction
@GwtAction
public final class DeleteTransactionOccurrenceAction
  extends SecuredAction<DeleteTransactionOccurrenceResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  /**
   * id of the occurrence to delete
   */
  private Long id;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private DeleteTransactionOccurrenceAction()
  {}

  /**
   *
   * @param id - the id of the occurrence to remove
   */
  public DeleteTransactionOccurrenceAction(Long id)
  {
    this.id = id;
  }


  // Instance Methods


  public Long getId()
  {
    return id;
  }

}
