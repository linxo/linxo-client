/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 24/09/2014 by hugues.
 */
package com.linxo.client.actions.pfm.upcoming;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action returns the UpcomingTransaction of the a given account
 * if the account id or the reference is not null (we look for the
 * account id first, then the reference), or from ALL the accounts
 * if the accountId is <code>null</code>.
 *
 * This class also serves as a base class to collect UpcomingTransactions
 * (like in the detection).
 *
 * @see GetUpcomingTransactionsResult
 */
@JsonAction
@GwtAction
public class GetUpcomingTransactionsAction
  extends SecuredAction<GetUpcomingTransactionsResult>
{
  /**
   * The id of the account we want to get upcoming transactions.
   * If null, we'll use the reference.
   */
  private Long accountId;

  /**
   * The reference of the account we want to get upcoming transactions.
   * Only used if the account id is null.
   */
  @AssertStringSize
  private String reference;

  @SuppressWarnings("UnusedDeclaration")
  protected GetUpcomingTransactionsAction()
  {} // s11n

  public GetUpcomingTransactionsAction(final Long accountId)
  {
    this.accountId = accountId;
  }

  @SuppressWarnings("UnusedDeclaration")
  public GetUpcomingTransactionsAction(final String reference)
  {
    this.reference = reference;
  }

  /**
   * Gets the id of the account we want to get upcoming transactions.
   * If null, we'll use the reference.
   *
   * Both the account id and the reference may be null. In that case we
   * get the upcoming transactions for all the user's accounts.
   *
   * @return The id of the account we want to get upcoming transactions.
   *         If null, we'll use the reference.
   */
  public Long getAccountId()
  {
    return accountId;
  }

  /**
   * Gets the reference of the account we want to get upcoming transactions.
   * Used only if the account id is null.
   *
   * Both the account id and the reference may be null. In that case we
   * get the upcoming transactions for all the user's accounts.
   *
   * @return The reference of the account we want to get upcoming transactions.
   *         Used only if the account id is null.
   */
  public String getReference()
  {
    return reference;
  }

}
