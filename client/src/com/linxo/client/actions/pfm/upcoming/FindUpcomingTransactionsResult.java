/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 11/02/2015 by hugues.
 */
package com.linxo.client.actions.pfm.upcoming;

import com.linxo.client.dto.upcoming.UpcomingTransaction;

import java.util.ArrayList;
import java.util.HashMap;

public class FindUpcomingTransactionsResult
    extends GetUpcomingTransactionsResult
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  protected FindUpcomingTransactionsResult(){}

  public FindUpcomingTransactionsResult(HashMap<Long, ArrayList<UpcomingTransaction>> transactions)
  {
    super(transactions);
  }


  // Instance Methods

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("FindUpcomingTransactionsResult{");
    sb.append(super.toString());
    sb.append('}');
    return sb.toString();
  }
}
