/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 24/09/2014 by hugues.
 */
package com.linxo.client.actions.pfm.upcoming;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.upcoming.UpcomingTransaction;

/**
 * Creates a new upcoming transaction.
 *
 * This action will return an error if the account or category referenced in
 * the new transaction does not exists or does not belong to the currently
 * logged in user.
 *
 * This action will return an error if the label of the transaction is null
 * or the empty string.
 *
 * This action will also persist the possibly attached modified occurrences.
 * Use {@link AddTransactionOccurrenceAction} to add these objects after
 * the Upcoming Transaction is created and has received an id.
 */
@JsonAction
@GwtAction
public final class AddUpcomingTransactionAction
  extends SecuredAction<AddUpcomingTransactionResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private UpcomingTransaction upcomingTransaction;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private AddUpcomingTransactionAction()
  {}

  public AddUpcomingTransactionAction(UpcomingTransaction upcomingTransaction)
  {
    this.upcomingTransaction = upcomingTransaction;
  }

  // Instance Methods


  public UpcomingTransaction getUpcomingTransaction()
  {
    return upcomingTransaction;
  }
}
