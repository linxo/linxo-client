/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 24/09/2014 by hugues.
 */
package com.linxo.client.actions.pfm.upcoming;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.upcoming.UpcomingTransactionOccurrence;

/**
 * Attaches a new modified occurrence to the upcoming transaction referenced.
 * This action will return an error if an attempt to add an occurrence for
 * a date where a modified occurrence already exists.
 *
 * This action will also return an error if you try to add an occurrence to
 * an UT that does not exist or does not belong to you, or if the account
 * or category referenced in the new occurrence does not exists or does not
 * belong to the currently logged in user.
 *
 * This action will return an error if the label of the transaction is null
 * or the empty string.
 */
@JsonAction
@GwtAction
public final class AddTransactionOccurrenceAction
  extends SecuredAction<AddTransactionOccurrenceResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  /**
   * The id of the {@link com.linxo.client.dto.upcoming.UpcomingTransaction} the
   * occurrence is attached.
   */
  private Long id;

  /**
   * The new occurrence to save.
   */
  private UpcomingTransactionOccurrence occurrence;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused")
  private AddTransactionOccurrenceAction()
  {}

  public AddTransactionOccurrenceAction(Long id, UpcomingTransactionOccurrence occurrence)
  {
    this.id = id;
    this.occurrence = occurrence;
  }


  // Instance Methods

  public Long getId()
  {
    return id;
  }

  public UpcomingTransactionOccurrence getOccurrence()
  {
    return occurrence;
  }

}
