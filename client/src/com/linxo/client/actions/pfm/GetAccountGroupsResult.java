/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.account.AccountGroupInfo;

import java.util.ArrayList;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetAccountGroupsResult
  extends SecuredResult
{
  private ArrayList<AccountGroupInfo> accountGroups;

  @SuppressWarnings("unused")
  private GetAccountGroupsResult() {}

  public GetAccountGroupsResult(final ArrayList<AccountGroupInfo> accountGroups)
  {
    this.accountGroups = accountGroups;
  }

  public ArrayList<AccountGroupInfo> getAccountGroups()
  {
    return accountGroups;
  }

}
