/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 11/07/2010 by hugues.
 */
package com.linxo.client.actions.pfm;

import com.linxo.client.data.CategoryType;
import com.linxo.client.data.ValidationConstants;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.ArrayList;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class GetTransactionsAction
    extends SecuredAction<GetTransactionsResult>
{
  private AccountType accountType;
  private ArrayList<AccountType> accountTypes;
  private Long accountId;

  @AssertStringSize
  private String accountReference;

  private ArrayList<String> labels;
  private Long categoryId;
  private Long tagId;

  @AssertStringSize(ValidationConstants.TRANSACTION_SEARCH_DATE_MAX_LENGTH)
  private String startDate;

  @AssertStringSize(ValidationConstants.TRANSACTION_SEARCH_DATE_MAX_LENGTH)
  private String endDate;

  @AssertStringSize(ValidationConstants.TRANSACTION_SEARCH_DATE_MAX_LENGTH)
  private String debitDate;
  private Double amount;
  private Double minAmount;
  private Double maxAmount;
  private boolean useBudgetDate;
  private boolean loadSubCategories=false;
  private boolean loadOnlyTaggedTransactions =false;
  private CategoryType categoryType;

  private Boolean excludeDuplicates;

  @Deprecated
  private Boolean useTrendsAccounts;

  /**
   * Whether we should get the transactions only from the accounts with includedInBudget = true.
   */
  private Boolean useBudgetAccounts;

  /**
   * Whether we should get the transactions only from the accounts with includedInSavings = true.
   */
  private Boolean useSavingsAccounts;

  /**
   * Whether we should get the transactions only from the accounts with includedInAssets = true.
   */
  private Boolean useAssetsAccounts;

  /**
   * The ids of the currently selected views ({@link com.linxo.client.dto.account.PersonalizedView}).
   * If accountId or accountReference is not null (or both), this field should be null. Else if
   * this field is null, all the accounts will be included. If not null, it should not be empty.
   */
  private ArrayList<Long> selectedViewIds;

  @AssertStringSize
  private String extCustomType;

  private boolean includeAvailableSums = false;

  // Pagination
  private int startRow;
  private int numRows;

  // account group id
  private Long accountGroupId;
  private Long importDate;

  public GetTransactionsAction() {}

  /**
   * Queries the transactions of the current user based on the given filters.
   *
   * @param accountType - filters transactions that are on the given accountType
   * @param accountId - filters transactions that are on the account represented by this id
   * @param labels - filter on the transactions that match the given tokens. The token may be on the label, note of check number of the transaction.
   * @param categoryId - filters transactions that are assigned the category with the given id
   * @param tagId - filters transactions that are assigned the tag with the given id
   * @param useTrendsAccounts - filters transactions that are on an account selected for the Trends (Graphics)
   * @param startRow - The row number of the first transaction to load (paging)
   * @param numRows - the max number of rows to return (paging)
   */
  @Deprecated
  public GetTransactionsAction(final AccountType accountType, final Long accountId,
                               final ArrayList<String> labels, final Long categoryId,
                               final Long tagId, final boolean useTrendsAccounts,
                               final int startRow, final int numRows)
  {
    this(
        accountType,
        accountId,
        null,               //accountReference
        labels,
        null,               // startDate
        null,               // endDate
        null,               // debitDate
        null,               // amount
        null,               // minAmount
        null,               // maxAmount
        false,              // useBudgetDate
        categoryId,
        false,              // loadSubCategories
        null,               // categoryType
        tagId,              // tagId
        false,              // loadOnlyTaggedTransactions
        useTrendsAccounts,
        null,               // extCustomType
        null,               // includeAvailableSums
        null,
        null,
        startRow,
        numRows
    );
  }

  /**
   * Queries the transactions of the current user based on the given filters.
   *
   * @param accountType - filters transactions that are on the given accountType
   * @param accountId - filters transactions that are on the account represented by this id
   * @param accountReference - filters transactions that are on the account represented by this reference. Can be used instead of the account id. Use null if you do not want to use this field.
   * @param labels - filter on the transactions that match the given tokens. The token may be on the label, note of check number of the transaction.
   * @param startDate - date format "M/d/yy", inclusive
   * @param endDate - date format "M/d/yy", inclusive
   * @param debitDate - date format "M/d/yy", exact match. Use null if you do not want to use this field.
   * @param amount - exact match. Use null if you do not want to use this field. Using 0 will search for transactions with amount 0.
   * @param minAmount - min amount, inclusive. Use null if you do not want to use this field. Using 0 will search for transactions with amount &gt;= 0.
   * @param maxAmount - max amount, inclusive. Use null if you do not want to use this field. Using 0 will search for transactions with amount &lt;= 0.
   * @param useBudgetDate - indicates if the date filters should be taken on the {@link com.linxo.client.dto.tx.TransactionInfo#budgetDate budget date}rather that the {@link com.linxo.client.dto.tx.TransactionInfo#date}
   * @param categoryId - filters transactions that are assigned the category with the given id
   * @param loadSubCats - indicates if we should also load transactions attached to the sub-categories when filtering on categories
   * @param categoryType - filters transactions that are assigned a category with the given categoryType
   * @param tagId - filters transactions that are assigned the tag with the given id
   * @param loadOnlyTaggedTransactions - when true, we load only transactions that are assigned at least one tag.
   * @param useTrendsAccounts - filters transactions that are on an account selected for the Trends (Graphics)
   * @param startRow - The row number of the first transaction to load (paging)
   * @param numRows - the max number of rows to return (paging)
   * @param extCustomType - filter transactions that have this extCustomType. Use null if you do not want to use this field.
   */
  @Deprecated
  public GetTransactionsAction(final AccountType accountType, final Long accountId,
                               final String accountReference, final ArrayList<String> labels,
                               final String startDate, final String endDate,
                               final String debitDate, final Double amount,
                               final Double minAmount, final Double maxAmount,
                               final boolean useBudgetDate, final Long categoryId,
                               final boolean loadSubCats, final CategoryType categoryType,
                               final Long tagId, final boolean loadOnlyTaggedTransactions,
                               final boolean useTrendsAccounts,
                               final int startRow, final int numRows,
                               final String extCustomType)
  {
    this(
        accountType,
        accountId,
        accountReference,
        labels,
        startDate,
        endDate,
        debitDate,
        amount,
        minAmount,
        maxAmount,
        useBudgetDate,
        categoryId,
        loadSubCats,
        categoryType,
        tagId,
        loadOnlyTaggedTransactions,
        useTrendsAccounts,
        extCustomType,
        null,               // includeAvailableSums
        null,
        null,
        startRow,
        numRows
    );
  }


  /**
   * Queries the transactions of the current user based on the given filters.
   * <p>
   * <strong>Note:</strong> This constructor will receive additional parameters as the
   * API evolves. If you require compatibility, use another constructor.
   * <p>
   * <strong>Note2:</strong> Paging parameters will always be at the end of the list of parameters.
   *
   * @param accountType - filters transactions that are on the given accountType
   * @param accountId - filters transactions that are on the account represented by this id
   * @param accountReference - filters transactions that are on the account represented by this reference. Can be used instead of the account id. Use null if you do not want to use this field.
   * @param labels - filter on the transactions that match the given tokens. The token may be on the label, note of check number of the transaction.
   * @param startDate - date format "M/d/yy", inclusive
   * @param endDate - date format "M/d/yy", inclusive
   * @param debitDate - date format "M/d/yy", exact match. Use null if you do not want to use this field.
   * @param amount - exact match. Use null if you do not want to use this field. Using 0 will search for transactions with amount 0.
   * @param minAmount - min amount, inclusive. Use null if you do not want to use this field. Using 0 will search for transactions with amount &gt;= 0.
   * @param maxAmount - max amount, inclusive. Use null if you do not want to use this field. Using 0 will search for transactions with amount &lt;= 0.
   * @param useBudgetDate - indicates if the date filters should be taken on the {@link com.linxo.client.dto.tx.TransactionInfo#budgetDate budget date}rather that the {@link com.linxo.client.dto.tx.TransactionInfo#date}
   * @param categoryId - filters transactions that are assigned the category with the given id
   * @param loadSubCats - indicates if we should also load transactions attached to the sub-categories when filtering on categories
   * @param categoryType - filters transactions that are assigned a category with the given categoryType
   * @param tagId - filters transactions that are assigned the tag with the given id
   * @param loadOnlyTaggedTransactions - when true, we load only transactions that are assigned at least one tag.
   * @param useTrendsAccounts - filters transactions that are on an account selected for the Trends (Graphics)
   * @param extCustomType - filter transactions that have this extCustomType. Use null if you do not want to use this field.
   * @param includeAvailableSums - indicate whether or not the result must contains the available sums
   * @param startRow - The row number of the first transaction to load (paging)
   * @param numRows - the max number of rows to return (paging)
   * @param accountGroupId - filters transactions that are on the account group represented by this id
   * @param importDate - number timestamp in seconds. Use null if you do not want to use this field.
   */
  @Deprecated
  public GetTransactionsAction(final AccountType accountType, final Long accountId,
                               final String accountReference, final ArrayList<String> labels,
                               final String startDate, final String endDate,
                               final String debitDate, final Double amount,
                               final Double minAmount, final Double maxAmount,
                               final boolean useBudgetDate, final Long categoryId,
                               final boolean loadSubCats, final CategoryType categoryType,
                               final Long tagId, final boolean loadOnlyTaggedTransactions,
                               final boolean useTrendsAccounts,
                               final String extCustomType,
                               final Boolean includeAvailableSums,
                               final Long accountGroupId, final Long importDate,
                               final int startRow, final int numRows
                               )
  {
    this.accountType = accountType;
    this.accountId = accountId;
    this.accountReference = accountReference;
    this.labels = labels;
    this.categoryId = categoryId;
    this.tagId = tagId;
    this.startRow = startRow;
    this.numRows = numRows;
    this.startDate = startDate;
    this.endDate = endDate;
    this.debitDate = debitDate;
    this.amount = amount;
    this.minAmount = minAmount;
    this.maxAmount = maxAmount;
    this.useBudgetDate = useBudgetDate;
    this.loadSubCategories = loadSubCats;
    this.loadOnlyTaggedTransactions = loadOnlyTaggedTransactions;
    this.categoryType = categoryType;
    this.useTrendsAccounts = useTrendsAccounts;
    this.extCustomType = extCustomType;
    this.accountGroupId = accountGroupId;
    this.importDate = importDate;

    if(includeAvailableSums != null) this.includeAvailableSums = includeAvailableSums; // /!\ unboxing
  }

  // Instance Methods


  public Long getAccountId()
  {
    return accountId;
  }

  public void setAccountId(Long accountId)
  {
    this.accountId = accountId;
  }

  public String getAccountReference()
  {
    return accountReference;
  }

  public void setAccountReference(String accountReference)
  {
    this.accountReference = accountReference;
  }

  public AccountType getAccountType()
  {
    return accountType;
  }

  public void setAccountType(AccountType accountType)
  {
    this.accountType = accountType;
  }

  public ArrayList<AccountType> getAccountTypes()
  {
    return accountTypes;
  }

  public void setAccountTypes(ArrayList<AccountType> accountTypes)
  {
    this.accountTypes = accountTypes;
  }

  public ArrayList<String> getLabels()
  {
    return labels;
  }

  public void setLabels(ArrayList<String> labels)
  {
    this.labels = labels;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }

  public void setCategoryId(Long categoryId)
  {
    this.categoryId = categoryId;
  }

  public Long getTagId()
  {
    return tagId;
  }

  public void setTagId(Long tagId)
  {
    this.tagId = tagId;
  }

  public int getNumRows()
  {
    return numRows;
  }

  public void setNumRows(int numRows)
  {
    this.numRows = numRows;
  }

  public int getStartRow()
  {
    return startRow;
  }

  public void setStartRow(int startRow)
  {
    this.startRow = startRow;
  }

  public String getStartDate()
  {
    return startDate;
  }

  public void setStartDate(String startDate)
  {
    this.startDate = startDate;
  }

  public String getEndDate()
  {
    return endDate;
  }

  public void setEndDate(String endDate)
  {
    this.endDate = endDate;
  }

  public boolean isUseBudgetDate()
  {
    return useBudgetDate;
  }

  public void setUseBudgetDate(boolean useBudgetDate)
  {
    this.useBudgetDate = useBudgetDate;
  }

  @Deprecated
  public boolean isUseTrendsAccounts()
  {
    return useTrendsAccounts == null ? false : useTrendsAccounts;
  }

  @Deprecated
  public void setUseTrendsAccounts(Boolean useTrendsAccounts)
  {
    this.useTrendsAccounts = useTrendsAccounts;
  }

  public boolean isLoadSubCategories()
  {
    return loadSubCategories;
  }

  public void setLoadSubCategories(boolean loadSubCategories)
  {
    this.loadSubCategories = loadSubCategories;
  }

  public boolean isLoadOnlyTaggedTransactions()
  {
    return loadOnlyTaggedTransactions;
  }

  public void setLoadOnlyTaggedTransactions(boolean loadOnlyTaggedTransactions)
  {
    this.loadOnlyTaggedTransactions = loadOnlyTaggedTransactions;
  }

  public CategoryType getCategoryType()
  {
    return categoryType;
  }

  public void setCategoryType(CategoryType categoryType)
  {
    this.categoryType = categoryType;
  }

  public String getDebitDate()
  {
    return debitDate;
  }

  public void setDebitDate(String debitDate)
  {
    this.debitDate = debitDate;
  }

  public Double getAmount()
  {
    return amount;
  }

  public void setAmount(Double amount)
  {
    this.amount = amount;
  }

  public Double getMinAmount()
  {
    return minAmount;
  }

  public void setMinAmount(Double minAmount)
  {
    this.minAmount = minAmount;
  }

  public Double getMaxAmount()
  {
    return maxAmount;
  }

  public void setMaxAmount(Double maxAmount)
  {
    this.maxAmount = maxAmount;
  }

  public String getExtCustomType()
  {
    return extCustomType;
  }

  public void setExtCustomType(String extCustomType)
  {
    this.extCustomType = extCustomType;
  }

  public boolean isIncludeAvailableSums()
  {
    return includeAvailableSums;
  }

  public void setIncludeAvailableSums(boolean includeAvailableSums)
  {
    this.includeAvailableSums = includeAvailableSums;
  }

  public ArrayList<Long> getSelectedViewIds()
  {
    return selectedViewIds;
  }

  public void setSelectedViewIds(ArrayList<Long> selectedViewIds)
  {
    this.selectedViewIds = selectedViewIds;
  }

  public Boolean getUseBudgetAccounts()
  {
    return useBudgetAccounts;
  }

  public void setUseBudgetAccounts(Boolean useBudgetAccounts)
  {
    this.useBudgetAccounts = useBudgetAccounts;
  }

  public Boolean getUseSavingsAccounts()
  {
    return useSavingsAccounts;
  }

  public void setUseSavingsAccounts(Boolean useSavingsAccounts)
  {
    this.useSavingsAccounts = useSavingsAccounts;
  }

  public Boolean getUseAssetsAccounts()
  {
    return useAssetsAccounts;
  }

  public void setUseAssetsAccounts(Boolean useAssetsAccounts)
  {
    this.useAssetsAccounts = useAssetsAccounts;
  }

  public Long getAccountGroupId() {
    return accountGroupId;
  }

  public void setAccountGroupId(Long accountGroupId) {
    this.accountGroupId = accountGroupId;
  }

  public Long getImportDate() {
    return importDate;
  }

  public void setImportDate(Long importDate) {
    this.importDate = importDate;
  }

  @Override
  public String toString()
  {
    return "GetTransactionsAction{"
           + "accountId=" + accountId
           + ", accountReference=" + accountReference
           + ", accountType=" + accountType
           + ", accountTypes=" + accountTypes
           + ", labels=" + labels
           + ", categoryId=" + categoryId
           + ", tagId=" + tagId
           + ", startDate=" + startDate
           + ", endDate=" + endDate
           + ", debitDate=" + debitDate
           + ", amount=" + amount
           + ", minAmount=" + minAmount
           + ", maxAmount=" + maxAmount
           + ", useBudgetDate=" + useBudgetDate
           + ", loadSubCategories=" + loadSubCategories
           + ", loadOnlyTaggedTransactions=" + loadOnlyTaggedTransactions
           + ", categoryType=" + categoryType
           + ", selectedViewIds=" + selectedViewIds
           + ", useTrendsAccounts=" + useTrendsAccounts
           + ", useBudgetAccounts=" + useBudgetAccounts
           + ", useSavingsAccounts=" + useSavingsAccounts
           + ", useAssetsAccounts=" + useAssetsAccounts
           + ", extCustomType=" + extCustomType
           + ", includeAvailableSums=" + includeAvailableSums
           + ", startRow=" + startRow
           + ", numRow=" + numRows
           + ", accountGroupId=" + accountGroupId
           + ", importDate=" + importDate
           + "}";
  }

}
