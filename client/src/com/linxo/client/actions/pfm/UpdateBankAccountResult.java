/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@Deprecated
public final class UpdateBankAccountResult
  extends SecuredResult
{
  public enum Status
  {
    Success,
    BadParameterFailure
  }

  private Status status;

  @SuppressWarnings("unused") // s11n
  private UpdateBankAccountResult() {}

  public UpdateBankAccountResult(final Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

}
