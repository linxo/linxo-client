/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 20/04/2011 by hugues.
 */
package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.dto.sync.FinancialInstitutionInfo;

@GwtAction
public final class CreateAccountGroupAction
    extends SecuredAction<CreateAccountGroupResult>
{


  // Instance Methods// Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  @AssertStringSize(nullable = false, voidable = false)
  private String name;

  @AssertStringSize
  private String bankUrl;

  @AssertStringSize
  private String bankLogoUrl;

  private Long fid;
  private Double balance;

  @SuppressWarnings("unused")
  // s11n
  private CreateAccountGroupAction()
  {
  }

  // Constructors
  public CreateAccountGroupAction(String name, String bankUrl, String bankLogoUrl, Long fid, Double balance)
  {
    this.name = name;
    this.bankUrl = bankUrl;
    this.bankLogoUrl = bankLogoUrl;
    this.fid = fid;
    this.balance = balance;
  }

  public String getBankLogoUrl()
  {
    return bankLogoUrl;
  }

  public String getBankUrl()
  {
    return bankUrl;
  }

  public Long getFid()
  {
    return fid;
  }

  public String getName()
  {
    return name;
  }

  public Double getBalance()
  {
    return balance;
  }
}
