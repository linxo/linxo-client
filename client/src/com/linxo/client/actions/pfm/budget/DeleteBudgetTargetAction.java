/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2015 by hugues.
 */
package com.linxo.client.actions.pfm.budget;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * An action to delete a budget target.
 *
 * If the given id does not respect the requirements given in
 * the {@link com.linxo.client.actions.services.BudgetService#deleteBudgetTarget(long) service method}
 * this LinxoAction will result in an {@code ErrorResult}.
 */
@JsonAction
@GwtAction
public class DeleteBudgetTargetAction
    extends SecuredAction<DeleteBudgetTargetResult>
{
  /**
   * The id of the budget target to delete.
   */
  private long budgetTargetId;


  @SuppressWarnings("unused") // s11n
  private DeleteBudgetTargetAction() {}

  public DeleteBudgetTargetAction(final long budgetTargetId)
  {
    this.budgetTargetId = budgetTargetId;
  }

  public final long getBudgetTargetId()
  {
    return budgetTargetId;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("DeleteBudgetTargetAction{");
    sb.append("super=").append(super.toString());
    sb.append(", budgetTargetId=").append(budgetTargetId);
    sb.append('}');
    return sb.toString();
  }
}
