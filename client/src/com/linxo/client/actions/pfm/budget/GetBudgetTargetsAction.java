/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2015 by hugues.
 */
package com.linxo.client.actions.pfm.budget;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * LinxoAction to get the budget targets defined for a given account
 * or for all the accounts included in the trends.
 *
 * @see com.linxo.client.actions.services.BudgetService#getBudgetTargets(Long)
 */
@JsonAction
@GwtAction
public class GetBudgetTargetsAction
    extends SecuredAction<GetBudgetTargetsResult>
{
  /**
   * The id of the account we want to get budget targets.
   * If null, we'll try to use the reference.
   */
  private Long accountId;

  /**
   * The reference of the account we want to get budget targets.
   * Only used if the account id is null.
   * If both the account id and the reference are null, we'll get
   * the budget targets for all the accounts included in the trends.
   */
  @AssertStringSize
  private String accountReference;


  public GetBudgetTargetsAction()
  {
    this(null, null);
  }

  private GetBudgetTargetsAction(final Long accountId, final String accountReference)
  {
    this.accountId = accountId;
    this.accountReference = accountReference;
  }

  public GetBudgetTargetsAction(final Long accountId)
  {
    this(accountId, null);
  }

  public GetBudgetTargetsAction(final String accountReference)
  {
    this(null, accountReference);
  }

  public final Long getAccountId()
  {
    return accountId;
  }

  public final String getAccountReference()
  {
    return accountReference;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("GetBudgetTargetsAction{");
    sb.append("super=").append(super.toString());
    sb.append(", accountId=").append(accountId);
    sb.append(", accountReference='").append(accountReference).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
