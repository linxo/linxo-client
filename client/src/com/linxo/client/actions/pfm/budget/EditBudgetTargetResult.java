/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2015 by hugues.
 */
package com.linxo.client.actions.pfm.budget;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.budget.BudgetTarget;

/**
 * LinxoResult of the {@link EditBudgetTargetAction}.
 */
public class EditBudgetTargetResult
    extends SecuredResult
{
  /**
   * The updated budget target.
   */
  private BudgetTarget budgetTarget;


  @SuppressWarnings("unused") // s11n
  private EditBudgetTargetResult() {}

  public EditBudgetTargetResult(final BudgetTarget budgetTarget)
  {
    this.budgetTarget = budgetTarget;
  }

  public final BudgetTarget getBudgetTarget()
  {
    return budgetTarget;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("EditBudgetTargetResult{");
    sb.append("super=").append(super.toString());
    sb.append(", budgetTarget=").append(budgetTarget);
    sb.append('}');
    return sb.toString();
  }
}
