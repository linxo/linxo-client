/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2015 by hugues.
 */
package com.linxo.client.actions.pfm.budget;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.upcoming.IntervalUnit;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * An action to get a budget target suggestion for a given category (including
 * or not subcategories), for a given account (or for all the accounts included
 * in the trends) interval unit, interval value and start date.
 */
@JsonAction
@GwtAction
public class GetBudgetTargetSuggestionAction
    extends SecuredAction<GetBudgetTargetSuggestionResult>
{
  /**
   * The id of the account the budget target suggestion we want to get is for.
   * If null, the suggestion will be for all the accounts included in the trends.
   */
  private Long accountId;

  /**
   * Can be used instead of the account id.
   */
  @AssertStringSize
  private String accountReference;

  /**
   * The id of the category we want to get a budget target suggestion for.
   */
  private long categoryId;

  /**
   * Whether the budget target suggestion we want to get is for
   * the category only or the category and its subcategories.
   */
  private boolean includeSubCategories;

  /**
   * Interval unit for the budget target suggestion we want to get.
   * (We can get a 3-months or a 7-days suggestion for instance.)
   */
  private IntervalUnit intervalUnit;

  /**
   * Interval value for the budget target suggestion we want to get.
   * (We can get a 3-months or a 7-days suggestion for instance.)
   */
  private int intervalValue;

  /**
   * Start date for the interval value and unit. That's the starting
   * point to calculate the budget target suggestion. (So it defines
   * when a 3-months or a 7-days suggestion starts.)
   */
  private LinxoDate startDate;


  @SuppressWarnings("unused") // s11n
  private GetBudgetTargetSuggestionAction() {}

  public GetBudgetTargetSuggestionAction(final Long accountId,
                                         final long categoryId, final boolean includeSubCategories,
                                         final IntervalUnit intervalUnit, final int intervalValue,
                                         final LinxoDate startDate)
  {
    this.accountId = accountId;
    this.accountReference = null;

    this.categoryId = categoryId;
    this.includeSubCategories = includeSubCategories;

    this.intervalUnit = intervalUnit;
    this.intervalValue = intervalValue;
    this.startDate = startDate;
  }

  public GetBudgetTargetSuggestionAction(final String accountReference,
                                         final long categoryId, final boolean includeSubCategories,
                                         final IntervalUnit intervalUnit, final int intervalValue,
                                         final LinxoDate startDate)
  {
    this.accountId = null;
    this.accountReference = accountReference;

    this.categoryId = categoryId;
    this.includeSubCategories = includeSubCategories;

    this.intervalUnit = intervalUnit;
    this.intervalValue = intervalValue;
    this.startDate = startDate;
  }

  public final Long getAccountId()
  {
    return accountId;
  }

  public final String getAccountReference()
  {
    return accountReference;
  }

  public final long getCategoryId()
  {
    return categoryId;
  }

  public final boolean isIncludeSubCategories()
  {
    return includeSubCategories;
  }

  public final IntervalUnit getIntervalUnit()
  {
    return intervalUnit;
  }

  public final int getIntervalValue()
  {
    return intervalValue;
  }

  public final LinxoDate getStartDate()
  {
    return startDate;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("GetBudgetTargetSuggestionAction{");
    sb.append("super=").append(super.toString());
    sb.append(", accountId=").append(accountId);
    sb.append(", accountReference='").append(accountReference).append('\'');
    sb.append(", categoryId=").append(categoryId);
    sb.append(", includeSubCategories=").append(includeSubCategories);
    sb.append(", intervalUnit=").append(intervalUnit);
    sb.append(", intervalValue=").append(intervalValue);
    sb.append(", startDate=").append(startDate);
    sb.append('}');
    return sb.toString();
  }
}
