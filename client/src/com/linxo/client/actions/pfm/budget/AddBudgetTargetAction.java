/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2015 by hugues.
 */
package com.linxo.client.actions.pfm.budget;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.budget.BudgetTarget;

/**
 * An action to add a budget target. You can use the GetBudgetTargetSuggestionAction
 * to get a budget target suggestion first. The BudgetTarget being added has no id yet.
 *
 * If the BudgetTarget in the action does not respect the requirements of the
 * {@link com.linxo.client.actions.services.BudgetService#addBudgetTarget(BudgetTarget) service method}
 * definition, this action will return an {@code ErrorResult}.
 */
@JsonAction
@GwtAction
public class AddBudgetTargetAction
    extends SecuredAction<AddBudgetTargetResult>
{
  /**
   * The budget target to add. It has no id yet.
   */
  private BudgetTarget budgetTarget;


  @SuppressWarnings("unused") // s11n
  private AddBudgetTargetAction() {}

  /**
   * Creates an LinxoAction to add a budget target.
   * @param budgetTarget - the budget target to add
   * @throws java.lang.NullPointerException if the budget is {@code null}.
   * @see com.linxo.client.actions.services.BudgetService#addBudgetTarget(BudgetTarget)
   */
  public AddBudgetTargetAction(final BudgetTarget budgetTarget) throws NullPointerException
  {
    if(budgetTarget == null) throw new NullPointerException("budgetTarget is null");
    this.budgetTarget = budgetTarget;
  }


  public final BudgetTarget getBudgetTarget()
  {
    return budgetTarget;
  }


  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("AddBudgetTargetAction{");
    sb.append("super=").append(super.toString());
    sb.append(", budgetTarget=").append(budgetTarget);
    sb.append('}');
    return sb.toString();
  }
}
