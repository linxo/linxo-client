/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2015 by hugues.
 */
package com.linxo.client.actions.pfm.budget;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.budget.BudgetTarget;

/**
 * An action to get history for a given budget target, for all the budget targets
 * of an account or for all the budget targets of the user.
 *
 * The given budget target may or may not have an id (it may or may not have already been saved).
 *
 * A budget history is a couple (date, budget value) where:
 * <ul>
 *    <li>
 *      The date is the first day of the period covered and is calculated based
 *      on the budget target interval unit, interval value and start date.
 *   </li>
 *   <li>
 *      The budget value is the sum of the transaction amounts on the period
 *      covered.
 *   </li>
 * </ul>
 * @see com.linxo.client.actions.services.BudgetService#getBudgetHistory(com.linxo.client.dto.budget.BudgetTarget, int)
 */
@JsonAction
@GwtAction
public class GetBudgetHistoryAction
    extends SecuredAction<GetBudgetHistoryResult>
{
  /**
   * The budget target we want to get history for. It may or may
   * not have an id (it may or may not have already been saved).
   * If null, we'll look at the account id and/or account reference.
   */
  private BudgetTarget budgetTarget;

  /**
   * The id of the account we want to get history for (for all its budget targets).
   * If null, we'll look at the account reference.
   */
  private Long accountId;

  /**
   * The reference of the account we want to get history for (for all its budget targets).
   * If null, we'll get history for all the user's budget targets.
   */
  @AssertStringSize
  private String accountReference;

  /**
   * The number of budget history we want to get.
   */
  private int historyCount;


  @SuppressWarnings("unused") // s11n
  private GetBudgetHistoryAction() {}

  public GetBudgetHistoryAction(final int historyCount)
  {
    this.historyCount = historyCount;
  }

  public GetBudgetHistoryAction(final BudgetTarget budgetTarget, final int historyCount)
  {
    this.budgetTarget = budgetTarget;
    this.historyCount = historyCount;
  }

  public GetBudgetHistoryAction(final Long accountId, final int historyCount)
  {
    this.accountId = accountId;
    this.historyCount = historyCount;
  }

  public GetBudgetHistoryAction(final String accountReference, final int historyCount)
  {
    this.accountReference = accountReference;
    this.historyCount = historyCount;
  }

  public final BudgetTarget getBudgetTarget()
  {
    return budgetTarget;
  }

  public final Long getAccountId()
  {
    return accountId;
  }

  public final String getAccountReference()
  {
    return accountReference;
  }

  public final int getHistoryCount()
  {
    return historyCount;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("GetBudgetHistoryAction{");
    sb.append("super=").append(super.toString());
    sb.append(", accountId=").append(accountId);
    sb.append(", budgetTarget=").append(budgetTarget);
    sb.append(", accountReference='").append(accountReference).append('\'');
    sb.append(", historyCount=").append(historyCount);
    sb.append('}');
    return sb.toString();
  }
}
