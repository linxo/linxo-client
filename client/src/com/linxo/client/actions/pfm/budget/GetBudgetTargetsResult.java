/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2015 by hugues.
 */
package com.linxo.client.actions.pfm.budget;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.budget.BudgetTarget;

import java.util.ArrayList;

/**
 * LinxoResult of the {@link GetBudgetTargetsAction}. It holds the list of all the
 * budget targets defined for the requested account or for all the
 * accounts included in the trends (based on the action parameters).
 *
 * If the {@link GetBudgetTargetsAction} was requested for a given account,
 * the result will also contain the BudgetTarget that apply to all the
 * accounts.
 */
public class GetBudgetTargetsResult
    extends SecuredResult
{
  private ArrayList<BudgetTarget> budgetTargets;


  @SuppressWarnings("unused") // s11n
  private GetBudgetTargetsResult() {}

  public GetBudgetTargetsResult(final ArrayList<BudgetTarget> budgetTargets)
  {
    this.budgetTargets = budgetTargets;
  }

  public final ArrayList<BudgetTarget> getBudgetTargets()
  {
    return budgetTargets;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("GetBudgetTargetsResult{");
    sb.append("super=").append(super.toString());
    sb.append(", budgetTargets=").append(budgetTargets);
    sb.append('}');
    return sb.toString();
  }
}
