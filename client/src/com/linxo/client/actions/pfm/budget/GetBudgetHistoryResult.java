/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 21/09/2015 by hugues.
 */
package com.linxo.client.actions.pfm.budget;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.budget.BudgetTarget;

import java.util.HashMap;

/**
 * LinxoResult of the {@link GetBudgetHistoryAction}. It holds a map containing
 * the requested budget history.
 *
 * A budget history is a couple (date, budget value) where:
 *  <ul>
 *     <li>
 *      The date is the first day of the period covered and is calculated based
 *      on the budget target interval unit, interval value and start date.
 *    </li>
 *    <li>
 *      The budget value is the sum of the transaction amounts on the period
 *      covered.
 *    </li>
 *  </ul>
 */
public class GetBudgetHistoryResult
    extends SecuredResult
{
  /**
   * A map containing the requested budget history.
   */
  private HashMap<BudgetTarget, HashMap<LinxoDate, Double>> budgetHistory;


  @SuppressWarnings("unused") // s11n
  private GetBudgetHistoryResult() {}

  public GetBudgetHistoryResult(final HashMap<BudgetTarget, HashMap<LinxoDate, Double>> budgetHistory)
  {
    this.budgetHistory = budgetHistory;
  }

  public final HashMap<BudgetTarget, HashMap<LinxoDate, Double>> getBudgetHistory()
  {
    return budgetHistory;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("GetBudgetHistoryResult{");
    sb.append("super=").append(super.toString());
    sb.append(", budgetHistory=").append(budgetHistory);
    sb.append('}');
    return sb.toString();
  }
}
