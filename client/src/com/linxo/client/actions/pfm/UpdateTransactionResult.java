/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.tx.DetailedTransactionInfo;
import com.linxo.client.dto.tx.TransactionInfo;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class UpdateTransactionResult
    extends SecuredResult
{
  public enum Status
  {
    Success,
    BadParameterFailure
  }

  private Status status;

  private TransactionInfo transaction;
  @Deprecated
  private DetailedTransactionInfo detailedTransaction;

  @SuppressWarnings("unused") // s11n
  private UpdateTransactionResult() {}

  public UpdateTransactionResult(final Status status, final TransactionInfo transaction)
  {
    this.status = status;
    this.transaction = transaction;
    this.detailedTransaction = null;
  }

  @Deprecated
  public UpdateTransactionResult(final Status status, final TransactionInfo transaction,
                                 @Deprecated
                                 final DetailedTransactionInfo detailedTransaction)
  {
    this.status = status;
    this.transaction = transaction;
    this.detailedTransaction = detailedTransaction;
  }

  /**
   * @return the call status.
   * The server may return a 500 error with an error message.
   */
  public Status getStatus()
  {
    return status;
  }

  /**
   * @return the updated transactionInfo
   */
  public TransactionInfo getTransaction()
  {
    return transaction;
  }

  /**
   * @return the updated transactionDetails
   * @deprecated Everything should be available in the {@link #getTransaction() transactionInfo} now
   */
  @Deprecated
  public DetailedTransactionInfo getDetailedTransaction()
  {
    return detailedTransaction;
  }

}
