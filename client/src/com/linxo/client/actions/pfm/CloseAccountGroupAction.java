/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class CloseAccountGroupAction
  extends SecuredAction<CloseAccountGroupResult>
{
  private long groupId;

  @SuppressWarnings("unused") // used for s11n
  private CloseAccountGroupAction()
  {}

  public CloseAccountGroupAction(final long groupId)
  {
    this.groupId = groupId;
  }

  public long getGroupId()
  {
    return groupId;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append( "CloseAccountGroupAction" );
    sb.append( "{groupId=" ).append(groupId);
    sb.append( '}' );
    return sb.toString();
  }

}
