/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action queries for the AccountGroup with the given group id.
 * This action must be performed with a logged in user.
 * The group must belong to this user.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class GetAccountGroupAction
  extends SecuredAction<GetAccountGroupResult>
{
  private long accountGroupId;

  @SuppressWarnings("unused")
  private GetAccountGroupAction() {}

  public GetAccountGroupAction(final long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("GetAccountGroupAction");
    sb.append("{accountGroupId=").append(accountGroupId);
    sb.append('}');
    return sb.toString();
  }

}
