/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * An action to delete a bank account. Deleting a bank account
 * also deletes its potential associated budget targets.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class DeleteBankAccountAction
  extends SecuredAction<DeleteBankAccountResult>
{
  private long accountGroupId;

  private long bankAccountId;

  @SuppressWarnings("unused") // used for s11n
  private DeleteBankAccountAction()
  {}

  public DeleteBankAccountAction(final long accountGroupId, final long bankAccountId)
  {
    this.accountGroupId = accountGroupId;
    this.bankAccountId = bankAccountId;
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  public long getBankAccountId()
  {
    return bankAccountId;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append( "DeleteBankAccountAction" );
    sb.append( "{accountGroupId=" ).append( accountGroupId );
    sb.append( ", bankAccountId=" ).append(bankAccountId);
    sb.append( '}' );
    return sb.toString();
  }

}
