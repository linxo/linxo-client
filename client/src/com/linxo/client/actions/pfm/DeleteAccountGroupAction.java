/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class DeleteAccountGroupAction
  extends SecuredAction<DeleteAccountGroupResult>
{
  private long accountGroupId;

  @SuppressWarnings("unused") // used for s11n
  private DeleteAccountGroupAction()
  {}

  public DeleteAccountGroupAction(final long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append( "DeleteAccountGroupAction" );
    sb.append( "{accountGroupId=" ).append( accountGroupId );
    sb.append( '}' );
    return sb.toString();
  }

}
