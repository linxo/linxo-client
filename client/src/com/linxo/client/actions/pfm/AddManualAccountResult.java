/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 20/04/2011 by hugues.
 */
package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;

public final class AddManualAccountResult
  extends SecuredResult
{
  private Long accountId;

  private Long personalizedViewId;

  @SuppressWarnings("unused") // s11n
  private AddManualAccountResult() {}

  public AddManualAccountResult(Long accountId, Long personalizedViewId)
  {
    this.accountId = accountId;
    this.personalizedViewId = personalizedViewId;
  }

  public AddManualAccountResult(Long accountId)
  {
    this(accountId, null);
  }

  public Long getAccountId()
  {
    return accountId;
  }

  public Long getPersonalizedViewId()
  {
    return personalizedViewId;
  }

}
