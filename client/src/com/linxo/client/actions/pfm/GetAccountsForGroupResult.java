/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.account.BankAccountInfo;

import java.util.ArrayList;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetAccountsForGroupResult
  extends SecuredResult
{
  private ArrayList<BankAccountInfo> bankAccounts;

  @SuppressWarnings("unused")
  private GetAccountsForGroupResult() {}

  public GetAccountsForGroupResult(final ArrayList<BankAccountInfo> bankAccounts)
  {
    this.bankAccounts = bankAccounts;
  }

  public ArrayList<BankAccountInfo> getBankAccounts()
  {
    return bankAccounts;
  }

}
