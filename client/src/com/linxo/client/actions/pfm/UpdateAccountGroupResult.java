/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;

/**
 *
 */
public final class UpdateAccountGroupResult
  extends SecuredResult
{
  public enum Status
  {
    Success,
    BadParameterFailure,
    NameAlreadyUsed
  }

  private Status status;

  @SuppressWarnings("unused") // s11n
  private UpdateAccountGroupResult() {}

  public UpdateAccountGroupResult(final Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

}
