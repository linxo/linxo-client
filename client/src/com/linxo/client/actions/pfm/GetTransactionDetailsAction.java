/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * @deprecated more information will be made available in the TransactionInfo
 */
@GwtAction
@JsonAction
@Deprecated
public final class GetTransactionDetailsAction
  extends SecuredAction<GetTransactionDetailsResult>
{
  private long transactionId;

  @SuppressWarnings("unused")
  private GetTransactionDetailsAction() {}

  public GetTransactionDetailsAction(final long transactionId)
  {
    this.transactionId = transactionId;
  }

  public long getTransactionId()
  {
    return transactionId;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("GetTransactionDetailsAction");
    sb.append("{transactionId=").append(transactionId);
    sb.append('}');
    return sb.toString();
  }

}
