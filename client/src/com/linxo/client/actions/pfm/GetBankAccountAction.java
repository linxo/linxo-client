/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 7:19:18 PM by tarunmalhotra.
*/
package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * LinxoAction to get an account. The account is specified using
 * either a bank account id or a reference.
 */
@SuppressWarnings({"DeserializableClassInSecureContext"})
@GwtAction
@JsonAction
public final class GetBankAccountAction
    extends SecuredAction<GetBankAccountResult>
{
  /** The id of the account we want to get. If null, we'll use the reference. */
  private Long accountId;

  /** The reference of the account we want to get. */
  @AssertStringSize
  private String reference;

  @SuppressWarnings("unused") // s11n
  private GetBankAccountAction()
  {} // nothing

  public GetBankAccountAction(Long accountId)
  {
    this.accountId = accountId;
  }

  @SuppressWarnings("UnusedDeclaration")
  public GetBankAccountAction(String reference)
  {
    this.reference = reference;
  }

  /**
   * Gets the id of the account we want to get.
   * If null, we'll use the reference.
   *
   * @return The id of the account we want to get.
   *         If null, we'll use the reference.
   */
  public final Long getAccountId()
  {
    return accountId;
  }

  /**
   * Gets the reference of the account we want to get.
   * Used only when the account id is null.
   *
   * @return The reference of the account we want to get.
   *         Used only when the account id is null.
   */
  public final String getReference()
  {
    return reference;
  }

  @Override
  public String toString()
  {
    return "GetBankAccountAction{"
           + "accountId=" + accountId
           + ", reference=" + accountId
           + '}';
  }

}
