/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.account.AccountGroupInfo;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetAccountGroupResult
  extends SecuredResult
{
  private AccountGroupInfo accountGroup;

  @SuppressWarnings("unused")
  private GetAccountGroupResult() {}

  public GetAccountGroupResult(final AccountGroupInfo accountGroup)
  {
    this.accountGroup = accountGroup;
  }

  public AccountGroupInfo getAccountGroup()
  {
    return accountGroup;
  }

}
