/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 01/10/2012 by hugues.
 */
package com.linxo.client.actions.pfm;


import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * LinxoAction to get a transaction based on its
 * transaction id or its reference.
 */
@GwtAction
@JsonAction
public final class GetTransactionAction
    extends SecuredAction<GetTransactionResult>
{
  /**
   * The id of the transaction details we want to get.
   * If null, we'll use the reference to get it.
   */
  private Long transactionId;

  /**
   * The reference of the transaction we want to get. May be null.
   * Used only if the transaction id is null.
   */
  @AssertStringSize
  private String reference;

  @SuppressWarnings("unused") // s11n
  private GetTransactionAction() {}

  public GetTransactionAction(final Long transactionId)
  {
    this.transactionId = transactionId;
  }

  public GetTransactionAction(final String reference)
  {
    this.reference = reference;
  }

  /**
   * Gets the id of the transaction details we want to get.
   *
   * @return The id of the transaction details we want to get.
   */
  public final Long getTransactionId()
  {
    return transactionId;
  }

  /**
   * Gets the reference of the transaction details we want to get.
   * May be null. Used only if the transaction id is null.
   *
   * @return The reference of the transaction details we want to get.
   *         May be null. Used only if the transaction id is null.
   */
  public final String getReference()
  {
    return reference;
  }

}
