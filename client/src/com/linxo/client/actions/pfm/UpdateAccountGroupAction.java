/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 *
 */
@GwtAction
@JsonAction
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class UpdateAccountGroupAction
  extends SecuredAction<UpdateAccountGroupResult>
{
  private long gid;

  @AssertStringSize(nullable = false, voidable = false)
  private String newName;

  @SuppressWarnings("unused")// s11n
  private UpdateAccountGroupAction() {}

  public UpdateAccountGroupAction(final long gid, final String newName)
  {
    this.gid = gid;
    this.newName = newName;
  }

  public long getGid()
  {
    return gid;
  }

  public String getNewName()
  {
    return newName;
  }

  @Override
  public String toString()
  {
    final StringBuilder sb = new StringBuilder();
    sb.append("UpdateAccountGroupAction");
    sb.append("{gid=").append(gid);
    sb.append(", newName=").append(newName);
    sb.append('}');
    return sb.toString();
  }

}
