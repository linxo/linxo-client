/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

import java.util.Map;

@GwtAction
@JsonAction
public final class UpdateMultipleTransactionsAction extends SecuredAction<UpdateMultipleTransactionsResult>
{
  private Long categoryId;
  private Map<Integer, Long> txIds;
  private Long tagId;
  private boolean delete;

  @SuppressWarnings("unused") // s11n
  protected UpdateMultipleTransactionsAction()
  {} // nothing

  public UpdateMultipleTransactionsAction(Map<Integer, Long> txIds, Long tagId, Long categoryId)
  {
    this.txIds = txIds;
    this.tagId = tagId;
    this.categoryId = categoryId;
  }

  public UpdateMultipleTransactionsAction(Map<Integer, Long> txIds, Long tagId, Long categoryId,boolean delete)
  {
    this.txIds = txIds;
    this.tagId = tagId;
    this.categoryId = categoryId;
    this.delete = delete;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }

  public Map<Integer, Long> getTxIds()
  {
    return txIds;
  }

  public Long getTagId()
  {
    return tagId;
  }

  public boolean isDelete()
  {
    return delete;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("UpdateMultipleTransactionsAction");
    sb.append("{tagId=").append(tagId);
    sb.append("{delete=").append(delete);
    sb.append(", categoryId=").append(categoryId);
    for (Map.Entry<Integer,Long> entry : txIds.entrySet()) {
      sb.append(", Row Number=").append(entry.getKey());
      sb.append(", Transaction Id=").append(entry.getValue());
    }
    sb.append('}');
    return sb.toString();
  }
}
