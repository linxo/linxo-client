/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class SynchronizeAccountGroupAction
  extends SecuredAction<SynchronizeAccountGroupResult>
{
  private long accountGroupId;

  @SuppressWarnings("unused") // s11n
  private SynchronizeAccountGroupAction()
  {}

  public SynchronizeAccountGroupAction(final long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
  }

  public long getAccountGroupId()
  {
    return accountGroupId;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("SynchronizeAccountGroupAction");
    sb.append("{accountGroupId=").append(accountGroupId);
    sb.append('}');
    return sb.toString();
  }

}
