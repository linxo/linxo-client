/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 7:19:32 PM by tarunmalhotra.
*/
package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.account.BankAccountInfo;

/**
 * LinxoResult for the GetBankAccountAction.
 */
@SuppressWarnings({"DeserializableClassInSecureContext"})
public final class GetBankAccountResult
    extends SecuredResult
{
  /** The account we wanted to get. */
  private BankAccountInfo bankAccount;

  @SuppressWarnings("unused") // s11N
  private GetBankAccountResult()
  {}

  public GetBankAccountResult(BankAccountInfo bankAccount)
  {
    this.bankAccount = bankAccount;
  }

  /**
   * Gets the account we wanted to get.
   *
   * @return The account we wanted to get.
   */
  @SuppressWarnings("unused") // json only
  public BankAccountInfo getBankAccount()
  {
    return bankAccount;
  }

}
