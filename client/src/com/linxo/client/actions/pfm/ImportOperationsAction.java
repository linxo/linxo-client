/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.pfm;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.dto.tx.TransactionInfo;

import java.util.ArrayList;

/**
 * This action queries for all the Account Groups of the currently logged-in user.
 */
@GwtAction
@SuppressWarnings({"DeserializableClassInSecureContext"})
public final class ImportOperationsAction
  extends SecuredAction<ImportOperationsResult>
{
  private Long accountId;
  private double newBalance;
  private ArrayList<TransactionInfo> transactions;

  @SuppressWarnings({ "UnusedDeclaration" })
  private ImportOperationsAction() {}

  public ImportOperationsAction(final Long accountId, final double newBalance, final ArrayList<TransactionInfo> transactions)
  {
    this.accountId = accountId;
    this.newBalance = newBalance;
    this.transactions = transactions;
  }

  public Long getAccountId()
  {
    return accountId;
  }

  public ArrayList<TransactionInfo> getTransactions()
  {
    return transactions;
  }

  public double getNewBalance()
  {
    return newBalance;
  }

}
