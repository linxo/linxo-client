/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 24/09/2014 by hugues.
 */
package com.linxo.client.actions.services;

import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.dto.upcoming.BankAccountForecast;
import com.linxo.client.dto.upcoming.UpcomingTransaction;
import com.linxo.client.dto.upcoming.UpcomingTransactionOccurrence;
import com.linxo.infrastructure.exceptions.FunctionalException;
import com.linxo.infrastructure.exceptions.TechnicalException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This interface describes the API exposed in order to manipulate
 * Upcoming Transaction DTOs for a given User.
 */
public interface UpcomingTransactionService {
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Instance Methods

  /**
   *
   * @param bankAccountId - When not null, returns only the UT for the given accountId.
   * @return A map of (accountId -&gt; UT). never null. Can be empty if not UT exists.
   * @throws FunctionalException if the accountId is not null and does not belong to the currentUser.
   * @throws TechnicalException if a technical problem occurs
   */
  HashMap<Long, ArrayList<UpcomingTransaction>> getUpcomingTransactions(Long bankAccountId)
      throws FunctionalException, TechnicalException;

  /**
   * Attempts to detect and possibly save new UpcomingTransactions based on the user's
   * history.
   *
   * @param accountId - when {@code null}, the detection will take place on all checkings accounts.
   *                  When not null, the detection will be limited to the given Checkings account.
   * @param saveDetected - when {@code true}, the returned UT will be already persisted. Otherwise
   *                     they will only be Object ready to be persisted.
   * @return A map of (accountId -&gt; UT). never null. Can be empty if not UT is detected.
   * @throws FunctionalException if the accountId is not null and does not belong to the currentUser.
   * @throws TechnicalException if a technical problem occurs
   */
  HashMap<Long, ArrayList<UpcomingTransaction>> findUpcomingTransactions(Long accountId, boolean saveDetected)
      throws FunctionalException, TechnicalException;

  /**
   *
   * @param upcomingTransaction - the UpcomingTransaction to add for the current user. This UpcomingTransaction
   *                            must have a few fields set like the bankAccountId and the CategoryId. These
   *                            fields must be of objects that the user has access to or the Service will throw
   *                            an IllegalArgumentException.
   * @return the saved Upcoming transaction with a new Id.
   * @throws FunctionalException if the accountId does not belong to the current user, if she
   * cannot access or use the categoryId, or if the label is null or the empty String.
   * @throws TechnicalException if a technical problem occurs
   */
  UpcomingTransaction addUpcomingTransaction(UpcomingTransaction upcomingTransaction);

  /**
   * Updates the upcoming transaction, but not the attached modified occurrences.
   * Modified occurrences should be updated independently using the method
   * {@link #editTransactionOccurrence}
   *
   *
   * @param upcomingTransaction - the UpcomingTransaction to edit for the current user. This UpcomingTransaction
   *                            must have a few fields set like the bankAccountId and the CategoryId. These
   *                            fields must be of objects that the user has access to or the Service will throw
   *                            an IllegalArgumentException.
   * @return the updated UpcomingTransaction.
   * @throws FunctionalException if the accountId does not belong to the user, if she
   * cannot access or use the categoryId, or if the label is null or the empty String.
   * @throws TechnicalException if a technical problem occurs
   */
  UpcomingTransaction editUpcomingTransaction(UpcomingTransaction upcomingTransaction);

  /**
   * Deletes the upcoming transaction, and the attached modified occurrences.
   *
   * NOTE: At the exit of this method, the transaction and its occurrences are also removed
   * on the client side.
   *
   * @param transactionId - the ID of the UpcomingTransaction to delete for the current user.
   * @return true if the transaction and its occurrences was removed on the server, false otherwise.
   * @throws FunctionalException if the transaction does not belong to the user.
   * @throws TechnicalException if a technical problem occurs
   */
  boolean deleteUpcomingTransaction(Long transactionId);

  /**
   * @param occurrenceId -
   * @return the transactionId of the {@link UpcomingTransaction} attached to this occurrence or null.
   * @throws NullPointerException if the occurrenceId is null
   * @throws TechnicalException if a technical problem occurs
   */
  Long getTransactionIdForOccurrenceId(Long occurrenceId) throws NullPointerException;


  /**
   * @param transactionId - required, or throws a NullPointerException
   * @param occurrence - the UpcomingTransactionOccurrence to add for the current user and this transactionId.
   *                   This occurrence must have a few fields set like the bankAccountId and the CategoryId. These
   *                   fields must be of objects that the user has access to or the Service will throw
   *                   an IllegalArgumentException.
   * @return the saved Upcoming transaction Occurrence with a new Id.
   * @throws NullPointerException if the transactionId is null
   * @throws FunctionalException if the accountId does not belong to the current user, if she
   * cannot access or use the categoryId, or if the label is null or the empty String.
   * @throws TechnicalException if a technical problem occurs
   */
  UpcomingTransactionOccurrence addTransactionOccurrence(Long transactionId, UpcomingTransactionOccurrence occurrence);

  /**
   * @param occurrence - the UpcomingTransactionOccurrence to update for the current user.
   *                   This occurrence must have a few fields set like the bankAccountId and the CategoryId. These
   *                   fields must be of objects that the user has access to or the Service will throw
   *                   an IllegalArgumentException.
   *
   *
   *                   In addition, the value of the {@link UpcomingTransactionOccurrence#originalDate}
   *                   must not have been changed or this will return an error.
   * @return the saved Upcoming transaction Occurrence with a new Id.
   * @throws FunctionalException
   * <ol>
   *  <li>
   *    if the accountId does not belong to the current user, if she cannot access or use the categoryId, or if the
   *    label is null or the empty String.
   *  </li>
   *  <li>
   *    Also thrown if the {@link UpcomingTransactionOccurrence#originalDate} is modified.
   *  </li>
   *  <li>
   *    Finally, it is thrown iof the user does not own the transaction this occurrence is attached to.
   *  </li>
   * </ol>
   * @throws TechnicalException if a technical problem occurs
   */
  UpcomingTransactionOccurrence editTransactionOccurrence(UpcomingTransactionOccurrence occurrence);

  /**
   * Deletes the upcoming transaction occurrence, removing it completely.
   *
   * NOTE: At the exit of this method, the occurrence is also removed
   * on the client side.
   *
   * @param occurrenceId - the ID of the UpcomingTransaction to delete for the current user,
   *                       required or throws a NullPointerException
   * @return true if the occurrence was removed on the server, false otherwise.
   * @throws NullPointerException if the occurrenceId is null
   * @throws FunctionalException if the transaction does not belong to the user.
   * @throws TechnicalException if a technical problem occurs
   */
  boolean deleteUpcomingTransactionOccurrence(Long occurrenceId);



  /**
   * The forecast for the given account, based on the UpcomingTransactions.
   * If the user has not access to UpcomingTransactions, the Forecast information
   * will be based on the current state.
   *
   * Note: We initially considered that values for
   * {@code lowBalanceThreshold < overdraftThreshold} was an error and an
   * {@code IllegalArgumentException} was thrown.
   *
   * Now, the service will behave gracefully and consider that
   * {@code overdraftThreshold = lowBalanceThreshold} in this case.
   *
   * @param bankAccount -
   * @param lowBalanceThreshold -
   * @param overdraftThreshold -
   * @return a BankAccountForecast object that shows the status of the account in the next 30 days
   * @throws java.lang.NullPointerException - if {@code bankAccount} is null
   */
  BankAccountForecast getForecast(BankAccountInfo bankAccount, double lowBalanceThreshold, double overdraftThreshold)
      throws IllegalArgumentException;


  /**
   *
   * @param upcomingTransactionId -
   * @return true if the UpcomingTransaction referenced by the upcomingTransactionId belongs to the
   * current user; false other wise.
   * @throws TechnicalException if a technical problem occurs
   */
  boolean userOwnsUpcomingTransaction(long upcomingTransactionId);

  boolean userCanUpdateTransaction(long upcomingTransactionId);

}
