/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
 
 
   Created on : 28/10/2014 by hugues.
 */
package com.linxo.client.actions.services;

import com.linxo.client.net.LinxoClient;
import com.linxo.client.dto.upcoming.UpcomingTransaction;
import com.linxo.client.dto.upcoming.UpcomingTransactionOccurrence;
import com.linxo.infrastructure.exceptions.FunctionalException;
import com.linxo.infrastructure.exceptions.PermissionException;
import com.linxo.infrastructure.exceptions.TechnicalException;
import com.linxo.client.actions.pfm.upcoming.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

/**
 * This implementation of the UpcomingTransactionService uses a LinxoClient
 * object to perform its queries to the server.
 */
public abstract class AbstractClientUpcomingTransactionService
    extends AbstractUpcomingTransactionService
    implements ClientUpcomingTransactionService
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private final LinxoClient linxoClient;

  private boolean requireReload = true;

  private HashMap<Long, ArrayList<UpcomingTransaction>> transactionsPerAccountId
      = new HashMap<Long, ArrayList<UpcomingTransaction>>();

  // Instance Initializers

  // Constructors

  public AbstractClientUpcomingTransactionService(LinxoClient linxoClient)
  {
    this.linxoClient = linxoClient;

    this.requireReload = true;
  }


  // Instance Methods

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           Public API - ClientUpcomingTransactionService
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  @Override
  public boolean isReloadRequired()
  {
    return requireReload;
  }

  @Override
  public void requireReload()
  {
    requireReload = true;
  }

  @Override
  public void flush()
  {
    this.transactionsPerAccountId.clear();
    this.requireReload = true;
  }

  @Override
  public abstract boolean hasUpcomingTransactionsPermission();


  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           Public API - UpcomingTransactionService
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  @Override
  public HashMap<Long, ArrayList<UpcomingTransaction>> getUpcomingTransactions(Long bankAccountId)
      throws FunctionalException, TechnicalException
  {
    if(!requireReload) {
      return getUpcomingTransactionsFromMap(bankAccountId);
    }

    if(!hasUpcomingTransactionsPermission()) {
      // By making this check, we ensure one-step higher that the user has the permissions for getting the upcoming transactions.
      // If it's not the case, it avoids making a server request (and finally a DB request) for nothing.
      throw new PermissionException();
    }

    transactionsPerAccountId.clear();

    GetUpcomingTransactionsResult result = linxoClient.sendAction(new GetUpcomingTransactionsAction((Long) null));

    transactionsPerAccountId.putAll(result.getTransactions());
    requireReload = false;

    return getUpcomingTransactionsFromMap(bankAccountId);
  }


  @Override
  public HashMap<Long, ArrayList<UpcomingTransaction>> findUpcomingTransactions(Long accountId, boolean saveDetected)
      throws FunctionalException, TechnicalException
  {
    FindUpcomingTransactionsResult result = linxoClient.sendAction(new FindUpcomingTransactionsAction(accountId, saveDetected));

    if(!saveDetected) {
      // return directly
      return result.getTransactions();
    }

    // It was requested to save the new transactions. So we're adding them to the map before we return
    // them to the invoker.
    for(Map.Entry<Long,ArrayList<UpcomingTransaction>> entry : result.getTransactions().entrySet())
    {
      if(entry.getValue().isEmpty())
      {
        //nothing to add
        continue;
      }
      Long resultAccountId = entry.getKey();
      ArrayList<UpcomingTransaction> existingTransactions = transactionsPerAccountId.get(resultAccountId);
      if (existingTransactions == null) {
        existingTransactions = new ArrayList<UpcomingTransaction>();
        transactionsPerAccountId.put(resultAccountId, existingTransactions);
      }
      existingTransactions.addAll(entry.getValue());
    }

    return result.getTransactions();
  }

  @Override
  public UpcomingTransaction addUpcomingTransaction(UpcomingTransaction upcomingTransaction)
  {
    checkUpcomingTransaction(upcomingTransaction);

    AddUpcomingTransactionResult result = linxoClient.sendAction(new AddUpcomingTransactionAction(upcomingTransaction));

    // The new transaction contains an id
    UpcomingTransaction added = result.getUpcomingTransaction();
    if(added == null) {
      throw new TechnicalException("SHOULD NOT HAPPEN : Upcoming transaction is null in returned value while adding");
    }

    // add the transaction to the appropriate place in the map
    addTransaction(added);

    return added;
  }

  @Override
  public UpcomingTransaction editUpcomingTransaction(UpcomingTransaction upcomingTransaction)
  {
    checkUpcomingTransaction(upcomingTransaction);

    EditUpcomingTransactionResult result = linxoClient.sendAction(new EditUpcomingTransactionAction(upcomingTransaction));

    // The edited transaction contains an id
    UpcomingTransaction edited = result.getUpcomingTransaction();
    if(edited == null) {
      throw new TechnicalException("SHOULD NOT HAPPEN : Upcoming transaction is null in returned value while editing");
    }

    // Replace the transaction to the appropriate place in the map (bank account might have moved !)
    if(replaceTransaction(edited) == null)
    {
      throw new FunctionalException("SHOULD NOT HAPPEN : upcoming transaction edited on the server but not found in the client");
    }

    return edited;
  }


  @Override
  public boolean deleteUpcomingTransaction(Long transactionId)
  {
    if (transactionId == null) {
      throw new NullPointerException("upcomingTransaction cannot be null");
    }

    try {
      linxoClient.sendAction(new DeleteUpcomingTransactionAction(transactionId));
    }
    catch(FunctionalException fe) {
      return false;
    }

    // remove with id
    removeTransactionWithId(transactionId);

    return true;
  }


  @Override
  public Long getTransactionIdForOccurrenceId(Long occurrenceId)
  {
    if (occurrenceId == null) {
      throw new NullPointerException("occurrenceId should not be null");
    }

    for(ArrayList<UpcomingTransaction> transactions : transactionsPerAccountId.values())
    {
      for (UpcomingTransaction transaction : transactions)
      {
        for(UpcomingTransactionOccurrence occurrence : transaction.getModifiedOccurrences())
        {
          if(occurrence.getId().equals(occurrenceId)) {
            return transaction.getId();
          }
        }
      }
    }
    // not found
    return null;
  }


  @Override
  public UpcomingTransactionOccurrence
  addTransactionOccurrence(Long transactionId, UpcomingTransactionOccurrence occurrence)
  {
    checkModifiedOccurrence(transactionId, occurrence);

    AddTransactionOccurrenceResult result = linxoClient
        .sendAction(new AddTransactionOccurrenceAction(transactionId, occurrence));

    UpcomingTransactionOccurrence saved = result.getOccurrence();
    if(saved == null)
    {
      throw new TechnicalException("SHOULD NOT HAPPEN : Occurrence is null in returned value while adding");
    }

    UpcomingTransaction transaction = getUpcomingTransactionWithId(transactionId);
    transaction.getModifiedOccurrences().add(saved);
    // As the UTOccurrence arrives directly, it has no UT attached to it. We have to fill it manually.
    saved.setUpcomingTransaction(transaction);

    return saved;
  }


  @Override
  public UpcomingTransactionOccurrence editTransactionOccurrence(UpcomingTransactionOccurrence occurrence)
  {
    // Let's get the transactionId
    Long transactionId = getTransactionIdForOccurrenceId(occurrence.getId());
    checkModifiedOccurrence(transactionId, occurrence);

    EditTransactionOccurrenceResult result = linxoClient.sendAction(new EditTransactionOccurrenceAction(occurrence));

    UpcomingTransactionOccurrence edited = result.getOccurrence();
    if(edited == null)
    {
      throw new TechnicalException("SHOULD NOT HAPPEN : Occurrence is null in returned value while adding");
    }

    UpcomingTransaction transaction = getUpcomingTransactionWithId(transactionId);
    boolean removed = false;
    Iterator<UpcomingTransactionOccurrence> occurrences = transaction.getModifiedOccurrences().iterator();
    while(occurrences.hasNext())
    {
      UpcomingTransactionOccurrence existing = occurrences.next();
      if(edited.getId().equals(existing.getId()))
      {
        occurrences.remove();
        removed = true;
        break;
      }
    }
    if(!removed)
    {
      throw new FunctionalException("SHOULD NOT HAPPEN : occurrence updated on the server but not found in the client");
    }
    transaction.getModifiedOccurrences().add(edited);
    // As the UTOccurrence arrives directly, it has no UT attached to it. We have to fill it manually.
    edited.setUpcomingTransaction(transaction);

    return edited;
  }


  @Override
  public boolean deleteUpcomingTransactionOccurrence(Long occurrenceId)
  {
    if (occurrenceId == null) {
      throw new NullPointerException("occurrenceId must not be null when deleting");
    }

    // Let's get the transactionId
    Long transactionId = getTransactionIdForOccurrenceId(occurrenceId);
    if (transactionId == null) {
      throw new FunctionalException("No transaction for this occurrenceId[" + occurrenceId + "]");
    }

    // We do not check the returned result since it has no condition. If an error
    // happens, this will raise an exception.
    linxoClient.sendAction(new DeleteTransactionOccurrenceAction(occurrenceId));

    UpcomingTransaction transaction = getUpcomingTransactionWithId(transactionId);
    Iterator<UpcomingTransactionOccurrence> occurrences = transaction.getModifiedOccurrences().iterator();
    while(occurrences.hasNext())
    {
      UpcomingTransactionOccurrence existing = occurrences.next();
      if(occurrenceId.equals(existing.getId()))
      {
        occurrences.remove();
        break;
      }
    }

    return true;
  }

  @Override
  public boolean userOwnsUpcomingTransaction(long upcomingTransactionId)
  {
    // In this service, we're going to just check if this transactionId is in
    // our local lists. If it is not, we just return false.
    //
    // In practice, this method should not get invoked in the client code...
    return getUpcomingTransactionWithId(upcomingTransactionId) != null;
  }

  @Override
  public boolean userCanUpdateTransaction(long upcomingTransactionId)
  {
    final UpcomingTransaction transaction = getUpcomingTransactionWithId(upcomingTransactionId);
    return transaction != null && transaction.isCreatedByUser();
  }

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           Utils/Checks Methods
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  /**
   * looks up the transaction with the given transactionId and remove it.
   * @param transactionId -
   * @return a set of bank account ids for which the list of upcoming transaction has changed, null if the upcoming transaction was not found
   */
  protected HashSet<Long> removeTransactionWithId(long transactionId)
  {
    for(ArrayList<UpcomingTransaction> transactions : transactionsPerAccountId.values())
    {
      for(Iterator<UpcomingTransaction> iterator = transactions.iterator();
          iterator.hasNext();)
      {
        UpcomingTransaction transaction = iterator.next();
        if(transaction.getId() == transactionId){
          HashSet<Long> result = new HashSet<Long>();
          result.add(transaction.getAccountId());
          iterator.remove();
          return result;
        }
      }
    }

    // not found
    return null;
  }

  /**
   * Add an upcoming transaction to our map
   * @param upcomingTransaction -
   * @return a set of bank account ids for which the list of upcoming transaction has changed
   */
  protected HashSet<Long> addTransaction(UpcomingTransaction upcomingTransaction)
  {
    ArrayList<UpcomingTransaction> transactions = transactionsPerAccountId.get(upcomingTransaction.getAccountId());
    if(transactions == null) {
      transactions = new ArrayList<UpcomingTransaction>();
      transactionsPerAccountId.put(upcomingTransaction.getAccountId(), transactions);
    }
    transactions.add(upcomingTransaction);

    HashSet<Long> result = new HashSet<Long>();
    result.add(upcomingTransaction.getAccountId());

    return result;
  }

  /**
   * Replaces an existing upcoming transaction in the map. Matching done via the id of the upcoming transaction
   * @param upcomingTransaction -
   * @return a set of bank account ids for which the list of its upcoming transactions has changed, null if the upcoming transaction was not found
   */
  protected HashSet<Long> replaceTransaction(UpcomingTransaction upcomingTransaction)
  {
    // We're iterating over all the elements in the map, looking for an existing same upcoming transaction
    // based on its id. Keep in mind that the upcoming transaction might have its bank account id changed.
    // So we're removing the upcoming transaction, and adding back to the appropriate account (may be the same)
    // As a result, the list of bank accounts that can see its upcoming transaction list changed is 1 or 2.
    HashSet<Long> bankAccountIds = new HashSet<Long>();

    for(ArrayList<UpcomingTransaction> transactions : transactionsPerAccountId.values())
    {
      for(ListIterator<UpcomingTransaction> iterator = transactions.listIterator();
          iterator.hasNext();)
      {
        UpcomingTransaction transaction = iterator.next();
        if(transaction.getId().equals(upcomingTransaction.getId())){
          bankAccountIds.add(transaction.getAccountId());
          iterator.remove();
        }
      }
    }

    if(bankAccountIds.size() == 0) {
      return null;
    }

    // We're not using the addTransaction method as it is overriden in clients and can trigger
    // forecast computation
    ArrayList<UpcomingTransaction> transactions = transactionsPerAccountId.get(upcomingTransaction.getAccountId());
    if(transactions == null)
    {
      transactions = new ArrayList<UpcomingTransaction>();
      transactionsPerAccountId.put(upcomingTransaction.getAccountId(), transactions);
    }
    transactions.add(upcomingTransaction);

    bankAccountIds.add(upcomingTransaction.getAccountId());

    // not found
    return bankAccountIds;
  }

  /**
   * looks up the transaction with the given transactionId and return it.
   * @param transactionId -
   * @return the UpcomingTransaction with the given id an element is found, {@code null} otherwise.
   */
  protected UpcomingTransaction getUpcomingTransactionWithId(long transactionId)
  {
    for(ArrayList<UpcomingTransaction> transactions : transactionsPerAccountId.values())
    {
      for (UpcomingTransaction transaction : transactions) {
        if (transaction.getId() == transactionId) {
          return transaction;
        }
      }
    }
    // not found
    return null;
  }


  protected HashMap<Long, ArrayList<UpcomingTransaction>> getUpcomingTransactionsFromMap(Long bankAccountId)
  {
    if (bankAccountId == null) {
      return transactionsPerAccountId;
    }

    HashMap<Long, ArrayList<UpcomingTransaction>> result = new HashMap<Long, ArrayList<UpcomingTransaction>>();

    ArrayList<UpcomingTransaction> uts = transactionsPerAccountId.get(bankAccountId);
    if(uts != null) {
      result.put(bankAccountId, uts);
    }
    return result;
  }


}
