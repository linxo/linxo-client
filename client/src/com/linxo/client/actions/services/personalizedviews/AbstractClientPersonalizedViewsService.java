/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.services.personalizedviews;

import com.linxo.client.data.LinxoColor;
import com.linxo.client.actions.AbstractLinxoAsyncClientCallback;
import com.linxo.client.actions.LinxoAsyncClient;
import com.linxo.client.actions.LinxoServiceCallback;
import com.linxo.client.dto.account.PersonalizedView;
import com.linxo.client.actions.pfm.personalizedview.*;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO-APP-6632 Add javadoc here.
 *
 * TODO-APP-6632 Check parameters everywhere in this class.
 */
public abstract class AbstractClientPersonalizedViewsService
    implements ClientPersonalizedViewsService
{
  private final LinxoAsyncClient client;

  private boolean reloadRequired = true;

  private List<PersonalizedView> personalizedViews = new ArrayList<PersonalizedView>();

  private List<LinxoColor> colors = new ArrayList<LinxoColor>();

  private LinxoColor allViewsColor = null;

  public AbstractClientPersonalizedViewsService(LinxoAsyncClient client)
  {
    this.client = client;
  }

  @Override
  public final void getPersonalizedViews(final LinxoServiceCallback<List<PersonalizedView>> callback)
  {
    // if there is no need to get the views from the server
    if (!reloadRequired) {
      // we just call the onSuccess() of the callback
      callback.onSuccess(personalizedViews);
      return;
    }

    // and get the views from the server
    client.sendAction(new GetPersonalizedViewsAction(),
                      new AbstractLinxoAsyncClientCallback<List<PersonalizedView>, GetPersonalizedViewsResult>(callback) {
      @Override
      public final void onSuccess(final GetPersonalizedViewsResult result)
      {
        // clear the current views
        personalizedViews.clear();
        // and add all the views we got from the server
        personalizedViews.addAll(result.getPersonalizedViews());
        reloadRequired = false;
        callback.onSuccess(personalizedViews);
      }
    });
  }

  @Override
  public final void getPersonalizedView(final long personalizedViewId,
                                        final LinxoServiceCallback<PersonalizedView> callback)
  {
    final PersonalizedView personalizedView = findPersonalizedView(personalizedViewId);
    if (personalizedView != null) {
      callback.onSuccess(personalizedView);
      return;
    }

    getPersonalizedViews(new LinxoServiceCallback<List<PersonalizedView>>() {
      @Override
      public final void onSuccess(final List<PersonalizedView> personalizedViews)
      {
        callback.onSuccess(findPersonalizedView(personalizedViewId));
      }

      @Override
      public final void onFailure()
      {
        callback.onFailure();
      }

      @Override
      public final void onThrowable(final Throwable throwable)
      {
        callback.onThrowable(throwable);
      }
    });
  }

  @Override
  public final void addPersonalizedView(final PersonalizedView personalizedView,
                                        final LinxoServiceCallback<PersonalizedView> callback)
  {
    client.sendAction(new AddPersonalizedViewAction(personalizedView),
                      new AbstractLinxoAsyncClientCallback<PersonalizedView, AddPersonalizedViewResult>(callback) {
      @Override
      public final void onSuccess(final AddPersonalizedViewResult result)
      {
        if (result.getStatus() != AddPersonalizedViewResult.Status.Success) {
          callback.onFailure();
          return;
        }

        // We just added a view, potentially with accounts which were assigned to other views,
        // we set the flag indicating a reload is required to true to get fresh views from the
        // server the next time we need them.
        reloadRequired = true;

        callback.onSuccess(result.getPersonalizedView());
      }
    });
  }

  @Override
  public final void editPersonalizedView(final PersonalizedView personalizedView,
                                         final LinxoServiceCallback<PersonalizedView> callback)
  {
    client.sendAction(new EditPersonalizedViewAction(personalizedView),
                      new AbstractLinxoAsyncClientCallback<PersonalizedView, EditPersonalizedViewResult>(callback) {
      @Override
      public final void onSuccess(final EditPersonalizedViewResult result)
      {
        if (result.getStatus() != EditPersonalizedViewResult.Status.Success) {
          callback.onFailure();
          return;
        }

        // We just edited a view, potentially giving it accounts which were assigned to
        // other views, we set the flag indicating a reload is required to true to get
        // fresh views from the server the next time we need them.
        reloadRequired = true;

        callback.onSuccess(result.getPersonalizedView());
      }
    });
  }

  @Override
  public final void deletePersonalizedView(final long personalizedViewId,
                                           final long destinationViewId,
                                           final LinxoServiceCallback<Void> callback)
  {
    client.sendAction(new DeletePersonalizedViewAction(personalizedViewId, destinationViewId),
                      new AbstractLinxoAsyncClientCallback<Void, DeletePersonalizedViewResult>(callback) {
      @Override
      public final void onSuccess(final DeletePersonalizedViewResult result)
      {
        if (result.getStatus() != DeletePersonalizedViewResult.Status.Success) {
          callback.onFailure();
          return;
        }

        // We just deleted a view, assigning its accounts to another view, we set the flag
        // indicating a reload is required to true to get fresh views from the server the
        // next time we need them.
        reloadRequired = true;

        callback.onSuccess(null);
      }
    });
  }

  @Override
  public final void getColors(final LinxoServiceCallback<List<LinxoColor>> callback)
  {
    // we already got the colors, we return them directly
    if (!colors.isEmpty()) {
      callback.onSuccess(colors);
      return;
    }

    // else we get the colors from the server
    client.sendAction(new GetPersonalizedViewsColorsAction(),
                      new AbstractLinxoAsyncClientCallback<List<LinxoColor>, GetPersonalizedViewsColorsResult>(callback) {
      @Override
      public final void onSuccess(final GetPersonalizedViewsColorsResult result)
      {
        // store the colors we got from the server
        storeColors(result);

        // and "return" the colors
        callback.onSuccess(colors);
      }
    });
  }

  @Override
  public final void getAvailableColors(final PersonalizedView editedView,
                                       final LinxoServiceCallback<List<LinxoColor>> callback)
  {
    // we already got the colors, we call doGetAvailableColors() directly
    if (!colors.isEmpty()) {
      doGetAvailableColors(editedView, callback);
      return;
    }

    // else we get the colors from the server
    getColors(new LinxoServiceCallback<List<LinxoColor>>() {
      @Override
      public final void onSuccess(final List<LinxoColor> linxoColors)
      {
        // and then we call doGetAvailableColors()
        doGetAvailableColors(editedView, callback);
      }

      @Override
      public final void onFailure()
      {
        callback.onFailure();
      }

      @Override
      public final void onThrowable(final Throwable throwable)
      {
        callback.onThrowable(throwable);
      }
    });
  }

  private void doGetAvailableColors(final PersonalizedView editedView,
                                    final LinxoServiceCallback<List<LinxoColor>> callback)
  {
    // if we don't need to get the views from the server, we return the available colors directly
    if (!reloadRequired) {
      callback.onSuccess(filterAvailableColors(editedView));
      return;
    }

    // else we get the views from the server
    getPersonalizedViews(new LinxoServiceCallback<List<PersonalizedView>>() {
      @Override
      public void onSuccess(List<PersonalizedView> personalizedViews)
      {
        // and then we return the available colors
        callback.onSuccess(filterAvailableColors(editedView));
      }

      @Override
      public void onFailure()
      {
        callback.onFailure();
      }

      @Override
      public void onThrowable(Throwable throwable)
      {
        callback.onThrowable(throwable);
      }
    });
  }

  @Override
  public final void getAllViewsColor(final LinxoServiceCallback<LinxoColor> callback)
  {
    // we already got the "all views" color, we return it directly
    if (allViewsColor != null) {
      callback.onSuccess(allViewsColor);
      return;
    }

    // else we get the colors from the server
    client.sendAction(new GetPersonalizedViewsColorsAction(),
                      new AbstractLinxoAsyncClientCallback<LinxoColor, GetPersonalizedViewsColorsResult>(callback) {
      @Override
      public final void onSuccess(final GetPersonalizedViewsColorsResult result)
      {
        // store the colors we got from the server
        storeColors(result);

        // and "return" the colors
        callback.onSuccess(allViewsColor);
      }
    });
  }

  @Override
  public final void requireReload()
  {
    this.reloadRequired = true;
  }

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  // Utils
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  private void storeColors(GetPersonalizedViewsColorsResult result)
  {
    // store the colors
    colors.clear();
    colors.addAll(result.getColors());
    // store the "all views" color
    allViewsColor = result.getAllViewsColor();
  }

  private PersonalizedView findPersonalizedView(final long personalizedViewId)
  {
    if (personalizedViews == null) {
      return null;
    }

    for (PersonalizedView personalizedView : personalizedViews) {
      if (personalizedView.getId() != null && personalizedViewId == personalizedView.getId()){
        return personalizedView;
      }
    }
    return null;
  }

  private List<LinxoColor> filterAvailableColors(final PersonalizedView editedView)
  {
    // put all the colors in a new list
    final List<LinxoColor> availableColors = new ArrayList<LinxoColor>(colors);

    // remove the colors of all the existing views
    for (PersonalizedView personalizedView : personalizedViews) {
      availableColors.remove(personalizedView.getColor());
    }

    // if we need the colors because we're editing a view (the other case is
    // when we're creating a new view), we add back the color of this view
    if (editedView != null) {
      availableColors.add(editedView.getColor());
    }

    return availableColors;
  }

}
