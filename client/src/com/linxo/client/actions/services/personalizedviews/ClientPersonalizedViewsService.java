/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.services.personalizedviews;

import com.linxo.client.data.LinxoColor;
import com.linxo.client.actions.LinxoServiceCallback;
import com.linxo.client.dto.account.PersonalizedView;

import java.util.List;

/**
 * Client version of the PersonalizedViewsService.
 *
 * This version is asynchronous: all the methods are void methods
 * and have a {@link LinxoServiceCallback} parameter.
 */
public interface ClientPersonalizedViewsService
{
  /**
   * Adds the given {@link PersonalizedView}.
   *
   * @param personalizedView The {@link PersonalizedView} to add.
   * @param callback The callback called when the method finishes.
   */
  void addPersonalizedView(PersonalizedView personalizedView,
                           LinxoServiceCallback<PersonalizedView> callback);

  /**
   * Edits the given {@link PersonalizedView}.
   *
   * @param personalizedView The {@link PersonalizedView} to edit.
   * @param callback The callback called when the method finishes.
   */
  void editPersonalizedView(PersonalizedView personalizedView,
                            LinxoServiceCallback<PersonalizedView> callback);

  /**
   * Deletes the {@link PersonalizedView} with the given id.
   * The accounts currently associated to this view will be
   * associated to the view with the given id.
   *
   * @param personalizedViewId The id of the {@link PersonalizedView} to delete.
   * @param destinationViewId The id of the destination {@link PersonalizedView}.
   * @param callback The callback called when the method finishes.
   */
  void deletePersonalizedView(long personalizedViewId,
                              long destinationViewId,
                              LinxoServiceCallback<Void> callback);

  /**
   * Gets all the user's {@link PersonalizedView}.
   *
   * @param callback The callback called when the method finishes.
   */
  void getPersonalizedViews(LinxoServiceCallback<List<PersonalizedView>> callback);

  /**
   * Gets the {@link PersonalizedView} with the given id.
   *
   * @param personalizedViewId The id of the {@link PersonalizedView} to get.
   * @param callback The callback called when the method finishes.
   */
  void getPersonalizedView(long personalizedViewId,
                           LinxoServiceCallback<PersonalizedView> callback);

  /**
   * Gets the colors that can be used in a {@link PersonalizedView}.
   *
   * @param callback The callback called when the method finishes.
   */
  void getColors(LinxoServiceCallback<List<LinxoColor>> callback);

  /**
   * Gets the colors that can be used when adding a new {@link PersonalizedView}
   * (parameter editedView is null in this case) or when editing an existing
   * {@link PersonalizedView} (parameter editedView is not null in this case).
   *
   * Here is the logic:
   *
   *  - When adding a new {@link PersonalizedView}, all the colors not
   *    already used in an existing view can be used.
   *
   *  - When editing an existing {@link PersonalizedView}, all the colors not
   *    already used in an existing view can be used, plus the color currently
   *    assigned to the view being edited.
   *
   * @param editedView Null if we're creating a new {@link PersonalizedView},
   *                   not null if we're editing an existing {@link PersonalizedView}.
   * @param callback The callback called when the method finishes.
   */
  void getAvailableColors(PersonalizedView editedView, LinxoServiceCallback<List<LinxoColor>> callback);

  /**
   * Gets the color to use when all the views are selected.
   *
   * @param callback The callback called when the method finishes.
   */
  void getAllViewsColor(LinxoServiceCallback<LinxoColor> callback);

  /**
   * Forces a reload of the list of {@link PersonalizedView}
   * at the next call of the service to get them.
   *
   * Used for instance when an account is moved to another view using an
   * {@link com.linxo.client.actions.pfm.account.EditBankAccountAction}.
   */
  void requireReload();
}
