/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 18/12/2014 by hugues.
 */
package com.linxo.client.actions.services;

/**
 * Describes the interface of the Upcoming Transactions Service on the client side.
 * It adds functions to make sure that the next calls will reload the
 * UpcomingTransactions from the server.
 */
public interface ClientUpcomingTransactionService
    extends UpcomingTransactionService
{
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Instance Methods

  /**
   * @return {@code true} if the next call to the service to get the UpcomingTransactions
   * will trigger a call to the server (and therefore will be blocking).
   */
  boolean isReloadRequired();

  /**
   * Force a reload of the Upcoming Transactions at the next call of the service to
   * get Upcoming Transactions.
   */
  void requireReload();
    
  /**
   * Flushes the service from all its data. Used when the user signs out
   */
  void flush();

  /**
   * Asks the client whether it has the UpcomingTransactions permission or not
   * Used to avoid performing useless queries to the server.
   *
   * @return
   */
  boolean hasUpcomingTransactionsPermission();
}
