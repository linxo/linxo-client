/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 17/09/2015 by hugues.
 */
package com.linxo.client.actions.services;

import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.upcoming.IntervalUnit;
import com.linxo.client.dto.budget.BudgetTarget;
import com.linxo.infrastructure.exceptions.FunctionalException;
import com.linxo.infrastructure.exceptions.TechnicalException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This interface describes the API exposed in order to manipulate
 * Budget Target and Budget History DTOs for a given User.
 */

public interface BudgetService {
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Instance Methods

  /**
   * Returns the BudgetTarget object with the given id, or null if it doesn't exist.
   *
   * @param budgetTargetId The id of the budget target we want to get.
   *
   * @return The BudgetTarget object with the given id, or null if it doesn't exist.
   *
   * @throws TechnicalException if a technical problem occurs
   * @throws FunctionalException if the budget target does not belong to the current user
   */
  BudgetTarget getBudgetTarget(long budgetTargetId)
      throws FunctionalException, TechnicalException;

  /**
   * Returns the list of BudgetTargets object the current user has already stored.
   * If a non-null bankAccountId is provided, this call returns only the target
   * for the given account
   *
   * @param bankAccountId - a bankAccountId, possibly {@code null}
   * @return the list (possibly empty) of BudgetTarget objects stored for the current User.
   * @throws TechnicalException if a technical problem occurs
   * @throws FunctionalException if the accountId does not belong to the current user
   */
  ArrayList<BudgetTarget> getBudgetTargets(Long bankAccountId)
      throws FunctionalException, TechnicalException;

  /**
   *
   * @param budgetTarget - the BudgetTarget to save. This object MUST have non-null
   *                     amount, categoryId, intervalUnit, intervalValue and startDate.
   * @return the saved Budget Target with a new Id.
   * @throws FunctionalException if the accountId does not belong to the current user, if she
   * cannot access or use the categoryId, or if one of the non-null fields is null.
   * @throws NullPointerException if the budgetTarget is null
   * @throws TechnicalException if a technical problem occurs
   */
  BudgetTarget addBudgetTarget(BudgetTarget budgetTarget)
      throws FunctionalException, TechnicalException, NullPointerException;

  /**
   * Updates the given budget target.
   *
   * @param budgetTarget - the BudgetTarget to edit for the current user. This BudgetTarget
   *                       MUST have a few fields set:
   *                     amount, categoryId, intervalUnit, intervalValue and startDate.
   *
   * @return the updated BudgetTarget
   * @throws FunctionalException if the accountId does not belong to the user, if she
   * cannot access or use the categoryId, or if any of the non-null fields is null.
   * @throws TechnicalException if a technical problem occurs
   * @throws NullPointerException if the budgetTarget is null
   */
  BudgetTarget editBudgetTarget(BudgetTarget budgetTarget)
      throws FunctionalException, TechnicalException, NullPointerException;

  /**
   * Deletes the budget target.
   *
   * @param budgetTargetId - the ID of the UpcomingTransaction to delete for the current user.
   * @return true if the budget target was removed on the server, false otherwise.
   * @throws FunctionalException if the budget target does not belong to the current user.
   * @throws TechnicalException if a technical problem occurs
   */
  boolean deleteBudgetTarget(long budgetTargetId)
      throws FunctionalException, TechnicalException;


  /**
   * Ask the service to suggest a BudgetTarget for the given BankAccount,
   * category (possibly including the subCategories), every intervalValue x intervalUnit
   * starting at startDate.
   *
   * In the result of the call, the {@link BudgetTarget#getAmount() amount}
   * will match the {@link BudgetTarget#getSuggestedAmount() suggested amount}, so that
   * this suggestion may be {@link #addBudgetTarget(BudgetTarget) immediately saved to the service}.
   *
   * The startDate is the first day of the current period calculated using the given interval
   * unit and interval value.
   * It cannot be after the current date.
   * It cannot be before:
   *   <pre>
   *     current date - (interval unit * interval_value) + 1 day
   *   </pre>
   * We add one day because using current date - (interval unit * interval_value) as the start date
   * is the same than using the current date (i.e. a new interval starts today in both cases).
   *
   * Example 1:
   * <ul>
   *   <li>We're on September 23rd, 2015.</li>
   *   <li>The given interval unit is 'month'.</li>
   *   <li>The given interval value is 3.</li>
   *   <li>So we want a suggestion for a 3-months budget.</li>
   *   <li>The startDate cannot be older than June 24th, 2015.</li>
   * </ul>
   * Example 2:
   * <ul>
   *   <li>We're on September 23rd, 2015.</li>
   *   <li>The given interval unit is 'day'.</li>
   *   <li>The given interval value is 7.</li>
   *   <li>So we want a suggestion for a 7-days budget.</li>
   *   <li>The startDate cannot be older than September 17th, 2015.</li>
   * </ul>
   *
   * @param accountId - the BankAccountID. May be null; in this case applies to all the user's
   *                    accounts included in the trends.
   * @param categoryId - the category id
   * @param includeSubCategories - indicate if the subcategories should be taken into account.
   * @param intervalUnit - the interval unit to consider for the suggestion
   * @param intervalValue - the number of interval unit to consider for the suggestion
   * @param startDate - the startingDate to consider for the suggestion, see above.
   *
   * @return a suggested BudgetTarget object that may be immediately saved
   *
   * @throws FunctionalException if the accountId does not belong to the current user, if she
   *         cannot access or use the categoryId, if a field is null or inconsistent, or if the
   *         given start date is invalid (see above).
   * @throws TechnicalException if a technical problem occurs
   */
  BudgetTarget suggestBudgetTarget(
      Long accountId,
      long categoryId,
      boolean includeSubCategories,
      IntervalUnit intervalUnit,
      int intervalValue,
      LinxoDate startDate
  ) throws FunctionalException, TechnicalException;


  /**
   * <p>
   * Returns a map of the algebraic sum of the transactions associated to the referenced
   * BudgetTarget (account, category, possibly sub categories) and for the periods defined
   * in the BudgetTarget.
   * </p>
   * <p>
   * For instance in the examples given in {@link BudgetTarget}, we could have a call to
   * {@code getBudgetHistory(1234, 2)} that is performed on <strong>march 14th, 2015</strong>
   * return the following value (json-ified) :
   * </p>
   * <pre>
   *   [
   *     // The sum amount of the transactions in february was 500 €
   *     { "day" : 1, "month" : 2, "year" : 2015 } : 500,
   *     // The sum amount of the transactions in march is 245€, but the month is not finished yet
   *     { "day" : 1, "month" : 3, "year" : 2015 } : 245
   *   ]
   * </pre>
   *
   *
   * @param budgetTargetId - id of the Budget target to compare
   * @param historyCount - number of BudgetTarget IntervalUnit in the past to collect compared
   *                     to the current date, including the current IntervalUnit
   * @return a map of (LinxoDate, Amount). The LinxoDate object is the start of the IntervalUnit
   * @throws FunctionalException if the budget target does not belong to the current user.
   * @throws TechnicalException if a technical problem occurs
   */
  HashMap<LinxoDate, Double> getBudgetHistory(long budgetTargetId, int historyCount)
      throws FunctionalException, TechnicalException;

  /**
   * Same as {@link #getBudgetHistory(long, int)} but with a non-stored BudgetTarget.
   *
   * If ever the given BudgetTarget has an id, the corresponding BudgetTarget will be
   * loaded from the DB and the values in the given BudgetTarget will be ignored.
   *
   * @return a map of (LinxoDate, Amount). The LinxoDate object is the start of the IntervalUnit
   * @throws FunctionalException if the budget target does not belong to the current user.
   * @throws TechnicalException if a technical problem occurs
   */
  HashMap<LinxoDate, Double> getBudgetHistory(BudgetTarget budgetTarget, int historyCount)
      throws FunctionalException, TechnicalException;

}
