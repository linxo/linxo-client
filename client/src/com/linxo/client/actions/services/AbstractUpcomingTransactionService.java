/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 28/10/2014 by hugues.
 */
package com.linxo.client.actions.services;

import com.linxo.client.data.DateUtils;
import com.linxo.client.data.LinxoDate;
import com.linxo.client.data.StringUtils;
import com.linxo.client.data.pfm.group.SynchroStatus;
import com.linxo.client.dto.account.BankAccountInfo;
import com.linxo.client.dto.upcoming.BankAccountForecast;
import com.linxo.client.dto.upcoming.BankAccountForecast.NoticeState;
import com.linxo.client.dto.upcoming.UpcomingTransaction;
import com.linxo.client.dto.upcoming.UpcomingTransactionOccurrence;
import com.linxo.infrastructure.exceptions.FunctionalException;
import com.linxo.infrastructure.exceptions.PermissionException;


import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class factors code for the server and client
 * implementation of the service
 */
public abstract class AbstractUpcomingTransactionService
    implements UpcomingTransactionService
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields


  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           API Methods
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o


  // Tested in the server implementation
  @Override
  public BankAccountForecast getForecast(BankAccountInfo bankAccount,  double lowBalanceThreshold, double overdraftThreshold)
  {
    if(bankAccount == null)
    {
      throw new NullPointerException("Cannot getForecast for null AccountInfo");
    }
    if(lowBalanceThreshold < overdraftThreshold) {
      overdraftThreshold = lowBalanceThreshold;
    }

    final LinxoDate now = LinxoDate.today();
    final LinxoDate yesterday = DateUtils.addDays(now, -1);
    double currentBalance = bankAccount.getCurrentBalance();

    // We display an account as "failed" if the last successful synchronization occured before
    // yesterday @0h00.
    // When this is a manual account/group, we do not take care of the currentBalanceDate.
    final LinxoDate currentBalanceDate = DateUtils.getLinxoDate(bankAccount.getCurrentBalanceDate());
    boolean syncFailed =
        bankAccount.getAccountGroupStatus() != SynchroStatus.None  // It is not a manual account.
            && (currentBalanceDate == null                       // never Synchronized
            || currentBalanceDate.compareTo(yesterday) < 0); // before yesterday

    if(syncFailed) {
      BankAccountForecast forecast = new BankAccountForecast();
      forecast.setBankAccountId(bankAccount.getId());
      forecast.setBalance(currentBalance);
      forecast.setBalances(new ArrayList<Double>());

      forecast.setNoticeBalance(currentBalance);
      forecast.setNoticeDate(LinxoDate.today());
      forecast.setNoticeDaySpan(0);

      forecast.setMinimumBalance(currentBalance);
      forecast.setMinimumDate(LinxoDate.today());
      forecast.setMinimumDaySpan(0);
      forecast.setMinimumState(BankAccountForecast.NoticeState.UnSynchronized);

      return forecast;
    }

    int forecastSpan = BankAccountForecast.MAX_FORECAST_SPAN;
    double computedBalance = currentBalance;
    ArrayList<Double> balances = new ArrayList<>(forecastSpan + 2);  // +2 because we add today (current) and today (end of day)
    balances.add(currentBalance);

    // noticeXXX denotes the data when the next status change is expected to happen : which day, what the balance will be etc...
    LinxoDate noticeDate = null;
    Double noticeBalance = null;
    Integer noticeBalanceIndex = null;
    Integer noticeDaySpan = null;

    // noticeXXX denotes the data when the minimum balance is expected to happen : which day, what the balance will be etc...
    LinxoDate minimumDate = now;
    double minimumBalance = currentBalance;
    int minimumBalanceIndex = 0;
    int minimumDaySpan = 0;
    NoticeState minimumState;


    // Get the list of transactions
    ArrayList<UpcomingTransaction> upcomingTransactions;

    try {
      HashMap<Long, ArrayList<UpcomingTransaction>> upcomingTransactionsMap = getUpcomingTransactions(bankAccount.getId());
      upcomingTransactions = upcomingTransactionsMap.get(bankAccount.getId());
    }
    catch(PermissionException pe){

      return getCurrentStateForecast(bankAccount, lowBalanceThreshold, overdraftThreshold);
    }

    // If there are not UT, we need to get the balances whatsoever
    // => An empty list will make the trick.
    if(upcomingTransactions == null)
    {
      upcomingTransactions = new ArrayList<>();
    }

    // We check for each day if every upcoming transaction occurs and then lookup the amount.
    for (int daySpan = 0; daySpan <= forecastSpan; daySpan++) {
      LinxoDate date = DateUtils.addDays(LinxoDate.today(), daySpan);
      double sumThatDay = 0;

      for(UpcomingTransaction upcomingTransaction : upcomingTransactions) {
        // Only query needed occurrences, excluding those due to the matching window...
        // But not when we look for today's forecast (we may have occurrences in the past that are re-scheduled toady).
        ArrayList<UpcomingTransactionOccurrence> occurrences = upcomingTransaction.occurrencesFromTo(date, date, false, (daySpan == 0));
        if(occurrences == null || occurrences.isEmpty()){
          continue;
        }

        // We use all the occurrences that are expected on the current date
        for(UpcomingTransactionOccurrence occurrence : occurrences)
        {
          if(daySpan == 0 && occurrence.getExpectedDate().compareTo(LinxoDate.today()) > 0) {
            // Occurrences obtained for today include the occurrences due to the matching window.
            // But we only want to count the occurrences that happens today or in the near past.
            // => Skip the occurrences obtained for today that happen in the future.
            continue;
          }
          sumThatDay += occurrence.getAmount();
        }
      }

      // At this point, {@code sumThatDay} contains the balance change for that day. This might be 0.
      computedBalance += sumThatDay;
      balances.add(computedBalance);


      if(computedBalance < minimumBalance) {
        minimumBalance = computedBalance;
        minimumBalanceIndex = balances.size() - 1;
        minimumDate = date;
        minimumDaySpan = daySpan;
      }

      // If we have not yet recorded the notice, we check the balances (current and future)
      // and remember it if there is a change.
      if(noticeDate == null) {
        boolean goingBelowOverdraft = (currentBalance >= overdraftThreshold && computedBalance < overdraftThreshold);
        boolean goingAboveOverdraft = (currentBalance < overdraftThreshold && computedBalance >= overdraftThreshold);
        boolean goingBelowLowBalance = (currentBalance > lowBalanceThreshold && computedBalance <= lowBalanceThreshold);
        boolean goingAboveLowBalance = (currentBalance <= lowBalanceThreshold && computedBalance > lowBalanceThreshold);

        if(goingBelowOverdraft || goingAboveOverdraft || goingBelowLowBalance || goingAboveLowBalance) {
          noticeDate = date;
          noticeBalance = computedBalance;
          noticeDaySpan = daySpan;
          noticeBalanceIndex = balances.size() - 1;
        }
      }
    }

    // At this point {@code minimumDate} and {@code minimumBalance} are not null.
    // We compute which icon to display (see spec)
    if(minimumBalance > lowBalanceThreshold) {
      minimumState = NoticeState.Sun;
    }
    else if(minimumBalance <= lowBalanceThreshold && minimumBalance >= overdraftThreshold) {
      minimumState = NoticeState.Clouds;
    }
    else if(currentBalance < overdraftThreshold) {
      minimumState = NoticeState.Tornado;
    }
    else {
      minimumState = NoticeState.Storm;
    }

    // At this point, {@code balances} contains the list of balances at the end of the day
    // for the account for each day between now and (now + forecastSpan days).
    // Also if a notice is to be displayed, {@code noticeDate}, {@code noticeBalance} and
    // {@code noticeDaySpan} are not null;
    if(noticeDate == null) {
      noticeBalanceIndex = balances.size() - 1;
      noticeBalance = balances.get(noticeBalanceIndex);
      noticeDate = DateUtils.addDays(now, forecastSpan);
      noticeDaySpan = forecastSpan;
    }


    BankAccountForecast forecast = new BankAccountForecast();
    forecast.setBankAccountId(bankAccount.getId());
    forecast.setBalance(currentBalance);
    forecast.setBalances(balances);

    forecast.setNoticeBalance(noticeBalance);
    forecast.setNoticeBalanceIndex(noticeBalanceIndex);
    forecast.setNoticeDate(noticeDate);
    forecast.setNoticeDaySpan(noticeDaySpan);

    forecast.setMinimumBalance(minimumBalance);
    forecast.setMinimumBalanceIndex(minimumBalanceIndex);
    forecast.setMinimumDate(minimumDate);
    forecast.setMinimumDaySpan(minimumDaySpan);
    forecast.setMinimumState(minimumState);

    return forecast;
  }

  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o
  //           Utils/Checks Methods
  //-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o-o

  /**
   * Checks that the UpcomingTransaction members are ready for add/edit.
   *
   * This method can be overridden to perform more checks
   *
   * @param upcomingTransaction -
   * @throws NullPointerException - if the transaction is null
   * @throws FunctionalException - if there is an error in the transaction
   */
  protected void checkUpcomingTransaction(UpcomingTransaction upcomingTransaction)
      throws NullPointerException, FunctionalException
  {
    if(upcomingTransaction == null){
      throw new NullPointerException("upcomingTransaction cannot be null");
    }

    if(StringUtils.isEmptyTrimmed(upcomingTransaction.getLabel()))
    {
      throw new FunctionalException("label field cannot be null or empty");
    }

    Long bankAccountId = upcomingTransaction.getAccountId();
    if(bankAccountId == null) {
      throw new FunctionalException("upcomingTransaction.bankAccountId cannot be null");
    }

    final Long categoryId = upcomingTransaction.getCategoryId();
    if(categoryId == null) {
      throw new FunctionalException("upcomingTransaction.categoryId cannot be null");
    }

//    if (upcomingTransaction.getAmount() == 0) {
//      throw new FunctionalException("upcomingTransaction.amount cannot be 0");
//    }
//    if (upcomingTransaction.getTemporalExpression() == null) {
//      throw new FunctionalException("upcomingTransaction.temporalExpression cannot be null");
//    }
//    if (upcomingTransaction.getTemporalExpression().getStartDate() == null) {
//      throw new FunctionalException("upcomingTransaction.temporalExpression.startDate cannot be null");
//    }
//    final LinxoDate startDate = upcomingTransaction.getTemporalExpression().getStartDate();
//    if (DateUtils.getDate(startDate).before(DateUtils.todaysDate())) {
//      throw new FunctionalException("upcomingTransaction.temporalExpression.startDate ["
//                                    + startDate + "] cannot be is the past");
//    }
  }

  /**
   * Checks that the UpcomingTransaction members are ready for add/edit.
   *
   * This method can be overridden to perform more checks
   *
   * @param transactionId - id of the associated transaction
   * @param occurrence - the occurrence
   * @throws NullPointerException -
   * @throws FunctionalException -
   */
  protected void checkModifiedOccurrence(Long transactionId, UpcomingTransactionOccurrence occurrence)
  {
    if (transactionId == null) {
      throw new FunctionalException("transactionId should not be null");
    }

    if (occurrence == null) {
      throw new NullPointerException("occurrence should not be null");
    }

    if (occurrence.getOriginalDate() == null) {
      throw new FunctionalException("New occurrence original date must not be null");
    }

    // If the occurrence is not a "skip", it must have a few values
    // correctly set.
    if (occurrence.getModifiedDate() != null) {
      if (StringUtils.isEmptyTrimmed(occurrence.getLabel())) {
        throw new FunctionalException("label field cannot be null or empty");
      }

      Long bankAccountId = occurrence.getAccountId();
      if (bankAccountId == null) {
        throw new FunctionalException("upcomingTransaction.bankAccountId cannot be null");
      }

      final Long categoryId = occurrence.getCategoryId();
      if (categoryId == null) {
        throw new FunctionalException("occurrence.categoryId cannot be null");
      }

//      if (occurrence.getAmount() == 0) {
//        throw new FunctionalException("occurrence.amount cannot be 0");
//      }
//      if (DateUtils.getDate(occurrence.getModifiedDate()).before(DateUtils.todaysDate())) {
//        throw new FunctionalException("occurrence.modifiedDate [" + occurrence.getModifiedDate()
//                                      + "] cannot be is the past");
//      }
    }
  }


  /**
   * @param bankAccount -
   * @param lowBalanceThreshold -
   * @param overdraftThreshold -
   * @return the forecast for the current state.
   *         This forecast may be displayed by default for non-authorized users.
   */
  private BankAccountForecast getCurrentStateForecast(BankAccountInfo bankAccount,
                                                      double lowBalanceThreshold,
                                                      double overdraftThreshold)
  {
    final double currentBalance = bankAccount.getCurrentBalance();
    final boolean balanceBelowLowBalance = currentBalance <= lowBalanceThreshold;
    final boolean balanceBelowOverdraft = currentBalance <= overdraftThreshold;

    final BankAccountForecast.NoticeState noticeState;
    if(balanceBelowOverdraft) {
      noticeState = BankAccountForecast.NoticeState.Storm;
    }
    else if (balanceBelowLowBalance) {
      noticeState = BankAccountForecast.NoticeState.Clouds;
    }
    else {
      noticeState = BankAccountForecast.NoticeState.Sun;
    }

    BankAccountForecast forecast = new BankAccountForecast();
    forecast.setBankAccountId(bankAccount.getId());
    forecast.setBalance(currentBalance);
    forecast.setBalances(new ArrayList<Double>());

    forecast.setMinimumState(noticeState);
    forecast.setMinimumBalance(currentBalance);
    forecast.setMinimumDate(LinxoDate.today());
    forecast.setMinimumDaySpan(0);

    forecast.setNoticeBalance(currentBalance);
    forecast.setNoticeDate(LinxoDate.today());
    forecast.setNoticeDaySpan(0);

    return forecast;
  }

}
