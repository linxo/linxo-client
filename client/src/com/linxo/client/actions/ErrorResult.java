/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 01/10/2014 by hugues.
 */
package com.linxo.client.actions;

public class ErrorResult
  implements LinxoResult
{

  public enum ErrorCode
  {
    ErrUnknown,            // Unknown error
    ErrBadSignature,       // The device sent an invalid signature
    ErrInvalidSession,     // The device needs to login, the previous session has timed out.
    ErrInvalidSecret,      // The device sent a null or invalid secret.
    ErrActionFormat,       // We've got a problem while parsing the action.
    ErrInvalidTimeStamp,   // The timestamp is not within accepted bounds @see ApiConfiguration#timestampCheckMargin
    ErrNoHandler,          // We've got no handler for such an action. This is a BIG bug !
    ErrPermissionDenied,   // User need to subscribe a better deal to access this feature
    ErrInvalidCredentials, // User credentials in the Hash could not login the user
  }

  private ErrorCode errorCode;

  private String errorMessage;

  private String failedAction;

  @SuppressWarnings("unused")
  private ErrorResult() {}

  ErrorResult(final ErrorCode errorCode, final String errorMessage, final String failedAction)
  {
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
    this.failedAction = failedAction;
  }

  @SuppressWarnings("unused")
  public ErrorCode getErrorCode()
  {
    return errorCode;
  }

  @SuppressWarnings("unused")
  public String getErrorMessage()
  {
    return errorMessage;
  }

  @SuppressWarnings("unused")
  public String getFailedAction()
  {
    return failedAction;
  }


  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer("ErrorResult{");
    sb.append("errorCode=").append(errorCode);
    sb.append(", errorMessage='").append(errorMessage).append('\'');
    sb.append(", failedAction='").append(failedAction).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
