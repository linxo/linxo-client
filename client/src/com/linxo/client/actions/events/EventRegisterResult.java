/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 12:26:55 PM by tarunmalhotra.
*/
package com.linxo.client.actions.events;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.actions.pfm.events.SynchronizationUpdateEvent;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class EventRegisterResult extends SecuredResult
{

  private int queueId;
  private List<SynchronizationUpdateEvent> synchronizationUpdateEvents;

  @SuppressWarnings("unused") // used for s11n
  private EventRegisterResult()
  {}

  public EventRegisterResult(int queueId, List<SynchronizationUpdateEvent> synchronizationUpdateEvents)
  {
    this.queueId = queueId;
    this.synchronizationUpdateEvents = synchronizationUpdateEvents;
  }

  public int getQueueId()
  {
    return queueId;
  }

  public List<SynchronizationUpdateEvent> getSynchronizationUpdateEvents()
  {
    return synchronizationUpdateEvents;
  }
}
