/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2013 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.builders;

import com.linxo.client.data.CategoryType;
import com.linxo.client.data.pfm.bank.AccountType;
import com.linxo.client.actions.pfm.GetTransactionsAction;

import java.util.ArrayList;

/**
 * Builder for the {@link com.linxo.client.actions.pfm.GetTransactionsAction}.
 */
public final class GetTransactionsActionBuilder
{
  private AccountType accountType;
  private ArrayList<AccountType> accountTypes;
  private Long accountId;
  private String accountReference;
  private ArrayList<String> labels;
  private Long categoryId;
  private Long tagId;
  private String startDate;
  private String endDate;
  private String debitDate;
  private Double amount;
  private Double minAmount;
  private Double maxAmount;
  private boolean useBudgetDate;
  private boolean loadSubCategories;
  private boolean loadOnlyTaggedTransactions;
  private CategoryType categoryType;
  @Deprecated
  private Boolean useTrendsAccounts;
  private Boolean useBudgetAccounts;
  private Boolean useSavingsAccounts;
  private Boolean useAssetsAccounts;
  private ArrayList<Long> selectedViewIds;
  private String extCustomType;
  private boolean includeAvailableSums;
  private Long accountGroupId;
  private Long importDate;

  private Integer startRow;
  private Integer numRows;

  public GetTransactionsActionBuilder withAccountType(AccountType accountType)
  {
    this.accountType = accountType;
    return this;
  }

  public GetTransactionsActionBuilder withAccountTypes(ArrayList<AccountType> accountTypes)
  {
    this.accountTypes = accountTypes;
    return this;
  }

  public GetTransactionsActionBuilder withAccountId(Long accountId)
  {
    this.accountId = accountId;
    return this;
  }

  public GetTransactionsActionBuilder withAccountReference(String accountReference)
  {
    this.accountReference = accountReference;
    return this;
  }

  public GetTransactionsActionBuilder withLabels(ArrayList<String> labels)
  {
    this.labels = labels;
    return this;
  }

  public GetTransactionsActionBuilder withCategoryId(Long categoryId)
  {
    this.categoryId = categoryId;
    return this;
  }

  public GetTransactionsActionBuilder withTagId(Long tagId)
  {
    this.tagId = tagId;
    return this;
  }

  public GetTransactionsActionBuilder withStartDate(String startDate)
  {
    this.startDate = startDate;
    return this;
  }

  public GetTransactionsActionBuilder withEndDate(String endDate)
  {
    this.endDate = endDate;
    return this;
  }

  public GetTransactionsActionBuilder withDebitDate(String debitDate)
  {
    this.debitDate = debitDate;
    return this;
  }

  public GetTransactionsActionBuilder withAmount(Double amount)
  {
    this.amount = amount;
    return this;
  }

  public GetTransactionsActionBuilder withMinAmount(Double minAmount)
  {
    this.minAmount = minAmount;
    return this;
  }

  public GetTransactionsActionBuilder withMaxAmount(Double maxAmount)
  {
    this.maxAmount = maxAmount;
    return this;
  }

  public GetTransactionsActionBuilder withUseBudgetDate(boolean useBudgetDate)
  {
    this.useBudgetDate = useBudgetDate;
    return this;
  }

  public GetTransactionsActionBuilder withLoadSubCategories(boolean loadSubCategories)
  {
    this.loadSubCategories = loadSubCategories;
    return this;
  }

  public GetTransactionsActionBuilder withLoadOnlyTaggedTransactions(boolean loadOnlyTaggedTransactions)
  {
    this.loadOnlyTaggedTransactions = loadOnlyTaggedTransactions;
    return this;
  }

  public GetTransactionsActionBuilder withCategoryType(CategoryType categoryType)
  {
    this.categoryType = categoryType;
    return this;
  }

  public GetTransactionsActionBuilder withExtCustomType(String extCustomType)
  {
    this.extCustomType = extCustomType;
    return this;
  }

  public GetTransactionsActionBuilder withSelectedViewIds(ArrayList<Long> selectedViewIds)
  {
    this.selectedViewIds = selectedViewIds;
    return this;
  }

  @Deprecated
  public GetTransactionsActionBuilder withUseTrendsAccounts(Boolean useTrendsAccounts)
  {
    this.useTrendsAccounts = useTrendsAccounts;
    return this;
  }

  public GetTransactionsActionBuilder withUseBudgetAccounts(Boolean useBudgetAccounts)
  {
    this.useBudgetAccounts = useBudgetAccounts;
    return this;
  }

  public GetTransactionsActionBuilder withUseSavingsAccounts(Boolean useSavingsAccounts)
  {
    this.useSavingsAccounts = useSavingsAccounts;
    return this;
  }

  public GetTransactionsActionBuilder withUseAssetsAccounts(Boolean useAssetsAccounts)
  {
    this.useAssetsAccounts = useAssetsAccounts;
    return this;
  }

  public GetTransactionsActionBuilder withIncludeAvailableSums(boolean includeAvailableSums)
  {
    this.includeAvailableSums = includeAvailableSums;
    return this;
  }

  public GetTransactionsActionBuilder withStartRow(Integer startRow)
  {
    this.startRow = startRow == null ? 0 : startRow;
    return this;
  }

  public GetTransactionsActionBuilder withNumRows(Integer numRows)
  {
    this.numRows = numRows == null ? 0 : numRows;
    return this;
  }

  public GetTransactionsActionBuilder withAccountGroupId(Long accountGroupId)
  {
    this.accountGroupId = accountGroupId;
    return this;
  }

  public GetTransactionsActionBuilder withImportDate(Long importDate)
  {
    this.importDate = importDate;
    return this;
  }

  public GetTransactionsAction build()
  {
    if (selectedViewIds != null) {
      if (accountId != null || accountReference != null) {
        throw new ActionBuilderException("Selected views should be null: accountId ["
                                         + accountId + "], accountReference [" + accountReference
                                         + "], selectedViewIds [" + selectedViewIds + "]");
      }
      if (selectedViewIds.isEmpty()) {
        throw new ActionBuilderException("Selected views should not be empty");
      }
    }

    final GetTransactionsAction action = new GetTransactionsAction();

    action.setAccountType(accountType);
    action.setAccountTypes(accountTypes);
    action.setAccountId(accountId);
    action.setAccountReference(accountReference);
    action.setLabels(labels);
    action.setCategoryId(categoryId);
    action.setTagId(tagId);
    action.setStartDate(startDate);
    action.setEndDate(endDate);
    action.setDebitDate(debitDate);
    action.setAmount(amount);
    action.setMinAmount(minAmount);
    action.setMaxAmount(maxAmount);
    action.setUseBudgetDate(useBudgetDate);
    action.setLoadSubCategories(loadSubCategories);
    action.setLoadOnlyTaggedTransactions(loadOnlyTaggedTransactions);
    action.setCategoryType(categoryType);
    action.setExtCustomType(extCustomType);
    action.setSelectedViewIds(selectedViewIds);
    action.setUseTrendsAccounts(useTrendsAccounts);
    action.setUseBudgetAccounts(useBudgetAccounts);
    action.setUseSavingsAccounts(useSavingsAccounts);
    action.setUseAssetsAccounts(useAssetsAccounts);
    action.setIncludeAvailableSums(includeAvailableSums);
    action.setStartRow(startRow);
    action.setNumRows(numRows);
    action.setAccountGroupId(accountGroupId);
    action.setImportDate(importDate);

    return action;

  }

}
