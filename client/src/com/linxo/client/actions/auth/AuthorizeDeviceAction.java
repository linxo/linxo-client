/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 11:32:07 PM by tarunmalhotra.
*/
package com.linxo.client.actions.auth;

import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.device.AppInfo;
import com.linxo.client.actions.pfm.sync.Credential;
import com.linxo.client.actions.LinxoAction;

import java.util.ArrayList;

@JsonAction
public final class AuthorizeDeviceAction
    //LINXO-CLIENT: additional spaces around "Action" added intentionally
    implements LinxoAction <AuthorizeDeviceResult>
{
  @AssertStringSize(nullable = false, voidable = false)
  private String deviceFamily;

  @AssertStringSize(nullable = false, voidable = false)
  private String deviceType;

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String deviceId;

  @AssertStringSize(trimWhenAsserting = false)
  private String identifier;

  @AssertStringSize(trimWhenAsserting = false)
  private String password;

  @AssertStringSize(nullable = false, voidable = false)
  private String deviceName;

  private AppInfo appInfo;

  // For FI authentication.
  private ArrayList<Credential> credentials;

  @SuppressWarnings("unused") // s11n
  public AuthorizeDeviceAction() {}

  @SuppressWarnings("unused") // json only
  public AuthorizeDeviceAction(String deviceFamily, String deviceType, String deviceId,
                               String identifier, String password, String deviceName,
                               AppInfo appInfo)
  {
    this.deviceFamily = deviceFamily;
    this.deviceType = deviceType;
    this.deviceId = deviceId;
    this.identifier = identifier;
    this.password = password;
    this.deviceName = deviceName;
    this.appInfo = appInfo;
  }

  public String getDeviceFamily()
  {
    return deviceFamily;
  }

  public String getDeviceId()
  {
    return deviceId;
  }

  public String getDeviceType()
  {
    return deviceType;
  }

  public String getIdentifier()
  {
    return identifier;
  }

  public String getPassword()
  {
    return password;
  }

  public String getDeviceName()
  {
    return deviceName;
  }

  public AppInfo getAppInfo()
  {
    return appInfo;
  }

  public ArrayList<Credential> getCredentials()
  {
    return credentials;
  }

  public void setCredentials(ArrayList<Credential> credentials)
  {
    this.credentials = credentials;
  }

}
