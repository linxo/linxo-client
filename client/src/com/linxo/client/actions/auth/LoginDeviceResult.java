/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful; but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 11:34:32 PM by tarunmalhotra.
*/
package com.linxo.client.actions.auth;

import com.linxo.client.data.auth.AuthStatus;
import com.linxo.client.dto.device.AppInfo;
import com.linxo.client.dto.sync.Key;
import com.linxo.client.dto.user.DealInfo;
import com.linxo.client.dto.user.PermissionInfo;
import com.linxo.client.dto.user.UserProfileInfo;
import com.linxo.client.actions.LinxoResult;

import java.util.ArrayList;
import java.util.HashSet;

public final class LoginDeviceResult
    implements LinxoResult //LINXO-CLIENT: additional spaces around "Result" added intentionally
{
  private boolean blocked;
  private boolean askForTermsAndConditions;
  private AuthStatus status;
  private DealInfo dealInfo;
  private HashSet<PermissionInfo> permissions;
  private AppInfo appInfo;
  private UserProfileInfo userProfileInfo;

  // For FI authentication.
  private String publicKey;
  private ArrayList<Key> requestedKeys = new ArrayList<Key>();
  private String errorCode;
  private String errorMessage;
  private Long groupId;

  @SuppressWarnings("unused") // s11n
  private LoginDeviceResult() {}

  public LoginDeviceResult(final boolean blocked,
                           final boolean askForTermsAndConditions,
                           final AuthStatus status,
                           final DealInfo dealInfo,
                           final HashSet<PermissionInfo> permissions,
                           final AppInfo appInfo,
                           final UserProfileInfo userProfileInfo)
  {
    this.blocked = blocked;
    this.askForTermsAndConditions = askForTermsAndConditions;
    this.status = status;
    this.dealInfo = dealInfo;
    this.appInfo = appInfo;
    this.userProfileInfo = userProfileInfo;
    this.permissions = permissions;
  }

  public LoginDeviceResult(final boolean blocked,
                           final boolean askForTermsAndConditions,
                           final AuthStatus status,
                           final DealInfo dealInfo,
                           final HashSet<PermissionInfo> permissions,
                           final AppInfo appInfo,
                           final UserProfileInfo userProfileInfo,
                           final String publicKey,
                           final ArrayList<Key> requestedKeys,
                           final String errorCode,
                           final String errorMessage,
                           final Long groupId)
  {
    this(blocked, askForTermsAndConditions, status, dealInfo, permissions,
         appInfo, userProfileInfo);

    // For FI authentication.
    this.publicKey = publicKey;
    this.requestedKeys = requestedKeys;
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
    this.groupId = groupId;
  }

  public boolean isAskForTermsAndConditions()
  {
    return askForTermsAndConditions;
  }

  public boolean isBlocked()
  {
    return blocked;
  }

  public AuthStatus getStatus()
  {
    return status;
  }

  public DealInfo getDeal()
  {
    return dealInfo;
  }

  public AppInfo getAppInfo()
  {
    return appInfo;
  }

  public UserProfileInfo getUserProfileInfo()
  {
    return userProfileInfo;
  }

  public HashSet<PermissionInfo> getPermissions()
  {
    return permissions;
  }

  public String getPublicKey()
  {
    return publicKey;
  }

  public ArrayList<Key> getRequestedKeys()
  {
    return requestedKeys;
  }

  public String getErrorCode()
  {
    return errorCode;
  }

  public String getErrorMessage()
  {
    return errorMessage;
  }

  public Long getGroupId()
  {
    return groupId;
  }

}
