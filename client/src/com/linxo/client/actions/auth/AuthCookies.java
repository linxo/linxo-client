/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/06/2010 by hugues.
 */
package com.linxo.client.actions.auth;

public interface AuthCookies
{
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  /**
   * Key for the authentication cookie between the server and the GWT client
   */
  static final String SESSION_COOKIE_NAME = "LinxoSession";

  /**
   * Key for the "remember me" cookie
   */
  static final String REMEMBER_COOKIE_NAME = "LinxoEmailRememberMe";
  static final int    REMEMBER_COOKIE_MAX_AGE = 15 * 86400;// 15 days in seconds

  static final String SESSION_TIMEOUT_COOKIE_NAME = "LinxoSessionTimeout";
  static final int    SESSION_TIMEOUT_COOKIE_MAX_AGE = REMEMBER_COOKIE_MAX_AGE;

  static final String VIEW_SELECTION_COOKIES = "LinxoPViewSelection";

  // Instance Methods
}
