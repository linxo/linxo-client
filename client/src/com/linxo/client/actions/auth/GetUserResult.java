/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 06/07/2010 by hugues.
 */
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredResult;

import java.util.Date;

/**
 * @deprecated use {@link com.linxo.client.actions.user.GetUserProfileResult} instead
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@Deprecated
public final class GetUserResult extends SecuredResult
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private String title;
  private String firstName;
  private String lastName;
  private Date lastConnection;
  private int sessionTimeout;
  private String email;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // used for s11n
  private GetUserResult(){}

  public GetUserResult(String title, String firstName, String lastName, Date lastConnection, int sessionTimeout, String email)
  {
    this.firstName = firstName;
    this.lastConnection = lastConnection;
    this.lastName = lastName;
    this.title = title;
    this.sessionTimeout = sessionTimeout;
    this.email = email;
  }

// Instance Methods


  public String getFirstName()
  {
    return firstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public String getTitle()
  {
    return title;
  }

  public Date getLastConnection()
  {
    return lastConnection;
  }

  public int getSessionTimeout()
  {
    return sessionTimeout;
  }

  public String getEmail()
  {
    return email;
  }
}
