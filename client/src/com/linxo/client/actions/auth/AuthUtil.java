/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/06/2010 by hugues.
 */
package com.linxo.client.actions.auth;


public class AuthUtil {

  // Nested Types (mixing inner and static classes is okay)
  public enum PassStrength {
    Weakest,
    Weak,
    Mediocre,
    Strong,
    Strongest,
  }


  // Static Fields

  public static final String LOGIN_TOKEN   = "Login";

  // Static Initializers

  // Static Methods

  /**
   * Must be compatible with {@code LoginValidator}
   * @param password the password string to be evaluated
   * @return the strength of the password
   */
  public static PassStrength checkPasswordStrength(String password) {
    /*
     * Derived  from :
     *              Jim Sloey - jsl...@justwild.us PasswordCheck.Java
     *              which were derived from Steve Moitozo's passwdmeter
     *              See http://www.geekwisdom.com/dyn/passwdmeter
     */

    boolean upper = false,
        lower = false,
        numbers = false,
        special = false;
    int intScore = 0,
        length = 0;

    if (password == null)
      return PassStrength.Weakest;

    // PASSWORD LENGTH
    length = password.length();
    if (length < 5) // length 4 or less
    {
      intScore += 3;
    }
    else if (length > 4 && password.length() < 8 ) // length between 5 and 7
    {
      intScore += 6;
    }
    else if( length > 7 && password.length() < 16 ) // length between 8 and 15
    {
      intScore += 12;
    }
    else if (length > 15) // length 16 or more
    {
      intScore += 18;
    }

    // LETTERS
    if (password.matches(".*[a-z]+.*"))
    {
      intScore += 1;
      lower=true;
    }
    if (password.matches(".*[A-Z]+.*"))
    {
      intScore += 5;
      upper=true;
    }

    // NUMBERS
    if (password.matches(".*[0-9]+.*"))
    {
      intScore += 5;
      numbers=true;
    }
    if (password.matches(".*[0-9].*[0-9].*[0-9].*"))
    {
      intScore += 3;
    }

    // SPECIAL CHAR
    if (password.matches(".*[:,!,@,#,$,%,^,&,*,?,_,~]+.*"))
    {
      intScore += 5;
      special=true;
    }
    if (password.matches(".*[:,!,@,#,$,%,^,&,*,?,_,~].*[:,!,@,#,$,%,^,&,*,?,_,~].*"))
    {
      intScore += 3;
    }

    // COMBOS
    if (upper && lower ) // [verified] both upper and lower case
    {
      intScore += 2;
    }
    if ((upper || lower ) && numbers ) // [verified] both letters and numbers
    {
      intScore += 2;
    }
    if ((upper || lower ) && numbers && special) // [verified] letters, numbers, and special characters
    {
      intScore += 2;
    }
    if (upper  && lower  && numbers  && special ) // [verified] upper, lower, numbers, and special characters
    {
      intScore += 2;
    }

    // Check score
    if (intScore < 16)
    {
      return PassStrength.Weakest;
    }
    else if (intScore > 15 && intScore < 25)
    {
      return PassStrength.Weak;
    }
    else if (intScore > 24 && intScore < 35)
    {
      return PassStrength.Mediocre;
    }
    else if (intScore > 34 && intScore < 45)
    {
      return PassStrength.Strong;
    }
    else
    {
      return PassStrength.Strongest;
    }
  }


  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  public enum SignUpStep {
    Information,
    Security,
    Activation,
    Login,
  }

  public enum ResetPasswordStep {
    RequestKey,
    VerifyKey,
    AnswerQuestion,
    ChangePassword,
  }
}
