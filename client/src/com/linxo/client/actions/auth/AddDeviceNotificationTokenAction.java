/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 7:54:01 PM by tarunmalhotra.
*/
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public final class AddDeviceNotificationTokenAction extends SecuredAction<AddDeviceNotificationTokenResult>
{
  @AssertStringSize(nullable = false)
  private String deviceId;

  @AssertStringSize(trimWhenAsserting = false)
  private String notificationToken;

  private Boolean iosBadge;
  private Boolean iosSound;
  private Boolean iosAlert;

  @SuppressWarnings("unused")
  // s11n
  private AddDeviceNotificationTokenAction()
  {
  } // nothing

  @SuppressWarnings("unused")
  // json only
  public AddDeviceNotificationTokenAction(String deviceId, String notificationToken)
  {
    this.deviceId = deviceId;
    this.notificationToken = notificationToken;
  }

  @SuppressWarnings("unused")
  // json only (Only for IOS)
  public AddDeviceNotificationTokenAction(String deviceId, String notificationToken, Boolean iosBadge,
                                          Boolean iosSound, Boolean iosAlert)
  {
    this.deviceId = deviceId;
    this.notificationToken = notificationToken;
    this.iosAlert = iosAlert;
    this.iosBadge = iosBadge;
    this.iosSound = iosSound;
  }

  public String getDeviceId()
  {
    return deviceId;
  }

  public void setDeviceId(String deviceId)
  {
    this.deviceId = deviceId;
  }

  /**
   * The token that we receive is a base 64 encoded String. This is stored in the db as a base64 encoded string.
   * Before sending this to the APNS
   * we need to decode this string and convert it into a byte[]
   *
   * @return the token
   */
  public String getNotificationToken()
  {
    return notificationToken;
  }

  public void setNotificationToken(String notificationToken)
  {
    this.notificationToken = notificationToken;
  }

  public Boolean getIosAlert()
  {
    return iosAlert;
  }

  public Boolean getIosBadge()
  {
    return iosBadge;
  }

  public Boolean getIosSound()
  {
    return iosSound;
  }
}
