/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 11:32:07 PM by tarunmalhotra.
*/
package com.linxo.client.actions.auth;

import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.device.AppInfo;
import com.linxo.client.actions.pfm.sync.Credential;
import com.linxo.client.actions.LinxoAction;

import java.util.ArrayList;

@JsonAction
public final class LoginDeviceAction
    //LINXO-CLIENT: additional spaces around "Action" added intentionally
    implements LinxoAction <LoginDeviceResult>
{
  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String token;

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String deviceId;

  private AppInfo appInfo;

  // For FI authentication.
  private ArrayList<Credential> credentials;

  @SuppressWarnings("unused") // s11n
  private LoginDeviceAction() {}

  @SuppressWarnings("unused") // Json only
  public LoginDeviceAction(String token, String deviceId, AppInfo appInfo)
  {
    this.token = token;
    this.deviceId = deviceId;
    this.appInfo = appInfo;
  }

  public String getDeviceId()
  {
    return deviceId;
  }

  public String getToken()
  {
    return token;
  }

  public AppInfo getAppInfo()
  {
    return appInfo;
  }

  public ArrayList<Credential> getCredentials()
  {
    return credentials;
  }

  public void setCredentials(ArrayList<Credential> credentials)
  {
    this.credentials = credentials;
  }

}
