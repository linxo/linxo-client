/*
   Copyright (c) 2008-2018 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
 */
package com.linxo.client.actions.auth;


import com.linxo.client.actions.JsonAction;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;


/**
 * This action allows to login a given user to Linxo simply based on his id.
 * The IP of the sender must be allowed to do so.
 */
@JsonAction
public final class TrustedLoginAction
    extends SecuredAction<TrustedLoginResult> {

    @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
    private String userId;
    
    @AssertStringSize
    private String deviceId;
    
    @AssertStringSize
    private String trustedLoginSecret;

    @SuppressWarnings("unused") // s11n
    private TrustedLoginAction() {
        // nothing
    }

    public TrustedLoginAction(final String userId, final String deviceId, final String trustedLoginSecret) {
        this.userId = userId;
        this.deviceId = deviceId;
        this.trustedLoginSecret = trustedLoginSecret;
    }

    public String getUserId() {
        return userId;
    }
    
    public String getDeviceId() {
        return deviceId;
    }
    

    public String getTrustedLoginSecret() {
        return trustedLoginSecret;
    }

    @Override
    public String toString() {
        return "TrustedLoginAction{"
               + "userId=" + userId
               + ", deviceId=" + deviceId
               + ", trustedLoginSecret=" + trustedLoginSecret
               + '}';
    }

}
