/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 01/07/2010 by hugues.
 */
package com.linxo.client.actions.auth.rp;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class SecretQuestionVerifyAction extends SecuredAction<SecretQuestionVerifyResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  @AssertStringSize
  private String answer;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused")
  private SecretQuestionVerifyAction(){}

  public SecretQuestionVerifyAction(String answer)
  {
    this.answer = answer;
  }

  // Instance Methods

  public String getAnswer()
  {
    return answer;
  }
}
