/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 02/07/2010 by hugues.
 */
package com.linxo.client.actions.auth.rp;

import com.linxo.client.actions.SecuredResult;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class ChangePasswordResult
    extends SecuredResult
{
  public enum Status
  {
    BadParameterFailure,
    Success,
  }

  private Status status;
  private String email;

  @SuppressWarnings("unused")
  private ChangePasswordResult() {}

  public ChangePasswordResult(Status status)
  {
    this.status = status;
  }

  public ChangePasswordResult(Status status, String email)
  {
    this.status = status;
    this.email = email;
  }

  public Status getStatus()
  {
    return status;
  }

  public String getEmail()
  {
    return email;
  }

}
