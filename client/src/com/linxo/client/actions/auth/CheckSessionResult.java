/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 05/07/2010 by hugues.
 */
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredResult;

/**
 * Receiving the result means that the session is okay,
 * otherwise an error is returned.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class CheckSessionResult extends SecuredResult
{
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  public CheckSessionResult(){}


  // Instance Methods
}
