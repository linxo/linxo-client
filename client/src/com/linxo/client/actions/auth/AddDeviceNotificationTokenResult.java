/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 7:54:16 PM by tarunmalhotra.
*/
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredResult;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public class AddDeviceNotificationTokenResult extends SecuredResult
{
  private boolean result;

  private AddDeviceNotificationTokenResult(){}

  public AddDeviceNotificationTokenResult(boolean result)
  {
    this.result = result;
  }

  public boolean isResult()
  {
    return result;
  }
}
