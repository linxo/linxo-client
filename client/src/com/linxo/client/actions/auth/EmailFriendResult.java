/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredResult;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public class EmailFriendResult
  extends SecuredResult
{
  public enum Status
  {
    InvalidEmail,
    CustomMessageTooLong,
    InvalidEmailAndCustomMessageTooLong,
    Success
  }

  private Status status;

  @SuppressWarnings("unused")
  private EmailFriendResult() {}

  public EmailFriendResult(final Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

}
