/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/06/2010 by hugues.
 */
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredResult;

/**
 * In case of successful login action, this bean contains the user_id, the first and last names
 * and the user profile Map. Other fields are worthless
 *
 * In case of failure. This beans has the fields user_id, first and last names and the profile
 * Map set to <code>null</code>. <code>blocked</code> indicates if this attempt has blocked
 * the user from login in from the IP address contained in <code>blockedAddress</code>.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public class LoginResult extends SecuredResult
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // if everything goes well...
  private Long   userId;
  private String firstName;
  private String lastName;
  private boolean isActivated;
  private boolean askForTermsAndConditions;

  // if things go bad
  private boolean  blocked;

  // Instance Initializers

  // Constructors

  protected LoginResult(){}

  public LoginResult(boolean blocked, Long userId,
                     String firstName, String lastName,
                     boolean isActivated, boolean askForTermsAndConditions)
  {
    this.blocked = blocked;
    this.firstName = firstName;
    this.lastName = lastName;
    this.isActivated = isActivated;
    this.askForTermsAndConditions = askForTermsAndConditions;
    this.userId = userId;
  }


  public String getFirstName()
  {
    return firstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public Long getUserId()
  {
    return userId;
  }

  public boolean isActivated()
  {
    return isActivated;
  }

  public boolean isAskForTermsAndConditions()
  {
    return askForTermsAndConditions;
  }

  public boolean isBlocked()
  {
    return blocked;
  }

  @Override
  public String toString()
  {
    final StringBuilder sb = new StringBuilder();
    sb.append("LoginResult");
    sb.append("{askForTermsAndConditions=").append(askForTermsAndConditions);
    sb.append(", userId=").append(userId);
    sb.append(", firstName='").append(firstName).append('\'');
    sb.append(", lastName='").append(lastName).append('\'');
    sb.append(", isVerified=").append(isActivated);
    sb.append(", blocked=").append(blocked);
    sb.append('}');
    return sb.toString();
  }
}
