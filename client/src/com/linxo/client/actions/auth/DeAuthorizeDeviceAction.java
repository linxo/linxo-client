/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 12/02/2011 by hugues.
 */
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public final class DeAuthorizeDeviceAction extends SecuredAction<DeAuthorizeDeviceResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String deviceId;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private DeAuthorizeDeviceAction(){}

  public DeAuthorizeDeviceAction(String deviceId)
  {
    this.deviceId = deviceId;
  }

  // Instance Methods

  public String getDeviceId()
  {
    return deviceId;
  }
}
