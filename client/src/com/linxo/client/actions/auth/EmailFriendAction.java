/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.auth;

import com.linxo.client.data.ValidationConstants;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 *
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class EmailFriendAction
    extends SecuredAction<EmailFriendResult>
{
  @AssertStringSize(voidable = false)
  private String destinationEmail;

  @AssertStringSize(ValidationConstants.EMAIL_FRIENDS_CUSTOM_MESSAGE_MAX_LENGTH)
  private String customMessage;
  private boolean ccSender;

  @SuppressWarnings("unused")
  private EmailFriendAction() {}

  public EmailFriendAction(final String destinationEmail, final String customMessage,
                           final boolean ccSender)
  {
    this.destinationEmail = destinationEmail;
    this.customMessage = customMessage;
    this.ccSender = ccSender;
  }

  public String getDestinationEmail()
  {
    return destinationEmail;
  }

  public String getCustomMessage()
  {
    return customMessage;
  }

  public boolean isCcSender()
  {
    return ccSender;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append( "EmailFriendAction" );
    sb.append( "{destinationEmail=" ).append( destinationEmail );
    sb.append( ", ccSender=" ).append( ccSender );
    sb.append( '}' );
    return sb.toString();
  }

}
