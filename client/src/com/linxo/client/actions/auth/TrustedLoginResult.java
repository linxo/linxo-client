/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/06/2010 by hugues.
 */
package com.linxo.client.actions.auth;

/**
 * In case of successful login action, this bean contains the user_id, the first and last names
 * and the user profile Map. Other fields are worthless
 *
 * In case of failure. This beans has the fields user_id, first and last names and the profile
 * Map set to <code>null</code>. <code>blocked</code> indicates if this attempt has blocked
 * the user from login in from the IP address contained in <code>blockedAddress</code>.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class TrustedLoginResult extends LoginResult
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Initializers

  // Constructors
  
  @SuppressWarnings("unused") // s11n
  protected TrustedLoginResult() {
      super();
  }

  public TrustedLoginResult(boolean blocked, Long userId,
                     String firstName, String lastName,
                     boolean isActivated, boolean askForTermsAndConditions)
  {
    super(blocked, userId, firstName, lastName, isActivated, askForTermsAndConditions);
  }

  @Override
  public String toString()
  {
    final StringBuilder sb = new StringBuilder();
    sb.append("LoginResult");
    sb.append("{askForTermsAndConditions=").append(this.isAskForTermsAndConditions());
    sb.append(", userId=").append(this.getUserId());
    sb.append(", firstName='").append(this.getFirstName()).append('\'');
    sb.append(", lastName='").append(this.getLastName()).append('\'');
    sb.append(", isVerified=").append(this.isActivated());
    sb.append(", blocked=").append(this.isBlocked());
    sb.append('}');
    return sb.toString();
  }
}
