/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 12/02/2011 by hugues.
 */
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.device.AppInfo;

@JsonAction
public final class DeAuthorizeDeviceNotLoggedInAction
    extends SecuredAction<DeAuthorizeDeviceNotLoggedInResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String deviceId;

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String token;

  private AppInfo appInfo;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private DeAuthorizeDeviceNotLoggedInAction(){}

  @SuppressWarnings("unused") // json only
  public DeAuthorizeDeviceNotLoggedInAction(String deviceId,String token, AppInfo appInfo)
  {
    this.deviceId = deviceId;
    this.token = token;
    this.appInfo = appInfo;
  }

  // Instance Methods

  public String getDeviceId()
  {
    return deviceId;
  }

  public String getToken()
  {
    return token;
  }

  public AppInfo getAppInfo()
  {
    return appInfo;
  }
}
