/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 07/07/2010 by hugues.
 */
package com.linxo.client.actions.auth;

import com.linxo.client.data.LinxoUrlParameter;
import com.linxo.client.data.ViewToken;
import com.linxo.client.actions.SecuredResult;

public final class LogoutResult extends SecuredResult
{
  public enum LogoutMessageType{
    USER,
    SYSTEM,
    DELETION,
    EXPIREDSUBSCRIPTION,
    PROMO_SUCCESS,
    PROMO_FAILURE,
    CANCELLED_IN_PAYPAL,
  }

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private String nextUrl;

  private LogoutMessageType logoutMessageType;

  private boolean transform;


  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused")
  private LogoutResult(){}

  public LogoutResult(String nextURL) {
    this.nextUrl = nextURL;
    transform = false;
  }

  public LogoutResult(String nextUrl, LogoutMessageType logoutMessageType)
  {
    this.nextUrl = nextUrl;
    this.logoutMessageType = logoutMessageType;
    transform = true;
  }

  // Instance Methods

  public String getNextUrl()
  {
    if (transform) {
      //noinspection StringBufferReplaceableByString
      return new StringBuilder()
          .append(nextUrl)
          .append(ViewToken.PARAM_SEPARATOR)
          .append(LinxoUrlParameter.Message.getParameter())
          .append(ViewToken.VALUE_SEPARATOR)
          .append(logoutMessageType.name())
          .toString();
    }
    else {
      return nextUrl;
    }
  }

}
