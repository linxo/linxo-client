/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/06/2010 by hugues.
 */
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action allows to login a given user to Linxo.
 * Note:
 *   This action can only be user with un-limited api-key. Using this action with a limited key
 *   will result in an error.
 */
@SuppressWarnings({ "DeserializableClassInSecureContext" })
@GwtAction
@JsonAction
public final class LoginAction extends SecuredAction<LoginResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String  email;

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String  password;

  private boolean rememberMe;
  private int     sessionTimeout;

  private Boolean secretMode;

  // Instance Initializers

  // Constructors
  @SuppressWarnings("unused")
  private LoginAction(){}

  /**
   *
   * @param email - the email identifier
   * @param password - the user password
   * @param rememberMe - a boolean that asks the server to set a remember me cookie
   * @param sessionTimeout - duration of the session in minutes. Max: 30 minutes.
   */
  public LoginAction(String email, String password, boolean rememberMe, int sessionTimeout)
  {
    this.email = email;
    this.password = password;
    this.rememberMe = rememberMe;
    this.sessionTimeout = sessionTimeout;
  }

  // Instance Methods

  public String getEmail()
  {
    return email;
  }

  public String getPassword()
  {
    return password;
  }

  public boolean isRememberMe()
  {
    return rememberMe;
  }

  public int getSessionTimeout()
  {
    return sessionTimeout;
  }

  public Boolean getSecretMode()
  {
    return secretMode;
  }

  public void setSecretMode(Boolean secretMode)
  {
    this.secretMode = secretMode;
  }

  @Override
  public String toString()
  {
    return "LoginAction{"
           + "email='" + email + '\''
           + ", rememberMe=" + rememberMe
           + ", sessionTimeout=" + sessionTimeout
           + ", secretMode=" + secretMode
           + '}';
  }

}
