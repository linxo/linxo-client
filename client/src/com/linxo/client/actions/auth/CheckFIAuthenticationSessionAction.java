/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.JsonAction;

/**
 * An action to check whether there is a valid FI authentication context.
 * TO BE USED ONLY FOR FI AUTHENTICATION.
 */
@JsonAction
public class CheckFIAuthenticationSessionAction
    extends SecuredAction<CheckFIAuthenticationSessionResult>
{
}
