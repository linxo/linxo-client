/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 06/04/2011 by hugues.
 */
package com.linxo.client.actions.auth.signup;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action and its result allows the user to sign up in a single pass.
 * It is intended to be used through an API signed call
 */
@JsonAction
@GwtAction
public final class SignUpAction
    extends SecuredAction<SignUpResult>
{
  @AssertStringSize
  private String firstName;

  @AssertStringSize
  private String lastName;

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String email;

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String password;

  @AssertStringSize
  private String question;

  @AssertStringSize
  private String answer;

  @AssertStringSize
  private String zipCode;

  private boolean acceptPartnerOffers;
  private boolean acceptTermsAndConditions;

  @AssertStringSize
  private String sponsorCode;

  @SuppressWarnings("unused") // s11n
  private SignUpAction()
  {}

  @SuppressWarnings("unused") // api action
  public SignUpAction(String email, String password)
  {
    this.email = email;
    this.password = password;
  }

  @SuppressWarnings("unused") // api action
  public SignUpAction(boolean acceptPartnerOffers, boolean acceptTermsAndConditions,
                      String email, String firstName, String lastName,
                      String zipCode, String password,
                      String answer, String question, String sponsorCode)
  {
    this.acceptPartnerOffers = acceptPartnerOffers;
    this.acceptTermsAndConditions = acceptTermsAndConditions;
    this.answer = answer;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
    this.question = question;
    this.zipCode = zipCode;
    this.sponsorCode = sponsorCode;
  }

  // Instance Methods

  public boolean isAcceptPartnerOffers()
  {
    return acceptPartnerOffers;
  }

  public boolean isAcceptTermsAndConditions()
  {
    return acceptTermsAndConditions;
  }

  public String getAnswer()
  {
    return answer;
  }

  public String getEmail()
  {
    return email;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public String getPassword()
  {
    return password;
  }

  public String getQuestion()
  {
    return question;
  }

  public String getZipCode()
  {
    return zipCode;
  }

  public String getSponsorCode()
  {
    return sponsorCode;
  }

}
