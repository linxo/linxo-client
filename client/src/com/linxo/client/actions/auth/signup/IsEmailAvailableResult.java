/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 03/06/2010 by hugues.
 */
package com.linxo.client.actions.auth.signup;

import com.linxo.client.actions.SecuredResult;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class IsEmailAvailableResult extends SecuredResult
{

  // Nested Types (mixing inner and static classes is okay)

  /**
   * Defines the Status of the result. If another error happens, it will be sent though the
   * onFailure handler.
   */
  public enum Status {
    Success,
    AlreadyUsed,
  }

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private Status status;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // used in serialization
  private IsEmailAvailableResult(){}

  public IsEmailAvailableResult(Status status)
  {
    this.status = status;
  }

  // Instance Methods

  public Status getStatus()
  {
    return status;
  }
}
