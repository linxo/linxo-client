/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 06/04/2011 by hugues.
 */
package com.linxo.client.actions.auth.signup;

import com.linxo.client.actions.SecuredResult;

import java.util.ArrayList;

public final class SignUpResult extends SecuredResult {

  // Nested Types (mixing inner and static classes is okay)
  public enum Status {
    Success, // what is not Success is an error
    EmailAlreadyExistFailure,
    FieldFailure, // see list of fields
    GeneralFailure,
  }

  public enum Field {
    FirstName,
    LastName,
    Email,
    Password,
    Question,
    Answer,
    ZipCode,
//    AcceptPartnersOffers, // not an error in the fields
    AcceptTermsAndConditions,
    SponsorCode, // the provided sponsor code is not in the DB
  }

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private Status status;
  private ArrayList<Field> fields;
  

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  private SignUpResult()
  {}

  public SignUpResult(Status status, ArrayList<Field> fields)
  {
    this.fields = fields;
    this.status = status;
  }

  // Instance Methods

  /**
   * @return the list of fields that have failed
   */
  public ArrayList<Field> getFields()
  {
    return fields;
  }

  public Status getStatus()
  {
    return status;
  }
}
