/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 07/07/2010 by hugues.
 */
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public final class LogoutAction extends SecuredAction<LogoutResult>
{

  // Nested Types (mixing inner and static classes is okay)

  public enum LogoutReason
  {
    User,
    System,
    AccountDeletion,
    ExpiredSubscription,
  }

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private LogoutReason logoutReason;

  // Instance Initializers

  // Constructors


  @SuppressWarnings("unused") // used by s11n
  private LogoutAction(){}

  public LogoutAction(LogoutReason logoutReason)
  {
    this.logoutReason = logoutReason;
  }


  // Instance Methods

  public LogoutReason getLogoutReason()
  {
    return logoutReason;
  }

}
