/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 3:22:49 PM by tarunmalhotra.
*/
package com.linxo.client.actions.auth;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public class UpdateDeviceIdAction extends SecuredAction<UpdateDeviceIdResult>
{
  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String deviceId;

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String deviceToken;

  @AssertStringSize(nullable = false, voidable = false, trimWhenAsserting = false)
  private String newDeviceId;

  @SuppressWarnings("unused")
  // s11n
  private UpdateDeviceIdAction()
  {
  }

  public UpdateDeviceIdAction(String deviceId, String newDeviceId, String deviceToken)
  {
    this.deviceId = deviceId;
    this.newDeviceId = newDeviceId;
    this.deviceToken = deviceToken;
  }

  public String getDeviceId()
  {
    return deviceId;
  }

  public String getDeviceToken()
  {
    return deviceToken;
  }

  public String getNewDeviceId()
  {
    return newDeviceId;
  }
}
