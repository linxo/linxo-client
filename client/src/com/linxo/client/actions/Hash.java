/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 30/09/2014 by hugues.
 */
package com.linxo.client.actions;

/**
 * Encapsulates the signature information to authenticate the commands sent to the server.
 */
@SuppressWarnings({
    "FieldCanBeLocal",   // Just doing encapsulation for the JSON parser
    "UnusedDeclaration"  // -- idem --
})
public class Hash
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  private String nonce;
  private String timeStamp;
  private String apiKey;
  private String signature;

  // Stateless identification with the email/password
  private String identifier;
  private String password;

  // Stateless identification with the deviceId/token
  private String deviceId;
  private String token;
  private String appIdentifier;   // These fields come in replacement of the AppInfo object when the
  private String appVersion;      // stateless device login occurs.

  // Instance Fields

  // Instance Initializers

  // Constructors

  private Hash(String nonce, String timeStamp, String apiKey, String signature,
              String identifier, String password,
              String deviceId, String token, String appIdentifier, String appVersion)
  {
    this.apiKey = apiKey;
    this.appIdentifier = appIdentifier;
    this.appVersion = appVersion;
    this.deviceId = deviceId;
    this.identifier = identifier;
    this.nonce = nonce;
    this.password = password;
    this.signature = signature;
    this.timeStamp = timeStamp;
    this.token = token;
  }

  /**
   * Creates a stateful Hash
   * @param nonce - the nonce, arbitrarily chosen
   * @param timeStamp - the long timestamp (epoch in seconds)
   * @param apiKey - the api Key
   * @param signature - the signature, according to the documentation
   */
  public Hash(String nonce, String timeStamp, String apiKey, String signature)
  {
    this(nonce, timeStamp, apiKey, signature, null, null, null, null);
  }

  /**
   * Creates a Stateless Hash with device identifiers
   * @param nonce - the nonce, arbitrarily chosen
   * @param timeStamp - the long timestamp (epoch in seconds)
   * @param apiKey - the api Key
   * @param signature - the signature, according to the documentation
   * @param deviceId - the deviceId used to register the device
   * @param token - the token received form the server through a previous call to
   *                {@link com.linxo.client.actions.auth.AuthorizeDeviceAction}
   * @param appIdentifier - the app identifier for the user's UI on the web site,
   *                      a simple string describing your application to users
   *                      (e.g. "Yoyodyne Inc. Banking App", or "com.yoyodyne.banking")
   * @param appVersion - A string representing the version of your app
   */
  public Hash(String nonce, String timeStamp, String apiKey, String signature,
              String deviceId, String token, String appIdentifier, String appVersion)
  {
    this(nonce, timeStamp, apiKey, signature, null, null, deviceId, token, appIdentifier, appVersion);
  }

  /**
   * Creates a Stateless Hash with user identifiers
   * @param nonce - the nonce, arbitrarily chosen
   * @param timeStamp - the long timestamp (epoch in seconds)
   * @param apiKey - the api Key
   * @param signature - the signature, according to the documentation
   * @param identifier - the User login email
   * @param password - the User password
   */
  public Hash(String nonce, String timeStamp, String apiKey, String signature, String identifier, String password)
  {
    this(nonce, timeStamp, apiKey, signature, identifier, password, null, null, null, null);
  }

  // Instance Methods

}
