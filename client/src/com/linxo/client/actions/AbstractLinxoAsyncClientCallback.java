/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions;

import  com.linxo.client.actions.SecuredResult;

/**
 * Abstract {@link LinxoAsyncClientCallback} to be used in services
 * using {@link LinxoServiceCallback}.
 *
 * This class defines the onFailure() method, just calling onFailure()
 * on the given {@link LinxoServiceCallback}.
 */
public abstract class AbstractLinxoAsyncClientCallback<T, R extends SecuredResult>
  implements LinxoAsyncClientCallback<R>
{
  private LinxoServiceCallback<T> callback;

  protected AbstractLinxoAsyncClientCallback(LinxoServiceCallback<T> callback)
  {
    this.callback = callback;
  }

  @Override
  public void onFailure()
  {
    callback.onFailure();
  }

  @Override
  public void onThrowable(Throwable throwable)
  {
    callback.onThrowable(throwable);
  }

}
