/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 01/10/2014 by hugues.
 */
package com.linxo.client.actions;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.UUID;

public class HashUtils
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  private static final Logger logger = Logger.getLogger(HashUtils.class);

  public static final String CHARSET = "UTF-8";
  public static final String SHA_1 = "SHA-1";

  public static final String BC_PROVIDER = "BC";

  // Static Initializers

  // Static Methods

  // Calculate the signature of the package
  /**
   * @param message to be signed sign
   * @return base64( sha1 message )
   * @throws RuntimeException if something goes wrong while signing the message
   */
  public static String hashSha1Base64String(String message) throws RuntimeException
  {
    return Base64.encodeBase64String(hashSha1String(message).getBytes())
        .replaceAll("\r\n", "");
  }

  public static String hashSha1String(String message) throws RuntimeException
  {
    return hashString(message, SHA_1);
  }

  private static String hashString(String message, String algorithm) throws RuntimeException
  {
    try{
      byte[] messageBytes = message.getBytes(CHARSET);

      MessageDigest hash = MessageDigest.getInstance(algorithm, BC_PROVIDER);
      hash.update(messageBytes);

      //proceed ....
      final String digest = Hex.encodeHexString(hash.digest());
      if(logger.isTraceEnabled()){
        logger.trace("SHA1 digest["+ digest + "]");
      }
      return digest;
    }
    catch(UnsupportedEncodingException uee) {
      throw new RuntimeException("Impossible to sign message ["+message+"]", uee);
    }
    catch(NoSuchAlgorithmException nsae) {
      throw new RuntimeException("Impossible to sign message ["+message+"]", nsae);
    }
    catch(NoSuchProviderException nspe) {
      throw new RuntimeException("Impossible to sign message ["+message+"]", nspe);
    }
  }

  // Generate nonce based on random UUID
  public static String generateNonce()
  {
    return UUID.randomUUID().toString();
  }

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods
}
