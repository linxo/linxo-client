/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.purchases;

import com.linxo.client.actions.SecuredResult;

public final class ValidatePurchaseResult
    extends SecuredResult
{
  public static enum Status
  {
    Success,
    InvalidPurchase,
    AlreadyPurchased,
    Error,
  }

  private Status status;

  @SuppressWarnings("unused") // s11n
  private ValidatePurchaseResult()
  {
    this.status = null;
  }

  public ValidatePurchaseResult(final Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

}
