/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.purchases;

import com.linxo.client.data.subscriptions.DealSource;
import com.linxo.client.data.subscriptions.PurchaseProperties;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.user.PaymentInfo;

import java.util.HashMap;

@GwtAction
@JsonAction
public final class ValidatePurchaseAction
    extends SecuredAction<ValidatePurchaseResult>
{
  private DealSource dealSource;

  @AssertStringSize
  private String offerReference;

  private PaymentInfo initialPayment;

  private HashMap<PurchaseProperties, String> purchaseProperties;

  @SuppressWarnings("unused") // s11n
  public ValidatePurchaseAction()
  {}

  public ValidatePurchaseAction(final HashMap<PurchaseProperties, String> purchaseProperties,
                                final String offerReference, final DealSource dealSource,
                                final PaymentInfo initialPayment)
  {
    this.purchaseProperties = purchaseProperties;
    this.offerReference = offerReference;
    this.initialPayment = initialPayment;
    this.dealSource = dealSource;
  }

  public DealSource getDealSource()
  {
    return dealSource;
  }

  public String getOfferReference()
  {
    return offerReference;
  }

  public HashMap<PurchaseProperties, String> getPurchaseProperties()
  {
    return purchaseProperties;
  }

  public PaymentInfo getInitialPayment()
  {
    return initialPayment;
  }

  @Override
  public String toString()
  {
    //noinspection StringBufferReplaceableByString
    final StringBuilder sb = new StringBuilder("ValidatePurchaseAction{");
    sb.append("dealSource=").append(dealSource);
    sb.append(", offerReference='").append(offerReference).append('\'');
    sb.append(", initialPayment=").append(initialPayment);
    sb.append(", purchaseProperties=").append(purchaseProperties);
    sb.append('}');
    return sb.toString();
  }
}
