/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 23/01/2015 by hugues.
 */
package com.linxo.client.actions.support.json;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * The above class uses the default serialization to get a JSON tree (represented by JsonElement),
 * and then calls the hook method {@link CustomizedTypeAdapterFactory#beforeWrite beforeWrite()}
 * to allow the subclass to customize that tree.
 * Similarly for de-serialization with {@link CustomizedTypeAdapterFactory#afterRead afterRead()}.
 * @param <C> the customized class
 */
public abstract class CustomizedTypeAdapterFactory<C>
    implements TypeAdapterFactory
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields
  private final Class<C> customizedClass;

  // Instance Initializers

  // Constructors
  public CustomizedTypeAdapterFactory(Class<C> customizedClass) {
    this.customizedClass = customizedClass;
  }

  // Instance Methods

  @SuppressWarnings("unchecked") // we use a runtime check to guarantee that 'C' and 'T' are equal
  @Override
  public final <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
    return type.getRawType() == customizedClass
        ? (TypeAdapter<T>) customizeMyClassAdapter(gson, (TypeToken<C>) type)
        : null;
  }

  private TypeAdapter<C> customizeMyClassAdapter(Gson gson, TypeToken<C> type) {
    final TypeAdapter<C> delegate = gson.getDelegateAdapter(this, type);
    final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);
    return new TypeAdapter<C>() {
      @Override public void write(JsonWriter out, C value) throws IOException {
        JsonElement tree = delegate.toJsonTree(value);
        beforeWrite(value, tree);
        elementAdapter.write(out, tree);
      }
      @Override public C read(JsonReader in) throws IOException
      {
        JsonElement tree = elementAdapter.read(in);
        afterRead(tree);
        C value = delegate.fromJsonTree(tree);
        afterParse(value);
        return value;
      }
    };
  }

  /**
   * Override this to muck with {@code toSerialize} before it is written to
   * the outgoing JSON stream.
   * @param source the source object to serialize
   * @param toSerialize the object that will be used to write the JSON
   */
  @SuppressWarnings({"unused", "DesignedForExtension"})
  protected void beforeWrite(C source, JsonElement toSerialize)
  {}

  /**
   * Override this to muck with {@code deserialized} before it parsed into
   * the application type.
   *
   * @param deserialized the object
   */
  @SuppressWarnings({"unused", "DesignedForExtension"})
  protected void afterRead(JsonElement deserialized)
  {}

  /**
   * Override this to muck with {@code result} after it is parsed into
   * the application type.
   * @param result the object to inspect and potentially update before the TypeAdapter returns it.
   */
  @SuppressWarnings({"unused", "DesignedForExtension"})
  protected void afterParse(C result)
  {}

}
