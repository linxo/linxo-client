/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions;

import  com.linxo.client.actions.SecuredAction;
import  com.linxo.client.actions.SecuredResult;

/**
 * This interface provides abstractions to the Action/Response transmission between
 * the client and the server in order to have common service code across various Java
 * client implementations (GWT, Android).
 *
 * Each implementation will have to implement the given Interface and provide them to services
 * via IoC or directly when constructing the service objects.
 */
public interface LinxoAsyncClient
{
  /**
   * Sends the given action and calls the given callback when the result is available.
   *
   * @param action The action to send.
   * @param callback The callback to call when the result is available.
   * @param <R> The result class for this call.
   */
  <R extends SecuredResult> void sendAction(SecuredAction<R> action, LinxoAsyncClientCallback<R> callback);
}
