package com.linxo.client.actions;

import org.apache.http.cookie.Cookie;

import java.util.List;

/**
 * A class wrapping a {@link LinxoResult} and a list of cookies.
 * The list of cookies can be re-used to send actions in the same
 * session (linxo server session).
 */
public class LinxoClientResult<R extends LinxoResult>
{
  private final List<Cookie> cookies;

  private final R linxoResult;

  public LinxoClientResult(final List<Cookie> cookies, final R linxoResult)
  {
    this.cookies = cookies;
    this.linxoResult = linxoResult;
  }

  public final List<Cookie> getCookies()
  {
    return cookies;
  }

  public final R getLinxoResult()
  {
    return linxoResult;
  }

}
