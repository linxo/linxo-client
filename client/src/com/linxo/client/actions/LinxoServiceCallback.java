/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions;

/**
 * Callback interface to be used in asynchronous services like the
 * {@link com.linxo.client.actions.services.personalizedviews.ClientPersonalizedViewsService}.
 *
 */
public interface LinxoServiceCallback<T>
{
  /**
   * Called when everything went fine.
   *
   * @param t The expected result of the call.
   */
  void onSuccess(T t);

  /**
   * An error occurred.
   */
  void onFailure();

  /**
   * An error occurred.
   *
   * @param throwable The Throwable thrown during the call.
   */
  void onThrowable(Throwable throwable);
}
