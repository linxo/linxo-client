/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 *
 */
@GwtAction
@JsonAction
public final class GetTagsAction
  extends SecuredAction<GetTagsResult>
{}
