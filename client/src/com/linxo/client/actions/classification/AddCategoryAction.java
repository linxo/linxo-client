/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * Here are the checks made by the server when it receives a AddCategoryAction:
 *  - If the user is not premium, an ErrorResult with a permission error is returned.
 *  - If the category name is null or empty (even once trimmed),
 *    a AddCategoryResult is returned with status Error.
 *  - If the category name is longer than 100 (once trimmed),
 *    a AddCategoryResult is returned with status Error.
 *  - If the category color is null, empty (even once trimmed) or unexpected,
 *    a AddCategoryResult is returned with status Error.
 *  - If the category icon is null, empty (even once trimmed) or unexpected,
 *    a AddCategoryResult is returned with status Error.
 *  - If the parent category is missing, a AddCategoryResult is returned with status Error.
 *  - If the parent category is unknown, a AddCategoryResult is returned with status Error.
 *  - If the parent category belongs to another user,
 *    a AddCategoryResult is returned with status Error.
 *  - If the parent category is the income theme,
 *    a AddCategoryResult is returned with status Error.
 *  - If the parent category is the uncategorized theme or one of its categories,
 *    a AddCategoryResult is returned with status Error.
 *  - If the parent category is the out-of-budget theme or one of its categories,
 *    a AddCategoryResult is returned with status Error.
 *  - If the parent category is a subcategory, a AddCategoryResult is returned with status Error.
 *  - If the parent category already has 10 subcategories,
 *    a AddCategoryResult is returned with status Error.
 *  - If there is an error saving the category to the DB or another unexpected error,
 *    a AddCategoryResult is returned with status Error.
 *
 * Possible colors are:
 * Custom:
 *  - e6411c
 *  - f07f0a
 *  - e00669
 *  - 80bd26
 *  - f8ea0d
 *  - 212282
 *  - aa0622
 *  - b56115
 *  - 72093e
 *  - e2badb
 * Basic:
 *  - c06378
 *  - e5d187
 *  - e2ac73
 *  - 6cb2a9
 *  - 64749a
 *  - 861251
 *  - e38e7d
 *  - 896e9b
 *  - bfc886
 *  - 3caee7
 *  - 64b425
 *  - d179a8
 *  - 7c7c7c
 *
 * Possible icons are:
 *   "custom-0", "custom-1", "custom-2", "custom-3", "custom-4", "custom-5", "custom-6", "custom-7",
 *   "custom-8", "custom-9", "custom-10", "custom-11", "custom-12", "custom-13",
 *   "0", "100", "200", "300",
 *   "200100", "200110", "200120", "200130", "200140", "200150", "200160", "200170", "200180", "200190",
 *   "400000", "400100", "400110", "400120", "400130", "400140", "400150", "400160", "400170", "400180", "400190", "400111", "400112",
 *   "400200", "400205", "400210", "400220", "400230", "400240", "400250", "400260", "400270", "400280", "400290",
 *   "400300", "400310", "400320", "400330", "400340", "400350",
 *   "400400", "400410", "400420", "400430", "400440", "400450", "400460", "400470",
 *   "400500", "400510", "400520", "400530", "400540",
 *   "400600", "400610", "400620",
 *   "400700", "400710", "400720", "400730", "400740", "400750", "400760",
 *   "400800", "400810", "400820", "400830", "400840", "400850",
 *   "400900", "400910", "400920", "400930", "400940",
 *   "401000", "401010", "401020", "401030", "401040", "401050", "401060", "401070", "401080",
 *   "600100", "600110", "600120", "600130", "600140", "600150", "600160", "600170"
 */
@GwtAction
@JsonAction
public final class AddCategoryAction
    extends SecuredAction<AddCategoryResult>
{

  @AssertStringSize(nullable = false, voidable = false)
  private String name;

  @AssertStringSize(nullable = false, voidable = false)
  private String color;

  private String icon;

  private Long parentId;

  @SuppressWarnings("unused") // s11n
  public AddCategoryAction() {}

  public AddCategoryAction(String name, String color, String icon, Long parentId)
  {
    this.name = name;
    this.color = color;
    this.icon = icon;
    this.parentId = parentId;
  }

  public String getName()
  {
    return name;
  }

  public String getColor()
  {
    return color;
  }

  public String getIcon()
  {
    return icon;
  }

  public Long getParentId()
  {
    return parentId;
  }

}
