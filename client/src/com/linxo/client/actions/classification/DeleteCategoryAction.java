/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * Here are the checks made by the server when it receives a DeleteCategoryAction:
 * <ul>
 *   <li>
 *     If the user is not premium, an ErrorResult with a permission error is returned.
 *   </li>
 *   <li>
 *     If the categoryId or the destinationId is null, a AddCategoryResult is returned with status Error.
 *   </li>
 *   <li>
 *     If the category is unknown, a AddCategoryResult is returned with status Error.
 *   </li>
 *   <li>
 *     If the destination category is unknown, a AddCategoryResult is returned with status Error.
 *   </li>
 *   <li>
 *     If the category is a default (as opposed to custom) category or if it belongs to another user,
 *     a AddCategoryResult is returned with status Error.
 *   </li>
 *   <li>
 *     If the destination category belongs to another user, a AddCategoryResult is returned with status Error.
 *   </li>
 *   <li>
 *     If the destination category is a masking category (a masking category is a category used
 *     to mask a default category, masking categories are not exposed to clients),
 *     a AddCategoryResult is returned with status Error.
 *   </li>
 *   <li>
 *     If the destination category is a theme, other than the uncategorized theme,
 *     a AddCategoryResult is returned with status Error.
 *   </li>
 *   <li>
 *     If there is an error saving the category to the DB or another unexpected error,
 *     a AddCategoryResult is returned with status Error.
 *   </li>
 * </ul>
 *
 * Deleting a category also deletes its potential associated budget targets.
 */
@GwtAction
@JsonAction
public final class DeleteCategoryAction
    extends SecuredAction<DeleteCategoryResult>
{
  private Long categoryId;

  private Long destinationId;

  @SuppressWarnings("unused") // s11n
  public DeleteCategoryAction() {}

  public DeleteCategoryAction(Long categoryId, Long destinationId)
  {
    this.categoryId = categoryId;
    this.destinationId = destinationId;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }

  public Long getDestinationId()
  {
    return destinationId;
  }

}
