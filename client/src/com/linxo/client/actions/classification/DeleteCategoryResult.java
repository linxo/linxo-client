/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredResult;

public final class DeleteCategoryResult
    extends SecuredResult
{
  public enum Status
  {
    Success,
    Error
  }

  private Status status;

  @SuppressWarnings("unused") // s11n
  public DeleteCategoryResult() {}

  public DeleteCategoryResult(Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

}
