/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 15/01/2015 by hugues.
 */
package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * This action counts all the items attached to the given Category#id.
 *
 * This action should be invoked when checking in the UI how many items are
 * using the given category prior to hide/remove it.
 */
@GwtAction
@JsonAction
public class CountItemsForCategoryAction
  extends SecuredAction<CountItemsForCategoryResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private long categoryId;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  public CountItemsForCategoryAction()
  {}

  public CountItemsForCategoryAction(long categoryId)
  {
    this.categoryId = categoryId;
  }

  // Instance Methods

  public long getCategoryId()
  {
    return categoryId;
  }
}
