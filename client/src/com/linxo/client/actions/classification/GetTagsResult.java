/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.tx.TagInfo;

import java.util.ArrayList;

/**
 *
 */
public final class GetTagsResult
  extends SecuredResult
{
  private ArrayList<TagInfo> tags;

  @SuppressWarnings("unused")
  private GetTagsResult() {}

  public GetTagsResult(final ArrayList<TagInfo> tags)
  {
    this.tags = tags;
  }

  public ArrayList<TagInfo> getTags()
  {
    return tags;
  }

}
