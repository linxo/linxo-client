/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>


   Created on : 07/10/2014 by hugues.
 */
package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredResult;

public final class AddCategoryResult
    extends SecuredResult
{
  public enum Status
  {
    Success,
    Error
  }

  private Status status;

  private Long categoryId;

  @SuppressWarnings("unused") // s11n
  public AddCategoryResult() {}

  public AddCategoryResult(Status status)
  {
    this.status = status;
  }

  public AddCategoryResult(Status status, Long categoryId)
  {
    this.status = status;
    this.categoryId = categoryId;
  }

  public Status getStatus()
  {
    return status;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }

}
