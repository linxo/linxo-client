/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * Returns the list of categories available. This action must be performed with a user logged in
 * as we reserve to add user-owned categories later.
 */
@GwtAction
@JsonAction
public class GetCategoriesAction
  extends SecuredAction<GetCategoriesResult>
{
}
