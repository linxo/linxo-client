/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2015 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 15/01/2015 by hugues.
 */
package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredResult;

public class CountItemsForCategoryResult
    extends SecuredResult
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private int transactionCount;
  private int upcomingTransactionCount;

  private int totalCount;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") // s11n
  public CountItemsForCategoryResult()
  {}

  public CountItemsForCategoryResult(int totalCount, int transactionCount, int upcomingTransactionCount)
  {
    this.totalCount = totalCount;
    this.transactionCount = transactionCount;
    this.upcomingTransactionCount = upcomingTransactionCount;
  }

  // Instance Methods

  public int getTotalCount()
  {
    return totalCount;
  }

  public int getTransactionCount()
  {
    return transactionCount;
  }

  public int getUpcomingTransactionCount()
  {
    return upcomingTransactionCount;
  }


  @Override
  public String toString()
  {
    final StringBuilder sb = new StringBuilder("CountItemsForCategoryResult{");
    sb.append("totalCount=").append(totalCount);
    sb.append(", transactionCount=").append(transactionCount);
    sb.append(", upcomingTransactionCount=").append(upcomingTransactionCount);
    sb.append('}');
    return sb.toString();
  }
}
