/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * Here are the checks made by the server when it receives a HideCategoryAction:
 *  - If the user is not premium, an ErrorResult with a permission error is returned.
 *  - If the categoryId or the destinationId is null,
 *    a AddCategoryResult is returned with status Error.
 *  - If the category to hide is unknown, a AddCategoryResult is returned with status Error.
 *  - If the destination category is unknown,
 *    a AddCategoryResult is returned with status Error.
 *  - If the category to hide is a not default (as opposed to custom) category,
 *    a AddCategoryResult is returned with status Error.
 *  - If the category to hide is a masking category (a masking category is a category
 *    used to mask a default category, masking categories are not exposed to clients),
 *    a AddCategoryResult is returned with status Error.
 *  - If the category to hide is a theme, a AddCategoryResult is returned with status Error.
 *  - If the category to hide is the uncategorized theme or one of its categories,
 *    a AddCategoryResult is returned with status Error.
 *  - If the category to hide is the out-of-budget theme or one of its categories,
 *    a AddCategoryResult is returned with status Error.
 *  - If the category to hide is already hidden,
 *    a AddCategoryResult is returned with status Error.
 *  - If the category to hide belongs to another user,
 *    a AddCategoryResult is returned with status Error.
 *  - If the destination category is a masking category (a masking category is a category
 *    used to mask a default category, masking categories are not exposed to clients),
 *    a AddCategoryResult is returned with status Error.
 *  - If the destination category is a them other than the uncategorized theme,
 *    a AddCategoryResult is returned with status Error.
 *  - If there is an error saving the category to the DB or another unexpected error,
 *    a AddCategoryResult is returned with status Error.
 */
@GwtAction
@JsonAction
public final class HideCategoryAction
    extends SecuredAction<HideCategoryResult>
{
  private Long categoryId;

  private Long destinationId;

  @SuppressWarnings("unused") // s11n
  public HideCategoryAction() {}

  public HideCategoryAction(Long categoryId, Long destinationId)
  {
    this.categoryId = categoryId;
    this.destinationId = destinationId;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }

  public Long getDestinationId()
  {
    return destinationId;
  }

}
