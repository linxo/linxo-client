/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * Here are the checks made by the server when it receives a ShowCategoryAction:
 *  - If the user is not premium, an ErrorResult with a permission error is returned.
 *  - If the categoryId is null, a AddCategoryResult is returned with status Error.
 *  - If the category to show is unknown, a AddCategoryResult is returned with status Error.
 *  - If the category to show is not a default (as opposed to custom) category,
 *    a AddCategoryResult is returned with status Error.
 *  - If the category to show is not currently hidden,
 *    a AddCategoryResult is returned with status Error.
 *  - If there is an error saving the category to the DB or another unexpected error,
 *    a AddCategoryResult is returned with status Error.
 */
@GwtAction
@JsonAction
public final class ShowCategoryAction
    extends SecuredAction<ShowCategoryResult>
{
  private Long categoryId;

  @SuppressWarnings("unused") // s11n
  public ShowCategoryAction() {}

  public ShowCategoryAction(Long categoryId)
  {
    this.categoryId = categoryId;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }

}
