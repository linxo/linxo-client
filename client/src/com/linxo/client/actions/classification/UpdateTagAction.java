/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 2:05:10 AM by tarunmalhotra.
*/
package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@SuppressWarnings({"DeserializableClassInSecureContext"})
@GwtAction
@JsonAction
public final class UpdateTagAction extends SecuredAction<UpdateTagResult>
{
  private long tagId;

  @AssertStringSize(nullable = false, voidable = false)
  private String tagName;

  @SuppressWarnings("unused") // s11n
  protected UpdateTagAction(){}
  
  public UpdateTagAction(long tagId, String tagName)
  {
    this.tagId = tagId;
    this.tagName = tagName;
  }

  public long getTagId()
  {
    return tagId;
  }

  public String getTagName()
  {
    return tagName;
  }

  @Override
  public String toString()
  {
    final StringBuffer sb = new StringBuffer();
    sb.append("UpdateTagAction");
    sb.append("{tagId=").append(tagId);
    sb.append(", tagName=").append(tagName);
    sb.append('}');
    return sb.toString();
  }
}
