/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.classification;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.tx.CatInfo;

import java.util.ArrayList;

/**
 *
 */
public final class GetCategoriesResult
  extends SecuredResult
{
  public static final String CAT_INFOS = "catInfos";
  public static final String NEXT_COLOR = "nextColor";

  private ArrayList<CatInfo> catInfos;

  private String nextColor;

  @SuppressWarnings("unused")
  private GetCategoriesResult() {}

  public GetCategoriesResult(final ArrayList<CatInfo> catInfos, final String nextColor)
  {
    this.catInfos = catInfos;
    this.nextColor = nextColor;
  }

  /**
   *
   * @return The list of themes. The whole tree of categories is loaded.
   */
  public ArrayList<CatInfo> getCatInfos()
  {
    return catInfos;
  }

  /**
   * @return the next color to be used when creating a parent custom category
   */
  public String getNextColor()
  {
    return nextColor;
  }

}
