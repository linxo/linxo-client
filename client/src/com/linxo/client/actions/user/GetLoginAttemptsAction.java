/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

/**
 * LinxoAction to get the user's login attempts.
 */
@GwtAction
@JsonAction
public final class GetLoginAttemptsAction
    extends SecuredAction<GetLoginAttemptsResult>
{
  public static final int DEFAULT_START_ROW = 0;
  public static final int DEFAULT_MAX_ROWS = 100;

  /**
   * Optional. If null, 0 will be used.
   */
  private Integer startRow;

  /**
   * Optional. If null, {@link #DEFAULT_MAX_ROWS 100} will be used.
   */
  private Integer maxRows;

  @SuppressWarnings("unused") // s11n
  private GetLoginAttemptsAction() {}

  public GetLoginAttemptsAction(final Integer startRow, final Integer maxRows)
  {
    this.startRow = startRow;
    this.maxRows = maxRows;
  }

  public final Integer getStartRow()
  {
    return startRow;
  }

  public final Integer getMaxRows()
  {
    return maxRows;
  }

}
