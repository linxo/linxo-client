/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.user;

import com.linxo.client.data.subscriptions.DealSource;
import com.linxo.client.data.subscriptions.Platform;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
public final class GetOffersAction
    extends SecuredAction<GetOffersResult> {

  /**
   * The deal source we want the available offers for.
   *
   * @deprecated You should now use the {@link Platform}
   *             parameter instead.
   */
  @Deprecated
  private DealSource dealSource;

  /**
   * The platform the user is currently on and
   * we want to get the available offers for.
   */
  private Platform platform;

  @SuppressWarnings("unused") // s11n
  private GetOffersAction() {
    // Nothing.
  }

  @Deprecated
  public GetOffersAction(DealSource dealSource) {
    this.dealSource = dealSource;
  }

  public GetOffersAction(Platform platform) {
    this.platform = platform;
  }

  @Deprecated
  public DealSource getDealSource() {
    return dealSource;
  }

  public Platform getPlatform() {
    return platform;
  }

}
