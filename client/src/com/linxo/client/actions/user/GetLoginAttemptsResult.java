/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.user.LoginAttempt;

import java.util.ArrayList;

/**
 * LinxoResult for the {@link GetLoginAttemptsAction}.
 * Contains a list of {@link LoginAttempt}.
 */
public final class GetLoginAttemptsResult
    extends SecuredResult
{
  private ArrayList<LoginAttempt> loginAttempts;

  @SuppressWarnings("unused") // s11n
  private GetLoginAttemptsResult() {}

  public GetLoginAttemptsResult(final ArrayList<LoginAttempt> loginAttempts)
  {
    this.loginAttempts = loginAttempts;
  }

  public final ArrayList<LoginAttempt> getLoginAttempts()
  {
    return loginAttempts;
  }

}
