/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 3:54:02 PM by tarunmalhotra.
*/
package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;
import com.linxo.client.dto.user.UserPreferenceInfo;

@Deprecated // UpdateUserPropertiesAction/Result should now be used
@GwtAction
@JsonAction
@SuppressWarnings({ "DeserializableClassInSecureContext", "deprecation" })
public final class UpdateUserPreferenceAction
    extends SecuredAction<UpdateUserPreferenceResult>
{
  private UserPreferenceInfo userPreferenceInfo;

  @SuppressWarnings("unused")
  protected UpdateUserPreferenceAction()
  {
  }

  public UpdateUserPreferenceAction(UserPreferenceInfo userPreferenceInfo)
  {
    this.userPreferenceInfo = userPreferenceInfo;
  }

  public UserPreferenceInfo getUserPreferenceInfo()
  {
    return userPreferenceInfo;
  }

}
