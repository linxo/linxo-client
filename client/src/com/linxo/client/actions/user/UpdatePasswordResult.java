/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 12:21:57 PM by tarunmalhotra.
*/
package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredResult;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class UpdatePasswordResult extends SecuredResult
{
  @Deprecated
  private int result;
  private ResultStatus resultStatus;
  public enum ResultStatus
  {
    SUCCESS(1),
    FAIL_OLD_PASSWORD(-1),
    FAIL_NEW_PASSWORD(0);

    private int value;
    private static final Map<Integer, ResultStatus> lookup = new HashMap<Integer, ResultStatus>();
    static {
      for (ResultStatus r : ResultStatus.values())
        lookup.put(r.getValue(), r);
    }
    private ResultStatus(final int value) {
      this.value = value;
    }

    public int getValue() {
      return this.value;
    }

    public static ResultStatus get(int value) {
      return lookup.get(value);
    }
  }

  @SuppressWarnings("unused")
  private UpdatePasswordResult(){}

  public UpdatePasswordResult(ResultStatus resultStatus)
  {
    this.result = resultStatus.getValue();
    this.resultStatus = resultStatus;
  }

  @Deprecated
  public int getResult()
  {
    return this.result;
  }

  public ResultStatus getResultStatus() {
    return resultStatus;
  }
}
