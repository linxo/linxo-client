/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2009 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   Created on : 12:21:57 PM by tarunmalhotra.
*/
package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.user.UserProfileInfo;

import java.util.Date;

@SuppressWarnings({ "DeserializableClassInSecureContext" })
public final class GetUserProfileResult extends SecuredResult
{
  private UserProfileInfo userProfileInfo;
  private Date lastConnection;
  private int sessionTimeout;
  private Boolean adminSession;

  @SuppressWarnings("unused")
  private GetUserProfileResult(){}

  public GetUserProfileResult(UserProfileInfo userProfileInfo, Date lastConnection, int sessionTimeout,
                              Boolean adminSession)
  {
    this.userProfileInfo = userProfileInfo;
    this.lastConnection = lastConnection;
    this.sessionTimeout = sessionTimeout;
    this.adminSession = adminSession;
  }

  public UserProfileInfo getUserInfo()
  {
    return userProfileInfo;
  }

  public Date getLastConnection()
  {
    return lastConnection;
  }

  public int getSessionTimeout()
  {
    return sessionTimeout;
  }

  public Boolean getAdminSession()
  {
    return adminSession;
  }

}
