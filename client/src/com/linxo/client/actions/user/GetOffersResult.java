/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.user.OfferInfo;

import java.util.ArrayList;

public final class GetOffersResult
    extends SecuredResult
{
  private ArrayList<OfferInfo> offers;
  private int daysWhenFollow;
  private int daysWhenShare;
  private int monthsWhenSponsored;

  @SuppressWarnings("unused")
  private GetOffersResult()
  {
    this.offers = null;
    this.daysWhenFollow = 1;
    this.daysWhenShare = 1;
    this.monthsWhenSponsored = 1;
  }

  public GetOffersResult(final ArrayList<OfferInfo> offers, final int daysWhenFollow,
                         final int daysWhenShare, final int monthsWhenSponsored)
  {
    this.offers = offers;
    this.daysWhenFollow = daysWhenFollow;
    this.daysWhenShare = daysWhenShare;
    this.monthsWhenSponsored = monthsWhenSponsored;
  }

  public ArrayList<OfferInfo> getOffers()
  {
    return offers;
  }

  public int getDaysWhenFollow()
  {
    return daysWhenFollow;
  }

  public int getDaysWhenShare()
  {
    return daysWhenShare;
  }

  public int getMonthsWhenSponsored()
  {
    return monthsWhenSponsored;
  }
}
