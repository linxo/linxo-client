/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 07/02/2013 by hugues.
 */
package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredResult;
import com.linxo.client.dto.user.PermissionInfo;

import java.util.HashSet;

public final class GetPermissionsResult extends SecuredResult
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private HashSet<PermissionInfo> permissions;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused")
  private GetPermissionsResult()
  {}

  public GetPermissionsResult(HashSet<PermissionInfo> permissions)
  {
    this.permissions = permissions;
  }

  // Instance Methods

  public HashSet<PermissionInfo> getPermissions()
  {
    return permissions;
  }
}
