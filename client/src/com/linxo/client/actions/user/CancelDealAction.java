/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
@SuppressWarnings({"DeserializableClassInSecureContext"})
public final class CancelDealAction
    extends SecuredAction<CancelDealResult>
{
  private long dealId;

  @SuppressWarnings("unused") // s11n
  private CancelDealAction() {}

  public CancelDealAction(long dealId)
  {
    this.dealId = dealId;
  }

  public long getDealId()
  {
    return dealId;
  }

}
