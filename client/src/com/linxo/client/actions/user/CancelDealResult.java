/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.user;

import com.linxo.client.actions.SecuredResult;

@SuppressWarnings({"DeserializableClassInSecureContext"})
public final class CancelDealResult
    extends SecuredResult
{
  public static enum Status
  {
    Success,
    Error,
  }

  private Status status;

  @SuppressWarnings("unused") // s11n
  private CancelDealResult() {}

  public CancelDealResult(Status status)
  {
    this.status = status;
  }

  public Status getStatus()
  {
    return status;
  }

}
