/* Imported by LinxoImporter - DO NOT EDIT */

package com.linxo.client.actions.secretmode;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.actions.GwtAction;

@GwtAction
public final class SecretModeAction
    extends SecuredAction<SecretModeResult>
{
  private boolean enable;

  @SuppressWarnings("unused") // s11n
  public SecretModeAction() {}

  public SecretModeAction(final boolean enable)
  {
    this.enable = enable;
  }

  public boolean isEnable()
  {
    return enable;
  }

}
