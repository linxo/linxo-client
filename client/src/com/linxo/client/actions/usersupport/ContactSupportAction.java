/* Imported by LinxoImporter - DO NOT EDIT */

/*
 * Copyright (c) 2008-2016 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */
package com.linxo.client.actions.usersupport;

import com.linxo.client.data.ValidationConstants;
import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.JsonAction;

/**
 * LinxoAction allowing the user to contact the server.
 */
@JsonAction
public final class ContactSupportAction
    extends SecuredAction<ContactSupportResult>
{
  @AssertStringSize
  private String firstName;

  @AssertStringSize
  private String lastName;

  @AssertStringSize
  private String subject;

  @AssertStringSize(ValidationConstants.EMAIL_FRIENDS_CUSTOM_MESSAGE_MAX_LENGTH)
  private String message;

  @AssertStringSize
  private String email;

  @SuppressWarnings("unused") // s11n
  private ContactSupportAction() {}

  public ContactSupportAction(final String firstName, final String lastName, final String subject,
                              final String message, final String email)
  {
    this.firstName = firstName;
    this.lastName = lastName;
    this.subject = subject;
    this.message = message;
    this.email = email;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public String getSubject()
  {
    return subject;
  }

  public String getMessage()
  {
    return message;
  }

  public String getEmail()
  {
    return email;
  }

}
