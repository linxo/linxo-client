/* Imported by LinxoImporter - DO NOT EDIT */

/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 10/11/2010 by hugues.
 */
package com.linxo.client.actions.usersupport;

import com.linxo.client.actions.SecuredAction;
import com.linxo.client.shared.AssertStringSize;
import com.linxo.client.actions.GwtAction;
import com.linxo.client.actions.JsonAction;

@GwtAction
@JsonAction
@SuppressWarnings({"DeserializableClassInSecureContext"})
public final class BankRequestAction extends SecuredAction<BankRequestResult>
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  @AssertStringSize
  private String name;

  @AssertStringSize
  private String webSite;

  @AssertStringSize
  private String loginUrl;

  private boolean allowContact;

  // Instance Initializers

  // Constructors

  @SuppressWarnings("unused") //s11n
  private BankRequestAction()
  {}

  public BankRequestAction(String name, String webSite, String loginUrl)
  {
    this.name = name;
    this.webSite = webSite;
    this.loginUrl = loginUrl;
  }

  public BankRequestAction(String name, String webSite, String loginUrl, boolean allowContact)
  {
    this(name,webSite,loginUrl);
    this.allowContact = allowContact;
  }

  // Instance Methods


  public String getLoginUrl()
  {
    return loginUrl;
  }

  public String getName()
  {
    return name;
  }

  public String getWebSite()
  {
    return webSite;
  }

  public boolean isAllowContact()
  {
    return allowContact;
  }
}
