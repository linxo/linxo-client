/*
 * Copyright (c) 2008-2009 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */

package com.linxo.crypto;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.engines.DESedeEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.generators.DESedeKeyGenerator;
import org.bouncycastle.crypto.params.DESedeParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.util.encoders.Hex;
import org.apache.log4j.Logger;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.util.Date;
import java.util.Iterator;

public class Encryption
{

  // Nested Types (mixing inner and static classes is okay)

  public enum Algorithm {
    DES3(DESedeParameters.DES_EDE_KEY_LENGTH*8), // 8*24=192
    AES256(256);

    int keySize;

    Algorithm(int keySize)
    {
      this.keySize = keySize;
    }

    public int getKeySize()
    {
      return keySize;
    }
  }

  // Static Fields

  private static final String DEFAULT_PROVIDER = "BC";
  private static final String DEFAULT_IDENTITY = "LINXO SAS";
  private static final String ASYMMETRIC_ENCRYPTION_ALGORITHM = "RSA";
  private static final String AES_CIPHER_SUITE = "AES/ECB/PKCS5Padding";

  private static final Logger logger = Logger.getLogger(Encryption.class);

  // package private for testing purpose
  static final String DEFAULT_PASSWORD = CryptoUtils.generateRandomString(16);

  static final int BUFFER_SIZE = 1<<16;

  // Static Initializers

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  // Static Methods

  ///////////////////////////////////////////////////////////////////////////////
  //
  //      Symmetric Encryption
  //
  ///////////////////////////////////////////////////////////////////////////////


  private static PaddedBufferedBlockCipher createAndInitCipher(String encrypt_key, boolean encoding)
  {
    //Setup the DESede cipher engine, create a PaddedBufferedBlockCipher in CBC mode.
    PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(
        new CBCBlockCipher(new DESedeEngine()));

    // init the cipher for encoding/decoding with the provided encrypt_key
    cipher.init(encoding, new KeyParameter(Hex.decode(encrypt_key)));

    if(logger.isDebugEnabled()){
      logger.debug("cipher initialized with block size="+cipher.getBlockSize());
    }
    return cipher;
  }

  public static String symmetricDecrypt(Algorithm algorithm, String encryptionKey, String encryptedValue)
      throws CryptoException
  {
    if(algorithm == Algorithm.DES3) {
      return symmetricDESDecrypt(encryptionKey, encryptedValue);
    }
    else if (algorithm == Algorithm.AES256) {
      return symmetricAESDecrypt(encryptionKey, encryptedValue);
    }
    else {
      throw new CryptoException("Cannot decrypt : Unknown algorithm: " + algorithm);
    }
  }

  public static String symmetricEncrypt(Algorithm algorithm, String encryptionKey, String plainValue)
      throws CryptoException
  {
    if(algorithm == Algorithm.DES3) {
      return symmetricDESEncrypt(encryptionKey, plainValue);
    }
    else if (algorithm == Algorithm.AES256) {
      return symmetricAESEncrypt(encryptionKey, plainValue);
    }
    else {
      throw new CryptoException("Cannot encrypt : Unknown algorithm: " + algorithm);
    }
  }

  /**
   * @param algorithm the algorithm to be used during the encryption
   * @return a key String to be used with
   * {@link #symmetricEncrypt(Algorithm, String, String)} and {@link #symmetricDecrypt(Algorithm, String, String)}.
   * @throws CryptoException if the provided algorithm is unknown
   */
  public static String generateSymmetricKey(Algorithm algorithm) throws CryptoException
  {
    if(algorithm == Algorithm.DES3) {
      return generateDES3Key(algorithm.getKeySize());
    }
    else if (algorithm == Algorithm.AES256) {
      return generateAESKey(algorithm.getKeySize());
    }
    else {
      throw new CryptoException("Cannot generate symmetric key : Unknown algorithm: " + algorithm);
    }
  }


  // 3-DES

  private static String symmetricDESDecrypt(String encryptionKey, String encryptedValue) throws CryptoException
  {
    // create and prepare a cipher for decoding
    PaddedBufferedBlockCipher cipher = createAndInitCipher(encryptionKey, false);

    //prepare the byte arrays for decoding
    byte[] in = Hex.decode(encryptedValue);
    byte[] out = new byte[cipher.getOutputSize(in.length)];


    int size = cipher.processBytes(in, 0, in.length, out, 0);
    try {
      // Process any bytes left within the cipher
      size += cipher.doFinal(out, size);
    }
    catch (org.bouncycastle.crypto.CryptoException ce) {
      logger.warn("Caught exception while decrypting: " + encryptedValue, ce);
      throw new CryptoException("Caught exception while decrypting", ce);
    }
    try {
      return new String (out, 0, size, "UTF-16");
    }
    catch (UnsupportedEncodingException e) {
      // This should never happen ("UTF16" is the base charset for Strings).
      throw new CryptoException("This should never happen (UTF-16) is the base charset for Strings).", e);
    }
  }

  private static String symmetricDESEncrypt(String encryptionKey, String plainValue) throws CryptoException
  {
    // create and prepare a cipher for encoding
    PaddedBufferedBlockCipher cipher = createAndInitCipher(encryptionKey, true);

    try {
      // initialize data arrays
      byte[] in = plainValue.getBytes("UTF-16");
      byte[] out = new byte[cipher.getOutputSize(in.length)];
      // encode the input
      int size = cipher.processBytes(in, 0, in.length, out, 0);
      // Process any bytes left in the cipher
      size += cipher.doFinal(out, size);
      return new String(Hex.encode(out, 0, size));
    }
    catch (org.bouncycastle.crypto.CryptoException ce) {
      logger.warn("Caught exception while encrypting: " + plainValue, ce);
      throw new CryptoException("Caught exception while encrypting", ce);
    }
    catch (UnsupportedEncodingException e) {
      // This should never happen ("UTF16" is the base charset for Strings).
      throw new CryptoException("This should never happen (UTF-16) is the base charset for Strings).", e);
    }
  }

  private static String generateDES3Key(int keySize)
  {
    /*
     * The process of creating a new key requires a
     * number of steps.
     *
     * First, create the parameters for the key generator
     * which are a secure random number generator, and
     * the length of the key (in bits).
     */
    byte[] key;
    SecureRandom sr;
    try
    {
      sr = new SecureRandom();
    }
    catch (Exception nsa)
    {
      logger.error("Hmmm, no SHA1PRNG, you need the Sun implementation");
      return null;
    }
    KeyGenerationParameters kgp = new KeyGenerationParameters( sr, keySize);

    /*
     * Second, initialise the key generator with the parameters
     */
    DESedeKeyGenerator kg = new DESedeKeyGenerator();
    kg.init(kgp);

    /*
     * Third, and finally, generate the key
     */
    key = kg.generateKey();

    /*
     * We can now output the key to the file, but first
     * hex encode the key so that we can return a String
     */
    byte[] keyhex = Hex.encode(key);
    return new String(keyhex);
  }


  // AES

  private static String generateAESKey(int strength) throws CryptoException
  {
    try {
      KeyGenerator keygen = KeyGenerator.getInstance("AES");
      SecureRandom random = new SecureRandom();
      keygen.init(strength, random);

      SecretKey key = keygen.generateKey();

      byte[] keyhex = Hex.encode(key.getEncoded());
      return new String(keyhex);
    }
    catch (NoSuchAlgorithmException nsae)
    {
      logger.fatal(nsae);
      throw new CryptoException(nsae.getMessage());
    }
  }

  private static String symmetricAESEncrypt(String encryptionKey, String plainValue) throws CryptoException
  {
    final Key key = new SecretKeySpec(Hex.decode(encryptionKey), "AES");
    final Cipher cipher;
    try{
      cipher = Cipher.getInstance(AES_CIPHER_SUITE, DEFAULT_PROVIDER);
      cipher.init(Cipher.ENCRYPT_MODE, key);
    }
    catch(GeneralSecurityException e) {
      throw new CryptoException("SHOULD NOT HAPPEN : could not init cipher", e);
    }

    ByteArrayOutputStream bOut = new ByteArrayOutputStream();
    CipherOutputStream cOut = new CipherOutputStream(bOut, cipher);

    final byte[] messageBytes;
    try {
      messageBytes = plainValue.getBytes("UTF-8");
    }
    catch(UnsupportedEncodingException e) {
      // This should never happen ("UTF16" is the base charset for Strings).
      throw new CryptoException("This should never happen (UTF-8) is the base charset for Strings).", e);
    }

    try {
      for (byte messageByte : messageBytes) {
        cOut.write(messageByte);
      }

      cOut.flush();
      cOut.close();

      bOut.flush();
      bOut.close();
    }
    catch (IOException e)
    {
      logger.fatal(e);
      throw new CryptoException(e.getMessage());
    }

    byte[]    bytes = bOut.toByteArray();

    try {
      return new String(Hex.encode(bytes), "UTF-8");
    }
    catch (UnsupportedEncodingException e) {
      // This should never happen ("UTF16" is the base charset for Strings).
      throw new CryptoException("This should never happen (UTF-8) is the base charset for Strings).", e);
    }

  }

  private static String symmetricAESDecrypt(String encryptionKey, String encryptedValue) throws CryptoException
  {
    Key key = new SecretKeySpec(Hex.decode(encryptionKey), "AES");

    final Cipher cipher;
    try{
      cipher = Cipher.getInstance(AES_CIPHER_SUITE, DEFAULT_PROVIDER);
      cipher.init(Cipher.DECRYPT_MODE, key);
    }
    catch(GeneralSecurityException gse) {
      logger.fatal("SHOULD NOT HAPPEN: impossible to generate the cipher:", gse);
      throw new CryptoException("impossible to generate the cipher", gse);
    }

    ByteArrayOutputStream bOut = new ByteArrayOutputStream();
    CipherOutputStream cOut = new CipherOutputStream(bOut, cipher);

    byte[] bytes = Hex.decode(encryptedValue);
    try
    {
      for (byte aByte : bytes)
      {
        cOut.write(aByte);
      }
      cOut.flush();
      cOut.close();

      bOut.flush();
      bOut.close();
    }
    catch (IOException e)
    {
      throw new CryptoException("Error while decrypting: ", e);
    }

    byte[] outBytes = bOut.toByteArray();
    final String result;
    try{
      result = new String(outBytes, "UTF-8");
    }
    catch(UnsupportedEncodingException e){
      throw new CryptoException("SHOULD NOT HAPPEN: does not support UTF-8", e);
    }
    return result;

  }


  ///////////////////////////////////////////////////////////////////////////////
  //
  //      Asymmetric encryption
  //
  ///////////////////////////////////////////////////////////////////////////////


  /**
   * Generates an RSA keyPair to be used with client requests
   * @param keyLength the length of the key
   * @return an RSQ KeyPair
   * @throws CryptoException if the algorithm is not found while initializing the ley
   */
  public static PGPSecretKey generateKeyPair(int keyLength) throws CryptoException
  {
    if(keyLength < CryptoUtils.DEFAULT_KEY_LENGTH){
      logger.warn("Generating a key with lower size, thus lower strength " +
          "(default:"+CryptoUtils.DEFAULT_KEY_LENGTH+"), " +
          "(size:"+keyLength+")");
    }

    try {
      KeyPairGenerator kpg = KeyPairGenerator
          .getInstance(ASYMMETRIC_ENCRYPTION_ALGORITHM, DEFAULT_PROVIDER);
      kpg.initialize(keyLength);

      final KeyPair kp = kpg.generateKeyPair();


      return new PGPSecretKey(PGPSignature.DEFAULT_CERTIFICATION, PGPPublicKey.RSA_GENERAL,
          kp.getPublic(), kp.getPrivate(), new Date(),
          DEFAULT_IDENTITY, PGPEncryptedData.CAST5, DEFAULT_PASSWORD.toCharArray(),
          null, null, new SecureRandom(), DEFAULT_PROVIDER);
    }
    catch(GeneralSecurityException exc) {
      throw new CryptoException("Impossible to create the KeyPair", exc);
    }
    catch(PGPException exc) {
      throw new CryptoException("Impossible to create the PGPKeyPair", exc);
    }
  }


  public static PGPSecretKey readSecretKey(String pgpArmoredKey)
      throws CryptoException
  {
    try {
      InputStream in = PGPUtil.getDecoderStream(new ByteArrayInputStream(pgpArmoredKey.getBytes()));
      PGPObjectFactory factory = new PGPObjectFactory(in);

      PGPSecretKeyRing keyRing = (PGPSecretKeyRing)factory.nextObject();
      return keyRing.getSecretKey();
    }
    catch(IOException ioe){
      throw new CryptoException("Could not read the key from String="+pgpArmoredKey, ioe);
    }
  }

  public static PGPPublicKey readPublicKey(String pgpArmoredKey)
      throws CryptoException
  {
    try {
      InputStream in = PGPUtil.getDecoderStream(new ByteArrayInputStream(pgpArmoredKey.getBytes()));
      PGPObjectFactory factory = new PGPObjectFactory(in);

      PGPPublicKeyRing keyRing = (PGPPublicKeyRing)factory.nextObject();
      return keyRing.getPublicKey();
    }
    catch(IOException ioe){
      throw new CryptoException("Could not read the key from String="+pgpArmoredKey, ioe);
    }
  }


  public static String asymmetricEncrypt(PGPPublicKey publicKey, String message) throws CryptoException
  {
    try {// /ECB/PKCS1Padding
      Cipher rsaCipher = Cipher.getInstance(ASYMMETRIC_ENCRYPTION_ALGORITHM, DEFAULT_PROVIDER);
      rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey.getKey(DEFAULT_PROVIDER));

      // initialize data arrays
      byte[] in = message.getBytes("UTF-16");

      // encode the input
      byte[] out  = rsaCipher.doFinal(in);
      return new String(Hex.encode(out, 0, out.length));
    }
    catch(GeneralSecurityException exc){
      throw new CryptoException("Impossible to encrypt", exc);
    }
    catch(PGPException exc){
      throw new CryptoException("Impossible to translate PGPKey", exc);
    }
    catch (UnsupportedEncodingException e) {
      // This should never happen ("UTF16" is the base charset for Strings).
      logger.error("This should never happen (UTF-16) is the base charset for Strings).", e);
      throw new CryptoException("This should never happen (UTF-16) is the base charset for Strings).", e);
    }
    catch(RuntimeException re){
      throw new CryptoException("RuntimeException caught while decrypting).", re);
    }
  }

  public static String asymmetricDecrypt(PGPSecretKey privateKey, String message) throws CryptoException
  {
    try {
      Cipher rsaCipher = Cipher.getInstance(ASYMMETRIC_ENCRYPTION_ALGORITHM, DEFAULT_PROVIDER);
      rsaCipher.init(Cipher.DECRYPT_MODE, privateKey.extractPrivateKey(DEFAULT_PASSWORD.toCharArray(), DEFAULT_PROVIDER).getKey());

      //prepare the byte arrays for decoding
      byte[] in = Hex.decode(message);
      byte[] out = new byte[rsaCipher.getOutputSize(in.length)];

      int size = rsaCipher.doFinal(in, 0, in.length, out, 0);
      return new String (out, 0, size, "UTF-16");
    }
    catch (GeneralSecurityException ce) {
      throw new CryptoException("Caught exception while decrypting", ce);
    }
    catch (PGPException ce) {
      throw new CryptoException("Caught exception while opening the secret Key", ce);
    }
    catch (UnsupportedEncodingException e) {
      // This should never happen ("UTF16" is the base charset for Strings).
      throw new CryptoException("This should never happen (UTF-16) is the base charset for Strings).", e);
    }
    catch(RuntimeException re){
      throw new CryptoException("RuntimeException caught while decrypting).", re);
    }
  }

  public static String decryptFromJavaScript(PGPSecretKey secretKey, String encryptedMessage) throws CryptoException
  {
    return decryptFromJavaScript(secretKey, DEFAULT_PASSWORD, encryptedMessage);
  }

  public static String decryptFromJavaScript(PGPSecretKey secretKey, String password, String encryptedMessage) throws CryptoException
  {
    try {
      InputStream in = PGPUtil.getDecoderStream(new ByteArrayInputStream(encryptedMessage.getBytes("UTF-8")));

      PGPObjectFactory        factory = new PGPObjectFactory(in);
      PGPEncryptedDataList    encStream;

      Object o = factory.nextObject();
      //
      // the first object might be a PGP marker packet.
      //
      if (o instanceof PGPEncryptedDataList)
      {
        encStream = (PGPEncryptedDataList)o;
      }
      else
      {
        encStream = (PGPEncryptedDataList)factory.nextObject();
      }

      if(encStream == null){
        throw new CryptoException ("Encoded stream object is null");
      }
      Iterator iterator = encStream.getEncryptedDataObjects();

      PGPPrivateKey privateKey = secretKey.extractPrivateKey(password.toCharArray(), DEFAULT_PROVIDER);
      InputStream clear = ((PGPPublicKeyEncryptedData)iterator.next()).getDataStream(privateKey, DEFAULT_PROVIDER);

      PGPObjectFactory    plainFact = new PGPObjectFactory(clear);


      Object message;
      while((message = plainFact.nextObject()) != null){
        if(!(message instanceof PGPLiteralData)){
          continue;
        }

        PGPLiteralData clearData = (PGPLiteralData)message;
        InputStream    unc = clearData.getInputStream();

        byte[] data = new byte[1024];
        int read;
        StringBuffer sb = new StringBuffer();
        while((read = unc.read(data, 0, 1024)) != -1)
        {
          String tmp = new String (data, 0, read);
          sb.append(tmp);
        }

        unc.close();
        String almostFinal = sb.toString();

        return almostFinal.replace("\r\n", ""); // there's a final '\n' added at the end
      }

      throw new CryptoException("Did not find message in encrypted data.");
    }
    catch(IOException ioe){
        throw new CryptoException("Caught exception while decrypting", ioe);
    }
    catch (GeneralSecurityException ce) {
      throw new CryptoException("Caught exception while decrypting", ce);
    }
    catch (PGPException ce) {
      throw new CryptoException("Caught exception while opening the secret Key", ce);
    }
  }

  public static String encryptLikeJavaScript(String expectedMessage, String publicKey)
      throws CryptoException/*IOException, CryptoException, NoSuchProviderException, PGPException*/
  {
    try {
      // Create the PGPLiteralData (BCPG feels better in UTF-8...)
      byte[] lBytes = expectedMessage.getBytes("UTF-8");

      ByteArrayOutputStream bOut = new ByteArrayOutputStream();
      PGPLiteralDataGenerator lGen = new PGPLiteralDataGenerator();
      OutputStream lOut = lGen.open(bOut, PGPLiteralData.UTF8,
          PGPLiteralData.CONSOLE, lBytes.length, PGPLiteralData.NOW);
      lOut.write(lBytes);
      lGen.close();

      // Encrypt the literal data using the public key
      final PGPPublicKey pgpPublicKey = Encryption.readPublicKey(publicKey);

      byte[] inBytes = bOut.toByteArray();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ArmoredOutputStream aos = new ArmoredOutputStream(baos);

      // Using bigger encryption keys sizes will not work and generates exception
      // on the server. Alternatively, we could use CAST5, like in the key generation.
      // or install the Unlimited key strength Jurisdiction policy files on the server
      // http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-plat-419418.html#jce_policy-6-oth-JPR
      PGPEncryptedDataGenerator encGen = new PGPEncryptedDataGenerator(
          SymmetricKeyAlgorithmTags.AES_128, false, new SecureRandom(), "BC");

      encGen.addMethod(pgpPublicKey);

      OutputStream cOut = encGen.open(aos, inBytes.length);

      cOut.write(inBytes);
      cOut.close();
      aos.close();
      baos.close();

      byte[] encBytes = baos.toByteArray();

      return new String(encBytes, "UTF-8");
    }
    catch (Exception exc) {
      throw new CryptoException("Error during encryption", exc);
    }
  }

  /**
   * Generates an outputStream that encrypts the data to the provided outputStream.
   *
   * @param out - the stream where the encrypted data will be written
   * @param publicKey - the public key PGP message block
   * @param filename - the filename of the "file"
   * @param armor - when true, generate an armored message
   * @param withIntegrityCheck - when true, adds an integrity packet
   * @return the encryption stream where to write clear data
   * @throws CryptoException if something fails while generating the stream
   */
  public static OutputStream openAsymmetricEncryptionStream(OutputStream out, String publicKey, String filename,
                                                            boolean armor, boolean withIntegrityCheck)
      throws CryptoException
  {
    // Generate the public key object out of the publicKey string
    final PGPPublicKey pgpPublicKey = Encryption.readPublicKey(publicKey);

    // Put the -- PGP MESSAGE -- headers around the actual message if needed
    OutputStream aOut;
    if (armor) {
      aOut = new ArmoredOutputStream(out);
    }
    else {
      aOut = out;
    }



    // Init encrypted data generator
    // Using bigger encryption keys sizes will not work and generates exception
    // on the server. Alternatively, we could use CAST5, like in the key generation.
    // or install the Unlimited key strength Jurisdiction policy files on the server
    // http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-plat-419418.html#jce_policy-6-oth-JPR
    PGPEncryptedDataGenerator encGen = new PGPEncryptedDataGenerator(
        SymmetricKeyAlgorithmTags.AES_256, withIntegrityCheck, new SecureRandom(), DEFAULT_PROVIDER);

    try {
      encGen.addMethod(pgpPublicKey);
    }
    catch(NoSuchProviderException e) {
      throw new CryptoException("SHOULD NOT HAPPEN : BC should be already registered", e);
    }
    catch(PGPException e) {
      throw new CryptoException("Caught Exception while configuring stream with provided key ["+publicKey+"]", e);
    }

    try{
      OutputStream cOut = encGen.open(aOut, new byte[BUFFER_SIZE]);

    /*
      // Init compression  (optional)
      PGPCompressedDataGenerator compressedDataGenerator = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);
      OutputStream compressedOut = compressedDataGenerator.open(encryptedOut); 
     */

      PGPLiteralDataGenerator literalDataGenerator = new PGPLiteralDataGenerator();
      OutputStream lOut = literalDataGenerator.open(cOut, PGPLiteralData.BINARY, filename, new Date(), new byte[BUFFER_SIZE]);

      return new EncryptedOutputStream(cOut, lOut, (armor)?aOut:null);
    }
    catch(Exception e) { // PGPException + IOException
      throw new CryptoException("Caught Exception while opening encoded stream", e);
    }
  }

  /**
   * @param publicKey a PGP public key
   * @param armored should we include the PGP armor
   * @return the String block
   * @throws CryptoException in case the key cannot be serialized
   */
  public static String getPublicKeyBlock(PGPPublicKey publicKey, boolean armored) throws CryptoException
  {
    try{
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();

      OutputStream os = armored?new ArmoredOutputStream(baos):baos;

      publicKey.encode(os);
      os.close();

      return baos.toString();
    }
    catch(IOException ioe){
      throw new CryptoException("Error while encoding the key", ioe);
    }
  }

  /**
   * @param secretKey a PGP public key
   * @param armored should we include the PGP armor
   * @return the String block
   * @throws CryptoException in case the key cannot be serialized
   */
  public static String getSecretKeyBlock(PGPSecretKey secretKey, boolean armored) throws CryptoException
  {
    try{
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();

      OutputStream os = armored?new ArmoredOutputStream(baos):baos;
      secretKey.encode(os);
      os.close();

      return baos.toString();
    }
    catch(IOException ioe){
      throw new CryptoException("Error while encoding the key", ioe);
    }
  }

  public static void main (String[] argv) throws Exception {
    System.err.println(generateSymmetricKey(Algorithm.valueOf(argv[0])));
  }
  // Instance Fields
 
  // Instance Initializers

  // Constructors

  // Instance Methods
}
