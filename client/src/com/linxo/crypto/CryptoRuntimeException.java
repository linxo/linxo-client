package com.linxo.crypto;

/**
 *
 */
public class CryptoRuntimeException extends RuntimeException {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  public CryptoRuntimeException(String message, Throwable cause)
  {
    super(message, cause);
  }

  public CryptoRuntimeException(String s)
  {
    super(s);
  }

}
