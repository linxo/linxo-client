/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 16/02/2011 by hugues.
 */
package com.linxo.crypto;

import java.security.SecureRandom;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CryptoUtils {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields
  private static final Random RANDOM = new SecureRandom();
  private static final Pattern AMBIGUOUS_CHARS = Pattern.compile("[0O]");
  private static final Pattern AMBIGUOUS_DIGITS = Pattern.compile("[0]");

  public static final int DEFAULT_TOKEN_LENGTH = 10;

  public static final int DEFAULT_KEY_LENGTH = 1024;

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  /**
   * Generates a random string of <code>length</code> characters.
   * @param length the max length of string to output
   * @return a random string of <code>length</code> characters.
   */
  public static String generateRandomString(int length)
  {
    long keyLong = RANDOM.nextLong();
    String keyString = String.format ( "%1$0"+length+"X", keyLong );
    // then make sure we do not have ambiguous characters (O's and 0's for instance).
    Matcher m = AMBIGUOUS_CHARS.matcher(keyString);
    keyString = m.replaceAll("A"); // we replace the ambiguous chars with A's
    return keyString.substring( 0, length ) ;
  }

  /**
   * Generates a random string of <code>length</code> characters.
   * This string contains only digits, EXCEPT 0.
   *
   * @param length The max length of string to output.
   * @return A random string of <code>length</code> characters.
   *         This string contains only digits, EXCEPT 0.
   */
  public static String generateRandomNumericString(final int length)
  {
    if (length < 1) { return ""; }
    final long keyLong = Math.abs(RANDOM.nextLong());
    // %1 = first parameter, $ = format starts, 0 = use 0 for padding (if any),
    // length = length of the expected result
    String keyString = String.format("%1$0" + length + "d", keyLong);
    // then make sure we do not have ambiguous characters (O's and 0's for instance).
    final Matcher m = AMBIGUOUS_DIGITS.matcher(keyString);
    keyString = m.replaceAll("9"); // we replace the ambiguous chars with 9's
    return keyString.substring(0, length);
  }

}
