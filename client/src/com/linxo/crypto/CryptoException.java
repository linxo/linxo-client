/*
 * Copyright (c) 2008-2009 Linxo, All Rights Reserved.
 *
 *    COPYRIGHT:
 *         This software is the property of Linxo.
 *         It cannot be copied, used, or modified without obtaining an
 *         authorization from the authors or a person mandated by Linxo.
 *         If such an authorization is provided, any modified version
 *         or copy of the software has to contain this header.
 *
 *    WARRANTIES:
 *         This software is made available by the authors in the hope
 *         that it will be useful, but without any warranty.
 *         Linxo is not liable for any consequence related to
 *         the use of the provided software.
 */

package com.linxo.crypto;

/**
 * An Exception thrown when a problem occurs while encrypting/decrypting
 */
public class CryptoException extends Exception
{

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  public CryptoException(String message, Throwable cause)
  {
    super(message, cause);
  }

  public CryptoException(String s)
  {
    super(s);
  }

  // Instance Methods
}
