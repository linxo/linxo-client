/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 17/08/2012 by hugues.
 */
package com.linxo.crypto;

import java.io.IOException;
import java.io.OutputStream;

public final class EncryptedOutputStream extends OutputStream {

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Static Initializers

  // Static Methods

  // Instance Fields

  private final OutputStream armoredStream;

  /**
   * The underlying encryption stream where we write the clear data
   */
  private final OutputStream literalStream;

  /**
   * The underlying stream that performs the compression
   */
  private final OutputStream encryptionStream;

  // Instance Initializers

  // Constructors

  public EncryptedOutputStream(OutputStream encryptionStream, OutputStream literalStream, OutputStream armoredStream)
  {
    this.encryptionStream = encryptionStream;
    this.literalStream = literalStream;
    this.armoredStream = armoredStream;
  }


  // Instance Methods


  @Override
  public void write(int i) throws IOException
  {
    literalStream.write(i);
  }


  @Override
  public void close() throws IOException
  {
    super.close();
    literalStream.close();
    encryptionStream.close();
    if(armoredStream != null) {
      armoredStream.close();
    }
  }
}
