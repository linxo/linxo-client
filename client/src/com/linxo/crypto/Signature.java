/*
   Copyright (c) 2008-2013 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 08/04/2011 by hugues.
 */
package com.linxo.crypto;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

public class Signature {
  public static final String CHARSET = "UTF-8";
  public static final String SHA_1 = "SHA-1";
  public static final String MD5 = "MD5";

  public static final String BC_PROVIDER = "BC";

  // Nested Types (mixing inner and static classes is okay)

  // Static Fields
  private static final Logger logger = Logger.getLogger(Signature.class);

  // Static Initializers
  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  // Static Methods

  // Instance Fields

  // Instance Initializers

  // Constructors

  // Instance Methods

  /**
   * @param message to be signed sign
   * @return base64( sha1 message )
   * @throws CryptoRuntimeException if something goes wrong while signing the message
   */
  public static String hashSha1Base64String(String message) throws CryptoRuntimeException
  {
    return Base64.encodeBase64String(hashSha1String(message).getBytes());
  }

  public static String hashSha1String(String message) throws CryptoRuntimeException
  {
    return hashString(message, SHA_1);
  }

  public static String hashMd5String(String message) throws CryptoRuntimeException
  {
    return hashString(message, MD5);
  }

  private static String hashString(String message, String algorithm) throws CryptoRuntimeException
  {
    try{
      byte[] messageBytes = message.getBytes(CHARSET);

      MessageDigest hash = MessageDigest.getInstance(algorithm, BC_PROVIDER);
      hash.update(messageBytes);

      //proceed ....
      final String digest = Hex.encodeHexString(hash.digest());
      if(logger.isDebugEnabled()){
        logger.debug("SHA1 digest["+ digest + "]");
      }
      return digest;
    }
    catch(UnsupportedEncodingException uee) {
      throw new CryptoRuntimeException("Impossible to sign message ["+message+"]", uee);
    }
    catch(NoSuchAlgorithmException nsae) {
      throw new CryptoRuntimeException("Impossible to sign message ["+message+"]", nsae);
    }
    catch(NoSuchProviderException nspe) {
      throw new CryptoRuntimeException("Impossible to sign message ["+message+"]", nspe);
    }
  }

}
