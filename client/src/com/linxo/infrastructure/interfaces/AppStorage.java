/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 14/10/2014 by hugues.
 */
package com.linxo.infrastructure.interfaces;

public interface AppStorage {
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Instance Methods

  /**
   * Saves data to local folder
   * @param key - The storage key
   * @param data - The string data to store
   * @return {@code true} if file successfully stored, otherwise {@code false}
   */
  Boolean saveProtected(String key, String data);

  /**
   * Saves data to local folder
   * @param key - The storage key
   * @param data - The data to store
   * @return {@code true} if file successfully stored, otherwise {@code false}
   */
  Boolean save(String key, String data);

  /**
   * Get data from local folder
   * @param key - The storage key
   * @return The requested data value to key exist, otherwise default value
   */
  String getStringData(String key);

  /**
   * Get integer data from local folder
   * @param key - The storage key
   * @return The requested data value to key exist, otherwise default value
   */
  Integer getIntegerData(String key);

  /**
   * Get protected data from local folder
   * @param key - The storage key
   * @return The requested data value to key exist, otherwise default value
   */
  String getStringDataProtected(String key);

  /**
   * Get protected integer data from local folder
   * @param key - The storage key
   * @return The requested data value to key exist, otherwise default value
   */
  Integer getIntegerDataProtected(String key);

  /**
   * Deletes a key from the local folder
   * @param key - The key to delete
   * @return {@code true} if file successfully deleted, otherwise {@code false}
   */
  Boolean delete(String key);

}
