/*
   Copyright (c) 2008-2014 Linxo, All Rights Reserved.

   COPYRIGHT:
        This software is the property of Linxo.
        It cannot be copied, used, or modified without obtaining an
        authorization from the authors or a person mandated by Linxo.
        If such an authorization is provided, any modified version
        or copy of the software has to contain this header.

   WARRANTIES:
        This software is made available by the authors in the hope
        that it will be useful, but without any warranty.
        Linxo is not liable for any consequence related to
        the use of the provided software.
   </pre>
   
   
   Created on : 14/10/2014 by hugues.
 */
package com.linxo.infrastructure.interfaces;

/**
 * This interface defines property Strings that need to be provided in the context of an app running on a device
 */
public interface AppDeviceConfig {
  // Nested Types (mixing inner and static classes is okay)

  // Static Fields

  // Instance Methods

  /**
   * @return The phone brand and model.
   * Example: Nokia Lumia 625
   */
  String getDeviceType();

  /**
   * @return  example: WINDOWS_PHONE pour Windows Phone
   */
  String getDeviceFamily();

  /**
   * @return example: Mike gorgeous Lumia. Device API may expose the name
   */
  String getDeviceName();

  /**
   * @return Example: "com.linxo.wp" for Windows Phone
   */
  String getAppIdentifier();

  /**
   * @return Example: "1.0.0"
   */
  String getAppVersion ();

  /**
   * @return Server domain. Examples: wwws.linxo.com or partners.linxo.com
   */
  String getDomain();

  /**
   * @return API Key to communicate with server
   */
  String getApiKey();

  /**
   * @return API Secret to communicate with server
   */
  String getApiSecret ();

}
